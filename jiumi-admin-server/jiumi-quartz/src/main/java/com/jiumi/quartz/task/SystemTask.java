package com.jiumi.quartz.task;

import com.jiumi.baseconfig.service.IBaseAppointmentService;
import com.jiumi.baseconfig.service.IBaseContractService;
import com.jiumi.baseconfig.service.IBaseUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.jiumi.common.utils.StringUtils;

/**
 * 定时任务调度测试
 *
 * @author jiumi
 */
@Component("systemTask")
public class SystemTask
{
    private static final Logger logger = LoggerFactory.getLogger(SystemTask.class);

    @Autowired
    private IBaseContractService baseContractService;

    @Autowired
    private IBaseUserService userInfoService;

    @Autowired
    private IBaseAppointmentService baseAppointmentService;


    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }

    public void createPayorder()
    {
        logger.info("自动生成合同订单开始");
        baseContractService.autoCreatePayorder();
        logger.info("自动生成合同订单结束");
    }

    public void autoSetUnClick()
    {
        logger.info("自动设置未打卡");
        userInfoService.autoSetUserUnClick();
        logger.info("自动设置未打卡");
    }

    public void autoReferrerEmploy()
    {
        logger.info("自动推荐雇主");
        baseAppointmentService.autoReferrerEmployer();
        logger.info("自动推荐雇主");
    }

    public void autoAppoinmentOffline()
    {
        logger.info("雇主预约信息自动下线");
        baseAppointmentService.autoAppoinmentOffline();
        logger.info("雇主预约信息自动下线");
    }
}

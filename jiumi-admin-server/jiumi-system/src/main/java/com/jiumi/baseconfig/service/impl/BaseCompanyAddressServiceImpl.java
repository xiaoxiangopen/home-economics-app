package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseCompanyAddressMapper;
import com.jiumi.baseconfig.domain.BaseCompanyAddress;
import com.jiumi.baseconfig.service.IBaseCompanyAddressService;

/**
 * 公司地址Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseCompanyAddressServiceImpl implements IBaseCompanyAddressService
{
    @Autowired
    private BaseCompanyAddressMapper baseCompanyAddressMapper;

    /**
     * 查询公司地址
     *
     * @param id 公司地址主键
     * @return 公司地址
     */
    @Override
    public BaseCompanyAddress selectBaseCompanyAddressById(Long id)
    {
        return baseCompanyAddressMapper.selectBaseCompanyAddressById(id);
    }

    /**
     * 查询公司地址列表
     *
     * @param baseCompanyAddress 公司地址
     * @return 公司地址
     */
    @Override
    public List<BaseCompanyAddress> selectBaseCompanyAddressList(BaseCompanyAddress baseCompanyAddress)
    {
        return baseCompanyAddressMapper.selectBaseCompanyAddressList(baseCompanyAddress);
    }

    /**
     * 新增公司地址
     *
     * @param baseCompanyAddress 公司地址
     * @return 结果
     */
    @Override
    public int insertBaseCompanyAddress(BaseCompanyAddress baseCompanyAddress)
    {
        return baseCompanyAddressMapper.insertBaseCompanyAddress(baseCompanyAddress);
    }

    /**
     * 修改公司地址
     *
     * @param baseCompanyAddress 公司地址
     * @return 结果
     */
    @Override
    public int updateBaseCompanyAddress(BaseCompanyAddress baseCompanyAddress)
    {
        return baseCompanyAddressMapper.updateBaseCompanyAddress(baseCompanyAddress);
    }

    /**
     * 批量删除公司地址
     *
     * @param ids 需要删除的公司地址主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAddressByIds(Long[] ids)
    {
        return baseCompanyAddressMapper.deleteBaseCompanyAddressByIds(ids);
    }

    /**
     * 删除公司地址信息
     *
     * @param id 公司地址主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAddressById(Long id)
    {
        return baseCompanyAddressMapper.deleteBaseCompanyAddressById(id);
    }

    @Override
    public int deleteBaseCompanyAddressByCompanyId(Long id) {
        return baseCompanyAddressMapper.deleteBaseCompanyAddressByCompanyId(id);
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseLables;
import com.jiumi.baseconfig.service.IBaseLablesService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 标签管理Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/lables")
public class BaseLablesController extends BaseController
{
    @Autowired
    private IBaseLablesService baseLablesService;

    /**
     * 查询标签管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseLables baseLables)
    {
        startPage();
        List<BaseLables> list = baseLablesService.selectBaseLablesList(baseLables);
        return getDataTable(list);
    }

    /**
     * 导出标签管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:export')")
    @Log(title = "标签管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseLables baseLables)
    {
        List<BaseLables> list = baseLablesService.selectBaseLablesList(baseLables);
        ExcelUtil<BaseLables> util = new ExcelUtil<BaseLables>(BaseLables.class);
        util.exportExcel(response, list, "标签管理数据");
    }

    /**
     * 获取标签管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseLablesService.selectBaseLablesById(id));
    }

    /**
     * 新增标签管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:add')")
    @Log(title = "标签管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseLables baseLables)
    {
        return toAjax(baseLablesService.insertBaseLables(baseLables));
    }

    /**
     * 修改标签管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:edit')")
    @Log(title = "标签管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseLables baseLables)
    {
        return toAjax(baseLablesService.updateBaseLables(baseLables));
    }

    /**
     * 删除标签管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:lables:remove')")
    @Log(title = "标签管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseLablesService.deleteBaseLablesByIds(ids));
    }
}

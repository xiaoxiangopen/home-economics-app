package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseServiceType;

/**
 * 服务项目Service接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseServiceTypeService 
{
    /**
     * 查询服务项目
     * 
     * @param id 服务项目主键
     * @return 服务项目
     */
    public BaseServiceType selectBaseServiceTypeById(Long id);

    /**
     * 查询服务项目列表
     * 
     * @param baseServiceType 服务项目
     * @return 服务项目集合
     */
    public List<BaseServiceType> selectBaseServiceTypeList(BaseServiceType baseServiceType);

    /**
     * 新增服务项目
     * 
     * @param baseServiceType 服务项目
     * @return 结果
     */
    public int insertBaseServiceType(BaseServiceType baseServiceType);

    /**
     * 修改服务项目
     * 
     * @param baseServiceType 服务项目
     * @return 结果
     */
    public int updateBaseServiceType(BaseServiceType baseServiceType);

    /**
     * 批量删除服务项目
     * 
     * @param ids 需要删除的服务项目主键集合
     * @return 结果
     */
    public int deleteBaseServiceTypeByIds(Long[] ids);

    /**
     * 删除服务项目信息
     * 
     * @param id 服务项目主键
     * @return 结果
     */
    public int deleteBaseServiceTypeById(Long id);
}

package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 服务项目对象 base_service_type
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseServiceType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 服务名称 */
    @Excel(name = "服务名称")
    private String name;

    /** 图标 */
    @Excel(name = "图标")
    private String iconUrl;

    /** 排序编号 */
    @Excel(name = "排序编号")
    private Long sortNo;

    /** 是否可用 */
    @Excel(name = "是否可用")
    private String useableFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setIconUrl(String iconUrl)
    {
        this.iconUrl = iconUrl;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }
    public void setSortNo(Long sortNo)
    {
        this.sortNo = sortNo;
    }

    public Long getSortNo()
    {
        return sortNo;
    }
    public void setUseableFlag(String useableFlag)
    {
        this.useableFlag = useableFlag;
    }

    public String getUseableFlag()
    {
        return useableFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("iconUrl", getIconUrl())
            .append("remark", getRemark())
            .append("sortNo", getSortNo())
            .append("useableFlag", getUseableFlag())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

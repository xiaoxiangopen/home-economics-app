package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.UserEvaluate;
import com.jiumi.baseconfig.service.IUserEvaluateService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 阿姨评价Controller
 *
 * @author jiumi
 * @date 2023-01-13
 */
@RestController
@RequestMapping("/baseconfig/evaluate")
public class UserEvaluateController extends BaseController
{
    @Autowired
    private IUserEvaluateService userEvaluateService;

    /**
     * 查询阿姨评价列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserEvaluate userEvaluate)
    {
        startPage();
        List<UserEvaluate> list = userEvaluateService.selectUserEvaluateList(userEvaluate);
        return getDataTable(list);
    }

    /**
     * 导出阿姨评价列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:export')")
    @Log(title = "阿姨评价", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserEvaluate userEvaluate)
    {
        List<UserEvaluate> list = userEvaluateService.selectUserEvaluateList(userEvaluate);
        ExcelUtil<UserEvaluate> util = new ExcelUtil<UserEvaluate>(UserEvaluate.class);
        util.exportExcel(response, list, "阿姨评价数据");
    }

    /**
     * 获取阿姨评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(userEvaluateService.selectUserEvaluateById(id));
    }

    /**
     * 新增阿姨评价
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:add')")
    @Log(title = "阿姨评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserEvaluate userEvaluate)
    {
        return toAjax(userEvaluateService.insertUserEvaluate(userEvaluate));
    }

    /**
     * 修改阿姨评价
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:edit')")
    @Log(title = "阿姨评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserEvaluate userEvaluate)
    {
        return toAjax(userEvaluateService.updateUserEvaluate(userEvaluate));
    }

    /**
     * 删除阿姨评价
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:evaluate:remove')")
    @Log(title = "阿姨评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userEvaluateService.deleteUserEvaluateByIds(ids));
    }
}

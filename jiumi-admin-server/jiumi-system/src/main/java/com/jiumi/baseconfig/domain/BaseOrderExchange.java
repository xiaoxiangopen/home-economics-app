package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 用户兑换奖品记录对象 base_order_exchange
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseOrderExchange extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    @Excel(name = "兑换编号")
    private String orderCode;

    /** 用户ID */
    private Long userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 用户手机号 */
    @Excel(name = "用户手机号")
    private String phone;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private Long orderAmount;

    /** 兑换商品ID */

    private Long goodsId;

    @Excel(name = "兑换商品")
    private String goodsName;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long goodsNum;

    /** 兑换时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "兑换时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date exchangeTime;

    /** 邮寄状态 */
    @Excel(name = "邮寄地址")
    private String address;

    /** 快递类型 */
    @Excel(name = "快递类型",dictType="base_express_type")
    private String expressageType;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String expressageCode;

    /** 状态 01待发货 02已完成 */
    @Excel(name = "邮寄状态",dictType="base_goods_send_status")
    private String status;

    private String coverImage;

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setOrderAmount(Long orderAmount)
    {
        this.orderAmount = orderAmount;
    }

    public Long getOrderAmount()
    {
        return orderAmount;
    }
    public void setGoodsId(Long goodsId)
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId()
    {
        return goodsId;
    }
    public void setGoodsNum(Long goodsNum)
    {
        this.goodsNum = goodsNum;
    }

    public Long getGoodsNum()
    {
        return goodsNum;
    }
    public void setExchangeTime(Date exchangeTime)
    {
        this.exchangeTime = exchangeTime;
    }

    public Date getExchangeTime()
    {
        return exchangeTime;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setExpressageType(String expressageType)
    {
        this.expressageType = expressageType;
    }

    public String getExpressageType()
    {
        return expressageType;
    }
    public void setExpressageCode(String expressageCode)
    {
        this.expressageCode = expressageCode;
    }

    public String getExpressageCode()
    {
        return expressageCode;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("phone", getPhone())
            .append("orderAmount", getOrderAmount())
            .append("goodsId", getGoodsId())
            .append("goodsNum", getGoodsNum())
            .append("exchangeTime", getExchangeTime())
            .append("address", getAddress())
            .append("expressageType", getExpressageType())
            .append("expressageCode", getExpressageCode())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

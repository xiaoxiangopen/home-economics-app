package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.CalcConfig;
import com.jiumi.baseconfig.service.ICalcConfigService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 工资计算器Controller
 *
 * @author jiumi
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/baseconfig/calcconfig")
public class CalcConfigController extends BaseController
{
    @Autowired
    private ICalcConfigService calcConfigService;

    /**
     * 查询工资计算器列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(CalcConfig calcConfig)
    {
        startPage();
        List<CalcConfig> list = calcConfigService.selectCalcConfigList(calcConfig);
        return getDataTable(list);
    }

    /**
     * 导出工资计算器列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:export')")
    @Log(title = "工资计算器", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CalcConfig calcConfig)
    {
        List<CalcConfig> list = calcConfigService.selectCalcConfigList(calcConfig);
        ExcelUtil<CalcConfig> util = new ExcelUtil<CalcConfig>(CalcConfig.class);
        util.exportExcel(response, list, "工资计算器数据");
    }

    /**
     * 获取工资计算器详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(calcConfigService.selectCalcConfigById(id));
    }

    /**
     * 新增工资计算器
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:add')")
    @Log(title = "工资计算器", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CalcConfig calcConfig)
    {
        return toAjax(calcConfigService.insertCalcConfig(calcConfig));
    }

    /**
     * 修改工资计算器
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:edit')")
    @Log(title = "工资计算器", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CalcConfig calcConfig)
    {
        return toAjax(calcConfigService.updateCalcConfig(calcConfig));
    }

    /**
     * 删除工资计算器
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:calcconfig:remove')")
    @Log(title = "工资计算器", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(calcConfigService.deleteCalcConfigByIds(ids));
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserClockMapper;
import com.jiumi.baseconfig.domain.BaseUserClock;
import com.jiumi.baseconfig.service.IBaseUserClockService;

/**
 * 用户打卡记录Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserClockServiceImpl implements IBaseUserClockService
{
    @Autowired
    private BaseUserClockMapper baseUserClockMapper;

    /**
     * 查询用户打卡记录
     *
     * @param id 用户打卡记录主键
     * @return 用户打卡记录
     */
    @Override
    public BaseUserClock selectBaseUserClockById(Long id)
    {
        return baseUserClockMapper.selectBaseUserClockById(id);
    }

    /**
     * 查询用户打卡记录列表
     *
     * @param baseUserClock 用户打卡记录
     * @return 用户打卡记录
     */
    @Override
    public List<BaseUserClock> selectBaseUserClockList(BaseUserClock baseUserClock)
    {
        return baseUserClockMapper.selectBaseUserClockList(baseUserClock);
    }

    /**
     * 新增用户打卡记录
     *
     * @param baseUserClock 用户打卡记录
     * @return 结果
     */
    @Override
    public int insertBaseUserClock(BaseUserClock baseUserClock)
    {
        return baseUserClockMapper.insertBaseUserClock(baseUserClock);
    }

    /**
     * 修改用户打卡记录
     *
     * @param baseUserClock 用户打卡记录
     * @return 结果
     */
    @Override
    public int updateBaseUserClock(BaseUserClock baseUserClock)
    {
        return baseUserClockMapper.updateBaseUserClock(baseUserClock);
    }

    /**
     * 批量删除用户打卡记录
     *
     * @param ids 需要删除的用户打卡记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserClockByIds(Long[] ids)
    {
        return baseUserClockMapper.deleteBaseUserClockByIds(ids);
    }

    /**
     * 删除用户打卡记录信息
     *
     * @param id 用户打卡记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserClockById(Long id)
    {
        return baseUserClockMapper.deleteBaseUserClockById(id);
    }

    @Override
    public BaseUserClock selectLatestUserClockList(BaseUserClock clock) {
        return baseUserClockMapper.selectLatestUserClockList(clock);
    }
}

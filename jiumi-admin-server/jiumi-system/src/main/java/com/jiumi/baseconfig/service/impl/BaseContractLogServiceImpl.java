package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseContractLogMapper;
import com.jiumi.baseconfig.domain.BaseContractLog;
import com.jiumi.baseconfig.service.IBaseContractLogService;

/**
 * 合同日志Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseContractLogServiceImpl implements IBaseContractLogService
{
    @Autowired
    private BaseContractLogMapper baseContractLogMapper;

    /**
     * 查询合同日志
     *
     * @param id 合同日志主键
     * @return 合同日志
     */
    @Override
    public BaseContractLog selectBaseContractLogById(Long id)
    {
        return baseContractLogMapper.selectBaseContractLogById(id);
    }

    /**
     * 查询合同日志列表
     *
     * @param baseContractLog 合同日志
     * @return 合同日志
     */
    @Override
    public List<BaseContractLog> selectBaseContractLogList(BaseContractLog baseContractLog)
    {
        return baseContractLogMapper.selectBaseContractLogList(baseContractLog);
    }

    /**
     * 新增合同日志
     *
     * @param baseContractLog 合同日志
     * @return 结果
     */
    @Override
    public int insertBaseContractLog(BaseContractLog baseContractLog)
    {
        baseContractLog.setCreateTime(DateUtils.getNowDate());
        return baseContractLogMapper.insertBaseContractLog(baseContractLog);
    }

    /**
     * 修改合同日志
     *
     * @param baseContractLog 合同日志
     * @return 结果
     */
    @Override
    public int updateBaseContractLog(BaseContractLog baseContractLog)
    {
        return baseContractLogMapper.updateBaseContractLog(baseContractLog);
    }

    /**
     * 批量删除合同日志
     *
     * @param ids 需要删除的合同日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseContractLogByIds(Long[] ids)
    {
        return baseContractLogMapper.deleteBaseContractLogByIds(ids);
    }

    /**
     * 删除合同日志信息
     *
     * @param id 合同日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseContractLogById(Long id)
    {
        return baseContractLogMapper.deleteBaseContractLogById(id);
    }

    @Override
    public void saveContractLogs(Long id, Long userId, String content, String username) {
        BaseContractLog logs=new BaseContractLog();
        logs.setContractId(id);
        logs.setUserId(userId);
        logs.setContent(content);
        logs.setCreateBy(username);
        logs.setCreateTime(DateUtils.getNowDate());
        baseContractLogMapper.insertBaseContractLog(logs);
    }
}

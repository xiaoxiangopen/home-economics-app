package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseOrderCombine;
import com.jiumi.baseconfig.service.IBaseOrderCombineService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 合单记录Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/combine")
public class BaseOrderCombineController extends BaseController
{
    @Autowired
    private IBaseOrderCombineService baseOrderCombineService;

    /**
     * 查询合单记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseOrderCombine baseOrderCombine)
    {
        startPage();
        List<BaseOrderCombine> list = baseOrderCombineService.selectBaseOrderCombineList(baseOrderCombine);
        return getDataTable(list);
    }

    /**
     * 导出合单记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:export')")
    @Log(title = "合单记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseOrderCombine baseOrderCombine)
    {
        List<BaseOrderCombine> list = baseOrderCombineService.selectBaseOrderCombineList(baseOrderCombine);
        ExcelUtil<BaseOrderCombine> util = new ExcelUtil<BaseOrderCombine>(BaseOrderCombine.class);
        util.exportExcel(response, list, "合单记录数据");
    }

    /**
     * 获取合单记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseOrderCombineService.selectBaseOrderCombineById(id));
    }

    /**
     * 新增合单记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:add')")
    @Log(title = "合单记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseOrderCombine baseOrderCombine)
    {
        return toAjax(baseOrderCombineService.insertBaseOrderCombine(baseOrderCombine));
    }

    /**
     * 修改合单记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:edit')")
    @Log(title = "合单记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseOrderCombine baseOrderCombine)
    {
        return toAjax(baseOrderCombineService.updateBaseOrderCombine(baseOrderCombine));
    }

    /**
     * 删除合单记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:combine:remove')")
    @Log(title = "合单记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseOrderCombineService.deleteBaseOrderCombineByIds(ids));
    }
}

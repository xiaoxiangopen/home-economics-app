package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseLablesMapper;
import com.jiumi.baseconfig.domain.BaseLables;
import com.jiumi.baseconfig.service.IBaseLablesService;

/**
 * 标签管理Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseLablesServiceImpl implements IBaseLablesService
{
    @Autowired
    private BaseLablesMapper baseLablesMapper;

    /**
     * 查询标签管理
     *
     * @param id 标签管理主键
     * @return 标签管理
     */
    @Override
    public BaseLables selectBaseLablesById(Long id)
    {
        return baseLablesMapper.selectBaseLablesById(id);
    }

    /**
     * 查询标签管理列表
     *
     * @param baseLables 标签管理
     * @return 标签管理
     */
    @Override
    public List<BaseLables> selectBaseLablesList(BaseLables baseLables)
    {
        return baseLablesMapper.selectBaseLablesList(baseLables);
    }

    /**
     * 新增标签管理
     *
     * @param baseLables 标签管理
     * @return 结果
     */
    @Override
    public int insertBaseLables(BaseLables baseLables)
    {
        baseLables.setCreateTime(DateUtils.getNowDate());
        return baseLablesMapper.insertBaseLables(baseLables);
    }

    /**
     * 修改标签管理
     *
     * @param baseLables 标签管理
     * @return 结果
     */
    @Override
    public int updateBaseLables(BaseLables baseLables)
    {
        return baseLablesMapper.updateBaseLables(baseLables);
    }

    /**
     * 批量删除标签管理
     *
     * @param ids 需要删除的标签管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseLablesByIds(Long[] ids)
    {
        return baseLablesMapper.deleteBaseLablesByIds(ids);
    }

    /**
     * 删除标签管理信息
     *
     * @param id 标签管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseLablesById(Long id)
    {
        return baseLablesMapper.deleteBaseLablesById(id);
    }
}

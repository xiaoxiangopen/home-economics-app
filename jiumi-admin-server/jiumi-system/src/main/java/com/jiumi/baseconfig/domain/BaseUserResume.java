package com.jiumi.baseconfig.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 求职简历对象 base_user_resume
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUserResume extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String certCode;

    /** 学历 */
    @Excel(name = "学历")
    private String education;

    /** 工作经验 */
    @Excel(name = "工作经验")
    private String workExperience;

    /** 宗教 */
    @Excel(name = "宗教")
    private String religion;

    /** 身高 */
    @Excel(name = "身高")
    private BigDecimal height;

    /** 体重 */
    @Excel(name = "体重")
    private BigDecimal weigh;

    /** 个人优势 */
    @Excel(name = "个人优势")
    private String introduction;

    /** 附件地址 */
    @Excel(name = "附件地址")
    private String imageUrl;

    /** 视频地址 */
    @Excel(name = "视频地址")
    private String videoUrl;

    /** 默认简历 */
    @Excel(name = "默认简历")
    private String defaultFlag;

    /** 健康证有效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "健康证有效时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date healthTime;

    /** 健康证图片 */
    @Excel(name = "健康证图片")
    private String healthUrl;

    /** 能力标签 */
    @Excel(name = "能力标签")
    private String abilityLabel;


    private String nickName;


    private String phonenumber;

    private String avatar;

    /** 认证名称 */
    private String authName;

    /** 有效期 */
    private String sex;

    /** 出生日期 */

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    /** 生肖 */
    @Excel(name = "生肖")
    private String zodiac;

    /** 星座 */
    @Excel(name = "星座")
    private String constellation;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;
    private String certImage1;
    private String certImage2;

    private String typeId;
    private String typeName;

    private Long lowSalary;
    /** 最高工资 */
    private Long highSalary;

    private String liveType;
    private String liveTypeName;
    private Long companyId;
    private String companyName;

    private Integer minAge;
    private Integer maxAge;

    private String signType;

    private Integer minWorkExperience;
    private Integer maxWorkExperience;

    private String certFlag;

    private String contactPhone;

    private String intentionCity;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date currentTime;

    public Date getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Date currentTime) {
        this.currentTime = currentTime;
    }

    public String getIntentionCity() {
        return intentionCity;
    }

    public void setIntentionCity(String intentionCity) {
        this.intentionCity = intentionCity;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getCertFlag() {
        return certFlag;
    }

    public void setCertFlag(String certFlag) {
        this.certFlag = certFlag;
    }

    public Integer getMinWorkExperience() {
        return minWorkExperience;
    }

    public void setMinWorkExperience(Integer minWorkExperience) {
        this.minWorkExperience = minWorkExperience;
    }

    public Integer getMaxWorkExperience() {
        return maxWorkExperience;
    }

    public void setMaxWorkExperience(Integer maxWorkExperience) {
        this.maxWorkExperience = maxWorkExperience;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getLowSalary() {
        return lowSalary;
    }

    public void setLowSalary(Long lowSalary) {
        this.lowSalary = lowSalary;
    }

    public Long getHighSalary() {
        return highSalary;
    }

    public void setHighSalary(Long highSalary) {
        this.highSalary = highSalary;
    }

    public String getLiveType() {
        return liveType;
    }

    public void setLiveType(String liveType) {
        this.liveType = liveType;
    }

    public String getLiveTypeName() {
        return liveTypeName;
    }

    public void setLiveTypeName(String liveTypeName) {
        this.liveTypeName = liveTypeName;
    }

    public String getCertImage1() {
        return certImage1;
    }

    public void setCertImage1(String certImage1) {
        this.certImage1 = certImage1;
    }

    public String getCertImage2() {
        return certImage2;
    }

    public void setCertImage2(String certImage2) {
        this.certImage2 = certImage2;
    }

    private String certEndDate;

    public String getCertEndDate() {
        return certEndDate;
    }

    public void setCertEndDate(String certEndDate) {
        this.certEndDate = certEndDate;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getZodiac() {
        return zodiac;
    }

    public void setZodiac(String zodiac) {
        this.zodiac = zodiac;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    private List<BaseUserResumeExperience> experienceList;

    private List<BaseUserResumeCertificate> certList;
    private List<UserResumeHealthcert> healthList;

    public List<UserResumeHealthcert> getHealthList() {
        return healthList;
    }

    public void setHealthList(List<UserResumeHealthcert> healthList) {
        this.healthList = healthList;
    }

    private BaseUserIntention  intention;

    public List<BaseUserResumeExperience> getExperienceList() {
        return experienceList;
    }

    public void setExperienceList(List<BaseUserResumeExperience> experienceList) {
        this.experienceList = experienceList;
    }

    public List<BaseUserResumeCertificate> getCertList() {
        return certList;
    }

    public void setCertList(List<BaseUserResumeCertificate> certList) {
        this.certList = certList;
    }

    public BaseUserIntention getIntention() {
        return intention;
    }

    public void setIntention(BaseUserIntention intention) {
        this.intention = intention;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setCertCode(String certCode)
    {
        this.certCode = certCode;
    }

    public String getCertCode()
    {
        return certCode;
    }
    public void setEducation(String education)
    {
        this.education = education;
    }

    public String getEducation()
    {
        return education;
    }
    public void setWorkExperience(String workExperience)
    {
        this.workExperience = workExperience;
    }

    public String getWorkExperience()
    {
        return workExperience;
    }
    public void setReligion(String religion)
    {
        this.religion = religion;
    }

    public String getReligion()
    {
        return religion;
    }
    public void setHeight(BigDecimal height)
    {
        this.height = height;
    }

    public BigDecimal getHeight()
    {
        return height;
    }
    public void setWeigh(BigDecimal weigh)
    {
        this.weigh = weigh;
    }

    public BigDecimal getWeigh()
    {
        return weigh;
    }
    public void setIntroduction(String introduction)
    {
        this.introduction = introduction;
    }

    public String getIntroduction()
    {
        return introduction;
    }
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }
    public void setVideoUrl(String videoUrl)
    {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl()
    {
        return videoUrl;
    }
    public void setDefaultFlag(String defaultFlag)
    {
        this.defaultFlag = defaultFlag;
    }

    public String getDefaultFlag()
    {
        return defaultFlag;
    }
    public void setHealthTime(Date healthTime)
    {
        this.healthTime = healthTime;
    }

    public Date getHealthTime()
    {
        return healthTime;
    }
    public void setHealthUrl(String healthUrl)
    {
        this.healthUrl = healthUrl;
    }

    public String getHealthUrl()
    {
        return healthUrl;
    }
    public void setAbilityLabel(String abilityLabel)
    {
        this.abilityLabel = abilityLabel;
    }

    public String getAbilityLabel()
    {
        return abilityLabel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("certCode", getCertCode())
            .append("education", getEducation())
            .append("workExperience", getWorkExperience())
            .append("religion", getReligion())
            .append("height", getHeight())
            .append("weigh", getWeigh())
            .append("introduction", getIntroduction())
            .append("imageUrl", getImageUrl())
            .append("videoUrl", getVideoUrl())
            .append("defaultFlag", getDefaultFlag())
            .append("healthTime", getHealthTime())
            .append("healthUrl", getHealthUrl())
            .append("abilityLabel", getAbilityLabel())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}

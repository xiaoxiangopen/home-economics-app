package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BusinessConfigMapper;
import com.jiumi.baseconfig.domain.BusinessConfig;
import com.jiumi.baseconfig.service.IBusinessConfigService;

/**
 * 业务设置Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-07
 */
@Service
public class BusinessConfigServiceImpl implements IBusinessConfigService
{
    @Autowired
    private BusinessConfigMapper businessConfigMapper;

    /**
     * 查询业务设置
     *
     * @param id 业务设置主键
     * @return 业务设置
     */
    @Override
    public BusinessConfig selectBusinessConfigById(Long id)
    {
        return businessConfigMapper.selectBusinessConfigById(id);
    }

    /**
     * 查询业务设置列表
     *
     * @param businessConfig 业务设置
     * @return 业务设置
     */
    @Override
    public List<BusinessConfig> selectBusinessConfigList(BusinessConfig businessConfig)
    {
        return businessConfigMapper.selectBusinessConfigList(businessConfig);
    }

    /**
     * 新增业务设置
     *
     * @param businessConfig 业务设置
     * @return 结果
     */
    @Override
    public int insertBusinessConfig(BusinessConfig businessConfig)
    {
        businessConfig.setCreateTime(DateUtils.getNowDate());
        return businessConfigMapper.insertBusinessConfig(businessConfig);
    }

    /**
     * 修改业务设置
     *
     * @param businessConfig 业务设置
     * @return 结果
     */
    @Override
    public int updateBusinessConfig(BusinessConfig businessConfig)
    {
        businessConfig.setUpdateTime(DateUtils.getNowDate());
        return businessConfigMapper.updateBusinessConfig(businessConfig);
    }

    /**
     * 批量删除业务设置
     *
     * @param ids 需要删除的业务设置主键
     * @return 结果
     */
    @Override
    public int deleteBusinessConfigByIds(Long[] ids)
    {
        return businessConfigMapper.deleteBusinessConfigByIds(ids);
    }

    /**
     * 删除业务设置信息
     *
     * @param id 业务设置主键
     * @return 结果
     */
    @Override
    public int deleteBusinessConfigById(Long id)
    {
        return businessConfigMapper.deleteBusinessConfigById(id);
    }
}

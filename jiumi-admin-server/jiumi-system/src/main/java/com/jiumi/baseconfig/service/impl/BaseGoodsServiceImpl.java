package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseGoodsMapper;
import com.jiumi.baseconfig.domain.BaseGoods;
import com.jiumi.baseconfig.service.IBaseGoodsService;

/**
 * 积分商品Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseGoodsServiceImpl implements IBaseGoodsService
{
    @Autowired
    private BaseGoodsMapper baseGoodsMapper;

    /**
     * 查询积分商品
     *
     * @param id 积分商品主键
     * @return 积分商品
     */
    @Override
    public BaseGoods selectBaseGoodsById(Long id)
    {
        return baseGoodsMapper.selectBaseGoodsById(id);
    }

    /**
     * 查询积分商品列表
     *
     * @param baseGoods 积分商品
     * @return 积分商品
     */
    @Override
    public List<BaseGoods> selectBaseGoodsList(BaseGoods baseGoods)
    {
        return baseGoodsMapper.selectBaseGoodsList(baseGoods);
    }

    /**
     * 新增积分商品
     *
     * @param baseGoods 积分商品
     * @return 结果
     */
    @Override
    public int insertBaseGoods(BaseGoods baseGoods)
    {
        baseGoods.setCreateTime(DateUtils.getNowDate());
        return baseGoodsMapper.insertBaseGoods(baseGoods);
    }

    /**
     * 修改积分商品
     *
     * @param baseGoods 积分商品
     * @return 结果
     */
    @Override
    public int updateBaseGoods(BaseGoods baseGoods)
    {
        baseGoods.setUpdateTime(DateUtils.getNowDate());
        return baseGoodsMapper.updateBaseGoods(baseGoods);
    }

    /**
     * 批量删除积分商品
     *
     * @param ids 需要删除的积分商品主键
     * @return 结果
     */
    @Override
    public int deleteBaseGoodsByIds(Long[] ids)
    {
        return baseGoodsMapper.deleteBaseGoodsByIds(ids);
    }

    /**
     * 删除积分商品信息
     *
     * @param id 积分商品主键
     * @return 结果
     */
    @Override
    public int deleteBaseGoodsById(Long id)
    {
        return baseGoodsMapper.deleteBaseGoodsById(id);
    }
}

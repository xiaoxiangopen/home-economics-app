package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseAppointment;
import com.jiumi.common.core.domain.AjaxResult;

/**
 * 预约Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseAppointmentService
{
    /**
     * 查询预约
     *
     * @param id 预约主键
     * @return 预约
     */
    public BaseAppointment selectBaseAppointmentById(Long id);

    /**
     * 查询预约列表
     *
     * @param baseAppointment 预约
     * @return 预约集合
     */
    public List<BaseAppointment> selectBaseAppointmentList(BaseAppointment baseAppointment);

    /**
     * 新增预约
     *
     * @param baseAppointment 预约
     * @return 结果
     */
    public int insertBaseAppointment(BaseAppointment baseAppointment);

    /**
     * 修改预约
     *
     * @param baseAppointment 预约
     * @return 结果
     */
    public int updateBaseAppointment(BaseAppointment baseAppointment);

    /**
     * 批量删除预约
     *
     * @param ids 需要删除的预约主键集合
     * @return 结果
     */
    public int deleteBaseAppointmentByIds(Long[] ids);

    /**
     * 删除预约信息
     *
     * @param id 预约主键
     * @return 结果
     */
    public int deleteBaseAppointmentById(Long id);

    AjaxResult saveBaseAppointment(BaseAppointment appointment);

    List<BaseAppointment> selectAgencyAppointmentList(BaseAppointment appointParam);

    List<BaseAppointment> selectUserFocusAppointmentList(BaseAppointment param);

    List<BaseAppointment> selectManageAppointmentList(BaseAppointment baseAppointment);

    List<BaseAppointment> selectCompanyAppointmentList(BaseAppointment appointment);

    void autoReferrerEmployer();

    void autoAppoinmentOffline();

    void updateBaseAppointmentOffLine(Long userId);
}

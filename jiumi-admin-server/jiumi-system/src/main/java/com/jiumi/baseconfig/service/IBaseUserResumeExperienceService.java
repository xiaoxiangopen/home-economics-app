package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserResumeExperience;

/**
 * 工作经历Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseUserResumeExperienceService
{
    /**
     * 查询工作经历
     *
     * @param id 工作经历主键
     * @return 工作经历
     */
    public BaseUserResumeExperience selectBaseUserResumeExperienceById(Long id);

    /**
     * 查询工作经历列表
     *
     * @param baseUserResumeExperience 工作经历
     * @return 工作经历集合
     */
    public List<BaseUserResumeExperience> selectBaseUserResumeExperienceList(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 新增工作经历
     *
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    public int insertBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 修改工作经历
     *
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    public int updateBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 批量删除工作经历
     *
     * @param ids 需要删除的工作经历主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeExperienceByIds(Long[] ids);

    /**
     * 删除工作经历信息
     *
     * @param id 工作经历主键
     * @return 结果
     */
    public int deleteBaseUserResumeExperienceById(Long id);

    void deleteBaseUserResumeExperienceByResumeId(Long id);

    void deleteResumeExperienceByCertCode(String certCode);
}

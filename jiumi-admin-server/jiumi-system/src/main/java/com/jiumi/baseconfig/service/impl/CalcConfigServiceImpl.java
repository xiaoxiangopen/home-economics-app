package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.DictUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.CalcConfigMapper;
import com.jiumi.baseconfig.domain.CalcConfig;
import com.jiumi.baseconfig.service.ICalcConfigService;

/**
 * 工资计算器Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-10
 */
@Service
public class CalcConfigServiceImpl implements ICalcConfigService
{
    @Autowired
    private CalcConfigMapper calcConfigMapper;

    /**
     * 查询工资计算器
     *
     * @param id 工资计算器主键
     * @return 工资计算器
     */
    @Override
    public CalcConfig selectCalcConfigById(String id)
    {
        return calcConfigMapper.selectCalcConfigById(id);
    }

    /**
     * 查询工资计算器列表
     *
     * @param calcConfig 工资计算器
     * @return 工资计算器
     */
    @Override
    public List<CalcConfig> selectCalcConfigList(CalcConfig calcConfig)
    {
        return calcConfigMapper.selectCalcConfigList(calcConfig);
    }

    /**
     * 新增工资计算器
     *
     * @param calcConfig 工资计算器
     * @return 结果
     */
    @Override
    public int insertCalcConfig(CalcConfig calcConfig)
    {
        calcConfig.setMeasureName(DictUtils.getDictLabel("base_measure_type",calcConfig.getMeasureType()));
        calcConfig.setCreateTime(DateUtils.getNowDate());
        return calcConfigMapper.insertCalcConfig(calcConfig);
    }

    /**
     * 修改工资计算器
     *
     * @param calcConfig 工资计算器
     * @return 结果
     */
    @Override
    public int updateCalcConfig(CalcConfig calcConfig)
    {
        calcConfig.setMeasureName(DictUtils.getDictLabel("base_measure_type",calcConfig.getMeasureType()));
        return calcConfigMapper.updateCalcConfig(calcConfig);
    }

    /**
     * 批量删除工资计算器
     *
     * @param ids 需要删除的工资计算器主键
     * @return 结果
     */
    @Override
    public int deleteCalcConfigByIds(String[] ids)
    {
        return calcConfigMapper.deleteCalcConfigByIds(ids);
    }

    /**
     * 删除工资计算器信息
     *
     * @param id 工资计算器主键
     * @return 结果
     */
    @Override
    public int deleteCalcConfigById(String id)
    {
        return calcConfigMapper.deleteCalcConfigById(id);
    }
}

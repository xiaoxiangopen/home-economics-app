package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserFocusMapper;
import com.jiumi.baseconfig.domain.BaseUserFocus;
import com.jiumi.baseconfig.service.IBaseUserFocusService;

/**
 * 用户关注Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserFocusServiceImpl implements IBaseUserFocusService
{
    @Autowired
    private BaseUserFocusMapper baseUserFocusMapper;

    /**
     * 查询用户关注
     *
     * @param id 用户关注主键
     * @return 用户关注
     */
    @Override
    public BaseUserFocus selectBaseUserFocusById(Long id)
    {
        return baseUserFocusMapper.selectBaseUserFocusById(id);
    }

    /**
     * 查询用户关注列表
     *
     * @param baseUserFocus 用户关注
     * @return 用户关注
     */
    @Override
    public List<BaseUserFocus> selectBaseUserFocusList(BaseUserFocus baseUserFocus)
    {
        return baseUserFocusMapper.selectBaseUserFocusList(baseUserFocus);
    }

    /**
     * 新增用户关注
     *
     * @param baseUserFocus 用户关注
     * @return 结果
     */
    @Override
    public int insertBaseUserFocus(BaseUserFocus baseUserFocus)
    {
        baseUserFocus.setCreateTime(DateUtils.getNowDate());
        return baseUserFocusMapper.insertBaseUserFocus(baseUserFocus);
    }

    /**
     * 修改用户关注
     *
     * @param baseUserFocus 用户关注
     * @return 结果
     */
    @Override
    public int updateBaseUserFocus(BaseUserFocus baseUserFocus)
    {
        return baseUserFocusMapper.updateBaseUserFocus(baseUserFocus);
    }

    /**
     * 批量删除用户关注
     *
     * @param ids 需要删除的用户关注主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserFocusByIds(Long[] ids)
    {
        return baseUserFocusMapper.deleteBaseUserFocusByIds(ids);
    }

    /**
     * 删除用户关注信息
     *
     * @param id 用户关注主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserFocusById(Long id)
    {
        return baseUserFocusMapper.deleteBaseUserFocusById(id);
    }

    @Override
    public void deleteBaseUserFocusByUserId(Long userId) {
        baseUserFocusMapper.deleteBaseUserFocusByUserId(userId);
    }
}

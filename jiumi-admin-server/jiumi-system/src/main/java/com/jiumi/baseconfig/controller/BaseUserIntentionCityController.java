package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserIntentionCity;
import com.jiumi.baseconfig.service.IBaseUserIntentionCityService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 求职意向城市Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/intentioncity")
public class BaseUserIntentionCityController extends BaseController
{
    @Autowired
    private IBaseUserIntentionCityService baseUserIntentionCityService;

    /**
     * 查询求职意向城市列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserIntentionCity baseUserIntentionCity)
    {
        startPage();
        List<BaseUserIntentionCity> list = baseUserIntentionCityService.selectBaseUserIntentionCityList(baseUserIntentionCity);
        return getDataTable(list);
    }

    /**
     * 导出求职意向城市列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:export')")
    @Log(title = "求职意向城市", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserIntentionCity baseUserIntentionCity)
    {
        List<BaseUserIntentionCity> list = baseUserIntentionCityService.selectBaseUserIntentionCityList(baseUserIntentionCity);
        ExcelUtil<BaseUserIntentionCity> util = new ExcelUtil<BaseUserIntentionCity>(BaseUserIntentionCity.class);
        util.exportExcel(response, list, "求职意向城市数据");
    }

    /**
     * 获取求职意向城市详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserIntentionCityService.selectBaseUserIntentionCityById(id));
    }

    /**
     * 新增求职意向城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:add')")
    @Log(title = "求职意向城市", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserIntentionCity baseUserIntentionCity)
    {
        return toAjax(baseUserIntentionCityService.insertBaseUserIntentionCity(baseUserIntentionCity));
    }

    /**
     * 修改求职意向城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:edit')")
    @Log(title = "求职意向城市", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserIntentionCity baseUserIntentionCity)
    {
        return toAjax(baseUserIntentionCityService.updateBaseUserIntentionCity(baseUserIntentionCity));
    }

    /**
     * 删除求职意向城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intentioncity:remove')")
    @Log(title = "求职意向城市", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserIntentionCityService.deleteBaseUserIntentionCityByIds(ids));
    }
}

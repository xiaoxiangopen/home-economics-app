package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 文章对象 base_article
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 文章类型 */
    @Excel(name = "文章类型")
    private String type;

    /** 封面图 */
    @Excel(name = "封面图")
    private String coverImage;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 是否显示 */
    @Excel(name = "是否显示")
    private String showFlag;

    /** 阅读量 */
    @Excel(name = "阅读量")
    private Long readNum;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setCoverImage(String coverImage)
    {
        this.coverImage = coverImage;
    }

    public String getCoverImage()
    {
        return coverImage;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setShowFlag(String showFlag)
    {
        this.showFlag = showFlag;
    }

    public String getShowFlag()
    {
        return showFlag;
    }
    public void setReadNum(Long readNum)
    {
        this.readNum = readNum;
    }

    public Long getReadNum()
    {
        return readNum;
    }
    public void setPublishTime(Date publishTime)
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime()
    {
        return publishTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("type", getType())
            .append("coverImage", getCoverImage())
            .append("content", getContent())
            .append("showFlag", getShowFlag())
            .append("readNum", getReadNum())
            .append("publishTime", getPublishTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

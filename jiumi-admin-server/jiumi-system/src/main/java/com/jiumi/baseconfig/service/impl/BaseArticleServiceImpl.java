package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseArticleMapper;
import com.jiumi.baseconfig.domain.BaseArticle;
import com.jiumi.baseconfig.service.IBaseArticleService;

/**
 * 文章Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseArticleServiceImpl implements IBaseArticleService
{
    @Autowired
    private BaseArticleMapper baseArticleMapper;

    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    @Override
    public BaseArticle selectBaseArticleById(Long id)
    {
        return baseArticleMapper.selectBaseArticleById(id);
    }

    /**
     * 查询文章列表
     *
     * @param baseArticle 文章
     * @return 文章
     */
    @Override
    public List<BaseArticle> selectBaseArticleList(BaseArticle baseArticle)
    {
        return baseArticleMapper.selectBaseArticleList(baseArticle);
    }

    /**
     * 新增文章
     *
     * @param baseArticle 文章
     * @return 结果
     */
    @Override
    public int insertBaseArticle(BaseArticle baseArticle)
    {
        baseArticle.setCreateTime(DateUtils.getNowDate());
        return baseArticleMapper.insertBaseArticle(baseArticle);
    }

    /**
     * 修改文章
     *
     * @param baseArticle 文章
     * @return 结果
     */
    @Override
    public int updateBaseArticle(BaseArticle baseArticle)
    {
        baseArticle.setUpdateTime(DateUtils.getNowDate());
        return baseArticleMapper.updateBaseArticle(baseArticle);
    }

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteBaseArticleByIds(Long[] ids)
    {
        return baseArticleMapper.deleteBaseArticleByIds(ids);
    }

    /**
     * 删除文章信息
     *
     * @param id 文章主键
     * @return 结果
     */
    @Override
    public int deleteBaseArticleById(Long id)
    {
        return baseArticleMapper.deleteBaseArticleById(id);
    }
}

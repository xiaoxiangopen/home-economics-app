package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseGoods;
import com.jiumi.baseconfig.service.IBaseGoodsService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 积分商品Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/goods")
public class BaseGoodsController extends BaseController
{
    @Autowired
    private IBaseGoodsService baseGoodsService;

    /**
     * 查询积分商品列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseGoods baseGoods)
    {
        startPage();
        List<BaseGoods> list = baseGoodsService.selectBaseGoodsList(baseGoods);
        return getDataTable(list);
    }

    /**
     * 导出积分商品列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:export')")
    @Log(title = "积分商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseGoods baseGoods)
    {
        List<BaseGoods> list = baseGoodsService.selectBaseGoodsList(baseGoods);
        ExcelUtil<BaseGoods> util = new ExcelUtil<BaseGoods>(BaseGoods.class);
        util.exportExcel(response, list, "积分商品数据");
    }

    /**
     * 获取积分商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseGoodsService.selectBaseGoodsById(id));
    }

    /**
     * 新增积分商品
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:add')")
    @Log(title = "积分商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseGoods baseGoods)
    {
        return toAjax(baseGoodsService.insertBaseGoods(baseGoods));
    }

    /**
     * 修改积分商品
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:edit')")
    @Log(title = "积分商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseGoods baseGoods)
    {
        return toAjax(baseGoodsService.updateBaseGoods(baseGoods));
    }

    /**
     * 删除积分商品
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:goods:remove')")
    @Log(title = "积分商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseGoodsService.deleteBaseGoodsByIds(ids));
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.UserResumeHealthcert;
import com.jiumi.baseconfig.service.IUserResumeHealthcertService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 健康证书Controller
 *
 * @author jiumi
 * @date 2023-06-15
 */
@RestController
@RequestMapping("/baseconfig/healthcert")
public class UserResumeHealthcertController extends BaseController
{
    @Autowired
    private IUserResumeHealthcertService userResumeHealthcertService;

    /**
     * 查询健康证书列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserResumeHealthcert userResumeHealthcert)
    {
        startPage();
        List<UserResumeHealthcert> list = userResumeHealthcertService.selectUserResumeHealthcertList(userResumeHealthcert);
        return getDataTable(list);
    }

    /**
     * 导出健康证书列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:export')")
    @Log(title = "健康证书", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserResumeHealthcert userResumeHealthcert)
    {
        List<UserResumeHealthcert> list = userResumeHealthcertService.selectUserResumeHealthcertList(userResumeHealthcert);
        ExcelUtil<UserResumeHealthcert> util = new ExcelUtil<UserResumeHealthcert>(UserResumeHealthcert.class);
        util.exportExcel(response, list, "健康证书数据");
    }

    /**
     * 获取健康证书详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(userResumeHealthcertService.selectUserResumeHealthcertById(id));
    }

    /**
     * 新增健康证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:add')")
    @Log(title = "健康证书", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserResumeHealthcert userResumeHealthcert)
    {
        return toAjax(userResumeHealthcertService.insertUserResumeHealthcert(userResumeHealthcert));
    }

    /**
     * 修改健康证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:edit')")
    @Log(title = "健康证书", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserResumeHealthcert userResumeHealthcert)
    {
        return toAjax(userResumeHealthcertService.updateUserResumeHealthcert(userResumeHealthcert));
    }

    /**
     * 删除健康证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:healthcert:remove')")
    @Log(title = "健康证书", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userResumeHealthcertService.deleteUserResumeHealthcertByIds(ids));
    }
}

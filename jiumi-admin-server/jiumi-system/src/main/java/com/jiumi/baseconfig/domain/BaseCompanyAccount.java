package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 公司招聘人员对象 base_company_account
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseCompanyAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 是否主要Y是N否 */
    @Excel(name = "是否主要Y是N否")
    private String isMain;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    private String userName;
    private String phonenumber;
    private int auntNum;
    private int employNum;
    private int publishAunt;
    private int publishEmploy;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getAuntNum() {
        return auntNum;
    }

    public void setAuntNum(int auntNum) {
        this.auntNum = auntNum;
    }

    public int getEmployNum() {
        return employNum;
    }

    public void setEmployNum(int employNum) {
        this.employNum = employNum;
    }

    public int getPublishAunt() {
        return publishAunt;
    }

    public void setPublishAunt(int publishAunt) {
        this.publishAunt = publishAunt;
    }

    public int getPublishEmploy() {
        return publishEmploy;
    }

    public void setPublishEmploy(int publishEmploy) {
        this.publishEmploy = publishEmploy;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setIsMain(String isMain)
    {
        this.isMain = isMain;
    }

    public String getIsMain()
    {
        return isMain;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("userId", getUserId())
            .append("isMain", getIsMain())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}

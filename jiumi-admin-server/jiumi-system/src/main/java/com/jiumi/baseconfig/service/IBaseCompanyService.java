package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCompany;

/**
 * 家政公司Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseCompanyService
{
    /**
     * 查询家政公司
     *
     * @param id 家政公司主键
     * @return 家政公司
     */
    public BaseCompany selectBaseCompanyById(Long id);

    /**
     * 查询家政公司列表
     *
     * @param baseCompany 家政公司
     * @return 家政公司集合
     */
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany);

    /**
     * 新增家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    public int insertBaseCompany(BaseCompany baseCompany);

    /**
     * 修改家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    public int updateBaseCompany(BaseCompany baseCompany);

    /**
     * 批量删除家政公司
     *
     * @param ids 需要删除的家政公司主键集合
     * @return 结果
     */
    public int deleteBaseCompanyByIds(Long[] ids);

    /**
     * 删除家政公司信息
     *
     * @param id 家政公司主键
     * @return 结果
     */
    public int deleteBaseCompanyById(Long id);

    BaseCompany selectBaseCompanyByName(String companyName);

    BaseCompany selectBaseCompanyByUserId(Long userId);

    BaseCompany selectBaseCompanyByLienceCode(String businessLicenseCode);
}

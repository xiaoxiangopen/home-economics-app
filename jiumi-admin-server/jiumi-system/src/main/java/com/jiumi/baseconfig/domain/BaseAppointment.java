package com.jiumi.baseconfig.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 预约对象 base_appointment
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseAppointment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private String code;

    /** 类型01月嫂02育儿嫂03钟点工 */
    @Excel(name = "服务类型",dictType = "base_service_type")
    private String categoryType;

    @Excel(name = "发布人姓名")
    private String   authName;

    @Excel(name = "发布人手机号")
    private String   phonenumber;

    @Excel(name = "所属公司")
    private String   companyName;
    private Long     authUserId;

    @Excel(name = "授权人")
    private String   authUserName;

    /** 发布信息用户ID */
    private Long userId;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String userName;

    /** 联系人手机号 */
    @Excel(name = "联系人手机号")
    private String userPhone;

    /** 联系人地址 */
    @Excel(name = "联系人地址")
    private String userAddress;

    /** 服务内容 */
    @Excel(name = "服务内容")
    private String needServiceType;

    /** 服务周期 */
    @Excel(name = "服务周期")
    private Long servicePeriod;

    /** 工资预算 */
    @Excel(name = "工资预算")
    private BigDecimal salaryAmount;

    /** 合单奖励 */
    @Excel(name = "合单奖励")
    private BigDecimal combineAmount;

    /** 预产期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预产期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date babyExpectDate;

    /** 宝宝数量 */
    @Excel(name = "宝宝数量")
    private String babyNum;

    /** 妈妈年龄 */
    @Excel(name = "妈妈年龄")
    private String motherAge;

    /** 是否新手妈妈 */
    @Excel(name = "是否新手妈妈",dictType ="sys_yes_no" )
    private String firstFlag;

    /** 您与孕妈的关系 */
    @Excel(name = "您与孕妈的关系")
    private String relation;

    /** 工作内容 */
    @Excel(name = "工作内容")
    private String workContent;

    /** 用工类型01住家02不住家03都可以 */
    @Excel(name = "用工类型",dictType = "base_work_home_type")
    private String workType;

    /** 每月上班天数 */
    @Excel(name = "每月上班天数")
    private String monthWorkDays;

    /** 保姆重点服务内容 */
    @Excel(name = "保姆重点服务内容")
    private String importService;

    /** 家庭面积 */
    @Excel(name = "家庭面积")
    private String homeArea;

    /** 家庭人数 */
    @Excel(name = "家庭人数")
    private String homePersonNum;

    /** 籍贯要求 */
    @Excel(name = "籍贯要求")
    private String requireNative;

    /** 年龄要求 */
    @Excel(name = "年龄要求")
    private String requireAge;

    /** 技能要求 */
    @Excel(name = "技能要求")
    private String requireSkill;

    /** 用工类型01上午用人02下午用人 */
    @Excel(name = "用工类型",dictType = "base_work_time_type")
    private String useWorkerType;

    /** 每日用工时长 */
    @Excel(name = "每日用工时长")
    private String workLong;

    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date applyTime;

    /** 预约状态 */
    @Excel(name = "预约状态",dictType = "base_appointment_status")
    private String applyStatus;

    /** 置顶状态 */
    @Excel(name = "置顶状态",dictType = "sys_yes_no")
    private String topFlag;

    private Integer minAge;
    private Integer maxAge;

    private String isAdmin;



    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getAuthUserId() {
        return authUserId;
    }

    public void setAuthUserId(Long authUserId) {
        this.authUserId = authUserId;
    }

    public String getAuthUserName() {
        return authUserName;
    }

    public void setAuthUserName(String authUserName) {
        this.authUserName = authUserName;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setCategoryType(String categoryType)
    {
        this.categoryType = categoryType;
    }

    public String getCategoryType()
    {
        return categoryType;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserPhone(String userPhone)
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone()
    {
        return userPhone;
    }
    public void setUserAddress(String userAddress)
    {
        this.userAddress = userAddress;
    }

    public String getUserAddress()
    {
        return userAddress;
    }
    public void setNeedServiceType(String needServiceType)
    {
        this.needServiceType = needServiceType;
    }

    public String getNeedServiceType()
    {
        return needServiceType;
    }
    public void setServicePeriod(Long servicePeriod)
    {
        this.servicePeriod = servicePeriod;
    }

    public Long getServicePeriod()
    {
        return servicePeriod;
    }
    public void setSalaryAmount(BigDecimal salaryAmount)
    {
        this.salaryAmount = salaryAmount;
    }

    public BigDecimal getSalaryAmount()
    {
        return salaryAmount;
    }
    public void setCombineAmount(BigDecimal combineAmount)
    {
        this.combineAmount = combineAmount;
    }

    public BigDecimal getCombineAmount()
    {
        return combineAmount;
    }
    public void setBabyExpectDate(Date babyExpectDate)
    {
        this.babyExpectDate = babyExpectDate;
    }

    public Date getBabyExpectDate()
    {
        return babyExpectDate;
    }
    public void setBabyNum(String babyNum)
    {
        this.babyNum = babyNum;
    }

    public String getBabyNum()
    {
        return babyNum;
    }
    public void setMotherAge(String motherAge)
    {
        this.motherAge = motherAge;
    }

    public String getMotherAge()
    {
        return motherAge;
    }
    public void setFirstFlag(String firstFlag)
    {
        this.firstFlag = firstFlag;
    }

    public String getFirstFlag()
    {
        return firstFlag;
    }
    public void setRelation(String relation)
    {
        this.relation = relation;
    }

    public String getRelation()
    {
        return relation;
    }
    public void setWorkContent(String workContent)
    {
        this.workContent = workContent;
    }

    public String getWorkContent()
    {
        return workContent;
    }
    public void setWorkType(String workType)
    {
        this.workType = workType;
    }

    public String getWorkType()
    {
        return workType;
    }
    public void setMonthWorkDays(String monthWorkDays)
    {
        this.monthWorkDays = monthWorkDays;
    }

    public String getMonthWorkDays()
    {
        return monthWorkDays;
    }
    public void setImportService(String importService)
    {
        this.importService = importService;
    }

    public String getImportService()
    {
        return importService;
    }
    public void setHomeArea(String homeArea)
    {
        this.homeArea = homeArea;
    }

    public String getHomeArea()
    {
        return homeArea;
    }
    public void setHomePersonNum(String homePersonNum)
    {
        this.homePersonNum = homePersonNum;
    }

    public String getHomePersonNum()
    {
        return homePersonNum;
    }
    public void setRequireNative(String requireNative)
    {
        this.requireNative = requireNative;
    }

    public String getRequireNative()
    {
        return requireNative;
    }
    public void setRequireAge(String requireAge)
    {
        this.requireAge = requireAge;
    }

    public String getRequireAge()
    {
        return requireAge;
    }
    public void setRequireSkill(String requireSkill)
    {
        this.requireSkill = requireSkill;
    }

    public String getRequireSkill()
    {
        return requireSkill;
    }
    public void setUseWorkerType(String useWorkerType)
    {
        this.useWorkerType = useWorkerType;
    }

    public String getUseWorkerType()
    {
        return useWorkerType;
    }
    public void setWorkLong(String workLong)
    {
        this.workLong = workLong;
    }

    public String getWorkLong()
    {
        return workLong;
    }
    public void setApplyTime(Date applyTime)
    {
        this.applyTime = applyTime;
    }

    public Date getApplyTime()
    {
        return applyTime;
    }
    public void setApplyStatus(String applyStatus)
    {
        this.applyStatus = applyStatus;
    }

    public String getApplyStatus()
    {
        return applyStatus;
    }
    public void setTopFlag(String topFlag)
    {
        this.topFlag = topFlag;
    }

    public String getTopFlag()
    {
        return topFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("categoryType", getCategoryType())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("userPhone", getUserPhone())
            .append("userAddress", getUserAddress())
            .append("needServiceType", getNeedServiceType())
            .append("servicePeriod", getServicePeriod())
            .append("salaryAmount", getSalaryAmount())
            .append("combineAmount", getCombineAmount())
            .append("babyExpectDate", getBabyExpectDate())
            .append("babyNum", getBabyNum())
            .append("motherAge", getMotherAge())
            .append("firstFlag", getFirstFlag())
            .append("relation", getRelation())
            .append("workContent", getWorkContent())
            .append("workType", getWorkType())
            .append("monthWorkDays", getMonthWorkDays())
            .append("importService", getImportService())
            .append("homeArea", getHomeArea())
            .append("homePersonNum", getHomePersonNum())
            .append("requireNative", getRequireNative())
            .append("requireAge", getRequireAge())
            .append("requireSkill", getRequireSkill())
            .append("useWorkerType", getUseWorkerType())
            .append("workLong", getWorkLong())
            .append("applyTime", getApplyTime())
            .append("applyStatus", getApplyStatus())
            .append("topFlag", getTopFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

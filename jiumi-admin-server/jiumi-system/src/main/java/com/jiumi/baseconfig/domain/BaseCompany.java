package com.jiumi.baseconfig.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 家政公司对象 base_company
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户ID */
    private Long userId;

    /** 公司简称 */
    @Excel(name = "公司简称")
    private String shortName;

    /** 公司全称 */
    @Excel(name = "公司全称")
    private String companyName;

    /** 营业执照图片 */
    private String businessLicenseCode;
    private String businessLicenseUrl;

    /** 公章图片 */
    private String sealUrl;

    /** 联系人 */
    @Excel(name = "负责人姓名")
    private String contactName;

    /** 联系人电话 */
    @Excel(name = "负责人电话")
    private String contactPhone;

    @Excel(name = "子账号")
    private int accountNum;
    @Excel(name = "邀请阿姨")
    private int auntNum;

    @Excel(name = "邀请雇主")
    private int employNum;

    @Excel(name = "发布阿姨")
    private int publishAunt;

    @Excel(name = "发布雇主")
    private int publishEmploy;

    @Excel(name = "累计签约")
    private int signNum;



    private String companyAddress;
    private int companyAddressNum;

    public String getBusinessLicenseCode() {
        return businessLicenseCode;
    }

    public void setBusinessLicenseCode(String businessLicenseCode) {
        this.businessLicenseCode = businessLicenseCode;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "认证时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date authTime;

    /** 认证状态01待审核02已认证03认证不通过 */
    @Excel(name = "认证状态",readConverterExp = "01=待审核,02=已认证,03=认证不通过")
    private String authStatus;

    @Excel(name = "备注")
    private String authRemark;

    @Excel(name = "显示状态",readConverterExp = "Y=显示,N=不显示")
    private String availableFlag;

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public int getCompanyAddressNum() {
        return companyAddressNum;
    }

    public void setCompanyAddressNum(int companyAddressNum) {
        this.companyAddressNum = companyAddressNum;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public int getAuntNum() {
        return auntNum;
    }

    public void setAuntNum(int auntNum) {
        this.auntNum = auntNum;
    }

    public int getEmployNum() {
        return employNum;
    }

    public void setEmployNum(int employNum) {
        this.employNum = employNum;
    }

    public int getPublishAunt() {
        return publishAunt;
    }

    public void setPublishAunt(int publishAunt) {
        this.publishAunt = publishAunt;
    }

    public int getPublishEmploy() {
        return publishEmploy;
    }

    public void setPublishEmploy(int publishEmploy) {
        this.publishEmploy = publishEmploy;
    }

    public int getSignNum() {
        return signNum;
    }

    public void setSignNum(int signNum) {
        this.signNum = signNum;
    }

    public Date getAuthTime() {
        return authTime;
    }

    public void setAuthTime(Date authTime) {
        this.authTime = authTime;
    }

    public String getAuthRemark() {
        return authRemark;
    }

    public void setAuthRemark(String authRemark) {
        this.authRemark = authRemark;
    }

    private List<BaseCompanyAddress> adddressList;

    public String getAvailableFlag() {
        return availableFlag;
    }

    public void setAvailableFlag(String availableFlag) {
        this.availableFlag = availableFlag;
    }

    public List<BaseCompanyAddress> getAdddressList() {
        return adddressList;
    }

    public void setAdddressList(List<BaseCompanyAddress> adddressList) {
        this.adddressList = adddressList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getShortName()
    {
        return shortName;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setBusinessLicenseUrl(String businessLicenseUrl)
    {
        this.businessLicenseUrl = businessLicenseUrl;
    }

    public String getBusinessLicenseUrl()
    {
        return businessLicenseUrl;
    }
    public void setSealUrl(String sealUrl)
    {
        this.sealUrl = sealUrl;
    }

    public String getSealUrl()
    {
        return sealUrl;
    }
    public void setContactName(String contactName)
    {
        this.contactName = contactName;
    }

    public String getContactName()
    {
        return contactName;
    }
    public void setContactPhone(String contactPhone)
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }
    public void setAuthStatus(String authStatus)
    {
        this.authStatus = authStatus;
    }

    public String getAuthStatus()
    {
        return authStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("shortName", getShortName())
            .append("companyName", getCompanyName())
            .append("businessLicenseUrl", getBusinessLicenseUrl())
            .append("sealUrl", getSealUrl())
            .append("contactName", getContactName())
            .append("contactPhone", getContactPhone())
            .append("authStatus", getAuthStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.jiumi.baseconfig.domain.BaseCompanyAccount;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.mapper.BaseCompanyAccountMapper;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.common.core.redis.RedisCache;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseCompanyMapper;
import com.jiumi.baseconfig.domain.BaseCompany;
import com.jiumi.baseconfig.service.IBaseCompanyService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 家政公司Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseCompanyServiceImpl implements IBaseCompanyService {
    @Autowired
    private BaseCompanyMapper baseCompanyMapper;

    @Autowired
    private BaseUserMapper baseUserMapper;

    @Autowired
    private BaseCompanyAccountMapper baseCompanyAccountMapper;

    @Autowired
    private RedisCache redisCache;
    /**
     * 查询家政公司
     *
     * @param id 家政公司主键
     * @return 家政公司
     */
    @Override
    public BaseCompany selectBaseCompanyById(Long id) {
        return baseCompanyMapper.selectBaseCompanyById(id);
    }

    /**
     * 查询家政公司列表
     *
     * @param baseCompany 家政公司
     * @return 家政公司
     */
    @Override
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany) {
        return baseCompanyMapper.selectBaseCompanyList(baseCompany);
    }

    /**
     * 新增家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    @Override
    public int insertBaseCompany(BaseCompany baseCompany) {
        baseCompany.setCreateTime(DateUtils.getNowDate());
        return baseCompanyMapper.insertBaseCompany(baseCompany);
    }

    /**
     * 修改家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    @Transactional
    @Override
    public int updateBaseCompany(BaseCompany baseCompany) {
        baseCompany.setUpdateTime(DateUtils.getNowDate());
        if (!"01".equals(baseCompany.getAuthStatus())) {
            baseCompany.setAuthTime(DateUtils.getNowDate());
        }
        BaseCompany currentcompany = baseCompanyMapper.selectBaseCompanyById(baseCompany.getId());
        if ("01".equals(currentcompany.getAuthStatus()) && "02".equals(baseCompany.getAuthStatus())) {
            BaseUser currentUser = new BaseUser();
            currentUser.setUserId(currentcompany.getUserId());
            currentUser.setAuthStatus("02");
            currentUser.setAuthTime(DateUtils.getNowDate());
            currentUser.setCompanyId(baseCompany.getId());
            currentUser.setCompanyName(currentcompany.getCompanyName());
            baseUserMapper.updateBaseUser(currentUser);


            BaseCompanyAccount account = new BaseCompanyAccount();
            account.setCompanyId(currentcompany.getId());
            account.setUserId(currentcompany.getUserId());
            List<BaseCompanyAccount> checkList= baseCompanyAccountMapper.selectBaseCompanyAccountList(account);
            if(checkList.size()==0) {
                account.setIsMain("Y");
                account.setStatus("Y");
                account.setCreateTime(DateUtils.getNowDate());
                account.setCreateBy(baseCompany.getUpdateBy());
                baseCompanyAccountMapper.insertBaseCompanyAccount(account);
            }

        }
        int result= baseCompanyMapper.updateBaseCompany(baseCompany);
        if(result>0){
            BaseCompanyAccount account = new BaseCompanyAccount();
            account.setCompanyId(baseCompany.getId());
            List<BaseCompanyAccount> companyUserList= baseCompanyAccountMapper.selectBaseCompanyAccountList(account);
            companyUserList.stream().forEach(userAccount->{
                if("N".equals(baseCompany.getAvailableFlag())) {
                    userAccount.setStatus("N");
                }else{
                    userAccount.setStatus("Y");
                }
                userAccount.setUpdateTime(DateUtils.getNowDate());
                userAccount.setUpdateBy(baseCompany.getUpdateBy());
                int updateNum=baseCompanyAccountMapper.updateBaseCompanyAccount(userAccount);
                if(updateNum>0 && "N".equals(baseCompany.getAvailableFlag())){
                    redisCache.setCacheObject("accountExprie-"+userAccount.getUserId(),"Y",365, TimeUnit.DAYS);
                }else{
                    redisCache.deleteObject("accountExprie-"+userAccount.getUserId());
                }
            });
        }
        return result;
    }

    /**
     * 批量删除家政公司
     *
     * @param ids 需要删除的家政公司主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyByIds(Long[] ids) {
        return baseCompanyMapper.deleteBaseCompanyByIds(ids);
    }

    /**
     * 删除家政公司信息
     *
     * @param id 家政公司主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyById(Long id) {
        return baseCompanyMapper.deleteBaseCompanyById(id);
    }

    @Override
    public BaseCompany selectBaseCompanyByName(String companyName) {
        return baseCompanyMapper.selectBaseCompanyByName(companyName);
    }

    @Override
    public BaseCompany selectBaseCompanyByUserId(Long userId) {
        return baseCompanyMapper.selectBaseCompanyByUserId(userId);
    }

    @Override
    public BaseCompany selectBaseCompanyByLienceCode(String businessLicenseCode) {
        return baseCompanyMapper.selectBaseCompanyByLienceCode(businessLicenseCode);
    }
}

package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户消息提醒对象 base_user_message
 *
 * @author jiumi
 * @date 2023-02-13
 */
public class UserMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 消息分类01签约通知02推荐阿姨03缴费提醒 */
    @Excel(name = "消息分类01签约通知02推荐阿姨03缴费提醒")
    private String category;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 连接ID */
    @Excel(name = "连接ID")
    private String linkId;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String msgContent;

    /** 是否阅读Y是N否 */
    @Excel(name = "是否阅读Y是N否")
    private String readStatus;

    /** 阅读时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "阅读时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date readTime;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getCategory()
    {
        return category;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setLinkId(String linkId)
    {
        this.linkId = linkId;
    }

    public String getLinkId()
    {
        return linkId;
    }
    public void setMsgContent(String msgContent)
    {
        this.msgContent = msgContent;
    }

    public String getMsgContent()
    {
        return msgContent;
    }
    public void setReadStatus(String readStatus)
    {
        this.readStatus = readStatus;
    }

    public String getReadStatus()
    {
        return readStatus;
    }
    public void setReadTime(Date readTime)
    {
        this.readTime = readTime;
    }

    public Date getReadTime()
    {
        return readTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("category", getCategory())
            .append("title", getTitle())
            .append("linkId", getLinkId())
            .append("msgContent", getMsgContent())
            .append("readStatus", getReadStatus())
            .append("createTime", getCreateTime())
            .append("readTime", getReadTime())
            .toString();
    }
}

package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.RegisterHistory;

/**
 * 用户记录Service接口
 * 
 * @author jiumi
 * @date 2023-09-04
 */
public interface IRegisterHistoryService 
{
    /**
     * 查询用户记录
     * 
     * @param id 用户记录主键
     * @return 用户记录
     */
    public RegisterHistory selectRegisterHistoryById(Long id);

    /**
     * 查询用户记录列表
     * 
     * @param registerHistory 用户记录
     * @return 用户记录集合
     */
    public List<RegisterHistory> selectRegisterHistoryList(RegisterHistory registerHistory);

    /**
     * 新增用户记录
     * 
     * @param registerHistory 用户记录
     * @return 结果
     */
    public int insertRegisterHistory(RegisterHistory registerHistory);

    /**
     * 修改用户记录
     * 
     * @param registerHistory 用户记录
     * @return 结果
     */
    public int updateRegisterHistory(RegisterHistory registerHistory);

    /**
     * 批量删除用户记录
     * 
     * @param ids 需要删除的用户记录主键集合
     * @return 结果
     */
    public int deleteRegisterHistoryByIds(Long[] ids);

    /**
     * 删除用户记录信息
     * 
     * @param id 用户记录主键
     * @return 结果
     */
    public int deleteRegisterHistoryById(Long id);
}

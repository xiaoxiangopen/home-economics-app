package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserResumeExperienceMapper;
import com.jiumi.baseconfig.domain.BaseUserResumeExperience;
import com.jiumi.baseconfig.service.IBaseUserResumeExperienceService;

/**
 * 工作经历Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserResumeExperienceServiceImpl implements IBaseUserResumeExperienceService
{
    @Autowired
    private BaseUserResumeExperienceMapper baseUserResumeExperienceMapper;

    /**
     * 查询工作经历
     *
     * @param id 工作经历主键
     * @return 工作经历
     */
    @Override
    public BaseUserResumeExperience selectBaseUserResumeExperienceById(Long id)
    {
        return baseUserResumeExperienceMapper.selectBaseUserResumeExperienceById(id);
    }

    /**
     * 查询工作经历列表
     *
     * @param baseUserResumeExperience 工作经历
     * @return 工作经历
     */
    @Override
    public List<BaseUserResumeExperience> selectBaseUserResumeExperienceList(BaseUserResumeExperience baseUserResumeExperience)
    {
        return baseUserResumeExperienceMapper.selectBaseUserResumeExperienceList(baseUserResumeExperience);
    }

    /**
     * 新增工作经历
     *
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    @Override
    public int insertBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience)
    {
        baseUserResumeExperience.setCreateTime(DateUtils.getNowDate());
        return baseUserResumeExperienceMapper.insertBaseUserResumeExperience(baseUserResumeExperience);
    }

    /**
     * 修改工作经历
     *
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    @Override
    public int updateBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience)
    {
        return baseUserResumeExperienceMapper.updateBaseUserResumeExperience(baseUserResumeExperience);
    }

    /**
     * 批量删除工作经历
     *
     * @param ids 需要删除的工作经历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeExperienceByIds(Long[] ids)
    {
        return baseUserResumeExperienceMapper.deleteBaseUserResumeExperienceByIds(ids);
    }

    /**
     * 删除工作经历信息
     *
     * @param id 工作经历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeExperienceById(Long id)
    {
        return baseUserResumeExperienceMapper.deleteBaseUserResumeExperienceById(id);
    }

    @Override
    public void deleteBaseUserResumeExperienceByResumeId(Long id) {
        baseUserResumeExperienceMapper.deleteBaseUserResumeExperienceByResumeId(id);
    }

    @Override
    public void deleteResumeExperienceByCertCode(String certCode) {
        baseUserResumeExperienceMapper.deleteResumeExperienceByCertCode(certCode);
    }
}

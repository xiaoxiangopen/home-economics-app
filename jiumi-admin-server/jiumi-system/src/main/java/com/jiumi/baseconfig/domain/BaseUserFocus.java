package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 用户关注对象 base_user_focus
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUserFocus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 关注类型01职位雇主02阿姨 */
    @Excel(name = "关注类型01职位雇主02阿姨")
    private String type;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long focusUserId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setFocusUserId(Long focusUserId)
    {
        this.focusUserId = focusUserId;
    }

    public Long getFocusUserId()
    {
        return focusUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("type", getType())
            .append("focusUserId", getFocusUserId())
            .append("createTime", getCreateTime())
            .toString();
    }
}

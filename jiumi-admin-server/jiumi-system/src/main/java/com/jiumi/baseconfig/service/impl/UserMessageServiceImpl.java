package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.UserMessageMapper;
import com.jiumi.baseconfig.domain.UserMessage;
import com.jiumi.baseconfig.service.IUserMessageService;

/**
 * 用户消息提醒Service业务层处理
 *
 * @author jiumi
 * @date 2023-02-13
 */
@Service
public class UserMessageServiceImpl implements IUserMessageService
{
    @Autowired
    private UserMessageMapper userMessageMapper;

    /**
     * 查询用户消息提醒
     *
     * @param id 用户消息提醒主键
     * @return 用户消息提醒
     */
    @Override
    public UserMessage selectUserMessageById(String id)
    {
        return userMessageMapper.selectUserMessageById(id);
    }

    /**
     * 查询用户消息提醒列表
     *
     * @param userMessage 用户消息提醒
     * @return 用户消息提醒
     */
    @Override
    public List<UserMessage> selectUserMessageList(UserMessage userMessage)
    {
        return userMessageMapper.selectUserMessageList(userMessage);
    }

    /**
     * 新增用户消息提醒
     *
     * @param userMessage 用户消息提醒
     * @return 结果
     */
    @Override
    public int insertUserMessage(UserMessage userMessage)
    {
        userMessage.setCreateTime(DateUtils.getNowDate());
        return userMessageMapper.insertUserMessage(userMessage);
    }

    /**
     * 修改用户消息提醒
     *
     * @param userMessage 用户消息提醒
     * @return 结果
     */
    @Override
    public int updateUserMessage(UserMessage userMessage)
    {
        return userMessageMapper.updateUserMessage(userMessage);
    }

    /**
     * 批量删除用户消息提醒
     *
     * @param ids 需要删除的用户消息提醒主键
     * @return 结果
     */
    @Override
    public int deleteUserMessageByIds(String[] ids)
    {
        return userMessageMapper.deleteUserMessageByIds(ids);
    }

    /**
     * 删除用户消息提醒信息
     *
     * @param id 用户消息提醒主键
     * @return 结果
     */
    @Override
    public int deleteUserMessageById(String id)
    {
        return userMessageMapper.deleteUserMessageById(id);
    }

    @Override
    public void readAllUserMessageList(Long id) {
        userMessageMapper.readAllUserMessageList(id+"");
    }

    @Override
    public void deleteUserMessageByUserId(Long userId) {
        userMessageMapper.deleteUserMessageByUserId(userId);
    }
}

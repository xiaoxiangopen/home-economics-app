package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.ScoreHistory;
import com.jiumi.baseconfig.service.IScoreHistoryService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 积分记录信息Controller
 *
 * @author jiumi
 * @date 2022-12-08
 */
@RestController
@RequestMapping("/baseconfig/ScoreHistory")
public class ScoreHistoryController extends BaseController
{
    @Autowired
    private IScoreHistoryService scoreHistoryService;

    /**
     * 查询积分记录信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScoreHistory scoreHistory)
    {
        startPage();
        List<ScoreHistory> list = scoreHistoryService.selectScoreHistoryList(scoreHistory);
        return getDataTable(list);
    }

    /**
     * 导出积分记录信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:export')")
    @Log(title = "积分记录信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScoreHistory scoreHistory)
    {
        List<ScoreHistory> list = scoreHistoryService.selectScoreHistoryList(scoreHistory);
        ExcelUtil<ScoreHistory> util = new ExcelUtil<ScoreHistory>(ScoreHistory.class);
        util.exportExcel(response, list, "积分记录信息数据");
    }

    /**
     * 获取积分记录信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scoreHistoryService.selectScoreHistoryById(id));
    }

    /**
     * 新增积分记录信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:add')")
    @Log(title = "积分记录信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScoreHistory scoreHistory)
    {
        return toAjax(scoreHistoryService.insertScoreHistory(scoreHistory));
    }

    /**
     * 修改积分记录信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:edit')")
    @Log(title = "积分记录信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScoreHistory scoreHistory)
    {
        return toAjax(scoreHistoryService.updateScoreHistory(scoreHistory));
    }

    /**
     * 删除积分记录信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:ScoreHistory:remove')")
    @Log(title = "积分记录信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scoreHistoryService.deleteScoreHistoryByIds(ids));
    }
}

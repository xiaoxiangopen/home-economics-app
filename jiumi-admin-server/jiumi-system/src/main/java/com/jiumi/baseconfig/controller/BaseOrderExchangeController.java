package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseOrderExchange;
import com.jiumi.baseconfig.service.IBaseOrderExchangeService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户兑换奖品记录Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/exchange")
public class BaseOrderExchangeController extends BaseController
{
    @Autowired
    private IBaseOrderExchangeService baseOrderExchangeService;

    /**
     * 查询用户兑换奖品记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseOrderExchange baseOrderExchange)
    {
        startPage();
        List<BaseOrderExchange> list = baseOrderExchangeService.selectBaseOrderExchangeList(baseOrderExchange);
        return getDataTable(list);
    }

    /**
     * 导出用户兑换奖品记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:export')")
    @Log(title = "用户兑换奖品记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseOrderExchange baseOrderExchange)
    {
        List<BaseOrderExchange> list = baseOrderExchangeService.selectBaseOrderExchangeList(baseOrderExchange);
        ExcelUtil<BaseOrderExchange> util = new ExcelUtil<BaseOrderExchange>(BaseOrderExchange.class);
        util.exportExcel(response, list, "用户兑换记录");
    }

    /**
     * 获取用户兑换奖品记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseOrderExchangeService.selectBaseOrderExchangeById(id));
    }

    /**
     * 新增用户兑换奖品记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:add')")
    @Log(title = "用户兑换奖品记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseOrderExchange baseOrderExchange)
    {
        return toAjax(baseOrderExchangeService.insertBaseOrderExchange(baseOrderExchange));
    }

    /**
     * 修改用户兑换奖品记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:edit')")
    @Log(title = "用户兑换奖品记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseOrderExchange baseOrderExchange)
    {
        return toAjax(baseOrderExchangeService.updateBaseOrderExchange(baseOrderExchange));
    }

    /**
     * 删除用户兑换奖品记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:exchange:remove')")
    @Log(title = "用户兑换奖品记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseOrderExchangeService.deleteBaseOrderExchangeByIds(ids));
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserIntentionMapper;
import com.jiumi.baseconfig.domain.BaseUserIntention;
import com.jiumi.baseconfig.service.IBaseUserIntentionService;

/**
 * 求职意向Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserIntentionServiceImpl implements IBaseUserIntentionService
{
    @Autowired
    private BaseUserIntentionMapper baseUserIntentionMapper;

    /**
     * 查询求职意向
     *
     * @param id 求职意向主键
     * @return 求职意向
     */
    @Override
    public BaseUserIntention selectBaseUserIntentionById(Long id)
    {
        return baseUserIntentionMapper.selectBaseUserIntentionById(id);
    }

    /**
     * 查询求职意向列表
     *
     * @param baseUserIntention 求职意向
     * @return 求职意向
     */
    @Override
    public List<BaseUserIntention> selectBaseUserIntentionList(BaseUserIntention baseUserIntention)
    {
        return baseUserIntentionMapper.selectBaseUserIntentionList(baseUserIntention);
    }

    /**
     * 新增求职意向
     *
     * @param baseUserIntention 求职意向
     * @return 结果
     */
    @Override
    public int insertBaseUserIntention(BaseUserIntention baseUserIntention)
    {
        baseUserIntention.setCreateTime(DateUtils.getNowDate());
        return baseUserIntentionMapper.insertBaseUserIntention(baseUserIntention);
    }

    /**
     * 修改求职意向
     *
     * @param baseUserIntention 求职意向
     * @return 结果
     */
    @Override
    public int updateBaseUserIntention(BaseUserIntention baseUserIntention)
    {
        return baseUserIntentionMapper.updateBaseUserIntention(baseUserIntention);
    }

    /**
     * 批量删除求职意向
     *
     * @param ids 需要删除的求职意向主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserIntentionByIds(Long[] ids)
    {
        return baseUserIntentionMapper.deleteBaseUserIntentionByIds(ids);
    }

    /**
     * 删除求职意向信息
     *
     * @param id 求职意向主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserIntentionById(Long id)
    {
        return baseUserIntentionMapper.deleteBaseUserIntentionById(id);
    }

    @Override
    public BaseUserIntention selectBaseUserIntentionByUserId(Long userId) {
        return baseUserIntentionMapper.selectBaseUserIntentionByUserId(userId);
    }

    @Override
    public void deleteBaseUserIntentionByUserId(Long userId) {
        baseUserIntentionMapper.deleteBaseUserIntentionByUserId(userId);
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserResume;
import com.jiumi.baseconfig.service.IBaseUserResumeService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 求职简历Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/resume")
public class BaseUserResumeController extends BaseController
{
    @Autowired
    private IBaseUserResumeService baseUserResumeService;

    /**
     * 查询求职简历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserResume baseUserResume)
    {
        startPage();
        List<BaseUserResume> list = baseUserResumeService.selectBaseUserResumeList(baseUserResume);
        return getDataTable(list);
    }

    /**
     * 导出求职简历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:export')")
    @Log(title = "求职简历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserResume baseUserResume)
    {
        List<BaseUserResume> list = baseUserResumeService.selectBaseUserResumeList(baseUserResume);
        ExcelUtil<BaseUserResume> util = new ExcelUtil<BaseUserResume>(BaseUserResume.class);
        util.exportExcel(response, list, "求职简历数据");
    }

    /**
     * 获取求职简历详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserResumeService.selectBaseUserResumeById(id));
    }

    /**
     * 新增求职简历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:add')")
    @Log(title = "求职简历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserResume baseUserResume)
    {
        return toAjax(baseUserResumeService.insertBaseUserResume(baseUserResume));
    }

    /**
     * 修改求职简历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:edit')")
    @Log(title = "求职简历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserResume baseUserResume)
    {
        return toAjax(baseUserResumeService.updateBaseUserResume(baseUserResume));
    }

    /**
     * 删除求职简历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:resume:remove')")
    @Log(title = "求职简历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserResumeService.deleteBaseUserResumeByIds(ids));
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseCompanyAccount;
import com.jiumi.baseconfig.service.IBaseCompanyAccountService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 公司招聘人员Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/account")
public class BaseCompanyAccountController extends BaseController
{
    @Autowired
    private IBaseCompanyAccountService baseCompanyAccountService;

    /**
     * 查询公司招聘人员列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyAccount baseCompanyAccount)
    {
        startPage();
        List<BaseCompanyAccount> list = baseCompanyAccountService.selectBaseCompanyAccountList(baseCompanyAccount);
        return getDataTable(list);
    }

    /**
     * 导出公司招聘人员列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:export')")
    @Log(title = "公司招聘人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyAccount baseCompanyAccount)
    {
        List<BaseCompanyAccount> list = baseCompanyAccountService.selectBaseCompanyAccountList(baseCompanyAccount);
        ExcelUtil<BaseCompanyAccount> util = new ExcelUtil<BaseCompanyAccount>(BaseCompanyAccount.class);
        util.exportExcel(response, list, "公司招聘人员数据");
    }

    /**
     * 获取公司招聘人员详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseCompanyAccountService.selectBaseCompanyAccountById(id));
    }

    /**
     * 新增公司招聘人员
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:add')")
    @Log(title = "公司招聘人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyAccount baseCompanyAccount)
    {
        return toAjax(baseCompanyAccountService.insertBaseCompanyAccount(baseCompanyAccount));
    }

    /**
     * 修改公司招聘人员
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:edit')")
    @Log(title = "公司招聘人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyAccount baseCompanyAccount)
    {
        return toAjax(baseCompanyAccountService.updateBaseCompanyAccount(baseCompanyAccount));
    }

    /**
     * 删除公司招聘人员
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:account:remove')")
    @Log(title = "公司招聘人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyAccountService.deleteBaseCompanyAccountByIds(ids));
    }
}

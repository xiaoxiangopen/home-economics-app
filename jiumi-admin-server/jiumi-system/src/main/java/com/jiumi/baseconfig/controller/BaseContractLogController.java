package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseContractLog;
import com.jiumi.baseconfig.service.IBaseContractLogService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 合同日志Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/contractlog")
public class BaseContractLogController extends BaseController
{
    @Autowired
    private IBaseContractLogService baseContractLogService;

    /**
     * 查询合同日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseContractLog baseContractLog)
    {
        startPage();
        List<BaseContractLog> list = baseContractLogService.selectBaseContractLogList(baseContractLog);
        return getDataTable(list);
    }

    /**
     * 导出合同日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:export')")
    @Log(title = "合同日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseContractLog baseContractLog)
    {
        List<BaseContractLog> list = baseContractLogService.selectBaseContractLogList(baseContractLog);
        ExcelUtil<BaseContractLog> util = new ExcelUtil<BaseContractLog>(BaseContractLog.class);
        util.exportExcel(response, list, "合同日志数据");
    }

    /**
     * 获取合同日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseContractLogService.selectBaseContractLogById(id));
    }

    /**
     * 新增合同日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:add')")
    @Log(title = "合同日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseContractLog baseContractLog)
    {
        return toAjax(baseContractLogService.insertBaseContractLog(baseContractLog));
    }

    /**
     * 修改合同日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:edit')")
    @Log(title = "合同日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseContractLog baseContractLog)
    {
        return toAjax(baseContractLogService.updateBaseContractLog(baseContractLog));
    }

    /**
     * 删除合同日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractlog:remove')")
    @Log(title = "合同日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseContractLogService.deleteBaseContractLogByIds(ids));
    }
}

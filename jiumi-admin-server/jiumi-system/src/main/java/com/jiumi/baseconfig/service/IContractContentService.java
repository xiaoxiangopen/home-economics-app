package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.ContractContent;

/**
 * 合同内容Service接口
 * 
 * @author jiumi
 * @date 2023-03-15
 */
public interface IContractContentService 
{
    /**
     * 查询合同内容
     * 
     * @param id 合同内容主键
     * @return 合同内容
     */
    public ContractContent selectContractContentById(Long id);

    /**
     * 查询合同内容列表
     * 
     * @param contractContent 合同内容
     * @return 合同内容集合
     */
    public List<ContractContent> selectContractContentList(ContractContent contractContent);

    /**
     * 新增合同内容
     * 
     * @param contractContent 合同内容
     * @return 结果
     */
    public int insertContractContent(ContractContent contractContent);

    /**
     * 修改合同内容
     * 
     * @param contractContent 合同内容
     * @return 结果
     */
    public int updateContractContent(ContractContent contractContent);

    /**
     * 批量删除合同内容
     * 
     * @param ids 需要删除的合同内容主键集合
     * @return 结果
     */
    public int deleteContractContentByIds(Long[] ids);

    /**
     * 删除合同内容信息
     * 
     * @param id 合同内容主键
     * @return 结果
     */
    public int deleteContractContentById(Long id);
}

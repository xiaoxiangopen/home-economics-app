package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserClock;

/**
 * 用户打卡记录Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseUserClockMapper
{
    /**
     * 查询用户打卡记录
     *
     * @param id 用户打卡记录主键
     * @return 用户打卡记录
     */
    public BaseUserClock selectBaseUserClockById(Long id);

    /**
     * 查询用户打卡记录列表
     *
     * @param baseUserClock 用户打卡记录
     * @return 用户打卡记录集合
     */
    public List<BaseUserClock> selectBaseUserClockList(BaseUserClock baseUserClock);

    /**
     * 新增用户打卡记录
     *
     * @param baseUserClock 用户打卡记录
     * @return 结果
     */
    public int insertBaseUserClock(BaseUserClock baseUserClock);

    /**
     * 修改用户打卡记录
     *
     * @param baseUserClock 用户打卡记录
     * @return 结果
     */
    public int updateBaseUserClock(BaseUserClock baseUserClock);

    /**
     * 删除用户打卡记录
     *
     * @param id 用户打卡记录主键
     * @return 结果
     */
    public int deleteBaseUserClockById(Long id);

    /**
     * 批量删除用户打卡记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserClockByIds(Long[] ids);

    BaseUserClock selectLatestUserClockList(BaseUserClock clock);
}

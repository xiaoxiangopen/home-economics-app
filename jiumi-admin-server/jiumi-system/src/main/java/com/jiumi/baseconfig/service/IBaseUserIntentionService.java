package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserIntention;

/**
 * 求职意向Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseUserIntentionService
{
    /**
     * 查询求职意向
     *
     * @param id 求职意向主键
     * @return 求职意向
     */
    public BaseUserIntention selectBaseUserIntentionById(Long id);

    /**
     * 查询求职意向列表
     *
     * @param baseUserIntention 求职意向
     * @return 求职意向集合
     */
    public List<BaseUserIntention> selectBaseUserIntentionList(BaseUserIntention baseUserIntention);

    /**
     * 新增求职意向
     *
     * @param baseUserIntention 求职意向
     * @return 结果
     */
    public int insertBaseUserIntention(BaseUserIntention baseUserIntention);

    /**
     * 修改求职意向
     *
     * @param baseUserIntention 求职意向
     * @return 结果
     */
    public int updateBaseUserIntention(BaseUserIntention baseUserIntention);

    /**
     * 批量删除求职意向
     *
     * @param ids 需要删除的求职意向主键集合
     * @return 结果
     */
    public int deleteBaseUserIntentionByIds(Long[] ids);

    /**
     * 删除求职意向信息
     *
     * @param id 求职意向主键
     * @return 结果
     */
    public int deleteBaseUserIntentionById(Long id);

    BaseUserIntention selectBaseUserIntentionByUserId(Long userID);

    void deleteBaseUserIntentionByUserId(Long userId);
}

package com.jiumi.baseconfig.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jiumi.baseconfig.domain.BaseCityInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lun.zhang
 * @create 2022/7/12 14:14
 */
@Data
public class TreeSelectVo implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private String value;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeSelectVo> children;

    public TreeSelectVo()
    {

    }


    public TreeSelectVo(BaseCityInfo cityInfo)
    {
        this.value = cityInfo.getCode();
        this.label = cityInfo.getValue();
        this.children = cityInfo.getChildren().stream().map(TreeSelectVo::new).collect(Collectors.toList());
    }

}

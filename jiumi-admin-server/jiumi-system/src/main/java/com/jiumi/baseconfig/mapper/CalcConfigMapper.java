package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.CalcConfig;

/**
 * 工资计算器Mapper接口
 * 
 * @author jiumi
 * @date 2022-12-10
 */
public interface CalcConfigMapper 
{
    /**
     * 查询工资计算器
     * 
     * @param id 工资计算器主键
     * @return 工资计算器
     */
    public CalcConfig selectCalcConfigById(String id);

    /**
     * 查询工资计算器列表
     * 
     * @param calcConfig 工资计算器
     * @return 工资计算器集合
     */
    public List<CalcConfig> selectCalcConfigList(CalcConfig calcConfig);

    /**
     * 新增工资计算器
     * 
     * @param calcConfig 工资计算器
     * @return 结果
     */
    public int insertCalcConfig(CalcConfig calcConfig);

    /**
     * 修改工资计算器
     * 
     * @param calcConfig 工资计算器
     * @return 结果
     */
    public int updateCalcConfig(CalcConfig calcConfig);

    /**
     * 删除工资计算器
     * 
     * @param id 工资计算器主键
     * @return 结果
     */
    public int deleteCalcConfigById(String id);

    /**
     * 批量删除工资计算器
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCalcConfigByIds(String[] ids);
}

package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 合同订单Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseOrderMapper
{
    /**
     * 查询合同订单
     *
     * @param id 合同订单主键
     * @return 合同订单
     */
    public BaseOrder selectBaseOrderById(Long id);

    /**
     * 查询合同订单列表
     *
     * @param baseOrder 合同订单
     * @return 合同订单集合
     */
    public List<BaseOrder> selectBaseOrderList(BaseOrder baseOrder);

    /**
     * 新增合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    public int insertBaseOrder(BaseOrder baseOrder);

    /**
     * 修改合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    public int updateBaseOrder(BaseOrder baseOrder);

    /**
     * 删除合同订单
     *
     * @param id 合同订单主键
     * @return 结果
     */
    public int deleteBaseOrderById(Long id);

    /**
     * 批量删除合同订单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseOrderByIds(Long[] ids);

    List<BaseOrder> selectCompanyOrderList(BaseOrder baseOrder);

    BaseOrder selectBaseOrderByOrderCode(@Param("orderCode") String orderCode);

    BaseOrder selectBaseOrderByTradeCode(@Param("tradeCode") String tradeCode);
}

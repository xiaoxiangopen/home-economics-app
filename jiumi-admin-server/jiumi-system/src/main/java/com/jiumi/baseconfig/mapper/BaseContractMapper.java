package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseContract;
import org.apache.ibatis.annotations.Param;

/**
 * 合同管理Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseContractMapper
{
    /**
     * 查询合同管理
     *
     * @param id 合同管理主键
     * @return 合同管理
     */
    public BaseContract selectBaseContractById(Long id);

    /**
     * 查询合同管理列表
     *
     * @param baseContract 合同管理
     * @return 合同管理集合
     */
    public List<BaseContract> selectBaseContractList(BaseContract baseContract);

    /**
     * 新增合同管理
     *
     * @param baseContract 合同管理
     * @return 结果
     */
    public int insertBaseContract(BaseContract baseContract);

    /**
     * 修改合同管理
     *
     * @param baseContract 合同管理
     * @return 结果
     */
    public int updateBaseContract(BaseContract baseContract);

    /**
     * 删除合同管理
     *
     * @param id 合同管理主键
     * @return 结果
     */
    public int deleteBaseContractById(Long id);

    /**
     * 批量删除合同管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseContractByIds(Long[] ids);

    List selectUserContractList(BaseContract contract);

    List selectCompanyContractList(BaseContract baseContract);

    List<BaseContract> selectEffectContractList();

    BaseContract selectBaseContractByCode(@Param("contractCode") String contractCode);

    List selectAgencyContractList(BaseContract contract);

    List<BaseContract> selectContractListByDate(@Param("dateStr") String dateStr);
}

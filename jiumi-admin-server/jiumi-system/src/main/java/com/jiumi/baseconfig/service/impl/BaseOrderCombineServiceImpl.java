package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseOrderCombineMapper;
import com.jiumi.baseconfig.domain.BaseOrderCombine;
import com.jiumi.baseconfig.service.IBaseOrderCombineService;

/**
 * 合单记录Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseOrderCombineServiceImpl implements IBaseOrderCombineService
{
    @Autowired
    private BaseOrderCombineMapper baseOrderCombineMapper;

    /**
     * 查询合单记录
     *
     * @param id 合单记录主键
     * @return 合单记录
     */
    @Override
    public BaseOrderCombine selectBaseOrderCombineById(Long id)
    {
        return baseOrderCombineMapper.selectBaseOrderCombineById(id);
    }

    /**
     * 查询合单记录列表
     *
     * @param baseOrderCombine 合单记录
     * @return 合单记录
     */
    @Override
    public List<BaseOrderCombine> selectBaseOrderCombineList(BaseOrderCombine baseOrderCombine)
    {
        return baseOrderCombineMapper.selectBaseOrderCombineList(baseOrderCombine);
    }

    /**
     * 新增合单记录
     *
     * @param baseOrderCombine 合单记录
     * @return 结果
     */
    @Override
    public int insertBaseOrderCombine(BaseOrderCombine baseOrderCombine)
    {
        baseOrderCombine.setCreateTime(DateUtils.getNowDate());
        return baseOrderCombineMapper.insertBaseOrderCombine(baseOrderCombine);
    }

    /**
     * 修改合单记录
     *
     * @param baseOrderCombine 合单记录
     * @return 结果
     */
    @Override
    public int updateBaseOrderCombine(BaseOrderCombine baseOrderCombine)
    {
        baseOrderCombine.setUpdateTime(DateUtils.getNowDate());
        return baseOrderCombineMapper.updateBaseOrderCombine(baseOrderCombine);
    }

    /**
     * 批量删除合单记录
     *
     * @param ids 需要删除的合单记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderCombineByIds(Long[] ids)
    {
        return baseOrderCombineMapper.deleteBaseOrderCombineByIds(ids);
    }

    /**
     * 删除合单记录信息
     *
     * @param id 合单记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderCombineById(Long id)
    {
        return baseOrderCombineMapper.deleteBaseOrderCombineById(id);
    }

    @Override
    public List selectUserOrderCombineList(BaseOrderCombine baseOrderCombine) {
        return baseOrderCombineMapper.selectUserOrderCombineList(baseOrderCombine);
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.jiumi.baseconfig.mapper.BusinessConfigMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BusinessConfig;
import com.jiumi.baseconfig.service.IBusinessConfigService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 业务设置Controller
 *
 * @author jiumi
 * @date 2022-12-07
 */
@RestController
@RequestMapping("/baseconfig/BusinessConfig")
public class BusinessConfigController extends BaseController
{
    @Autowired
    private IBusinessConfigService businessConfigService;



    /**
     * 查询业务设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusinessConfig businessConfig)
    {
        startPage();
        List<BusinessConfig> list = businessConfigService.selectBusinessConfigList(businessConfig);
        return getDataTable(list);
    }

    /**
     * 导出业务设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:export')")
    @Log(title = "业务设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessConfig businessConfig)
    {
        List<BusinessConfig> list = businessConfigService.selectBusinessConfigList(businessConfig);
        ExcelUtil<BusinessConfig> util = new ExcelUtil<BusinessConfig>(BusinessConfig.class);
        util.exportExcel(response, list, "业务设置数据");
    }

    /**
     * 获取业务设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(businessConfigService.selectBusinessConfigById(id));
    }

    /**
     * 新增业务设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:add')")
    @Log(title = "业务设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusinessConfig businessConfig)
    {
        return toAjax(businessConfigService.insertBusinessConfig(businessConfig));
    }

    /**
     * 修改业务设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:edit')")
    @Log(title = "业务设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusinessConfig businessConfig)
    {
        return toAjax(businessConfigService.updateBusinessConfig(businessConfig));
    }

    /**
     * 删除业务设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:BusinessConfig:remove')")
    @Log(title = "业务设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessConfigService.deleteBusinessConfigByIds(ids));
    }
}

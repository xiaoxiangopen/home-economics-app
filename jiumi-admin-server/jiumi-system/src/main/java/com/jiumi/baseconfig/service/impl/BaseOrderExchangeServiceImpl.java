package com.jiumi.baseconfig.service.impl;

import java.util.List;

import com.jiumi.baseconfig.domain.BaseGoods;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.baseconfig.service.IScoreHistoryService;
import com.jiumi.common.core.domain.entity.SysUser;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.uuid.Seq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseOrderExchangeMapper;
import com.jiumi.baseconfig.domain.BaseOrderExchange;
import com.jiumi.baseconfig.service.IBaseOrderExchangeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户兑换奖品记录Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseOrderExchangeServiceImpl implements IBaseOrderExchangeService
{
    @Autowired
    private BaseOrderExchangeMapper baseOrderExchangeMapper;

    @Autowired
    private BaseUserMapper baseUserMapper;

    @Autowired
    private IScoreHistoryService scoreHistoryService;

    /**
     * 查询用户兑换奖品记录
     *
     * @param id 用户兑换奖品记录主键
     * @return 用户兑换奖品记录
     */
    @Override
    public BaseOrderExchange selectBaseOrderExchangeById(Long id)
    {
        return baseOrderExchangeMapper.selectBaseOrderExchangeById(id);
    }

    /**
     * 查询用户兑换奖品记录列表
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 用户兑换奖品记录
     */
    @Override
    public List<BaseOrderExchange> selectBaseOrderExchangeList(BaseOrderExchange baseOrderExchange)
    {
        return baseOrderExchangeMapper.selectBaseOrderExchangeList(baseOrderExchange);
    }

    /**
     * 新增用户兑换奖品记录
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    @Override
    public int insertBaseOrderExchange(BaseOrderExchange baseOrderExchange)
    {
        baseOrderExchange.setCreateTime(DateUtils.getNowDate());
        return baseOrderExchangeMapper.insertBaseOrderExchange(baseOrderExchange);
    }

    /**
     * 修改用户兑换奖品记录
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    @Override
    public int updateBaseOrderExchange(BaseOrderExchange baseOrderExchange)
    {
        baseOrderExchange.setUpdateTime(DateUtils.getNowDate());
        return baseOrderExchangeMapper.updateBaseOrderExchange(baseOrderExchange);
    }

    /**
     * 批量删除用户兑换奖品记录
     *
     * @param ids 需要删除的用户兑换奖品记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderExchangeByIds(Long[] ids)
    {
        return baseOrderExchangeMapper.deleteBaseOrderExchangeByIds(ids);
    }

    /**
     * 删除用户兑换奖品记录信息
     *
     * @param id 用户兑换奖品记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderExchangeById(Long id)
    {
        return baseOrderExchangeMapper.deleteBaseOrderExchangeById(id);
    }

    @Transactional
    @Override
    public int userExchangeGoods(BaseGoods goods,int goodsNum, BaseUser currentUser, String userName, String phone, String address) {
        //增加记录
        int orderAmount=goods.getPrice().intValue()*goodsNum;
        BaseOrderExchange exchange=new BaseOrderExchange();
        exchange.setOrderCode(Seq.getId());
        exchange.setUserId(currentUser.getUserId());
        exchange.setUserName(currentUser.getUserName());
        exchange.setPhone(currentUser.getPhonenumber());
        exchange.setOrderAmount(Long.valueOf(orderAmount));
        exchange.setGoodsNum(Long.valueOf(goodsNum));
        exchange.setGoodsId(Long.valueOf(goods.getId()));
        exchange.setExchangeTime(DateUtils.getNowDate());
        exchange.setAddress(address);
        exchange.setStatus("01");
        exchange.setCreateBy(currentUser.getUserName());
        exchange.setCreateTime(DateUtils.getNowDate());
        int result=baseOrderExchangeMapper.insertBaseOrderExchange(exchange);
        //减用户积分
        if(result>0){
            BaseUser u=new BaseUser();
            u.setUpdateBy(currentUser.getUserName());
            u.setUpdateTime(DateUtils.getNowDate());
            u.setScoreAmount(Long.valueOf(orderAmount));
            u.setUserId(currentUser.getUserId());
            baseUserMapper.updateUserScore(u);

            //增加使用记录
            scoreHistoryService.useScore(currentUser.getUserId(),currentUser.getUserName(),"积分兑换",Long.valueOf(orderAmount));
        }


        return result;
    }
}

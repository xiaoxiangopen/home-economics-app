package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 积分商品对象 base_goods
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 奖品名称 */
    @Excel(name = "奖品名称")
    private String goodsName;

    /** 规格描述 */
    @Excel(name = "规格描述")
    private String skuName;

    /** 积分 */
    @Excel(name = "积分")
    private Long price;

    /** 封面图片 */
    @Excel(name = "封面图片")
    private String coverImage;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortNo;

    /** 库存 */
    @Excel(name = "库存")
    private Long stockNum;

    /** 状态 */
    @Excel(name = "状态",dictType = "base_goods_status")
    private String status;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setSkuName(String skuName)
    {
        this.skuName = skuName;
    }

    public String getSkuName()
    {
        return skuName;
    }
    public void setPrice(Long price)
    {
        this.price = price;
    }

    public Long getPrice()
    {
        return price;
    }
    public void setCoverImage(String coverImage)
    {
        this.coverImage = coverImage;
    }

    public String getCoverImage()
    {
        return coverImage;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage()
    {
        return image;
    }
    public void setSortNo(Long sortNo)
    {
        this.sortNo = sortNo;
    }

    public Long getSortNo()
    {
        return sortNo;
    }
    public void setStockNum(Long stockNum)
    {
        this.stockNum = stockNum;
    }

    public Long getStockNum()
    {
        return stockNum;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsName", getGoodsName())
            .append("skuName", getSkuName())
            .append("price", getPrice())
            .append("coverImage", getCoverImage())
            .append("image", getImage())
            .append("sortNo", getSortNo())
            .append("stockNum", getStockNum())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}

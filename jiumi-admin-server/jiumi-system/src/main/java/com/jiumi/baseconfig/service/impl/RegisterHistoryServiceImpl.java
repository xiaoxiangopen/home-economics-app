package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.RegisterHistoryMapper;
import com.jiumi.baseconfig.domain.RegisterHistory;
import com.jiumi.baseconfig.service.IRegisterHistoryService;

/**
 * 用户记录Service业务层处理
 *
 * @author jiumi
 * @date 2023-09-04
 */
@Service
public class RegisterHistoryServiceImpl implements IRegisterHistoryService
{
    @Autowired
    private RegisterHistoryMapper registerHistoryMapper;

    /**
     * 查询用户记录
     *
     * @param id 用户记录主键
     * @return 用户记录
     */
    @Override
    public RegisterHistory selectRegisterHistoryById(Long id)
    {
        return registerHistoryMapper.selectRegisterHistoryById(id);
    }

    /**
     * 查询用户记录列表
     *
     * @param registerHistory 用户记录
     * @return 用户记录
     */
    @Override
    public List<RegisterHistory> selectRegisterHistoryList(RegisterHistory registerHistory)
    {
        return registerHistoryMapper.selectRegisterHistoryList(registerHistory);
    }

    /**
     * 新增用户记录
     *
     * @param registerHistory 用户记录
     * @return 结果
     */
    @Override
    public int insertRegisterHistory(RegisterHistory registerHistory)
    {
        registerHistory.setCreateTime(DateUtils.getNowDate());
        return registerHistoryMapper.insertRegisterHistory(registerHistory);
    }

    /**
     * 修改用户记录
     *
     * @param registerHistory 用户记录
     * @return 结果
     */
    @Override
    public int updateRegisterHistory(RegisterHistory registerHistory)
    {
        return registerHistoryMapper.updateRegisterHistory(registerHistory);
    }

    /**
     * 批量删除用户记录
     *
     * @param ids 需要删除的用户记录主键
     * @return 结果
     */
    @Override
    public int deleteRegisterHistoryByIds(Long[] ids)
    {
        return registerHistoryMapper.deleteRegisterHistoryByIds(ids);
    }

    /**
     * 删除用户记录信息
     *
     * @param id 用户记录主键
     * @return 结果
     */
    @Override
    public int deleteRegisterHistoryById(Long id)
    {
        return registerHistoryMapper.deleteRegisterHistoryById(id);
    }
}

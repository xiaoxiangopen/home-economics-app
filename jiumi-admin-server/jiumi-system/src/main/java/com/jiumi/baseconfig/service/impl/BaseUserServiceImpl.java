package com.jiumi.baseconfig.service.impl;

import java.util.List;

import com.jiumi.baseconfig.domain.BusinessConfig;
import com.jiumi.baseconfig.mapper.BusinessConfigMapper;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.service.IBaseUserService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户信息Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserServiceImpl implements IBaseUserService
{
    @Autowired
    private BaseUserMapper baseUserMapper;

    @Autowired
    private BusinessConfigMapper businessConfigMapper;

    /**
     * 查询用户信息
     *
     * @param userId 用户信息主键
     * @return 用户信息
     */
    @Override
    public BaseUser selectBaseUserByUserId(Long userId)
    {
        return baseUserMapper.selectBaseUserByUserId(userId);
    }

    /**
     * 查询用户信息列表
     *
     * @param baseUser 用户信息
     * @return 用户信息
     */
    @Override
    public List<BaseUser> selectBaseUserList(BaseUser baseUser)
    {
        List<BaseUser> userList= baseUserMapper.selectBaseUserList(baseUser);
        return userList;
    }

    /**
     * 新增用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    @Override
    public int insertBaseUser(BaseUser baseUser)
    {
        baseUser.setCreateTime(DateUtils.getNowDate());
        return baseUserMapper.insertBaseUser(baseUser);
    }

    /**
     * 修改用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    @Override
    public int updateBaseUser(BaseUser baseUser)
    {
        baseUser.setUpdateTime(DateUtils.getNowDate());
        return baseUserMapper.updateBaseUser(baseUser);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserByUserIds(Long[] userIds)
    {
        return baseUserMapper.deleteBaseUserByUserIds(userIds);
    }

    /**
     * 删除用户信息信息
     *
     * @param userId 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserByUserId(Long userId)
    {
        return baseUserMapper.deleteBaseUserByUserId(userId);
    }

    @Override
    public BaseUser selectUserByUserName(String username) {
        return baseUserMapper.selectUserByUserName(username);
    }

    @Override
    public List<BaseUser> queryUserData(BaseUser user) {
        return baseUserMapper.queryUserData(user);
    }

    @Override
    public int resetUserPwdByPhone(String userName, String password) {
        return baseUserMapper.resetUserPwdByPhone(userName,password);
    }

    @Override
    public List selectCompanyUserList(BaseUser baseUser) {
        return baseUserMapper.selectCompanyUserList(baseUser);
    }

    @Override
    public List selectCompanyEpmloyList(BaseUser baseUser) {
        return baseUserMapper.selectCompanyEpmloyList(baseUser);
    }

    @Override
    public List selectCompanyAuntList(BaseUser baseUser) {
        return baseUserMapper.selectCompanyAuntList(baseUser);
    }

    @Override
    public List<BaseUser> selectCompanyAccountList(BaseUser param) {
        return baseUserMapper.selectCompanyAccountList(param);
    }

    @Override
    public BaseUser selectUserByUserPhone(String phonenumber) {
        return baseUserMapper.selectUserByUserPhone(phonenumber);
    }

    @Override
    public BaseUser selectUserByReferrerCode(String referrerCode) {
        return baseUserMapper.selectUserByReferrerCode(referrerCode);
    }

    @Override
    public int updateAuthUser(BaseUser baseUser) {
        return baseUserMapper.updateAuthUser(baseUser);
    }

    @Override
    public BaseUser selectUserByUserCertCode(String certCode) {
        return baseUserMapper.selectUserByUserCertCode(certCode);
    }

    @Override
    public List<BaseUser> selectUserFocusList(BaseUser param) {
        return baseUserMapper.selectUserFocusList(param);
    }

    @Override
    public List<BaseUser> selectUserInviteList(BaseUser userInfo) {
        return baseUserMapper.selectUserInviteList(userInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoSetUserUnClick() {
        BaseUser param=new BaseUser();
        param.setUserType("04");
        param.setClickStatus("02");
        List<BaseUser> userList= baseUserMapper.selectBaseUserList(param);
        BusinessConfig config=businessConfigMapper.selectBusinessConfigById(2L);
        Long effectTime=Long.valueOf(config.getValue());
        userList.stream().forEach(user->{
            Long clickTime=user.getClickTime()!=null?user.getClickTime().getTime():0L;
            Long diffmin=(DateUtils.getNowDate().getTime()-clickTime)/1000/60;
            if(diffmin>effectTime*60){
                BaseUser newUser=new BaseUser();
                newUser.setUserId(user.getUserId());
                newUser.setClickStatus("01");
                newUser.setStatus("02");
                newUser.setUpdateTime(DateUtils.getNowDate());
                baseUserMapper.updateBaseUser(newUser);
            }
        });
    }

    @Override
    public int updateBaseUserScore(BaseUser referrerUser) {
        referrerUser.setUpdateTime(DateUtils.getNowDate());
        return baseUserMapper.updateBaseUserScore(referrerUser);
    }

    @Override
    public List<BaseUser> selectStallUserListList(BaseUser baseUser) {
        return baseUserMapper.selectStallUserListList(baseUser);
    }

    @Override
    public List selectContractAuntList(BaseUser baseUser) {
        return baseUserMapper.selectContractAuntList(baseUser);
    }

    @Override
    public List selectContractEmployList(BaseUser baseUser) {
        return baseUserMapper.selectContractEmployList(baseUser);
    }

    @Override
    public List<BaseUser> selectBaseUserByAuthUserId(Long userId) {
        return baseUserMapper.selectBaseUserByAuthUserId(userId);
    }

    @Override
    public List selectQueryCompanyEpmloyList(BaseUser baseUser) {
        return baseUserMapper.selectQueryCompanyEpmloyList(baseUser);
    }

    @Override
    public List<BaseUser> queryUserInfo(BaseUser user) {
        return baseUserMapper.selectContractEmployList(user);
    }

}

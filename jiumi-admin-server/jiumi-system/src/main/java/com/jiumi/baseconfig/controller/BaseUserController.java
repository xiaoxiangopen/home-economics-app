package com.jiumi.baseconfig.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.jiumi.baseconfig.domain.BaseAunt;
import com.jiumi.baseconfig.domain.BaseEmployee;
import com.jiumi.baseconfig.domain.BaseWorker;
import com.jiumi.common.utils.bean.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.service.IBaseUserService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户信息Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/userinfo")
public class BaseUserController extends BaseController
{
    @Autowired
    private IBaseUserService baseUserService;

    /**
     * 查询用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUser baseUser)
    {
        startPage();
        List<BaseUser> list = baseUserService.selectBaseUserList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/getStallUserList")
    public TableDataInfo getStallUserList(BaseUser baseUser)
    {
        startPage();
        List<BaseUser> list = baseUserService.selectStallUserListList(baseUser);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:export')")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUser baseUser)
    {
        List<BaseUser> list = baseUserService.selectBaseUserList(baseUser);
        ExcelUtil<BaseUser> util = new ExcelUtil<BaseUser>(BaseUser.class);
        util.exportExcel(response, list, "用户信息数据");
    }

    @Log(title = "认证雇主导出信息", businessType = BusinessType.EXPORT)
    @PostMapping("/employeeExport")
    public void employeeExport(HttpServletResponse response, BaseUser baseUser)
    {
        List<BaseUser> list = baseUserService.selectBaseUserList(baseUser);
        List<BaseEmployee> employList=new ArrayList<>();
        list.stream().forEach(data->{
            BaseEmployee empyee=new BaseEmployee();
            BeanUtils.copyBeanProp(empyee,data);
            employList.add(empyee);
        });
        ExcelUtil<BaseEmployee> util = new ExcelUtil<BaseEmployee>(BaseEmployee.class);
        util.exportExcel(response, employList, "认证雇主");
    }

    @Log(title = "认证阿姨导出信息", businessType = BusinessType.EXPORT)
    @PostMapping("/auntExport")
    public void auntExport(HttpServletResponse response, BaseUser baseUser)
    {
        List<BaseUser> list = baseUserService.selectBaseUserList(baseUser);
        List<BaseAunt> employList=new ArrayList<>();
        list.stream().forEach(data->{
            BaseAunt aunt=new BaseAunt();
            BeanUtils.copyBeanProp(aunt,data);
            employList.add(aunt);
        });
        ExcelUtil<BaseAunt> util = new ExcelUtil<BaseAunt>(BaseAunt.class);
        util.exportExcel(response, employList, "认证阿姨");
    }

    @Log(title = "员工信息导出", businessType = BusinessType.EXPORT)
    @PostMapping("/workerExport")
    public void workerExport(HttpServletResponse response, BaseUser baseUser)
    {
        List<BaseUser> list = baseUserService.selectStallUserListList(baseUser);
        List<BaseWorker> employList=new ArrayList<>();
        list.stream().forEach(data->{
            BaseWorker aunt=new BaseWorker();
            BeanUtils.copyBeanProp(aunt,data);
            employList.add(aunt);
        });
        ExcelUtil<BaseWorker> util = new ExcelUtil<BaseWorker>(BaseWorker.class);
        util.exportExcel(response, employList, "员工信息");
    }

    /**
     * 获取用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return success(baseUserService.selectBaseUserByUserId(userId));
    }

    /**
     * 新增用户信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:add')")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUser baseUser)
    {
        return toAjax(baseUserService.insertBaseUser(baseUser));
    }

    /**
     * 修改用户信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:edit')")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUser baseUser)
    {
        return toAjax(baseUserService.updateBaseUser(baseUser));
    }

    /**
     * 删除用户信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:userinfo:remove')")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(baseUserService.deleteBaseUserByUserIds(userIds));
    }
}

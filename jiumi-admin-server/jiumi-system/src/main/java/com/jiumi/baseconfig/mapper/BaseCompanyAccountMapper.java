package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCompanyAccount;

/**
 * 公司招聘人员Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseCompanyAccountMapper
{
    /**
     * 查询公司招聘人员
     *
     * @param id 公司招聘人员主键
     * @return 公司招聘人员
     */
    public BaseCompanyAccount selectBaseCompanyAccountById(Long id);

    /**
     * 查询公司招聘人员列表
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 公司招聘人员集合
     */
    public List<BaseCompanyAccount> selectBaseCompanyAccountList(BaseCompanyAccount baseCompanyAccount);

    /**
     * 新增公司招聘人员
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 结果
     */
    public int insertBaseCompanyAccount(BaseCompanyAccount baseCompanyAccount);

    /**
     * 修改公司招聘人员
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 结果
     */
    public int updateBaseCompanyAccount(BaseCompanyAccount baseCompanyAccount);

    /**
     * 删除公司招聘人员
     *
     * @param id 公司招聘人员主键
     * @return 结果
     */
    public int deleteBaseCompanyAccountById(Long id);

    /**
     * 批量删除公司招聘人员
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseCompanyAccountByIds(Long[] ids);

    void deleteBaseCompanyAccountByCompanyId(Long id);
}

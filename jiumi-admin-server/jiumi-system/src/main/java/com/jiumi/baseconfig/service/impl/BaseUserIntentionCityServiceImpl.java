package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserIntentionCityMapper;
import com.jiumi.baseconfig.domain.BaseUserIntentionCity;
import com.jiumi.baseconfig.service.IBaseUserIntentionCityService;

/**
 * 求职意向城市Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserIntentionCityServiceImpl implements IBaseUserIntentionCityService
{
    @Autowired
    private BaseUserIntentionCityMapper baseUserIntentionCityMapper;

    /**
     * 查询求职意向城市
     *
     * @param id 求职意向城市主键
     * @return 求职意向城市
     */
    @Override
    public BaseUserIntentionCity selectBaseUserIntentionCityById(Long id)
    {
        return baseUserIntentionCityMapper.selectBaseUserIntentionCityById(id);
    }

    /**
     * 查询求职意向城市列表
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 求职意向城市
     */
    @Override
    public List<BaseUserIntentionCity> selectBaseUserIntentionCityList(BaseUserIntentionCity baseUserIntentionCity)
    {
        return baseUserIntentionCityMapper.selectBaseUserIntentionCityList(baseUserIntentionCity);
    }

    /**
     * 新增求职意向城市
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 结果
     */
    @Override
    public int insertBaseUserIntentionCity(BaseUserIntentionCity baseUserIntentionCity)
    {
        return baseUserIntentionCityMapper.insertBaseUserIntentionCity(baseUserIntentionCity);
    }

    /**
     * 修改求职意向城市
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 结果
     */
    @Override
    public int updateBaseUserIntentionCity(BaseUserIntentionCity baseUserIntentionCity)
    {
        return baseUserIntentionCityMapper.updateBaseUserIntentionCity(baseUserIntentionCity);
    }

    /**
     * 批量删除求职意向城市
     *
     * @param ids 需要删除的求职意向城市主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserIntentionCityByIds(Long[] ids)
    {
        return baseUserIntentionCityMapper.deleteBaseUserIntentionCityByIds(ids);
    }

    /**
     * 删除求职意向城市信息
     *
     * @param id 求职意向城市主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserIntentionCityById(Long id)
    {
        return baseUserIntentionCityMapper.deleteBaseUserIntentionCityById(id);
    }

    @Override
    public void deleteBaseUserIntentionCityByIntentionId(Long id) {
        baseUserIntentionCityMapper.deleteBaseUserIntentionCityByIntentionId(id);
    }
}

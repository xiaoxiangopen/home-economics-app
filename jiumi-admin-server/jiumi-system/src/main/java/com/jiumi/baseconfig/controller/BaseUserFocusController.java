package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserFocus;
import com.jiumi.baseconfig.service.IBaseUserFocusService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户关注Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/focus")
public class BaseUserFocusController extends BaseController
{
    @Autowired
    private IBaseUserFocusService baseUserFocusService;

    /**
     * 查询用户关注列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserFocus baseUserFocus)
    {
        startPage();
        List<BaseUserFocus> list = baseUserFocusService.selectBaseUserFocusList(baseUserFocus);
        return getDataTable(list);
    }

    /**
     * 导出用户关注列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:export')")
    @Log(title = "用户关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserFocus baseUserFocus)
    {
        List<BaseUserFocus> list = baseUserFocusService.selectBaseUserFocusList(baseUserFocus);
        ExcelUtil<BaseUserFocus> util = new ExcelUtil<BaseUserFocus>(BaseUserFocus.class);
        util.exportExcel(response, list, "用户关注数据");
    }

    /**
     * 获取用户关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserFocusService.selectBaseUserFocusById(id));
    }

    /**
     * 新增用户关注
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:add')")
    @Log(title = "用户关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserFocus baseUserFocus)
    {
        return toAjax(baseUserFocusService.insertBaseUserFocus(baseUserFocus));
    }

    /**
     * 修改用户关注
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:edit')")
    @Log(title = "用户关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserFocus baseUserFocus)
    {
        return toAjax(baseUserFocusService.updateBaseUserFocus(baseUserFocus));
    }

    /**
     * 删除用户关注
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:focus:remove')")
    @Log(title = "用户关注", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserFocusService.deleteBaseUserFocusByIds(ids));
    }
}

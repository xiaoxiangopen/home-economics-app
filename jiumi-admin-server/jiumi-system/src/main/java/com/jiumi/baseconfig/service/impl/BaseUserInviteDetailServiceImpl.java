package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserInviteDetailMapper;
import com.jiumi.baseconfig.domain.BaseUserInviteDetail;
import com.jiumi.baseconfig.service.IBaseUserInviteDetailService;

/**
 * 用户邀请Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserInviteDetailServiceImpl implements IBaseUserInviteDetailService
{
    @Autowired
    private BaseUserInviteDetailMapper baseUserInviteDetailMapper;

    /**
     * 查询用户邀请
     *
     * @param id 用户邀请主键
     * @return 用户邀请
     */
    @Override
    public BaseUserInviteDetail selectBaseUserInviteDetailById(Long id)
    {
        return baseUserInviteDetailMapper.selectBaseUserInviteDetailById(id);
    }

    /**
     * 查询用户邀请列表
     *
     * @param baseUserInviteDetail 用户邀请
     * @return 用户邀请
     */
    @Override
    public List<BaseUserInviteDetail> selectBaseUserInviteDetailList(BaseUserInviteDetail baseUserInviteDetail)
    {
        return baseUserInviteDetailMapper.selectBaseUserInviteDetailList(baseUserInviteDetail);
    }

    /**
     * 新增用户邀请
     *
     * @param baseUserInviteDetail 用户邀请
     * @return 结果
     */
    @Override
    public int insertBaseUserInviteDetail(BaseUserInviteDetail baseUserInviteDetail)
    {
        baseUserInviteDetail.setCreateTime(DateUtils.getNowDate());
        return baseUserInviteDetailMapper.insertBaseUserInviteDetail(baseUserInviteDetail);
    }

    /**
     * 修改用户邀请
     *
     * @param baseUserInviteDetail 用户邀请
     * @return 结果
     */
    @Override
    public int updateBaseUserInviteDetail(BaseUserInviteDetail baseUserInviteDetail)
    {
        return baseUserInviteDetailMapper.updateBaseUserInviteDetail(baseUserInviteDetail);
    }

    /**
     * 批量删除用户邀请
     *
     * @param ids 需要删除的用户邀请主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserInviteDetailByIds(Long[] ids)
    {
        return baseUserInviteDetailMapper.deleteBaseUserInviteDetailByIds(ids);
    }

    /**
     * 删除用户邀请信息
     *
     * @param id 用户邀请主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserInviteDetailById(Long id)
    {
        return baseUserInviteDetailMapper.deleteBaseUserInviteDetailById(id);
    }
}

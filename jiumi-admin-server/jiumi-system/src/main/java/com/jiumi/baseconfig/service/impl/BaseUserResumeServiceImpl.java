package com.jiumi.baseconfig.service.impl;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.domain.BaseUserIntention;
import com.jiumi.baseconfig.mapper.BaseUserIntentionMapper;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.DictUtils;
import com.jiumi.common.utils.SecurityUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.common.utils.uuid.Seq;
import com.jiumi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserResumeMapper;
import com.jiumi.baseconfig.domain.BaseUserResume;
import com.jiumi.baseconfig.service.IBaseUserResumeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 求职简历Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserResumeServiceImpl implements IBaseUserResumeService
{
    @Autowired
    private BaseUserResumeMapper baseUserResumeMapper;

    @Autowired
    private BaseUserMapper baseUserMapper;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private BaseUserIntentionMapper baseUserIntentionMapper;

    /**
     * 查询求职简历
     *
     * @param id 求职简历主键
     * @return 求职简历
     */
    @Override
    public BaseUserResume selectBaseUserResumeById(Long id)
    {
        return baseUserResumeMapper.selectBaseUserResumeById(id);
    }

    /**
     * 查询求职简历列表
     *
     * @param baseUserResume 求职简历
     * @return 求职简历
     */
    @Override
    public List<BaseUserResume> selectBaseUserResumeList(BaseUserResume baseUserResume)
    {
        return baseUserResumeMapper.selectBaseUserResumeList(baseUserResume);
    }

    /**
     * 新增求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    @Override
    public int insertBaseUserResume(BaseUserResume baseUserResume)
    {
        baseUserResume.setCreateTime(DateUtils.getNowDate());
        return baseUserResumeMapper.insertBaseUserResume(baseUserResume);
    }

    /**
     * 修改求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    @Override
    public int updateBaseUserResume(BaseUserResume baseUserResume)
    {
        baseUserResume.setUpdateTime(DateUtils.getNowDate());
        return baseUserResumeMapper.updateBaseUserResume(baseUserResume);
    }

    /**
     * 批量删除求职简历
     *
     * @param ids 需要删除的求职简历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeByIds(Long[] ids)
    {
        return baseUserResumeMapper.deleteBaseUserResumeByIds(ids);
    }

    /**
     * 删除求职简历信息
     *
     * @param id 求职简历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeById(Long id)
    {
        return baseUserResumeMapper.deleteBaseUserResumeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveBaseUserResume(BaseUserResume userResume) {
        BaseUser user=new BaseUser();
        user.setUserId(userResume.getUserId());
        user.setNickName(userResume.getNickName());
        user.setPhonenumber(userResume.getPhonenumber());
        user.setAvatar(userResume.getAvatar());
        user.setAuthName(userResume.getAuthName());
        user.setSex(userResume.getSex());
        user.setBirthDate(userResume.getBirthDate());
        user.setZodiac(userResume.getZodiac());
        user.setConstellation(userResume.getConstellation());
        user.setNation(userResume.getNation());
        user.setNativePlace(userResume.getNativePlace());
        user.setCertCode(userResume.getCertCode());
        user.setCertEndDate(userResume.getCertEndDate());
        user.setCertImage1(userResume.getCertImage1());
        user.setCertImage2(userResume.getCertImage2());
        user.setUpdateTime(DateUtils.getNowDate());
        user.setClickStatus("01");
        user.setClickTime(DateUtils.getNowDate());
        if(userResume.getIntention()!=null && userResume.getIntroduction()!=null){
            user.setStatus("01");
            user.setClickStatus("02");
            user.setClickTime(DateUtils.getNowDate());
        }
        if(StringUtils.isNotEmpty(userResume.getAuthName()) && StringUtils.isNotEmpty(userResume.getCertCode())){
            //user.setAuthStatus("01");
            user.setAuthType("03");
        }
        int result=baseUserMapper.updateBaseUser(user);

        BaseUserResume param=new BaseUserResume();
        param.setUserId(userResume.getUserId());
        List<BaseUserResume> resumeList= baseUserResumeMapper.selectBaseUserResumeList(param);
        if(resumeList.size()>0){
            baseUserResumeMapper.updateBaseUserResume(userResume);
        }else{
            baseUserResumeMapper.insertBaseUserResume(userResume);
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseUserResume saveBaseUserResumeAuth(BaseUserResume userResume,BaseUser currentUser) {
        BaseUser user=new BaseUser();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        user.setPassword(SecurityUtils.encryptPassword(password));
        user.setNickName(StringUtils.isEmpty(userResume.getAuthName())?userResume.getNickName():userResume.getAuthName());
        String userName="temp_"+ Seq.getId();
        user.setUserName(userName);
        user.setPhonenumber(null);
        user.setAvatar(userResume.getAvatar());
        user.setAuthName(userResume.getAuthName());
        user.setSex(userResume.getSex());
        user.setBirthDate(userResume.getBirthDate());
        user.setZodiac(userResume.getZodiac());
        user.setConstellation(userResume.getConstellation());
        user.setNation(userResume.getNation());
        user.setNativePlace(userResume.getNativePlace());
        user.setCertCode(userResume.getCertCode());
        user.setCertEndDate(userResume.getCertEndDate());
        user.setCertImage1(userResume.getCertImage1());
        user.setCertImage2(userResume.getCertImage2());
        //user.setInviteCode(currentUser.getReferrerCode());
        user.setAuthUserId(currentUser.getUserId());
        user.setPublishType("02");
        user.setPublishUserId(currentUser.getUserId());
        user.setCompanyId(currentUser.getCompanyId());
        user.setCompanyName(currentUser.getCompanyName());
        user.setCreateBy(currentUser.getUserName());
        user.setCreateTime(DateUtils.getNowDate());
        user.setUserType("04");
        user.setStatus("02");
        user.setClickStatus("01");
        user.setClickTime(DateUtils.getNowDate());
        if(userResume.getIntention()!=null && userResume.getIntroduction()!=null){
            user.setStatus("01");
            user.setClickStatus("02");
            user.setClickTime(DateUtils.getNowDate());
        }
        if(StringUtils.isNotEmpty(userResume.getCertCode())){
            //user.setAuthStatus("01");
            user.setAuthTime(DateUtils.getNowDate());
            user.setAuthType("03");
        }
        int result=baseUserMapper.insertBaseUser(user);
        userResume.setUserId(user.getUserId());

        BaseUserIntention intention=new BaseUserIntention();
        intention.setUserId(user.getUserId());
        intention.setSignType("01");
        baseUserIntentionMapper.insertBaseUserIntention(intention);

        userResume.setContactPhone(userResume.getContactPhone());
        BaseUserResume param=new BaseUserResume();
        param.setCertCode(userResume.getCertCode());
        List<BaseUserResume> resumeList= baseUserResumeMapper.selectBaseUserResumeList(param);
        if(resumeList.size()>0){
            userResume.setId(resumeList.get(0).getId());
            baseUserResumeMapper.updateBaseUserResume(userResume);
        }else{
            baseUserResumeMapper.insertBaseUserResume(userResume);
        }
        return userResume;
    }

    @Override
    public List<BaseUserResume> selectUserFocusResumeList(BaseUserResume param) {
        return baseUserResumeMapper.selectUserFocusResumeList(param);
    }

    @Override
    public int deleteBaseUserResumeByUserId(Long userId) {
        return baseUserResumeMapper.deleteBaseUserResumeByUserId(userId);
    }

    @Override
    public void deleteBaseUserResumeByCertCode(String certCode) {
        baseUserResumeMapper.deleteBaseUserResumeByCertCode(certCode);
    }

    @Override
    public BaseUserResume selectBaseUserResumeByUserCertCode(String certCode) {
        return baseUserResumeMapper.selectBaseUserResumeByUserCertCode(certCode);
    }

    @Override
    public void removeBaseUserResumeById(Long id) {
        baseUserResumeMapper.removeBaseUserResumeById(id);
    }

    @Override
    public int updateBaseUserResumeInfo(BaseUserResume resume) {
        return baseUserResumeMapper.updateBaseUserResumeInfo(resume);
    }


    @Override
    public BaseUserResume selectBaseUserResumeByUserId(Long userId) {
        return baseUserResumeMapper.selectBaseUserResumeByUserId(userId);
    }

    @Override
    public List<BaseUserResume> selectAuntUserList(BaseUserResume user) {
        List<BaseUserResume> auntList= baseUserResumeMapper.selectAuntUserList(user);
        return auntList;
    }


}

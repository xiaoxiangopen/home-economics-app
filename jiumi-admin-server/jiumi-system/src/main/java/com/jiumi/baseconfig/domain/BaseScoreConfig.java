package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 积分奖励设置对象 base_score_config
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseScoreConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 事项名称 */
    @Excel(name = "事项名称")
    private String itemName;

    /** 奖励积分数量 */
    @Excel(name = "奖励积分数量")
    private Long rewardAmount;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemName()
    {
        return itemName;
    }
    public void setRewardAmount(Long rewardAmount)
    {
        this.rewardAmount = rewardAmount;
    }

    public Long getRewardAmount()
    {
        return rewardAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("itemName", getItemName())
            .append("rewardAmount", getRewardAmount())
            .toString();
    }
}

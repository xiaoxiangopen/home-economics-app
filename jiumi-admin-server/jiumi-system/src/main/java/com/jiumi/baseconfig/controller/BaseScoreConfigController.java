package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseScoreConfig;
import com.jiumi.baseconfig.service.IBaseScoreConfigService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 积分奖励设置Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/scoreconfig")
public class BaseScoreConfigController extends BaseController
{
    @Autowired
    private IBaseScoreConfigService baseScoreConfigService;

    /**
     * 查询积分奖励设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseScoreConfig baseScoreConfig)
    {
        startPage();
        List<BaseScoreConfig> list = baseScoreConfigService.selectBaseScoreConfigList(baseScoreConfig);
        return getDataTable(list);
    }

    /**
     * 导出积分奖励设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:export')")
    @Log(title = "积分奖励设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseScoreConfig baseScoreConfig)
    {
        List<BaseScoreConfig> list = baseScoreConfigService.selectBaseScoreConfigList(baseScoreConfig);
        ExcelUtil<BaseScoreConfig> util = new ExcelUtil<BaseScoreConfig>(BaseScoreConfig.class);
        util.exportExcel(response, list, "积分奖励设置数据");
    }

    /**
     * 获取积分奖励设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseScoreConfigService.selectBaseScoreConfigById(id));
    }

    /**
     * 新增积分奖励设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:add')")
    @Log(title = "积分奖励设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseScoreConfig baseScoreConfig)
    {
        return toAjax(baseScoreConfigService.insertBaseScoreConfig(baseScoreConfig));
    }

    /**
     * 修改积分奖励设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:edit')")
    @Log(title = "积分奖励设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseScoreConfig baseScoreConfig)
    {
        return toAjax(baseScoreConfigService.updateBaseScoreConfig(baseScoreConfig));
    }

    /**
     * 删除积分奖励设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:scoreconfig:remove')")
    @Log(title = "积分奖励设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseScoreConfigService.deleteBaseScoreConfigByIds(ids));
    }
}

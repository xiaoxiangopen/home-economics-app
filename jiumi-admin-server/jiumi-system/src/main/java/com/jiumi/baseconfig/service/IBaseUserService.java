package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUser;

/**
 * 用户信息Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseUserService
{
    /**
     * 查询用户信息
     *
     * @param userId 用户信息主键
     * @return 用户信息
     */
    public BaseUser selectBaseUserByUserId(Long userId);

    /**
     * 查询用户信息列表
     *
     * @param baseUser 用户信息
     * @return 用户信息集合
     */
    public List<BaseUser> selectBaseUserList(BaseUser baseUser);

    /**
     * 新增用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    public int insertBaseUser(BaseUser baseUser);

    /**
     * 修改用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    public int updateBaseUser(BaseUser baseUser);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户信息主键集合
     * @return 结果
     */
    public int deleteBaseUserByUserIds(Long[] userIds);

    /**
     * 删除用户信息信息
     *
     * @param userId 用户信息主键
     * @return 结果
     */
    public int deleteBaseUserByUserId(Long userId);

    BaseUser selectUserByUserName(String username);

    List<BaseUser> queryUserData(BaseUser user);

    int resetUserPwdByPhone(String userName, String password);

    List selectCompanyUserList(BaseUser baseUser);

    List selectCompanyEpmloyList(BaseUser baseUser);

    List selectCompanyAuntList(BaseUser baseUser);

    List<BaseUser> selectCompanyAccountList(BaseUser param);

    BaseUser selectUserByUserPhone(String phonenumber);

    BaseUser selectUserByReferrerCode(String referrerCode);

    int updateAuthUser(BaseUser baseUser);

    BaseUser selectUserByUserCertCode(String certCode);

    List<BaseUser> selectUserFocusList(BaseUser param);

    List<BaseUser> selectUserInviteList(BaseUser userInfo);

    void autoSetUserUnClick();

    int updateBaseUserScore(BaseUser referrerUser);

    List<BaseUser> selectStallUserListList(BaseUser baseUser);

    List selectContractAuntList(BaseUser baseUser);

    List selectContractEmployList(BaseUser baseUser);

    List<BaseUser> selectBaseUserByAuthUserId(Long userId);

    List selectQueryCompanyEpmloyList(BaseUser baseUser);

    List<BaseUser> queryUserInfo(BaseUser user);
}

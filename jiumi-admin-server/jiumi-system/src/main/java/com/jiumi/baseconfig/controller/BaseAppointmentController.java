package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.service.IBaseUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseAppointment;
import com.jiumi.baseconfig.service.IBaseAppointmentService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 预约Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/appointment")
public class BaseAppointmentController extends BaseController
{
    @Autowired
    private IBaseAppointmentService baseAppointmentService;

    @Autowired
    private IBaseUserService baseUserService;
    /**
     * 查询预约列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseAppointment baseAppointment)
    {
        startPage();
        List<BaseAppointment> list = baseAppointmentService.selectManageAppointmentList(baseAppointment);
        return getDataTable(list);
    }

    /**
     * 导出预约列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:export')")
    @Log(title = "预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseAppointment baseAppointment)
    {
        List<BaseAppointment> list = baseAppointmentService.selectManageAppointmentList(baseAppointment);
        ExcelUtil<BaseAppointment> util = new ExcelUtil<BaseAppointment>(BaseAppointment.class);
        util.exportExcel(response, list, "预约数据");
    }

    /**
     * 获取预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseAppointmentService.selectBaseAppointmentById(id));
    }

    /**
     * 新增预约
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:add')")
    @Log(title = "预约", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseAppointment baseAppointment)
    {
        return toAjax(baseAppointmentService.insertBaseAppointment(baseAppointment));
    }

    @PostMapping("/authAppointmentUser")
    public AjaxResult authAppointmentUser(@RequestBody BaseAppointment baseAppointment)
    {
        BaseUser userInfo= baseUserService.selectBaseUserByUserId(baseAppointment.getUserId());
        if(userInfo==null){
            return AjaxResult.error("用户信息不存在");
        }
        BaseAppointment appointment=baseAppointmentService.selectBaseAppointmentById(baseAppointment.getId());
        if(appointment==null){
            return AjaxResult.error("预约信息不存在");
        }
        BaseUser publishUser=baseUserService.selectBaseUserByUserId(appointment.getUserId());
        if("03".equals(publishUser.getUserType())){
            publishUser.setCompanyId(userInfo.getCompanyId());
            publishUser.setCompanyName(userInfo.getCompanyName());
            publishUser.setAuthUserId(userInfo.getUserId());
            baseUserService.updateBaseUser(publishUser);
        }else{
            appointment.setUserId(userInfo.getUserId());
            appointment.setUpdateBy(getUsername());
            baseAppointmentService.updateBaseAppointment(appointment);
        }
        return AjaxResult.success("分配成功");
    }

    /**
     * 修改预约
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:edit')")
    @Log(title = "预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseAppointment baseAppointment)
    {
        return toAjax(baseAppointmentService.updateBaseAppointment(baseAppointment));
    }

    /**
     * 删除预约
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:appointment:remove')")
    @Log(title = "预约", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseAppointmentService.deleteBaseAppointmentByIds(ids));
    }
}

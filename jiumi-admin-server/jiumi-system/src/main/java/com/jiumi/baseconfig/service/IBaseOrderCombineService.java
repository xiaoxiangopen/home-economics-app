package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseOrderCombine;

/**
 * 合单记录Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseOrderCombineService
{
    /**
     * 查询合单记录
     *
     * @param id 合单记录主键
     * @return 合单记录
     */
    public BaseOrderCombine selectBaseOrderCombineById(Long id);

    /**
     * 查询合单记录列表
     *
     * @param baseOrderCombine 合单记录
     * @return 合单记录集合
     */
    public List<BaseOrderCombine> selectBaseOrderCombineList(BaseOrderCombine baseOrderCombine);

    /**
     * 新增合单记录
     *
     * @param baseOrderCombine 合单记录
     * @return 结果
     */
    public int insertBaseOrderCombine(BaseOrderCombine baseOrderCombine);

    /**
     * 修改合单记录
     *
     * @param baseOrderCombine 合单记录
     * @return 结果
     */
    public int updateBaseOrderCombine(BaseOrderCombine baseOrderCombine);

    /**
     * 批量删除合单记录
     *
     * @param ids 需要删除的合单记录主键集合
     * @return 结果
     */
    public int deleteBaseOrderCombineByIds(Long[] ids);

    /**
     * 删除合单记录信息
     *
     * @param id 合单记录主键
     * @return 结果
     */
    public int deleteBaseOrderCombineById(Long id);

    List<BaseOrderCombine> selectUserOrderCombineList(BaseOrderCombine baseOrderCombine);
}

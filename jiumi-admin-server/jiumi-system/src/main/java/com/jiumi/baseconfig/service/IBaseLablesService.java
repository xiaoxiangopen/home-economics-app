package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseLables;

/**
 * 标签管理Service接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseLablesService 
{
    /**
     * 查询标签管理
     * 
     * @param id 标签管理主键
     * @return 标签管理
     */
    public BaseLables selectBaseLablesById(Long id);

    /**
     * 查询标签管理列表
     * 
     * @param baseLables 标签管理
     * @return 标签管理集合
     */
    public List<BaseLables> selectBaseLablesList(BaseLables baseLables);

    /**
     * 新增标签管理
     * 
     * @param baseLables 标签管理
     * @return 结果
     */
    public int insertBaseLables(BaseLables baseLables);

    /**
     * 修改标签管理
     * 
     * @param baseLables 标签管理
     * @return 结果
     */
    public int updateBaseLables(BaseLables baseLables);

    /**
     * 批量删除标签管理
     * 
     * @param ids 需要删除的标签管理主键集合
     * @return 结果
     */
    public int deleteBaseLablesByIds(Long[] ids);

    /**
     * 删除标签管理信息
     * 
     * @param id 标签管理主键
     * @return 结果
     */
    public int deleteBaseLablesById(Long id);
}

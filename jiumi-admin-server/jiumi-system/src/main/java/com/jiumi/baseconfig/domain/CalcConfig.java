package com.jiumi.baseconfig.domain;

import java.math.BigDecimal;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 工资计算器对象 base_calc_config
 *
 * @author jiumi
 * @date 2022-12-10
 */
public class CalcConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    private String roleType;

    /** 指标ID */
    @Excel(name = "指标ID")
    private String measureType;

    /** 指标类型 */
    @Excel(name = "指标类型")
    private String measureName;

    /** 选项名称 */
    @Excel(name = "选项名称")
    private String itemName;

    /** 选项因素 */
    @Excel(name = "选项因素")
    private BigDecimal itemValue;

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setMeasureType(String measureType)
    {
        this.measureType = measureType;
    }

    public String getMeasureType()
    {
        return measureType;
    }
    public void setMeasureName(String measureName)
    {
        this.measureName = measureName;
    }

    public String getMeasureName()
    {
        return measureName;
    }
    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemName()
    {
        return itemName;
    }
    public void setItemValue(BigDecimal itemValue)
    {
        this.itemValue = itemValue;
    }

    public BigDecimal getItemValue()
    {
        return itemValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("measureType", getMeasureType())
            .append("measureName", getMeasureName())
            .append("itemName", getItemName())
            .append("itemValue", getItemValue())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserResumeExperience;
import com.jiumi.baseconfig.service.IBaseUserResumeExperienceService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 工作经历Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/experience")
public class BaseUserResumeExperienceController extends BaseController
{
    @Autowired
    private IBaseUserResumeExperienceService baseUserResumeExperienceService;

    /**
     * 查询工作经历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserResumeExperience baseUserResumeExperience)
    {
        startPage();
        List<BaseUserResumeExperience> list = baseUserResumeExperienceService.selectBaseUserResumeExperienceList(baseUserResumeExperience);
        return getDataTable(list);
    }

    /**
     * 导出工作经历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:export')")
    @Log(title = "工作经历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserResumeExperience baseUserResumeExperience)
    {
        List<BaseUserResumeExperience> list = baseUserResumeExperienceService.selectBaseUserResumeExperienceList(baseUserResumeExperience);
        ExcelUtil<BaseUserResumeExperience> util = new ExcelUtil<BaseUserResumeExperience>(BaseUserResumeExperience.class);
        util.exportExcel(response, list, "工作经历数据");
    }

    /**
     * 获取工作经历详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserResumeExperienceService.selectBaseUserResumeExperienceById(id));
    }

    /**
     * 新增工作经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:add')")
    @Log(title = "工作经历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.insertBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 修改工作经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:edit')")
    @Log(title = "工作经历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.updateBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 删除工作经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:experience:remove')")
    @Log(title = "工作经历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserResumeExperienceService.deleteBaseUserResumeExperienceByIds(ids));
    }
}

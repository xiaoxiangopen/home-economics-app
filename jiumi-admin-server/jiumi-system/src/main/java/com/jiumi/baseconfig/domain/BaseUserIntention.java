package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 求职意向对象 base_user_intention
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUserIntention extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 岗位编码 */
    @Excel(name = "岗位编码")
    private String typeId;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String typeName;

    /** 最低工资 */
    @Excel(name = "最低工资")
    private Long lowSalary;

    /** 最高工资 */
    @Excel(name = "最高工资")
    private Long highSalary;

    /** 是否住家01住家02不住家03都可以 */
    @Excel(name = "是否住家01住家02不住家03都可以")
    private String liveType;

    private String signType;

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    private List<BaseUserIntentionCity> cityList;

    public List<BaseUserIntentionCity> getCityList() {
        return cityList;
    }

    public void setCityList(List<BaseUserIntentionCity> cityList) {
        this.cityList = cityList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public String getTypeId()
    {
        return typeId;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setLowSalary(Long lowSalary)
    {
        this.lowSalary = lowSalary;
    }

    public Long getLowSalary()
    {
        return lowSalary;
    }
    public void setHighSalary(Long highSalary)
    {
        this.highSalary = highSalary;
    }

    public Long getHighSalary()
    {
        return highSalary;
    }
    public void setLiveType(String liveType)
    {
        this.liveType = liveType;
    }

    public String getLiveType()
    {
        return liveType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("typeId", getTypeId())
            .append("typeName", getTypeName())
            .append("lowSalary", getLowSalary())
            .append("highSalary", getHighSalary())
            .append("createTime", getCreateTime())
            .append("liveType", getLiveType())
            .toString();
    }
}

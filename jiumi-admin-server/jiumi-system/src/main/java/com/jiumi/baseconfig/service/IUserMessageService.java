package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.UserMessage;

/**
 * 用户消息提醒Service接口
 *
 * @author jiumi
 * @date 2023-02-13
 */
public interface IUserMessageService
{
    /**
     * 查询用户消息提醒
     *
     * @param id 用户消息提醒主键
     * @return 用户消息提醒
     */
    public UserMessage selectUserMessageById(String id);

    /**
     * 查询用户消息提醒列表
     *
     * @param userMessage 用户消息提醒
     * @return 用户消息提醒集合
     */
    public List<UserMessage> selectUserMessageList(UserMessage userMessage);

    /**
     * 新增用户消息提醒
     *
     * @param userMessage 用户消息提醒
     * @return 结果
     */
    public int insertUserMessage(UserMessage userMessage);

    /**
     * 修改用户消息提醒
     *
     * @param userMessage 用户消息提醒
     * @return 结果
     */
    public int updateUserMessage(UserMessage userMessage);

    /**
     * 批量删除用户消息提醒
     *
     * @param ids 需要删除的用户消息提醒主键集合
     * @return 结果
     */
    public int deleteUserMessageByIds(String[] ids);

    /**
     * 删除用户消息提醒信息
     *
     * @param id 用户消息提醒主键
     * @return 结果
     */
    public int deleteUserMessageById(String id);

    void readAllUserMessageList(Long id);

    void deleteUserMessageByUserId(Long userId);

}

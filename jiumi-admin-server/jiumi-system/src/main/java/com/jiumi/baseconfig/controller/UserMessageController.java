package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.UserMessage;
import com.jiumi.baseconfig.service.IUserMessageService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户消息提醒Controller
 *
 * @author jiumi
 * @date 2023-02-13
 */
@RestController
@RequestMapping("/baseconfig/usermessage")
public class UserMessageController extends BaseController
{
    @Autowired
    private IUserMessageService userMessageService;

    /**
     * 查询用户消息提醒列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserMessage userMessage)
    {
        startPage();
        List<UserMessage> list = userMessageService.selectUserMessageList(userMessage);
        return getDataTable(list);
    }

    /**
     * 导出用户消息提醒列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:export')")
    @Log(title = "用户消息提醒", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserMessage userMessage)
    {
        List<UserMessage> list = userMessageService.selectUserMessageList(userMessage);
        ExcelUtil<UserMessage> util = new ExcelUtil<UserMessage>(UserMessage.class);
        util.exportExcel(response, list, "用户消息提醒数据");
    }

    /**
     * 获取用户消息提醒详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(userMessageService.selectUserMessageById(id));
    }

    /**
     * 新增用户消息提醒
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:add')")
    @Log(title = "用户消息提醒", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserMessage userMessage)
    {
        return toAjax(userMessageService.insertUserMessage(userMessage));
    }

    /**
     * 修改用户消息提醒
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:edit')")
    @Log(title = "用户消息提醒", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserMessage userMessage)
    {
        return toAjax(userMessageService.updateUserMessage(userMessage));
    }

    /**
     * 删除用户消息提醒
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usermessage:remove')")
    @Log(title = "用户消息提醒", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(userMessageService.deleteUserMessageByIds(ids));
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.jiumi.common.core.redis.RedisCache;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseCompanyAccountMapper;
import com.jiumi.baseconfig.domain.BaseCompanyAccount;
import com.jiumi.baseconfig.service.IBaseCompanyAccountService;

/**
 * 公司招聘人员Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseCompanyAccountServiceImpl implements IBaseCompanyAccountService
{
    @Autowired
    private BaseCompanyAccountMapper baseCompanyAccountMapper;

    @Autowired
    private RedisCache redisCache;
    /**
     * 查询公司招聘人员
     *
     * @param id 公司招聘人员主键
     * @return 公司招聘人员
     */
    @Override
    public BaseCompanyAccount selectBaseCompanyAccountById(Long id)
    {
        return baseCompanyAccountMapper.selectBaseCompanyAccountById(id);
    }

    /**
     * 查询公司招聘人员列表
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 公司招聘人员
     */
    @Override
    public List<BaseCompanyAccount> selectBaseCompanyAccountList(BaseCompanyAccount baseCompanyAccount)
    {
        return baseCompanyAccountMapper.selectBaseCompanyAccountList(baseCompanyAccount);
    }

    /**
     * 新增公司招聘人员
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 结果
     */
    @Override
    public int insertBaseCompanyAccount(BaseCompanyAccount baseCompanyAccount)
    {
        baseCompanyAccount.setCreateTime(DateUtils.getNowDate());
        return baseCompanyAccountMapper.insertBaseCompanyAccount(baseCompanyAccount);
    }

    /**
     * 修改公司招聘人员
     *
     * @param baseCompanyAccount 公司招聘人员
     * @return 结果
     */
    @Override
    public int updateBaseCompanyAccount(BaseCompanyAccount baseCompanyAccount)
    {
        BaseCompanyAccount accountData= baseCompanyAccountMapper.selectBaseCompanyAccountById(baseCompanyAccount.getId());
        if(!"Y".equals(baseCompanyAccount.getStatus())){
            redisCache.setCacheObject("accountExprie-"+accountData.getUserId(),"Y",365, TimeUnit.DAYS);
        }else{
            redisCache.deleteObject("accountExprie-"+accountData.getUserId());
        }

        return baseCompanyAccountMapper.updateBaseCompanyAccount(baseCompanyAccount);
    }

    /**
     * 批量删除公司招聘人员
     *
     * @param ids 需要删除的公司招聘人员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAccountByIds(Long[] ids)
    {
        return baseCompanyAccountMapper.deleteBaseCompanyAccountByIds(ids);
    }

    /**
     * 删除公司招聘人员信息
     *
     * @param id 公司招聘人员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAccountById(Long id)
    {
        return baseCompanyAccountMapper.deleteBaseCompanyAccountById(id);
    }

    @Override
    public void deleteBaseCompanyAccountByCompanyId(Long companyId) {
        baseCompanyAccountMapper.deleteBaseCompanyAccountByCompanyId(companyId);
    }
}

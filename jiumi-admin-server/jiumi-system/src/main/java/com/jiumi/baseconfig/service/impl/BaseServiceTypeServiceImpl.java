package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseServiceTypeMapper;
import com.jiumi.baseconfig.domain.BaseServiceType;
import com.jiumi.baseconfig.service.IBaseServiceTypeService;

/**
 * 服务项目Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseServiceTypeServiceImpl implements IBaseServiceTypeService
{
    @Autowired
    private BaseServiceTypeMapper baseServiceTypeMapper;

    /**
     * 查询服务项目
     *
     * @param id 服务项目主键
     * @return 服务项目
     */
    @Override
    public BaseServiceType selectBaseServiceTypeById(Long id)
    {
        return baseServiceTypeMapper.selectBaseServiceTypeById(id);
    }

    /**
     * 查询服务项目列表
     *
     * @param baseServiceType 服务项目
     * @return 服务项目
     */
    @Override
    public List<BaseServiceType> selectBaseServiceTypeList(BaseServiceType baseServiceType)
    {
        return baseServiceTypeMapper.selectBaseServiceTypeList(baseServiceType);
    }

    /**
     * 新增服务项目
     *
     * @param baseServiceType 服务项目
     * @return 结果
     */
    @Override
    public int insertBaseServiceType(BaseServiceType baseServiceType)
    {
        baseServiceType.setCreateTime(DateUtils.getNowDate());
        return baseServiceTypeMapper.insertBaseServiceType(baseServiceType);
    }

    /**
     * 修改服务项目
     *
     * @param baseServiceType 服务项目
     * @return 结果
     */
    @Override
    public int updateBaseServiceType(BaseServiceType baseServiceType)
    {
        baseServiceType.setUpdateTime(DateUtils.getNowDate());
        return baseServiceTypeMapper.updateBaseServiceType(baseServiceType);
    }

    /**
     * 批量删除服务项目
     *
     * @param ids 需要删除的服务项目主键
     * @return 结果
     */
    @Override
    public int deleteBaseServiceTypeByIds(Long[] ids)
    {
        return baseServiceTypeMapper.deleteBaseServiceTypeByIds(ids);
    }

    /**
     * 删除服务项目信息
     *
     * @param id 服务项目主键
     * @return 结果
     */
    @Override
    public int deleteBaseServiceTypeById(Long id)
    {
        return baseServiceTypeMapper.deleteBaseServiceTypeById(id);
    }
}

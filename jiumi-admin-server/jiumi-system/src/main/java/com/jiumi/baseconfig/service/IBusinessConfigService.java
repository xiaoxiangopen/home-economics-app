package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BusinessConfig;

/**
 * 业务设置Service接口
 * 
 * @author jiumi
 * @date 2022-12-07
 */
public interface IBusinessConfigService 
{
    /**
     * 查询业务设置
     * 
     * @param id 业务设置主键
     * @return 业务设置
     */
    public BusinessConfig selectBusinessConfigById(Long id);

    /**
     * 查询业务设置列表
     * 
     * @param businessConfig 业务设置
     * @return 业务设置集合
     */
    public List<BusinessConfig> selectBusinessConfigList(BusinessConfig businessConfig);

    /**
     * 新增业务设置
     * 
     * @param businessConfig 业务设置
     * @return 结果
     */
    public int insertBusinessConfig(BusinessConfig businessConfig);

    /**
     * 修改业务设置
     * 
     * @param businessConfig 业务设置
     * @return 结果
     */
    public int updateBusinessConfig(BusinessConfig businessConfig);

    /**
     * 批量删除业务设置
     * 
     * @param ids 需要删除的业务设置主键集合
     * @return 结果
     */
    public int deleteBusinessConfigByIds(Long[] ids);

    /**
     * 删除业务设置信息
     * 
     * @param id 业务设置主键
     * @return 结果
     */
    public int deleteBusinessConfigById(Long id);
}

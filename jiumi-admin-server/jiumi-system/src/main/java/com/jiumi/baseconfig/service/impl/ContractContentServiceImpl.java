package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.ContractContentMapper;
import com.jiumi.baseconfig.domain.ContractContent;
import com.jiumi.baseconfig.service.IContractContentService;

/**
 * 合同内容Service业务层处理
 *
 * @author jiumi
 * @date 2023-03-15
 */
@Service
public class ContractContentServiceImpl implements IContractContentService
{
    @Autowired
    private ContractContentMapper contractContentMapper;

    /**
     * 查询合同内容
     *
     * @param id 合同内容主键
     * @return 合同内容
     */
    @Override
    public ContractContent selectContractContentById(Long id)
    {
        return contractContentMapper.selectContractContentById(id);
    }

    /**
     * 查询合同内容列表
     *
     * @param contractContent 合同内容
     * @return 合同内容
     */
    @Override
    public List<ContractContent> selectContractContentList(ContractContent contractContent)
    {
        return contractContentMapper.selectContractContentList(contractContent);
    }

    /**
     * 新增合同内容
     *
     * @param contractContent 合同内容
     * @return 结果
     */
    @Override
    public int insertContractContent(ContractContent contractContent)
    {
        contractContent.setCreateTime(DateUtils.getNowDate());
        return contractContentMapper.insertContractContent(contractContent);
    }

    /**
     * 修改合同内容
     *
     * @param contractContent 合同内容
     * @return 结果
     */
    @Override
    public int updateContractContent(ContractContent contractContent)
    {
        return contractContentMapper.updateContractContent(contractContent);
    }

    /**
     * 批量删除合同内容
     *
     * @param ids 需要删除的合同内容主键
     * @return 结果
     */
    @Override
    public int deleteContractContentByIds(Long[] ids)
    {
        return contractContentMapper.deleteContractContentByIds(ids);
    }

    /**
     * 删除合同内容信息
     *
     * @param id 合同内容主键
     * @return 结果
     */
    @Override
    public int deleteContractContentById(Long id)
    {
        return contractContentMapper.deleteContractContentById(id);
    }
}

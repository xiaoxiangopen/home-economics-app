package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserIntentionCity;

/**
 * 求职意向城市Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseUserIntentionCityService
{
    /**
     * 查询求职意向城市
     *
     * @param id 求职意向城市主键
     * @return 求职意向城市
     */
    public BaseUserIntentionCity selectBaseUserIntentionCityById(Long id);

    /**
     * 查询求职意向城市列表
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 求职意向城市集合
     */
    public List<BaseUserIntentionCity> selectBaseUserIntentionCityList(BaseUserIntentionCity baseUserIntentionCity);

    /**
     * 新增求职意向城市
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 结果
     */
    public int insertBaseUserIntentionCity(BaseUserIntentionCity baseUserIntentionCity);

    /**
     * 修改求职意向城市
     *
     * @param baseUserIntentionCity 求职意向城市
     * @return 结果
     */
    public int updateBaseUserIntentionCity(BaseUserIntentionCity baseUserIntentionCity);

    /**
     * 批量删除求职意向城市
     *
     * @param ids 需要删除的求职意向城市主键集合
     * @return 结果
     */
    public int deleteBaseUserIntentionCityByIds(Long[] ids);

    /**
     * 删除求职意向城市信息
     *
     * @param id 求职意向城市主键
     * @return 结果
     */
    public int deleteBaseUserIntentionCityById(Long id);

    void deleteBaseUserIntentionCityByIntentionId(Long id);
}

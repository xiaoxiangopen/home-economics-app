package com.jiumi.baseconfig.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 合同订单对象 base_order
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    private Long userId;
    private String userType;

    private String userName;
    private String userPhone;

    /** 订单标题 */
    @Excel(name = "订单标题")
    private String title;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderCode;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractCode;

    /** 支付金额 */
    @Excel(name = "支付金额")
    private BigDecimal orderAmount;


    private BigDecimal payAmount;

    /** 支付积分 */
    @Excel(name = "支付积分")
    private BigDecimal scoreAmount;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payType;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private String payStatus;

    /** 支付订单号 */
    @Excel(name = "支付订单号")
    private String tradeCode;

    private Long companyId;
    private String companyName;

    private Long targetUserId;

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotBlank(message = "请选择付款人")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    /** 支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setOrderCode(String orderCode)
    {
        this.orderCode = orderCode;
    }

    public String getOrderCode()
    {
        return orderCode;
    }
    public void setContractCode(String contractCode)
    {
        this.contractCode = contractCode;
    }

    @NotBlank(message = "关联合同不能为空")
    public String getContractCode()
    {
        return contractCode;
    }
    public void setOrderAmount(BigDecimal orderAmount)
    {
        this.orderAmount = orderAmount;
    }

    @NotNull(message = "账单金额不能为空")
    public BigDecimal getOrderAmount()
    {
        return orderAmount;
    }
    public void setScoreAmount(BigDecimal scoreAmount)
    {
        this.scoreAmount = scoreAmount;
    }

    public BigDecimal getScoreAmount()
    {
        return scoreAmount;
    }
    public void setPayType(String payType)
    {
        this.payType = payType;
    }

    public String getPayType()
    {
        return payType;
    }
    public void setPayStatus(String payStatus)
    {
        this.payStatus = payStatus;
    }

    public String getPayStatus()
    {
        return payStatus;
    }
    public void setTradeCode(String tradeCode)
    {
        this.tradeCode = tradeCode;
    }

    public String getTradeCode()
    {
        return tradeCode;
    }
    public void setPayTime(Date payTime)
    {
        this.payTime = payTime;
    }

    public Date getPayTime()
    {
        return payTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("orderCode", getOrderCode())
            .append("contractCode", getContractCode())
            .append("orderAmount", getOrderAmount())
            .append("scoreAmount", getScoreAmount())
            .append("payType", getPayType())
            .append("payStatus", getPayStatus())
            .append("tradeCode", getTradeCode())
            .append("payTime", getPayTime())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.jiumi.baseconfig.domain.*;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.baseconfig.mapper.BusinessConfigMapper;
import com.jiumi.baseconfig.mapper.UserMessageMapper;
import com.jiumi.baseconfig.service.IBaseUserIntentionCityService;
import com.jiumi.baseconfig.service.IBaseUserIntentionService;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseAppointmentMapper;
import com.jiumi.baseconfig.service.IBaseAppointmentService;

/**
 * 预约Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseAppointmentServiceImpl implements IBaseAppointmentService
{
    @Autowired
    private BaseAppointmentMapper baseAppointmentMapper;

    @Autowired
    private UserMessageMapper userMessageMapper;

    @Autowired
    private BaseUserMapper baseUserMapper;

    @Autowired
    private IBaseUserIntentionService baseUserIntentionService;

    @Autowired
    private IBaseUserIntentionCityService baseUserIntentionCityService;

    @Autowired
    private BusinessConfigMapper businessConfigMapper;
    /**
     * 查询预约
     *
     * @param id 预约主键
     * @return 预约
     */
    @Override
    public BaseAppointment selectBaseAppointmentById(Long id)
    {
        return baseAppointmentMapper.selectBaseAppointmentById(id);
    }

    /**
     * 查询预约列表
     *
     * @param baseAppointment 预约
     * @return 预约
     */
    @Override
    public List<BaseAppointment> selectBaseAppointmentList(BaseAppointment baseAppointment)
    {
        return baseAppointmentMapper.selectBaseAppointmentList(baseAppointment);
    }

    /**
     * 新增预约
     *
     * @param baseAppointment 预约
     * @return 结果
     */
    @Override
    public int insertBaseAppointment(BaseAppointment baseAppointment)
    {
        baseAppointment.setCreateTime(DateUtils.getNowDate());
        return baseAppointmentMapper.insertBaseAppointment(baseAppointment);
    }

    /**
     * 修改预约
     *
     * @param baseAppointment 预约
     * @return 结果
     */
    @Override
    public int updateBaseAppointment(BaseAppointment baseAppointment)
    {
        baseAppointment.setUpdateTime(DateUtils.getNowDate());
        return baseAppointmentMapper.updateBaseAppointment(baseAppointment);
    }

    /**
     * 批量删除预约
     *
     * @param ids 需要删除的预约主键
     * @return 结果
     */
    @Override
    public int deleteBaseAppointmentByIds(Long[] ids)
    {
        return baseAppointmentMapper.deleteBaseAppointmentByIds(ids);
    }

    /**
     * 删除预约信息
     *
     * @param id 预约主键
     * @return 结果
     */
    @Override
    public int deleteBaseAppointmentById(Long id)
    {
        return baseAppointmentMapper.deleteBaseAppointmentById(id);
    }


    @Override
    public AjaxResult saveBaseAppointment(BaseAppointment appointment) {
        int result=0;
        if(appointment.getId()==null){
            result= baseAppointmentMapper.insertBaseAppointment(appointment);
        }else{
            result= baseAppointmentMapper.updateBaseAppointment(appointment);
        }
        if(result>0){
            return AjaxResult.success("预约成功");
        }else{
            return AjaxResult.error("预约失败");
        }
    }

    @Override
    public List<BaseAppointment> selectAgencyAppointmentList(BaseAppointment appointParam) {
        return baseAppointmentMapper.selectAgencyAppointmentList(appointParam);
    }

    @Override
    public List<BaseAppointment> selectUserFocusAppointmentList(BaseAppointment param) {
        return baseAppointmentMapper.selectUserFocusAppointmentList(param);
    }

    @Override
    public List<BaseAppointment> selectManageAppointmentList(BaseAppointment baseAppointment) {
        return baseAppointmentMapper.selectManageAppointmentList(baseAppointment);
    }

    @Override
    public List<BaseAppointment> selectCompanyAppointmentList(BaseAppointment appointment) {
        return baseAppointmentMapper.selectCompanyAppointmentList(appointment);
    }

    @Override
    public void autoReferrerEmployer() {
        List<BaseAppointment> newList= baseAppointmentMapper.selectNewstAppointmentList();

        List<BaseUser> custList= baseUserMapper.selectAllCustData();

        custList.stream().forEach(cust->{
            BaseUserIntention intention=baseUserIntentionService.selectBaseUserIntentionByUserId(cust.getUserId());
            if(intention!=null){
                BaseUserIntentionCity param=new BaseUserIntentionCity();
                param.setIntentionId(intention.getId());
                List<BaseUserIntentionCity> cityList=baseUserIntentionCityService.selectBaseUserIntentionCityList(param);
                List<BaseAppointment> filterList=new ArrayList<>();
                for(BaseUserIntentionCity city : cityList){
                    List<BaseAppointment> list=newList.stream().filter(employ->intention.getTypeId().indexOf(employ.getCategoryType())!=-1 && employ.getUserAddress().indexOf(city.getProvince()+city.getCity())!=-1).collect(Collectors.toList());
                    list.stream().forEach(ap->{
                        if(filterList.size()==0){
                            filterList.add(ap);
                        }else {
                            BaseAppointment appoint = filterList.stream().filter(a -> a.getId().intValue() == ap.getId().intValue()).findFirst().orElse(null);
                            if (appoint == null) {
                                filterList.add(appoint);
                            }
                        }
                    });
                }
                int saveNum=0;
                for(int i=0;i<filterList.size();i++){
                        UserMessage msg = new UserMessage();
                        msg.setUserId(cust.getUserId());
                        msg.setCategory("03");
                        msg.setLinkId(String.valueOf(filterList.get(i).getId()));
                        List<UserMessage> msgList= userMessageMapper.selectUserMessageList(msg);
                        if(msgList.size()==0) {
                            saveNum=saveNum+1;
                            msg.setTitle("推荐雇主");
                            msg.setMsgContent("推荐雇主");
                            msg.setReadStatus("N");
                            msg.setCreateTime(DateUtils.getNowDate());
                            if(saveNum>2){
                                break;
                            }
                            userMessageMapper.insertUserMessage(msg);
                        }
                }

            }

        });
    }

    @Override
    public void autoAppoinmentOffline() {
        BaseAppointment param=new BaseAppointment();
        param.setApplyStatus("01");
        List<BaseAppointment> userList= baseAppointmentMapper.selectBaseAppointmentList(param);
        BusinessConfig config=businessConfigMapper.selectBusinessConfigById(4L);
        Long effectTime=Long.valueOf(config.getValue());
        userList.stream().forEach(user->{
            Long clickTime=user.getApplyTime()!=null?user.getApplyTime().getTime():0L;
            Long diffmin=(DateUtils.getNowDate().getTime()-clickTime)/1000/60;
            if(diffmin>effectTime*60){
                BaseAppointment appointment=new BaseAppointment();
                appointment.setId(user.getId());
                appointment.setApplyStatus("02");
                appointment.setUpdateTime(DateUtils.getNowDate());
                baseAppointmentMapper.updateBaseAppointment(appointment);
            }
        });
    }

    @Override
    public void updateBaseAppointmentOffLine(Long userId) {
        baseAppointmentMapper.updateBaseAppointmentOffLine(userId);
    }


}

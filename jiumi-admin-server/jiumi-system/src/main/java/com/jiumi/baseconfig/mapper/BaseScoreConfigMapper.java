package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseScoreConfig;

/**
 * 积分奖励设置Mapper接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseScoreConfigMapper 
{
    /**
     * 查询积分奖励设置
     * 
     * @param id 积分奖励设置主键
     * @return 积分奖励设置
     */
    public BaseScoreConfig selectBaseScoreConfigById(Long id);

    /**
     * 查询积分奖励设置列表
     * 
     * @param baseScoreConfig 积分奖励设置
     * @return 积分奖励设置集合
     */
    public List<BaseScoreConfig> selectBaseScoreConfigList(BaseScoreConfig baseScoreConfig);

    /**
     * 新增积分奖励设置
     * 
     * @param baseScoreConfig 积分奖励设置
     * @return 结果
     */
    public int insertBaseScoreConfig(BaseScoreConfig baseScoreConfig);

    /**
     * 修改积分奖励设置
     * 
     * @param baseScoreConfig 积分奖励设置
     * @return 结果
     */
    public int updateBaseScoreConfig(BaseScoreConfig baseScoreConfig);

    /**
     * 删除积分奖励设置
     * 
     * @param id 积分奖励设置主键
     * @return 结果
     */
    public int deleteBaseScoreConfigById(Long id);

    /**
     * 批量删除积分奖励设置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseScoreConfigByIds(Long[] ids);
}

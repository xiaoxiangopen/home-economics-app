package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseContract;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.common.core.domain.AjaxResult;

/**
 * 合同管理Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseContractService
{
    /**
     * 查询合同管理
     *
     * @param id 合同管理主键
     * @return 合同管理
     */
    public BaseContract selectBaseContractById(Long id);

    /**
     * 查询合同管理列表
     *
     * @param baseContract 合同管理
     * @return 合同管理集合
     */
    public List<BaseContract> selectBaseContractList(BaseContract baseContract);

    /**
     * 新增合同管理
     *
     * @param baseContract 合同管理
     * @return 结果
     */
    public int insertBaseContract(BaseContract baseContract);

    /**
     * 修改合同管理
     *
     * @param baseContract 合同管理
     * @return 结果
     */
    public int updateBaseContract(BaseContract baseContract);

    /**
     * 批量删除合同管理
     *
     * @param ids 需要删除的合同管理主键集合
     * @return 结果
     */
    public int deleteBaseContractByIds(Long[] ids);

    /**
     * 删除合同管理信息
     *
     * @param id 合同管理主键
     * @return 结果
     */
    public int deleteBaseContractById(Long id);

    List selectUserContractList(BaseContract contract);

    List selectCompanyContractList(BaseContract baseContract);

    void autoCreatePayorder();

    BaseContract selectBaseContractByCode(String contractCode);

    AjaxResult confirmSignContract(BaseContract baseContract, BaseUser currentUser);

    AjaxResult auntSignBaseContract(BaseContract baseContract, BaseUser userInfo);

    List selectAgencyContractList(BaseContract contract);

    List<BaseContract> selectContractListByDate(String dateStr);
}

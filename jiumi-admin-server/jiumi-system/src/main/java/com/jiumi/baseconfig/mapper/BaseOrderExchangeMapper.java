package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseOrderExchange;

/**
 * 用户兑换奖品记录Mapper接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseOrderExchangeMapper 
{
    /**
     * 查询用户兑换奖品记录
     * 
     * @param id 用户兑换奖品记录主键
     * @return 用户兑换奖品记录
     */
    public BaseOrderExchange selectBaseOrderExchangeById(Long id);

    /**
     * 查询用户兑换奖品记录列表
     * 
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 用户兑换奖品记录集合
     */
    public List<BaseOrderExchange> selectBaseOrderExchangeList(BaseOrderExchange baseOrderExchange);

    /**
     * 新增用户兑换奖品记录
     * 
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    public int insertBaseOrderExchange(BaseOrderExchange baseOrderExchange);

    /**
     * 修改用户兑换奖品记录
     * 
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    public int updateBaseOrderExchange(BaseOrderExchange baseOrderExchange);

    /**
     * 删除用户兑换奖品记录
     * 
     * @param id 用户兑换奖品记录主键
     * @return 结果
     */
    public int deleteBaseOrderExchangeById(Long id);

    /**
     * 批量删除用户兑换奖品记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseOrderExchangeByIds(Long[] ids);
}

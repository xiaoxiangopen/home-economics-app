package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUser;
import org.apache.ibatis.annotations.Param;

/**
 * 用户信息Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseUserMapper
{
    /**
     * 查询用户信息
     *
     * @param userId 用户信息主键
     * @return 用户信息
     */
    public BaseUser selectBaseUserByUserId(Long userId);

    /**
     * 查询用户信息列表
     *
     * @param baseUser 用户信息
     * @return 用户信息集合
     */
    public List<BaseUser> selectBaseUserList(BaseUser baseUser);

    /**
     * 新增用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    public int insertBaseUser(BaseUser baseUser);

    /**
     * 修改用户信息
     *
     * @param baseUser 用户信息
     * @return 结果
     */
    public int updateBaseUser(BaseUser baseUser);

    /**
     * 删除用户信息
     *
     * @param userId 用户信息主键
     * @return 结果
     */
    public int deleteBaseUserByUserId(Long userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserByUserIds(Long[] userIds);

    BaseUser selectUserByUserName(@Param("userName") String userName);

    void updateUserScore(BaseUser u);

    List<BaseUser> queryUserData(BaseUser user);

    int resetUserPwdByPhone(@Param("userName") String userName,@Param("password") String password);

    List selectCompanyUserList(BaseUser baseUser);

    List selectCompanyEpmloyList(BaseUser baseUser);

    List selectCompanyAuntList(BaseUser baseUser);

    List<BaseUser> selectCompanyAccountList(BaseUser param);

    BaseUser selectUserByUserPhone(@Param("phonenumber") String phonenumber);

    BaseUser selectUserByReferrerCode(@Param("referrerCode") String referrerCode);

    int updateAuthUser(BaseUser baseUser);

    BaseUser selectUserByUserCertCode(@Param("certCode") String certCode);

    List<BaseUser> selectUserFocusList(BaseUser param);

    List<BaseUser> selectUserInviteList(BaseUser userInfo);

    int updateBaseUserScore(BaseUser referrerUser);

    List<BaseUser> selectStallUserListList(BaseUser baseUser);

    List<BaseUser> selectAllCustData();

    List selectContractAuntList(BaseUser baseUser);

    List selectContractEmployList(BaseUser baseUser);

    List<BaseUser> selectBaseUserByAuthUserId(@Param("id") Long id);

    List selectQueryCompanyEpmloyList(BaseUser baseUser);

    List<BaseUser> queryUserInfo(BaseUser user);
}

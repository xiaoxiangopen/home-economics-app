package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCompanyAddress;

/**
 * 公司地址Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseCompanyAddressService
{
    /**
     * 查询公司地址
     *
     * @param id 公司地址主键
     * @return 公司地址
     */
    public BaseCompanyAddress selectBaseCompanyAddressById(Long id);

    /**
     * 查询公司地址列表
     *
     * @param baseCompanyAddress 公司地址
     * @return 公司地址集合
     */
    public List<BaseCompanyAddress> selectBaseCompanyAddressList(BaseCompanyAddress baseCompanyAddress);

    /**
     * 新增公司地址
     *
     * @param baseCompanyAddress 公司地址
     * @return 结果
     */
    public int insertBaseCompanyAddress(BaseCompanyAddress baseCompanyAddress);

    /**
     * 修改公司地址
     *
     * @param baseCompanyAddress 公司地址
     * @return 结果
     */
    public int updateBaseCompanyAddress(BaseCompanyAddress baseCompanyAddress);

    /**
     * 批量删除公司地址
     *
     * @param ids 需要删除的公司地址主键集合
     * @return 结果
     */
    public int deleteBaseCompanyAddressByIds(Long[] ids);

    /**
     * 删除公司地址信息
     *
     * @param id 公司地址主键
     * @return 结果
     */
    public int deleteBaseCompanyAddressById(Long id);

    int deleteBaseCompanyAddressByCompanyId(Long id);
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserClock;
import com.jiumi.baseconfig.service.IBaseUserClockService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户打卡记录Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/clock")
public class BaseUserClockController extends BaseController
{
    @Autowired
    private IBaseUserClockService baseUserClockService;

    /**
     * 查询用户打卡记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserClock baseUserClock)
    {
        startPage();
        List<BaseUserClock> list = baseUserClockService.selectBaseUserClockList(baseUserClock);
        return getDataTable(list);
    }

    /**
     * 导出用户打卡记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:export')")
    @Log(title = "用户打卡记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserClock baseUserClock)
    {
        List<BaseUserClock> list = baseUserClockService.selectBaseUserClockList(baseUserClock);
        ExcelUtil<BaseUserClock> util = new ExcelUtil<BaseUserClock>(BaseUserClock.class);
        util.exportExcel(response, list, "用户打卡记录数据");
    }

    /**
     * 获取用户打卡记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserClockService.selectBaseUserClockById(id));
    }

    /**
     * 新增用户打卡记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:add')")
    @Log(title = "用户打卡记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserClock baseUserClock)
    {
        return toAjax(baseUserClockService.insertBaseUserClock(baseUserClock));
    }

    /**
     * 修改用户打卡记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:edit')")
    @Log(title = "用户打卡记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserClock baseUserClock)
    {
        return toAjax(baseUserClockService.updateBaseUserClock(baseUserClock));
    }

    /**
     * 删除用户打卡记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:clock:remove')")
    @Log(title = "用户打卡记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserClockService.deleteBaseUserClockByIds(ids));
    }
}

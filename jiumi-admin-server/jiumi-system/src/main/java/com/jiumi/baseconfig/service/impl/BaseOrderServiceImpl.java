package com.jiumi.baseconfig.service.impl;

import java.util.List;

import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.mapper.BaseUserMapper;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseOrderMapper;
import com.jiumi.baseconfig.domain.BaseOrder;
import com.jiumi.baseconfig.service.IBaseOrderService;

/**
 * 合同订单Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseOrderServiceImpl implements IBaseOrderService
{
    @Autowired
    private BaseOrderMapper baseOrderMapper;

    @Autowired
    private BaseUserMapper baseUserMapper;

    /**
     * 查询合同订单
     *
     * @param id 合同订单主键
     * @return 合同订单
     */
    @Override
    public BaseOrder selectBaseOrderById(Long id)
    {
        BaseOrder baseOrder= baseOrderMapper.selectBaseOrderById(id);
        BaseUser baseUser= baseUserMapper.selectBaseUserByUserId(baseOrder.getUserId());
        baseOrder.setUserName(baseUser.getAuthName());
        baseOrder.setUserPhone(baseUser.getPhonenumber());
        return baseOrder;
    }

    /**
     * 查询合同订单列表
     *
     * @param baseOrder 合同订单
     * @return 合同订单
     */
    @Override
    public List<BaseOrder> selectBaseOrderList(BaseOrder baseOrder)
    {
        return baseOrderMapper.selectBaseOrderList(baseOrder);
    }

    /**
     * 新增合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    @Override
    public int insertBaseOrder(BaseOrder baseOrder)
    {
        baseOrder.setCreateTime(DateUtils.getNowDate());
        return baseOrderMapper.insertBaseOrder(baseOrder);
    }

    /**
     * 修改合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    @Override
    public int updateBaseOrder(BaseOrder baseOrder)
    {
        baseOrder.setUpdateTime(DateUtils.getNowDate());
        return baseOrderMapper.updateBaseOrder(baseOrder);
    }

    /**
     * 批量删除合同订单
     *
     * @param ids 需要删除的合同订单主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderByIds(Long[] ids)
    {
        return baseOrderMapper.deleteBaseOrderByIds(ids);
    }

    /**
     * 删除合同订单信息
     *
     * @param id 合同订单主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrderById(Long id)
    {
        return baseOrderMapper.deleteBaseOrderById(id);
    }

    @Override
    public List<BaseOrder> selectCompanyOrderList(BaseOrder baseOrder) {
        return baseOrderMapper.selectCompanyOrderList(baseOrder);
    }

    @Override
    public BaseOrder selectBaseOrderByOrderCode(String orderCode) {
        return baseOrderMapper.selectBaseOrderByOrderCode(orderCode);
    }

    @Override
    public BaseOrder selectBaseOrderByTradeCode(String outTradeNo) {
        return baseOrderMapper.selectBaseOrderByTradeCode(outTradeNo);
    }
}

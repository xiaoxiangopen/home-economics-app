package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseCompany;
import com.jiumi.baseconfig.service.IBaseCompanyService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 家政公司Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/company")
public class BaseCompanyController extends BaseController
{
    @Autowired
    private IBaseCompanyService baseCompanyService;

    /**
     * 查询家政公司列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompany baseCompany)
    {
        startPage();
        List<BaseCompany> list = baseCompanyService.selectBaseCompanyList(baseCompany);
        return getDataTable(list);
    }

    /**
     * 导出家政公司列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:export')")
    @Log(title = "家政公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompany baseCompany)
    {
        List<BaseCompany> list = baseCompanyService.selectBaseCompanyList(baseCompany);
        ExcelUtil<BaseCompany> util = new ExcelUtil<BaseCompany>(BaseCompany.class);
        util.exportExcel(response, list, "认证公司");
    }

    /**
     * 获取家政公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseCompanyService.selectBaseCompanyById(id));
    }

    /**
     * 新增家政公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:add')")
    @Log(title = "家政公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompany baseCompany)
    {
        return toAjax(baseCompanyService.insertBaseCompany(baseCompany));
    }

    /**
     * 修改家政公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:edit')")
    @Log(title = "家政公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompany baseCompany)
    {
        baseCompany.setUpdateBy(getUsername());
        return toAjax(baseCompanyService.updateBaseCompany(baseCompany));
    }

    /**
     * 删除家政公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:remove')")
    @Log(title = "家政公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyService.deleteBaseCompanyByIds(ids));
    }
}

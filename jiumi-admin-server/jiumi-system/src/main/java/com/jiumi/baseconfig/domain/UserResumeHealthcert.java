package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 健康证书对象 base_user_resume_healthcert
 *
 * @author jiumi
 * @date 2023-06-15
 */
public class UserResumeHealthcert extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 简历ID */
    @Excel(name = "简历ID")
    private Long resumeId;

    /** 健康证名称名称 */
    @Excel(name = "健康证名称名称")
    private String certificateName;

    /** 有效期 */
    @Excel(name = "有效期")
    private String inceptDate;

    /** 证件照片 */
    @Excel(name = "证件照片")
    private String certificateImage;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setResumeId(Long resumeId)
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId()
    {
        return resumeId;
    }
    public void setCertificateName(String certificateName)
    {
        this.certificateName = certificateName;
    }

    public String getCertificateName()
    {
        return certificateName;
    }
    public void setInceptDate(String inceptDate)
    {
        this.inceptDate = inceptDate;
    }

    public String getInceptDate()
    {
        return inceptDate;
    }
    public void setCertificateImage(String certificateImage)
    {
        this.certificateImage = certificateImage;
    }

    public String getCertificateImage()
    {
        return certificateImage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("resumeId", getResumeId())
            .append("certificateName", getCertificateName())
            .append("inceptDate", getInceptDate())
            .append("certificateImage", getCertificateImage())
            .toString();
    }
}

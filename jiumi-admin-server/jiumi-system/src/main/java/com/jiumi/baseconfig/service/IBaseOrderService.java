package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseOrder;

/**
 * 合同订单Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseOrderService
{
    /**
     * 查询合同订单
     *
     * @param id 合同订单主键
     * @return 合同订单
     */
    public BaseOrder selectBaseOrderById(Long id);

    /**
     * 查询合同订单列表
     *
     * @param baseOrder 合同订单
     * @return 合同订单集合
     */
    public List<BaseOrder> selectBaseOrderList(BaseOrder baseOrder);

    /**
     * 新增合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    public int insertBaseOrder(BaseOrder baseOrder);

    /**
     * 修改合同订单
     *
     * @param baseOrder 合同订单
     * @return 结果
     */
    public int updateBaseOrder(BaseOrder baseOrder);

    /**
     * 批量删除合同订单
     *
     * @param ids 需要删除的合同订单主键集合
     * @return 结果
     */
    public int deleteBaseOrderByIds(Long[] ids);

    /**
     * 删除合同订单信息
     *
     * @param id 合同订单主键
     * @return 结果
     */
    public int deleteBaseOrderById(Long id);

    List<BaseOrder> selectCompanyOrderList(BaseOrder baseOrder);

    BaseOrder selectBaseOrderByOrderCode(String orderCode);

    BaseOrder selectBaseOrderByTradeCode(String outTradeNo);
}

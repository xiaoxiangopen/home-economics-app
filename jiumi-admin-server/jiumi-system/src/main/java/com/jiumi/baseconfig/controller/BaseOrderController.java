package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseOrder;
import com.jiumi.baseconfig.service.IBaseOrderService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 合同订单Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/order")
public class BaseOrderController extends BaseController
{
    @Autowired
    private IBaseOrderService baseOrderService;

    /**
     * 查询合同订单列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseOrder baseOrder)
    {
        startPage();
        List<BaseOrder> list = baseOrderService.selectBaseOrderList(baseOrder);
        return getDataTable(list);
    }

    /**
     * 导出合同订单列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:export')")
    @Log(title = "合同订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseOrder baseOrder)
    {
        List<BaseOrder> list = baseOrderService.selectBaseOrderList(baseOrder);
        ExcelUtil<BaseOrder> util = new ExcelUtil<BaseOrder>(BaseOrder.class);
        util.exportExcel(response, list, "合同订单数据");
    }

    /**
     * 获取合同订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseOrderService.selectBaseOrderById(id));
    }

    /**
     * 新增合同订单
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:add')")
    @Log(title = "合同订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseOrder baseOrder)
    {
        return toAjax(baseOrderService.insertBaseOrder(baseOrder));
    }

    /**
     * 修改合同订单
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:edit')")
    @Log(title = "合同订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseOrder baseOrder)
    {
        return toAjax(baseOrderService.updateBaseOrder(baseOrder));
    }

    /**
     * 删除合同订单
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:order:remove')")
    @Log(title = "合同订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseOrderService.deleteBaseOrderByIds(ids));
    }
}

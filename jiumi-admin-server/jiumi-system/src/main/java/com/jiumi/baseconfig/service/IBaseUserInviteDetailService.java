package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserInviteDetail;

/**
 * 用户邀请Service接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseUserInviteDetailService 
{
    /**
     * 查询用户邀请
     * 
     * @param id 用户邀请主键
     * @return 用户邀请
     */
    public BaseUserInviteDetail selectBaseUserInviteDetailById(Long id);

    /**
     * 查询用户邀请列表
     * 
     * @param baseUserInviteDetail 用户邀请
     * @return 用户邀请集合
     */
    public List<BaseUserInviteDetail> selectBaseUserInviteDetailList(BaseUserInviteDetail baseUserInviteDetail);

    /**
     * 新增用户邀请
     * 
     * @param baseUserInviteDetail 用户邀请
     * @return 结果
     */
    public int insertBaseUserInviteDetail(BaseUserInviteDetail baseUserInviteDetail);

    /**
     * 修改用户邀请
     * 
     * @param baseUserInviteDetail 用户邀请
     * @return 结果
     */
    public int updateBaseUserInviteDetail(BaseUserInviteDetail baseUserInviteDetail);

    /**
     * 批量删除用户邀请
     * 
     * @param ids 需要删除的用户邀请主键集合
     * @return 结果
     */
    public int deleteBaseUserInviteDetailByIds(Long[] ids);

    /**
     * 删除用户邀请信息
     * 
     * @param id 用户邀请主键
     * @return 结果
     */
    public int deleteBaseUserInviteDetailById(Long id);
}

package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserResumeCertificate;
import org.apache.ibatis.annotations.Param;

/**
 * 职业证书Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseUserResumeCertificateMapper
{
    /**
     * 查询职业证书
     *
     * @param id 职业证书主键
     * @return 职业证书
     */
    public BaseUserResumeCertificate selectBaseUserResumeCertificateById(Long id);

    /**
     * 查询职业证书列表
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 职业证书集合
     */
    public List<BaseUserResumeCertificate> selectBaseUserResumeCertificateList(BaseUserResumeCertificate baseUserResumeCertificate);

    /**
     * 新增职业证书
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 结果
     */
    public int insertBaseUserResumeCertificate(BaseUserResumeCertificate baseUserResumeCertificate);

    /**
     * 修改职业证书
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 结果
     */
    public int updateBaseUserResumeCertificate(BaseUserResumeCertificate baseUserResumeCertificate);

    /**
     * 删除职业证书
     *
     * @param id 职业证书主键
     * @return 结果
     */
    public int deleteBaseUserResumeCertificateById(Long id);

    /**
     * 批量删除职业证书
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeCertificateByIds(Long[] ids);

    void deleteResumeCertificateByResumeId(Long id);

    void deleteResumeCertificateByCertCode(@Param("certCode") String certCode);
}

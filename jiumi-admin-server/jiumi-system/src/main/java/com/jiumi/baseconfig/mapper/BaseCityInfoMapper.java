package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCityInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 省市区Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseCityInfoMapper
{
    /**
     * 查询省市区
     *
     * @param id 省市区主键
     * @return 省市区
     */
    public BaseCityInfo selectBaseCityInfoById(Long id);

    /**
     * 查询省市区列表
     *
     * @param baseCityInfo 省市区
     * @return 省市区集合
     */
    public List<BaseCityInfo> selectBaseCityInfoList(BaseCityInfo baseCityInfo);

    /**
     * 新增省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    public int insertBaseCityInfo(BaseCityInfo baseCityInfo);

    /**
     * 修改省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    public int updateBaseCityInfo(BaseCityInfo baseCityInfo);

    /**
     * 删除省市区
     *
     * @param id 省市区主键
     * @return 结果
     */
    public int deleteBaseCityInfoById(Long id);

    /**
     * 批量删除省市区
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseCityInfoByIds(Long[] ids);

    BaseCityInfo selectBaseCityInfoByCode(@Param("code") String code);
}

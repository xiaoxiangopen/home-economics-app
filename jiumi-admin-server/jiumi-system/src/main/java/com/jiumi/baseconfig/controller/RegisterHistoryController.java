package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.RegisterHistory;
import com.jiumi.baseconfig.service.IRegisterHistoryService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户记录Controller
 *
 * @author jiumi
 * @date 2023-09-04
 */
@RestController
@RequestMapping("/baseconfig/registerhistory")
public class RegisterHistoryController extends BaseController
{
    @Autowired
    private IRegisterHistoryService registerHistoryService;

    /**
     * 查询用户记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(RegisterHistory registerHistory)
    {
        startPage();
        List<RegisterHistory> list = registerHistoryService.selectRegisterHistoryList(registerHistory);
        return getDataTable(list);
    }

    /**
     * 导出用户记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:export')")
    @Log(title = "用户记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RegisterHistory registerHistory)
    {
        List<RegisterHistory> list = registerHistoryService.selectRegisterHistoryList(registerHistory);
        ExcelUtil<RegisterHistory> util = new ExcelUtil<RegisterHistory>(RegisterHistory.class);
        util.exportExcel(response, list, "用户记录数据");
    }

    /**
     * 获取用户记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(registerHistoryService.selectRegisterHistoryById(id));
    }

    /**
     * 新增用户记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:add')")
    @Log(title = "用户记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RegisterHistory registerHistory)
    {
        return toAjax(registerHistoryService.insertRegisterHistory(registerHistory));
    }

    /**
     * 修改用户记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:edit')")
    @Log(title = "用户记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RegisterHistory registerHistory)
    {
        return toAjax(registerHistoryService.updateRegisterHistory(registerHistory));
    }

    /**
     * 删除用户记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:registerhistory:remove')")
    @Log(title = "用户记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(registerHistoryService.deleteRegisterHistoryByIds(ids));
    }
}

package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCityInfo;
import com.jiumi.baseconfig.vo.TreeSelectVo;
/**
 * 省市区Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseCityInfoService
{
    /**
     * 查询省市区
     *
     * @param id 省市区主键
     * @return 省市区
     */
    public BaseCityInfo selectBaseCityInfoById(Long id);

    /**
     * 查询省市区列表
     *
     * @param baseCityInfo 省市区
     * @return 省市区集合
     */
    public List<BaseCityInfo> selectBaseCityInfoList(BaseCityInfo baseCityInfo);

    /**
     * 新增省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    public int insertBaseCityInfo(BaseCityInfo baseCityInfo);

    /**
     * 修改省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    public int updateBaseCityInfo(BaseCityInfo baseCityInfo);

    /**
     * 批量删除省市区
     *
     * @param ids 需要删除的省市区主键集合
     * @return 结果
     */
    public int deleteBaseCityInfoByIds(Long[] ids);

    /**
     * 删除省市区信息
     *
     * @param id 省市区主键
     * @return 结果
     */
    public int deleteBaseCityInfoById(Long id);

    List<TreeSelectVo> buildDeptTreeSelect(List<BaseCityInfo> selectBaseCityInfoList);

    BaseCityInfo selectBaseCityInfoByCode(String province);
}

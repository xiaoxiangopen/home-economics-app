package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 轮播图对象 base_banner
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类01引导页02banner */
    @Excel(name = "分类01引导页02banner")
    private String categoryType;

    /** 图片 */
    @Excel(name = "图片")
    private String imageUrl;

    /** 指向类型01雇主02阿姨03资讯公告 */
    @Excel(name = "指向类型01雇主02阿姨03资讯公告")
    private String linkType;

    /** 文章id */
    private String itemId;

    /** 指向名称 */
    @Excel(name = "指向名称")
    private String itemName;

    /** 序号 */
    @Excel(name = "序号")
    private Long sort;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCategoryType(String categoryType)
    {
        this.categoryType = categoryType;
    }

    public String getCategoryType()
    {
        return categoryType;
    }
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }
    public void setLinkType(String linkType)
    {
        this.linkType = linkType;
    }

    public String getLinkType()
    {
        return linkType;
    }
    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public String getItemId()
    {
        return itemId;
    }
    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemName()
    {
        return itemName;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryType", getCategoryType())
            .append("imageUrl", getImageUrl())
            .append("linkType", getLinkType())
            .append("itemId", getItemId())
            .append("itemName", getItemName())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}

package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 求职意向城市对象 base_user_intention_city
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUserIntentionCity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 求职意向id */
    @Excel(name = "求职意向id")
    private Long intentionId;

    /** 省份 */
    @Excel(name = "省份")
    private String province;

    /** 城市 */
    @Excel(name = "城市")
    private String city;

    /** 区域 */
    @Excel(name = "区域")
    private String area;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setIntentionId(Long intentionId)
    {
        this.intentionId = intentionId;
    }

    public Long getIntentionId()
    {
        return intentionId;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCity()
    {
        return city;
    }
    public void setArea(String area)
    {
        this.area = area;
    }

    public String getArea()
    {
        return area;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("intentionId", getIntentionId())
            .append("province", getProvince())
            .append("city", getCity())
            .append("area", getArea())
            .toString();
    }
}

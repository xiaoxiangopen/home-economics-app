package com.jiumi.baseconfig.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.jiumi.baseconfig.vo.TreeSelectVo;
import com.jiumi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseCityInfoMapper;
import com.jiumi.baseconfig.domain.BaseCityInfo;
import com.jiumi.baseconfig.service.IBaseCityInfoService;

/**
 * 省市区Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseCityInfoServiceImpl implements IBaseCityInfoService
{
    @Autowired
    private BaseCityInfoMapper baseCityInfoMapper;

    /**
     * 查询省市区
     *
     * @param id 省市区主键
     * @return 省市区
     */
    @Override
    public BaseCityInfo selectBaseCityInfoById(Long id)
    {
        return baseCityInfoMapper.selectBaseCityInfoById(id);
    }

    /**
     * 查询省市区列表
     *
     * @param baseCityInfo 省市区
     * @return 省市区
     */
    @Override
    public List<BaseCityInfo> selectBaseCityInfoList(BaseCityInfo baseCityInfo)
    {
        return baseCityInfoMapper.selectBaseCityInfoList(baseCityInfo);
    }

    /**
     * 新增省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    @Override
    public int insertBaseCityInfo(BaseCityInfo baseCityInfo)
    {
        return baseCityInfoMapper.insertBaseCityInfo(baseCityInfo);
    }

    /**
     * 修改省市区
     *
     * @param baseCityInfo 省市区
     * @return 结果
     */
    @Override
    public int updateBaseCityInfo(BaseCityInfo baseCityInfo)
    {
        return baseCityInfoMapper.updateBaseCityInfo(baseCityInfo);
    }

    /**
     * 批量删除省市区
     *
     * @param ids 需要删除的省市区主键
     * @return 结果
     */
    @Override
    public int deleteBaseCityInfoByIds(Long[] ids)
    {
        return baseCityInfoMapper.deleteBaseCityInfoByIds(ids);
    }

    /**
     * 删除省市区信息
     *
     * @param id 省市区主键
     * @return 结果
     */
    @Override
    public int deleteBaseCityInfoById(Long id)
    {
        return baseCityInfoMapper.deleteBaseCityInfoById(id);
    }

    public List<BaseCityInfo> buildTree(List<BaseCityInfo> cityInfoList) {
        List<BaseCityInfo> returnList = new ArrayList<BaseCityInfo>();
        List<String> tempList = new ArrayList<String>();
        for (BaseCityInfo city : cityInfoList)
        {
            tempList.add(city.getCode());
        }
        for (BaseCityInfo city : cityInfoList)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(city.getParentCode()))
            {
                recursionFn(cityInfoList, city);
                returnList.add(city);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = cityInfoList;
        }
        return returnList;
    }

    @Override
    public List<TreeSelectVo> buildDeptTreeSelect(List<BaseCityInfo> cityInfoList) {
        List<BaseCityInfo> citysTree = buildTree(cityInfoList);
        return citysTree.stream().map(TreeSelectVo::new).collect(Collectors.toList());
    }

    @Override
    public BaseCityInfo selectBaseCityInfoByCode(String province) {
        return baseCityInfoMapper.selectBaseCityInfoByCode(province);
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<BaseCityInfo> list, BaseCityInfo t)
    {
        // 得到子节点列表
        List<BaseCityInfo> childList = getChildList(list, t);
        t.setChildren(childList);
        for (BaseCityInfo tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<BaseCityInfo> getChildList(List<BaseCityInfo> list, BaseCityInfo t)
    {
        List<BaseCityInfo> tlist = new ArrayList<BaseCityInfo>();
        Iterator<BaseCityInfo> it = list.iterator();
        while (it.hasNext())
        {
            BaseCityInfo n = (BaseCityInfo) it.next();
            if (StringUtils.isNotNull(n.getParentCode()) && n.getParentCode().equals( t.getCode() ))
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<BaseCityInfo> list, BaseCityInfo t)
    {
        return getChildList(list, t).size() > 0;
    }
}

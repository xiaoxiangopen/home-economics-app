package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseFeedback;

/**
 * 意见反馈Service接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseFeedbackService 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public BaseFeedback selectBaseFeedbackById(Long id);

    /**
     * 查询意见反馈列表
     * 
     * @param baseFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<BaseFeedback> selectBaseFeedbackList(BaseFeedback baseFeedback);

    /**
     * 新增意见反馈
     * 
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    public int insertBaseFeedback(BaseFeedback baseFeedback);

    /**
     * 修改意见反馈
     * 
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    public int updateBaseFeedback(BaseFeedback baseFeedback);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键集合
     * @return 结果
     */
    public int deleteBaseFeedbackByIds(Long[] ids);

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteBaseFeedbackById(Long id);
}

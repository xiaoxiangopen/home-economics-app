package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 合单记录对象 base_order_combine
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseOrderCombine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    private Long userId;

    @Excel(name = "发布人")
    private String publishUserName;

    /** 合单类型01阿姨找雇主02雇主找阿姨 */
    @Excel(name = "合单类型",dictType = "base_combine_type")
    private String type;

    @Excel(name = "预约类型")
    private String serviceType;


    /** 发布人ID */

    private Long publishUserId;

    private Long appointmentId;



    /** 阿姨方家政公司ID */
    private Long auntCompanyId;

    /** 阿姨ID */
    private Long auntUserId;

    private String auntType;

    /** 雇主家政公司ID */

    private Long employCompanyId;

    /** 雇主ID */
    private Long employUserId;

    //阿姨名称
    @Excel(name = "阿姨姓名")
    private String auntName;
    //阿姨头像
    private String auntAvatar;
    //期望工作类型
    private String auntTypeName;
    //工作经验
    private String auntWorkInfo;
    //年龄
    private String auntAge;
    //籍贯
    private String auntNative;
    //公司名称
    @Excel(name = "阿姨方家政公司")
    private String auntCompanyName;
    //最高工资
    private String auntHighSalary;
    //最低工资
    private String auntLowSalary;

    //需要阿姨类型
    @Excel(name = "雇主姓名")
    private String employName;
    @Excel(name = "雇主家政公司")
    private String employCompanyName;
    private String employType;
    //住家类型
    private String employWorkType;
    //休息天数
    private String employWorkDay;

    @Excel(name = "联系人姓名")
    private String userName;

    @Excel(name = "联系人电话")
    private String userPhone;

    @Excel(name = "联系人地址")
    private String userAddress;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date applyTime;

    /** 申请状态 */
    @Excel(name = "合单状态",dictType = "base_combine_status")
    private String applyStatus;

    /** 确认时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date confirmTime;



    private String isAdmin;



    public String getPublishUserName() {
        return publishUserName;
    }

    public void setPublishUserName(String publishUserName) {
        this.publishUserName = publishUserName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmployName() {
        return employName;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public String getEmployCompanyName() {
        return employCompanyName;
    }

    public void setEmployCompanyName(String employCompanyName) {
        this.employCompanyName = employCompanyName;
    }

    public String getAuntName() {
        return auntName;
    }

    public void setAuntName(String auntName) {
        this.auntName = auntName;
    }

    public String getAuntAvatar() {
        return auntAvatar;
    }

    public void setAuntAvatar(String auntAvatar) {
        this.auntAvatar = auntAvatar;
    }

    public String getAuntTypeName() {
        return auntTypeName;
    }

    public void setAuntTypeName(String auntTypeName) {
        this.auntTypeName = auntTypeName;
    }

    public String getAuntWorkInfo() {
        return auntWorkInfo;
    }

    public void setAuntWorkInfo(String auntWorkInfo) {
        this.auntWorkInfo = auntWorkInfo;
    }

    public String getAuntAge() {
        return auntAge;
    }

    public void setAuntAge(String auntAge) {
        this.auntAge = auntAge;
    }

    public String getAuntNative() {
        return auntNative;
    }

    public void setAuntNative(String auntNative) {
        this.auntNative = auntNative;
    }

    public String getAuntCompanyName() {
        return auntCompanyName;
    }

    public void setAuntCompanyName(String auntCompanyName) {
        this.auntCompanyName = auntCompanyName;
    }

    public String getAuntHighSalary() {
        return auntHighSalary;
    }

    public void setAuntHighSalary(String auntHighSalary) {
        this.auntHighSalary = auntHighSalary;
    }

    public String getAuntLowSalary() {
        return auntLowSalary;
    }

    public void setAuntLowSalary(String auntLowSalary) {
        this.auntLowSalary = auntLowSalary;
    }

    public String getEmployType() {
        return employType;
    }

    public void setEmployType(String employType) {
        this.employType = employType;
    }

    public String getEmployWorkType() {
        return employWorkType;
    }

    public void setEmployWorkType(String employWorkType) {
        this.employWorkType = employWorkType;
    }

    public String getEmployWorkDay() {
        return employWorkDay;
    }

    public void setEmployWorkDay(String employWorkDay) {
        this.employWorkDay = employWorkDay;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getAuntType() {
        return auntType;
    }

    public void setAuntType(String auntType) {
        this.auntType = auntType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPublishUserId(Long publishUserId)
    {
        this.publishUserId = publishUserId;
    }

    public Long getPublishUserId()
    {
        return publishUserId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setAuntCompanyId(Long auntCompanyId)
    {
        this.auntCompanyId = auntCompanyId;
    }

    public Long getAuntCompanyId()
    {
        return auntCompanyId;
    }
    public void setAuntUserId(Long auntUserId)
    {
        this.auntUserId = auntUserId;
    }

    public Long getAuntUserId()
    {
        return auntUserId;
    }
    public void setEmployCompanyId(Long employCompanyId)
    {
        this.employCompanyId = employCompanyId;
    }

    public Long getEmployCompanyId()
    {
        return employCompanyId;
    }
    public void setEmployUserId(Long employUserId)
    {
        this.employUserId = employUserId;
    }

    public Long getEmployUserId()
    {
        return employUserId;
    }
    public void setApplyTime(Date applyTime)
    {
        this.applyTime = applyTime;
    }

    public Date getApplyTime()
    {
        return applyTime;
    }
    public void setApplyStatus(String applyStatus)
    {
        this.applyStatus = applyStatus;
    }

    public String getApplyStatus()
    {
        return applyStatus;
    }
    public void setConfirmTime(Date confirmTime)
    {
        this.confirmTime = confirmTime;
    }

    public Date getConfirmTime()
    {
        return confirmTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("publishUserId", getPublishUserId())
            .append("type", getType())
            .append("auntCompanyId", getAuntCompanyId())
            .append("auntUserId", getAuntUserId())
            .append("employCompanyId", getEmployCompanyId())
            .append("employUserId", getEmployUserId())
            .append("applyTime", getApplyTime())
            .append("applyStatus", getApplyStatus())
            .append("confirmTime", getConfirmTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

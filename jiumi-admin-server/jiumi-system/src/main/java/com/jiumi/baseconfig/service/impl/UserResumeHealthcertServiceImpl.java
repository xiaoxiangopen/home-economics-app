package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.UserResumeHealthcertMapper;
import com.jiumi.baseconfig.domain.UserResumeHealthcert;
import com.jiumi.baseconfig.service.IUserResumeHealthcertService;

/**
 * 健康证书Service业务层处理
 *
 * @author jiumi
 * @date 2023-06-15
 */
@Service
public class UserResumeHealthcertServiceImpl implements IUserResumeHealthcertService
{
    @Autowired
    private UserResumeHealthcertMapper userResumeHealthcertMapper;

    /**
     * 查询健康证书
     *
     * @param id 健康证书主键
     * @return 健康证书
     */
    @Override
    public UserResumeHealthcert selectUserResumeHealthcertById(Long id)
    {
        return userResumeHealthcertMapper.selectUserResumeHealthcertById(id);
    }

    /**
     * 查询健康证书列表
     *
     * @param userResumeHealthcert 健康证书
     * @return 健康证书
     */
    @Override
    public List<UserResumeHealthcert> selectUserResumeHealthcertList(UserResumeHealthcert userResumeHealthcert)
    {
        return userResumeHealthcertMapper.selectUserResumeHealthcertList(userResumeHealthcert);
    }

    /**
     * 新增健康证书
     *
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    @Override
    public int insertUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert)
    {
        return userResumeHealthcertMapper.insertUserResumeHealthcert(userResumeHealthcert);
    }

    /**
     * 修改健康证书
     *
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    @Override
    public int updateUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert)
    {
        return userResumeHealthcertMapper.updateUserResumeHealthcert(userResumeHealthcert);
    }

    /**
     * 批量删除健康证书
     *
     * @param ids 需要删除的健康证书主键
     * @return 结果
     */
    @Override
    public int deleteUserResumeHealthcertByIds(Long[] ids)
    {
        return userResumeHealthcertMapper.deleteUserResumeHealthcertByIds(ids);
    }

    /**
     * 删除健康证书信息
     *
     * @param id 健康证书主键
     * @return 结果
     */
    @Override
    public int deleteUserResumeHealthcertById(Long id)
    {
        return userResumeHealthcertMapper.deleteUserResumeHealthcertById(id);
    }
}

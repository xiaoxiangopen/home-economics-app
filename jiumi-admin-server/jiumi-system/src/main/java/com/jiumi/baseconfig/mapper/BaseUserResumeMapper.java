package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserResume;
import org.apache.ibatis.annotations.Param;

/**
 * 求职简历Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseUserResumeMapper
{
    /**
     * 查询求职简历
     *
     * @param id 求职简历主键
     * @return 求职简历
     */
    public BaseUserResume selectBaseUserResumeById(Long id);

    /**
     * 查询求职简历列表
     *
     * @param baseUserResume 求职简历
     * @return 求职简历集合
     */
    public List<BaseUserResume> selectBaseUserResumeList(BaseUserResume baseUserResume);

    /**
     * 新增求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    public int insertBaseUserResume(BaseUserResume baseUserResume);

    /**
     * 修改求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    public int updateBaseUserResume(BaseUserResume baseUserResume);

    /**
     * 删除求职简历
     *
     * @param id 求职简历主键
     * @return 结果
     */
    public int deleteBaseUserResumeById(Long id);

    /**
     * 批量删除求职简历
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeByIds(Long[] ids);

    BaseUserResume selectBaseUserResumeByUserId(@Param("userId") Long userId);

    List<BaseUserResume> selectAuntUserList(BaseUserResume user);

    List<BaseUserResume> selectUserFocusResumeList(BaseUserResume param);

    int deleteBaseUserResumeByUserId(@Param("userId") Long userId);

    void deleteBaseUserResumeByCertCode(@Param("certCode") String certCode);

    BaseUserResume selectBaseUserResumeByUserCertCode(@Param("certCode") String certCode);

    void removeBaseUserResumeById(Long id);

    int updateBaseUserResumeInfo(BaseUserResume resume);
}

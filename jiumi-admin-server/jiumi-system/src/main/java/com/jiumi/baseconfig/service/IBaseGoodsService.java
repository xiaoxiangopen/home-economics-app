package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseGoods;

/**
 * 积分商品Service接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseGoodsService 
{
    /**
     * 查询积分商品
     * 
     * @param id 积分商品主键
     * @return 积分商品
     */
    public BaseGoods selectBaseGoodsById(Long id);

    /**
     * 查询积分商品列表
     * 
     * @param baseGoods 积分商品
     * @return 积分商品集合
     */
    public List<BaseGoods> selectBaseGoodsList(BaseGoods baseGoods);

    /**
     * 新增积分商品
     * 
     * @param baseGoods 积分商品
     * @return 结果
     */
    public int insertBaseGoods(BaseGoods baseGoods);

    /**
     * 修改积分商品
     * 
     * @param baseGoods 积分商品
     * @return 结果
     */
    public int updateBaseGoods(BaseGoods baseGoods);

    /**
     * 批量删除积分商品
     * 
     * @param ids 需要删除的积分商品主键集合
     * @return 结果
     */
    public int deleteBaseGoodsByIds(Long[] ids);

    /**
     * 删除积分商品信息
     * 
     * @param id 积分商品主键
     * @return 结果
     */
    public int deleteBaseGoodsById(Long id);
}

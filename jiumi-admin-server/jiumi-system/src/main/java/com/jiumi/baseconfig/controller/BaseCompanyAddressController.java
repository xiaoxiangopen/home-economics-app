package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseCompanyAddress;
import com.jiumi.baseconfig.service.IBaseCompanyAddressService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 公司地址Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/address")
public class BaseCompanyAddressController extends BaseController
{
    @Autowired
    private IBaseCompanyAddressService baseCompanyAddressService;

    /**
     * 查询公司地址列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyAddress baseCompanyAddress)
    {
        startPage();
        List<BaseCompanyAddress> list = baseCompanyAddressService.selectBaseCompanyAddressList(baseCompanyAddress);
        return getDataTable(list);
    }

    /**
     * 导出公司地址列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:export')")
    @Log(title = "公司地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyAddress baseCompanyAddress)
    {
        List<BaseCompanyAddress> list = baseCompanyAddressService.selectBaseCompanyAddressList(baseCompanyAddress);
        ExcelUtil<BaseCompanyAddress> util = new ExcelUtil<BaseCompanyAddress>(BaseCompanyAddress.class);
        util.exportExcel(response, list, "公司地址数据");
    }

    /**
     * 获取公司地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseCompanyAddressService.selectBaseCompanyAddressById(id));
    }

    /**
     * 新增公司地址
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:add')")
    @Log(title = "公司地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyAddress baseCompanyAddress)
    {
        return toAjax(baseCompanyAddressService.insertBaseCompanyAddress(baseCompanyAddress));
    }

    /**
     * 修改公司地址
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:edit')")
    @Log(title = "公司地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyAddress baseCompanyAddress)
    {
        return toAjax(baseCompanyAddressService.updateBaseCompanyAddress(baseCompanyAddress));
    }

    /**
     * 删除公司地址
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:remove')")
    @Log(title = "公司地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyAddressService.deleteBaseCompanyAddressByIds(ids));
    }
}

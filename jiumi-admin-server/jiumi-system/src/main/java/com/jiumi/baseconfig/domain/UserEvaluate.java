package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 阿姨评价对象 base_user_evaluate
 *
 * @author jiumi
 * @date 2023-01-13
 */
public class UserEvaluate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 评价类型01雇主02中介 */
    @Excel(name = "评价类型01雇主02中介")
    private String type;

    /** 评价人ID */
    @Excel(name = "评价人ID")
    private Long evaluateUserId;

    /** 评价人姓名 */
    @Excel(name = "评价人姓名")
    private String evaluateUserName;

    /** 合同ID */
    @Excel(name = "合同ID")
    private Long contractId;

    /** 星级 */
    @Excel(name = "星级")
    private Integer scoreNum;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String content;

    private String evaluateUserAvatar;

    public String getEvaluateUserAvatar() {
        return evaluateUserAvatar;
    }

    public void setEvaluateUserAvatar(String evaluateUserAvatar) {
        this.evaluateUserAvatar = evaluateUserAvatar;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }


    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setEvaluateUserId(Long evaluateUserId)
    {
        this.evaluateUserId = evaluateUserId;
    }

    public Long getEvaluateUserId()
    {
        return evaluateUserId;
    }
    public void setEvaluateUserName(String evaluateUserName)
    {
        this.evaluateUserName = evaluateUserName;
    }

    public String getEvaluateUserName()
    {
        return evaluateUserName;
    }
    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    @NotNull(message = "合同不能为空")
    public Long getContractId()
    {
        return contractId;
    }
    public void setScoreNum(Integer scoreNum)
    {
        this.scoreNum = scoreNum;
    }

    public Integer getScoreNum()
    {
        return scoreNum;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    @NotBlank(message = "评价内容不能为空")
    public String getContent()
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("type", getType())
            .append("evaluateUserId", getEvaluateUserId())
            .append("evaluateUserName", getEvaluateUserName())
            .append("contractId", getContractId())
            .append("scoreNum", getScoreNum())
            .append("content", getContent())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}

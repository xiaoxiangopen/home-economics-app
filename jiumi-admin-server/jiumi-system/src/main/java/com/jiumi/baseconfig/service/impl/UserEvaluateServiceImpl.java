package com.jiumi.baseconfig.service.impl;

import java.util.List;
import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.UserEvaluateMapper;
import com.jiumi.baseconfig.domain.UserEvaluate;
import com.jiumi.baseconfig.service.IUserEvaluateService;

/**
 * 阿姨评价Service业务层处理
 *
 * @author jiumi
 * @date 2023-01-13
 */
@Service
public class UserEvaluateServiceImpl implements IUserEvaluateService
{
    @Autowired
    private UserEvaluateMapper userEvaluateMapper;

    /**
     * 查询阿姨评价
     *
     * @param id 阿姨评价主键
     * @return 阿姨评价
     */
    @Override
    public UserEvaluate selectUserEvaluateById(Long id)
    {
        return userEvaluateMapper.selectUserEvaluateById(id);
    }

    /**
     * 查询阿姨评价列表
     *
     * @param userEvaluate 阿姨评价
     * @return 阿姨评价
     */
    @Override
    public List<UserEvaluate> selectUserEvaluateList(UserEvaluate userEvaluate)
    {
        return userEvaluateMapper.selectUserEvaluateList(userEvaluate);
    }

    /**
     * 新增阿姨评价
     *
     * @param userEvaluate 阿姨评价
     * @return 结果
     */
    @Override
    public int insertUserEvaluate(UserEvaluate userEvaluate)
    {
        userEvaluate.setCreateTime(DateUtils.getNowDate());
        return userEvaluateMapper.insertUserEvaluate(userEvaluate);
    }

    /**
     * 修改阿姨评价
     *
     * @param userEvaluate 阿姨评价
     * @return 结果
     */
    @Override
    public int updateUserEvaluate(UserEvaluate userEvaluate)
    {
        return userEvaluateMapper.updateUserEvaluate(userEvaluate);
    }

    /**
     * 批量删除阿姨评价
     *
     * @param ids 需要删除的阿姨评价主键
     * @return 结果
     */
    @Override
    public int deleteUserEvaluateByIds(Long[] ids)
    {
        return userEvaluateMapper.deleteUserEvaluateByIds(ids);
    }

    /**
     * 删除阿姨评价信息
     *
     * @param id 阿姨评价主键
     * @return 结果
     */
    @Override
    public int deleteUserEvaluateById(Long id)
    {
        return userEvaluateMapper.deleteUserEvaluateById(id);
    }
}

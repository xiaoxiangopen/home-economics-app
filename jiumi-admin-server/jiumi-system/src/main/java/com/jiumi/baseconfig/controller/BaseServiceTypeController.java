package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseServiceType;
import com.jiumi.baseconfig.service.IBaseServiceTypeService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 服务项目Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/servicetype")
public class BaseServiceTypeController extends BaseController
{
    @Autowired
    private IBaseServiceTypeService baseServiceTypeService;

    /**
     * 查询服务项目列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseServiceType baseServiceType)
    {
        startPage();
        List<BaseServiceType> list = baseServiceTypeService.selectBaseServiceTypeList(baseServiceType);
        return getDataTable(list);
    }

    @GetMapping("/getAllServiceType")
    public AjaxResult getAllServiceType(BaseServiceType baseServiceType)
    {
        List<BaseServiceType> list = baseServiceTypeService.selectBaseServiceTypeList(baseServiceType);
        return AjaxResult.success(list);
    }

    /**
     * 导出服务项目列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:export')")
    @Log(title = "服务项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseServiceType baseServiceType)
    {
        List<BaseServiceType> list = baseServiceTypeService.selectBaseServiceTypeList(baseServiceType);
        ExcelUtil<BaseServiceType> util = new ExcelUtil<BaseServiceType>(BaseServiceType.class);
        util.exportExcel(response, list, "服务项目数据");
    }

    /**
     * 获取服务项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseServiceTypeService.selectBaseServiceTypeById(id));
    }

    /**
     * 新增服务项目
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:add')")
    @Log(title = "服务项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseServiceType baseServiceType)
    {
        return toAjax(baseServiceTypeService.insertBaseServiceType(baseServiceType));
    }

    /**
     * 修改服务项目
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:edit')")
    @Log(title = "服务项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseServiceType baseServiceType)
    {
        return toAjax(baseServiceTypeService.updateBaseServiceType(baseServiceType));
    }

    /**
     * 删除服务项目
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:servicetype:remove')")
    @Log(title = "服务项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseServiceTypeService.deleteBaseServiceTypeByIds(ids));
    }
}

package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.ScoreHistory;

/**
 * 积分记录信息Service接口
 *
 * @author jiumi
 * @date 2022-12-08
 */
public interface IScoreHistoryService
{
    /**
     * 查询积分记录信息
     *
     * @param id 积分记录信息主键
     * @return 积分记录信息
     */
    public ScoreHistory selectScoreHistoryById(Long id);

    /**
     * 查询积分记录信息列表
     *
     * @param scoreHistory 积分记录信息
     * @return 积分记录信息集合
     */
    public List<ScoreHistory> selectScoreHistoryList(ScoreHistory scoreHistory);

    /**
     * 新增积分记录信息
     *
     * @param scoreHistory 积分记录信息
     * @return 结果
     */
    public int insertScoreHistory(ScoreHistory scoreHistory);

    /**
     * 修改积分记录信息
     *
     * @param scoreHistory 积分记录信息
     * @return 结果
     */
    public int updateScoreHistory(ScoreHistory scoreHistory);

    /**
     * 批量删除积分记录信息
     *
     * @param ids 需要删除的积分记录信息主键集合
     * @return 结果
     */
    public int deleteScoreHistoryByIds(Long[] ids);

    /**
     * 删除积分记录信息信息
     *
     * @param id 积分记录信息主键
     * @return 结果
     */
    public int deleteScoreHistoryById(Long id);

    void useScore(Long userId,String userName, String remark, Long price);
}

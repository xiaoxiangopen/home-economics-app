package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 用户邀请对象 base_user_invite_detail
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUserInviteDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 邀请人用户ID */
    @Excel(name = "邀请人用户ID")
    private Long inviteUserId;

    /** 邀请人姓名 */
    @Excel(name = "邀请人姓名")
    private String inviteName;

    /** 邀请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "邀请时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inviteTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setInviteUserId(Long inviteUserId)
    {
        this.inviteUserId = inviteUserId;
    }

    public Long getInviteUserId()
    {
        return inviteUserId;
    }
    public void setInviteName(String inviteName)
    {
        this.inviteName = inviteName;
    }

    public String getInviteName()
    {
        return inviteName;
    }
    public void setInviteTime(Date inviteTime)
    {
        this.inviteTime = inviteTime;
    }

    public Date getInviteTime()
    {
        return inviteTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("inviteUserId", getInviteUserId())
            .append("inviteName", getInviteName())
            .append("inviteTime", getInviteTime())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}

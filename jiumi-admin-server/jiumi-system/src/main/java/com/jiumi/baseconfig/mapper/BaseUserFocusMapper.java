package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseUserFocus;
import org.apache.ibatis.annotations.Param;

/**
 * 用户关注Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseUserFocusMapper
{
    /**
     * 查询用户关注
     *
     * @param id 用户关注主键
     * @return 用户关注
     */
    public BaseUserFocus selectBaseUserFocusById(Long id);

    /**
     * 查询用户关注列表
     *
     * @param baseUserFocus 用户关注
     * @return 用户关注集合
     */
    public List<BaseUserFocus> selectBaseUserFocusList(BaseUserFocus baseUserFocus);

    /**
     * 新增用户关注
     *
     * @param baseUserFocus 用户关注
     * @return 结果
     */
    public int insertBaseUserFocus(BaseUserFocus baseUserFocus);

    /**
     * 修改用户关注
     *
     * @param baseUserFocus 用户关注
     * @return 结果
     */
    public int updateBaseUserFocus(BaseUserFocus baseUserFocus);

    /**
     * 删除用户关注
     *
     * @param id 用户关注主键
     * @return 结果
     */
    public int deleteBaseUserFocusById(Long id);

    /**
     * 批量删除用户关注
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserFocusByIds(Long[] ids);

    void deleteBaseUserFocusByUserId(@Param("userId") Long userId);
}

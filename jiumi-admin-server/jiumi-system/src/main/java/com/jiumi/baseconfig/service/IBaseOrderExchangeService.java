package com.jiumi.baseconfig.service;

import java.util.List;

import com.jiumi.baseconfig.domain.BaseGoods;
import com.jiumi.baseconfig.domain.BaseOrderExchange;
import com.jiumi.baseconfig.domain.BaseUser;

/**
 * 用户兑换奖品记录Service接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface IBaseOrderExchangeService
{
    /**
     * 查询用户兑换奖品记录
     *
     * @param id 用户兑换奖品记录主键
     * @return 用户兑换奖品记录
     */
    public BaseOrderExchange selectBaseOrderExchangeById(Long id);

    /**
     * 查询用户兑换奖品记录列表
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 用户兑换奖品记录集合
     */
    public List<BaseOrderExchange> selectBaseOrderExchangeList(BaseOrderExchange baseOrderExchange);

    /**
     * 新增用户兑换奖品记录
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    public int insertBaseOrderExchange(BaseOrderExchange baseOrderExchange);

    /**
     * 修改用户兑换奖品记录
     *
     * @param baseOrderExchange 用户兑换奖品记录
     * @return 结果
     */
    public int updateBaseOrderExchange(BaseOrderExchange baseOrderExchange);

    /**
     * 批量删除用户兑换奖品记录
     *
     * @param ids 需要删除的用户兑换奖品记录主键集合
     * @return 结果
     */
    public int deleteBaseOrderExchangeByIds(Long[] ids);

    /**
     * 删除用户兑换奖品记录信息
     *
     * @param id 用户兑换奖品记录主键
     * @return 结果
     */
    public int deleteBaseOrderExchangeById(Long id);

    int userExchangeGoods(BaseGoods goods,int goodsNum, BaseUser currentUser, String userName, String phone, String address);
}

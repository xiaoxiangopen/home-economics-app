package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.ContractContent;
import com.jiumi.baseconfig.service.IContractContentService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 合同内容Controller
 *
 * @author jiumi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/baseconfig/contractContent")
public class ContractContentController extends BaseController
{
    @Autowired
    private IContractContentService contractContentService;

    /**
     * 查询合同内容列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:list')")
    @GetMapping("/list")
    public TableDataInfo list(ContractContent contractContent)
    {
        startPage();
        List<ContractContent> list = contractContentService.selectContractContentList(contractContent);
        return getDataTable(list);
    }

    /**
     * 导出合同内容列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:export')")
    @Log(title = "合同内容", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ContractContent contractContent)
    {
        List<ContractContent> list = contractContentService.selectContractContentList(contractContent);
        ExcelUtil<ContractContent> util = new ExcelUtil<ContractContent>(ContractContent.class);
        util.exportExcel(response, list, "合同内容数据");
    }

    /**
     * 获取合同内容详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(contractContentService.selectContractContentById(id));
    }

    /**
     * 新增合同内容
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:add')")
    @Log(title = "合同内容", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ContractContent contractContent)
    {
        return toAjax(contractContentService.insertContractContent(contractContent));
    }

    /**
     * 修改合同内容
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:edit')")
    @Log(title = "合同内容", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ContractContent contractContent)
    {
        return toAjax(contractContentService.updateContractContent(contractContent));
    }

    /**
     * 删除合同内容
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contractContent:remove')")
    @Log(title = "合同内容", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(contractContentService.deleteContractContentByIds(ids));
    }
}

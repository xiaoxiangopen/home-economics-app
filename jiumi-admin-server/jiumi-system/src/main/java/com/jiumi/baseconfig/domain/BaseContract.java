package com.jiumi.baseconfig.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 合同管理对象 base_contract
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    private Long createUserId;

    /** 合同类型01家政协议02家政协议三方03月嫂协议04月嫂协议三方 */
    @Excel(name = "合同类型01家政协议02家政协议三方03月嫂协议04月嫂协议三方")
    private String categoryType;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractCode;

    /** 甲方用户ID */
    private Long auserId;

    /** 甲姓名 */
    @Excel(name = "甲姓名")
    private String auserName;

    /** 甲方身份证号 */
    private String acertCode;

    /** 甲方手机号 */
    private String auserPhone;

    /** 甲方住址 */
    private String auserAddress;

    /** 已方用户ID */
    private Long buserId;

    /** 乙姓名 */
    @Excel(name = "乙姓名")
    private String buserName;

    /** 乙方身份证号 */
    private String bcertCode;

    /** 乙方手机号 */
    private String buserPhone;

    /** 乙方住址 */
    private String buserAddress;
    private String bcontactName;
    private String bcontactPhone;

    /** 丙方家政公司ID */
    private Long ccompanyId;

    /** 丙方公司 */
    @Excel(name = "丙方公司")
    private String ccompanyName;
    private String ccompanyPhone;

    /** 丙方用户ID */
    private Long cuserId;

    /** 丙方手机号 */
    private String cuserPhone;

    /** 丙方住址 */
    private String cuserAddress;

    /** 签约状态01待雇主签字02待阿姨签字03待确认04生效中05已结束06驳回07废弃 */
    @Excel(name = "签约状态01待雇主签字02待阿姨签字03待确认04生效中05已结束06驳回07废弃")
    private String status;

    /** 是否住家01住家02不住家 */
    private String liveType;

    /** 工作时间 */
    private String workTime;

    /** 服务类型：01 一般家务 02育儿主带03育儿辅带04 老人 05病人 */
    private String serviceType;

    /** 性别 */
    private String babySex;

    /** 月份 */
    private String babyMonth;

    /** 身体情况：自理，半自理，不能自理 */
    private String babyHealty;

    /** 服务地址 */
    private String serviceAddress;

    /** 预产期 */
    private Date expecteDate;

    /** 服务开始日期 */
    private Date serviceStartDate;

    /** 乙方工资 */
    private BigDecimal salaryAmount;

    /** 每周工作天数 */
    private Long workDayWeek;

    /** 补充条款 */
    private String otherContent;

    /** 签订类型01按年02按月 */
    @Excel(name = "签订类型01按年02按月")
    private String signType;

    /** 签单费 */
    private BigDecimal signAmount;

    /** 服务费 01 年 02 月 */
    private BigDecimal serviceAmount;

    private int servicePayDate;

    private String insuranceContent;

    /** 保证金 */
    private BigDecimal depositAmount;

    /** 服务天数(月嫂用) */
    private Long serviceDays;

    /** 签订日期 */
    private Date signDate;

    /** 甲方签字 */
    private String asignatureUrl;

    /** 已方签字日期 */
    private Date asignatureDate;

    /** 甲方签字 */
    private String bsignatureUrl;

    /** 已方签字日期 */
    private Date bsignatureDate;

    /** 联系中介公司电话 */
    private String ccontactPhone;

    private Long confirmUserId;
    private Date confirmTime;

    private String payType;
    private String payStatus;

    private String confirmsealUrl;

    private String isAdmin;
    private String effectFlag;

    public String getEffectFlag() {
        return effectFlag;
    }

    public void setEffectFlag(String effectFlag) {
        this.effectFlag = effectFlag;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getConfirmsealUrl() {
        return confirmsealUrl;
    }

    public void setConfirmsealUrl(String confirmsealUrl) {
        this.confirmsealUrl = confirmsealUrl;
    }

    public int getServicePayDate() {
        return servicePayDate;
    }

    public void setServicePayDate(int servicePayDate) {
        this.servicePayDate = servicePayDate;
    }

    public String getInsuranceContent() {
        return insuranceContent;
    }

    public void setInsuranceContent(String insuranceContent) {
        this.insuranceContent = insuranceContent;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getConfirmUserId() {
        return confirmUserId;
    }

    public void setConfirmUserId(Long confirmUserId) {
        this.confirmUserId = confirmUserId;
    }

    public Date getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getCcompanyPhone() {
        return ccompanyPhone;
    }

    public void setCcompanyPhone(String ccompanyPhone) {
        this.ccompanyPhone = ccompanyPhone;
    }

    public String getBcontactName() {
        return bcontactName;
    }

    public void setBcontactName(String bcontactName) {
        this.bcontactName = bcontactName;
    }

    public String getBcontactPhone() {
        return bcontactPhone;
    }

    public void setBcontactPhone(String bcontactPhone) {
        this.bcontactPhone = bcontactPhone;
    }

    /** 合同生效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同生效时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectDate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCategoryType(String categoryType)
    {
        this.categoryType = categoryType;
    }

    public String getCategoryType()
    {
        return categoryType;
    }
    public void setContractCode(String contractCode)
    {
        this.contractCode = contractCode;
    }

    public String getContractCode()
    {
        return contractCode;
    }
    public void setAuserId(Long auserId)
    {
        this.auserId = auserId;
    }

    public Long getAuserId()
    {
        return auserId;
    }
    public void setAuserName(String auserName)
    {
        this.auserName = auserName;
    }

    public String getAuserName()
    {
        return auserName;
    }
    public void setAcertCode(String acertCode)
    {
        this.acertCode = acertCode;
    }

    public String getAcertCode()
    {
        return acertCode;
    }
    public void setAuserPhone(String auserPhone)
    {
        this.auserPhone = auserPhone;
    }

    public String getAuserPhone()
    {
        return auserPhone;
    }
    public void setAuserAddress(String auserAddress)
    {
        this.auserAddress = auserAddress;
    }

    public String getAuserAddress()
    {
        return auserAddress;
    }
    public void setBuserId(Long buserId)
    {
        this.buserId = buserId;
    }

    public Long getBuserId()
    {
        return buserId;
    }
    public void setBuserName(String buserName)
    {
        this.buserName = buserName;
    }

    public String getBuserName()
    {
        return buserName;
    }
    public void setBcertCode(String bcertCode)
    {
        this.bcertCode = bcertCode;
    }

    public String getBcertCode()
    {
        return bcertCode;
    }
    public void setBuserPhone(String buserPhone)
    {
        this.buserPhone = buserPhone;
    }

    public String getBuserPhone()
    {
        return buserPhone;
    }
    public void setBuserAddress(String buserAddress)
    {
        this.buserAddress = buserAddress;
    }

    public String getBuserAddress()
    {
        return buserAddress;
    }
    public void setCcompanyId(Long ccompanyId)
    {
        this.ccompanyId = ccompanyId;
    }

    public Long getCcompanyId()
    {
        return ccompanyId;
    }
    public void setCcompanyName(String ccompanyName)
    {
        this.ccompanyName = ccompanyName;
    }

    public String getCcompanyName()
    {
        return ccompanyName;
    }
    public void setCuserId(Long cuserId)
    {
        this.cuserId = cuserId;
    }

    public Long getCuserId()
    {
        return cuserId;
    }
    public void setCuserPhone(String cuserPhone)
    {
        this.cuserPhone = cuserPhone;
    }

    public String getCuserPhone()
    {
        return cuserPhone;
    }
    public void setCuserAddress(String cuserAddress)
    {
        this.cuserAddress = cuserAddress;
    }

    public String getCuserAddress()
    {
        return cuserAddress;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setLiveType(String liveType)
    {
        this.liveType = liveType;
    }

    public String getLiveType()
    {
        return liveType;
    }
    public void setWorkTime(String workTime)
    {
        this.workTime = workTime;
    }

    public String getWorkTime()
    {
        return workTime;
    }
    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public String getServiceType()
    {
        return serviceType;
    }
    public void setBabySex(String babySex)
    {
        this.babySex = babySex;
    }

    public String getBabySex()
    {
        return babySex;
    }
    public void setBabyMonth(String babyMonth)
    {
        this.babyMonth = babyMonth;
    }

    public String getBabyMonth()
    {
        return babyMonth;
    }
    public void setBabyHealty(String babyHealty)
    {
        this.babyHealty = babyHealty;
    }

    public String getBabyHealty()
    {
        return babyHealty;
    }
    public void setServiceAddress(String serviceAddress)
    {
        this.serviceAddress = serviceAddress;
    }

    public String getServiceAddress()
    {
        return serviceAddress;
    }
    public void setExpecteDate(Date expecteDate)
    {
        this.expecteDate = expecteDate;
    }

    public Date getExpecteDate()
    {
        return expecteDate;
    }
    public void setServiceStartDate(Date serviceStartDate)
    {
        this.serviceStartDate = serviceStartDate;
    }

    public Date getServiceStartDate()
    {
        return serviceStartDate;
    }
    public void setSalaryAmount(BigDecimal salaryAmount)
    {
        this.salaryAmount = salaryAmount;
    }

    public BigDecimal getSalaryAmount()
    {
        return salaryAmount;
    }
    public void setWorkDayWeek(Long workDayWeek)
    {
        this.workDayWeek = workDayWeek;
    }

    public Long getWorkDayWeek()
    {
        return workDayWeek;
    }
    public void setOtherContent(String otherContent)
    {
        this.otherContent = otherContent;
    }

    public String getOtherContent()
    {
        return otherContent;
    }
    public void setSignType(String signType)
    {
        this.signType = signType;
    }

    public String getSignType()
    {
        return signType;
    }
    public void setSignAmount(BigDecimal signAmount)
    {
        this.signAmount = signAmount;
    }

    public BigDecimal getSignAmount()
    {
        return signAmount;
    }
    public void setServiceAmount(BigDecimal serviceAmount)
    {
        this.serviceAmount = serviceAmount;
    }

    public BigDecimal getServiceAmount()
    {
        return serviceAmount;
    }
    public void setDepositAmount(BigDecimal depositAmount)
    {
        this.depositAmount = depositAmount;
    }

    public BigDecimal getDepositAmount()
    {
        return depositAmount;
    }
    public void setServiceDays(Long serviceDays)
    {
        this.serviceDays = serviceDays;
    }

    public Long getServiceDays()
    {
        return serviceDays;
    }
    public void setSignDate(Date signDate)
    {
        this.signDate = signDate;
    }

    public Date getSignDate()
    {
        return signDate;
    }
    public void setAsignatureUrl(String asignatureUrl)
    {
        this.asignatureUrl = asignatureUrl;
    }

    public String getAsignatureUrl()
    {
        return asignatureUrl;
    }
    public void setAsignatureDate(Date asignatureDate)
    {
        this.asignatureDate = asignatureDate;
    }

    public Date getAsignatureDate()
    {
        return asignatureDate;
    }
    public void setBsignatureUrl(String bsignatureUrl)
    {
        this.bsignatureUrl = bsignatureUrl;
    }

    public String getBsignatureUrl()
    {
        return bsignatureUrl;
    }
    public void setBsignatureDate(Date bsignatureDate)
    {
        this.bsignatureDate = bsignatureDate;
    }

    public Date getBsignatureDate()
    {
        return bsignatureDate;
    }
    public void setCcontactPhone(String ccontactPhone)
    {
        this.ccontactPhone = ccontactPhone;
    }

    public String getCcontactPhone()
    {
        return ccontactPhone;
    }
    public void setEffectDate(Date effectDate)
    {
        this.effectDate = effectDate;
    }

    public Date getEffectDate()
    {
        return effectDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryType", getCategoryType())
            .append("contractCode", getContractCode())
            .append("auserId", getAuserId())
            .append("auserName", getAuserName())
            .append("acertCode", getAcertCode())
            .append("auserPhone", getAuserPhone())
            .append("auserAddress", getAuserAddress())
            .append("buserId", getBuserId())
            .append("buserName", getBuserName())
            .append("bcertCode", getBcertCode())
            .append("buserPhone", getBuserPhone())
            .append("buserAddress", getBuserAddress())
            .append("ccompanyId", getCcompanyId())
            .append("ccompanyName", getCcompanyName())
            .append("cuserId", getCuserId())
            .append("cuserPhone", getCuserPhone())
            .append("cuserAddress", getCuserAddress())
            .append("status", getStatus())
            .append("liveType", getLiveType())
            .append("workTime", getWorkTime())
            .append("serviceType", getServiceType())
            .append("babySex", getBabySex())
            .append("babyMonth", getBabyMonth())
            .append("babyHealty", getBabyHealty())
            .append("serviceAddress", getServiceAddress())
            .append("expecteDate", getExpecteDate())
            .append("serviceStartDate", getServiceStartDate())
            .append("salaryAmount", getSalaryAmount())
            .append("workDayWeek", getWorkDayWeek())
            .append("otherContent", getOtherContent())
            .append("signType", getSignType())
            .append("signAmount", getSignAmount())
            .append("serviceAmount", getServiceAmount())
            .append("depositAmount", getDepositAmount())
            .append("serviceDays", getServiceDays())
            .append("signDate", getSignDate())
            .append("asignatureUrl", getAsignatureUrl())
            .append("asignatureDate", getAsignatureDate())
            .append("bsignatureUrl", getBsignatureUrl())
            .append("bsignatureDate", getBsignatureDate())
            .append("ccontactPhone", getCcontactPhone())
            .append("effectDate", getEffectDate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}

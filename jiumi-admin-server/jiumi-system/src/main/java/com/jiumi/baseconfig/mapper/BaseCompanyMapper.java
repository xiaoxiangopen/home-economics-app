package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseCompany;
import org.apache.ibatis.annotations.Param;

/**
 * 家政公司Mapper接口
 *
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseCompanyMapper
{
    /**
     * 查询家政公司
     *
     * @param id 家政公司主键
     * @return 家政公司
     */
    public BaseCompany selectBaseCompanyById(Long id);

    /**
     * 查询家政公司列表
     *
     * @param baseCompany 家政公司
     * @return 家政公司集合
     */
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany);

    /**
     * 新增家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    public int insertBaseCompany(BaseCompany baseCompany);

    /**
     * 修改家政公司
     *
     * @param baseCompany 家政公司
     * @return 结果
     */
    public int updateBaseCompany(BaseCompany baseCompany);

    /**
     * 删除家政公司
     *
     * @param id 家政公司主键
     * @return 结果
     */
    public int deleteBaseCompanyById(Long id);

    /**
     * 批量删除家政公司
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseCompanyByIds(Long[] ids);

    BaseCompany selectBaseCompanyByName(@Param("companyName") String companyName);

    BaseCompany selectBaseCompanyByUserId(@Param("userId")Long userId);

    BaseCompany selectBaseCompanyByLienceCode(@Param("code")String code);
}

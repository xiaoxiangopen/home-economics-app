package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.UserResumeHealthcert;

/**
 * 健康证书Mapper接口
 * 
 * @author jiumi
 * @date 2023-06-15
 */
public interface UserResumeHealthcertMapper 
{
    /**
     * 查询健康证书
     * 
     * @param id 健康证书主键
     * @return 健康证书
     */
    public UserResumeHealthcert selectUserResumeHealthcertById(Long id);

    /**
     * 查询健康证书列表
     * 
     * @param userResumeHealthcert 健康证书
     * @return 健康证书集合
     */
    public List<UserResumeHealthcert> selectUserResumeHealthcertList(UserResumeHealthcert userResumeHealthcert);

    /**
     * 新增健康证书
     * 
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    public int insertUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert);

    /**
     * 修改健康证书
     * 
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    public int updateUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert);

    /**
     * 删除健康证书
     * 
     * @param id 健康证书主键
     * @return 结果
     */
    public int deleteUserResumeHealthcertById(Long id);

    /**
     * 批量删除健康证书
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserResumeHealthcertByIds(Long[] ids);
}

package com.jiumi.baseconfig.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.jiumi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.ScoreHistoryMapper;
import com.jiumi.baseconfig.domain.ScoreHistory;
import com.jiumi.baseconfig.service.IScoreHistoryService;

/**
 * 积分记录信息Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-08
 */
@Service
public class ScoreHistoryServiceImpl implements IScoreHistoryService {
    @Autowired
    private ScoreHistoryMapper scoreHistoryMapper;

    /**
     * 查询积分记录信息
     *
     * @param id 积分记录信息主键
     * @return 积分记录信息
     */
    @Override
    public ScoreHistory selectScoreHistoryById(Long id) {
        return scoreHistoryMapper.selectScoreHistoryById(id);
    }

    /**
     * 查询积分记录信息列表
     *
     * @param scoreHistory 积分记录信息
     * @return 积分记录信息
     */
    @Override
    public List<ScoreHistory> selectScoreHistoryList(ScoreHistory scoreHistory) {
        return scoreHistoryMapper.selectScoreHistoryList(scoreHistory);
    }

    /**
     * 新增积分记录信息
     *
     * @param scoreHistory 积分记录信息
     * @return 结果
     */
    @Override
    public int insertScoreHistory(ScoreHistory scoreHistory) {
        scoreHistory.setCreateTime(DateUtils.getNowDate());
        return scoreHistoryMapper.insertScoreHistory(scoreHistory);
    }

    /**
     * 修改积分记录信息
     *
     * @param scoreHistory 积分记录信息
     * @return 结果
     */
    @Override
    public int updateScoreHistory(ScoreHistory scoreHistory) {
        return scoreHistoryMapper.updateScoreHistory(scoreHistory);
    }

    /**
     * 批量删除积分记录信息
     *
     * @param ids 需要删除的积分记录信息主键
     * @return 结果
     */
    @Override
    public int deleteScoreHistoryByIds(Long[] ids) {
        return scoreHistoryMapper.deleteScoreHistoryByIds(ids);
    }

    /**
     * 删除积分记录信息信息
     *
     * @param id 积分记录信息主键
     * @return 结果
     */
    @Override
    public int deleteScoreHistoryById(Long id) {
        return scoreHistoryMapper.deleteScoreHistoryById(id);
    }

    @Override
    public void useScore(Long userId, String userName, String remark, Long price) {
        ScoreHistory sc = new ScoreHistory();
        sc.setUserId(userId);
        sc.setScore(new BigDecimal(price*-1));
        sc.setCreateTime(DateUtils.getNowDate());
        sc.setUserName(userName);
        sc.setRemark(remark);
        scoreHistoryMapper.insertScoreHistory(sc);
    }
}

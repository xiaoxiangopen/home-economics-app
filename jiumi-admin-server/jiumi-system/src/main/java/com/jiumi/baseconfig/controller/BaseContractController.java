package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseContract;
import com.jiumi.baseconfig.service.IBaseContractService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 合同管理Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/contract")
public class BaseContractController extends BaseController
{
    @Autowired
    private IBaseContractService baseContractService;

    /**
     * 查询合同管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseContract baseContract)
    {
        startPage();
        List<BaseContract> list = baseContractService.selectBaseContractList(baseContract);
        return getDataTable(list);
    }

    /**
     * 导出合同管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:export')")
    @Log(title = "合同管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseContract baseContract)
    {
        List<BaseContract> list = baseContractService.selectBaseContractList(baseContract);
        ExcelUtil<BaseContract> util = new ExcelUtil<BaseContract>(BaseContract.class);
        util.exportExcel(response, list, "合同管理数据");
    }

    /**
     * 获取合同管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseContractService.selectBaseContractById(id));
    }

    /**
     * 新增合同管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:add')")
    @Log(title = "合同管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseContract baseContract)
    {
        return toAjax(baseContractService.insertBaseContract(baseContract));
    }

    /**
     * 修改合同管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:edit')")
    @Log(title = "合同管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseContract baseContract)
    {
        return toAjax(baseContractService.updateBaseContract(baseContract));
    }

    /**
     * 删除合同管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:contract:remove')")
    @Log(title = "合同管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseContractService.deleteBaseContractByIds(ids));
    }
}

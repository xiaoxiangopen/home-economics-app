package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseUserInviteDetail;
import com.jiumi.baseconfig.service.IBaseUserInviteDetailService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 用户邀请Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/invitedetail")
public class BaseUserInviteDetailController extends BaseController
{
    @Autowired
    private IBaseUserInviteDetailService baseUserInviteDetailService;

    /**
     * 查询用户邀请列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserInviteDetail baseUserInviteDetail)
    {
        startPage();
        List<BaseUserInviteDetail> list = baseUserInviteDetailService.selectBaseUserInviteDetailList(baseUserInviteDetail);
        return getDataTable(list);
    }

    /**
     * 导出用户邀请列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:export')")
    @Log(title = "用户邀请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserInviteDetail baseUserInviteDetail)
    {
        List<BaseUserInviteDetail> list = baseUserInviteDetailService.selectBaseUserInviteDetailList(baseUserInviteDetail);
        ExcelUtil<BaseUserInviteDetail> util = new ExcelUtil<BaseUserInviteDetail>(BaseUserInviteDetail.class);
        util.exportExcel(response, list, "用户邀请数据");
    }

    /**
     * 获取用户邀请详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseUserInviteDetailService.selectBaseUserInviteDetailById(id));
    }

    /**
     * 新增用户邀请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:add')")
    @Log(title = "用户邀请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserInviteDetail baseUserInviteDetail)
    {
        return toAjax(baseUserInviteDetailService.insertBaseUserInviteDetail(baseUserInviteDetail));
    }

    /**
     * 修改用户邀请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:edit')")
    @Log(title = "用户邀请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserInviteDetail baseUserInviteDetail)
    {
        return toAjax(baseUserInviteDetailService.updateBaseUserInviteDetail(baseUserInviteDetail));
    }

    /**
     * 删除用户邀请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:invitedetail:remove')")
    @Log(title = "用户邀请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserInviteDetailService.deleteBaseUserInviteDetailByIds(ids));
    }
}

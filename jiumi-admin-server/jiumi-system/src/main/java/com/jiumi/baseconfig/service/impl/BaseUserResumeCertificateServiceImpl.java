package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseUserResumeCertificateMapper;
import com.jiumi.baseconfig.domain.BaseUserResumeCertificate;
import com.jiumi.baseconfig.service.IBaseUserResumeCertificateService;

/**
 * 职业证书Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseUserResumeCertificateServiceImpl implements IBaseUserResumeCertificateService
{
    @Autowired
    private BaseUserResumeCertificateMapper baseUserResumeCertificateMapper;

    /**
     * 查询职业证书
     *
     * @param id 职业证书主键
     * @return 职业证书
     */
    @Override
    public BaseUserResumeCertificate selectBaseUserResumeCertificateById(Long id)
    {
        return baseUserResumeCertificateMapper.selectBaseUserResumeCertificateById(id);
    }

    /**
     * 查询职业证书列表
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 职业证书
     */
    @Override
    public List<BaseUserResumeCertificate> selectBaseUserResumeCertificateList(BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return baseUserResumeCertificateMapper.selectBaseUserResumeCertificateList(baseUserResumeCertificate);
    }

    /**
     * 新增职业证书
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 结果
     */
    @Override
    public int insertBaseUserResumeCertificate(BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return baseUserResumeCertificateMapper.insertBaseUserResumeCertificate(baseUserResumeCertificate);
    }

    /**
     * 修改职业证书
     *
     * @param baseUserResumeCertificate 职业证书
     * @return 结果
     */
    @Override
    public int updateBaseUserResumeCertificate(BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return baseUserResumeCertificateMapper.updateBaseUserResumeCertificate(baseUserResumeCertificate);
    }

    /**
     * 批量删除职业证书
     *
     * @param ids 需要删除的职业证书主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeCertificateByIds(Long[] ids)
    {
        return baseUserResumeCertificateMapper.deleteBaseUserResumeCertificateByIds(ids);
    }

    /**
     * 删除职业证书信息
     *
     * @param id 职业证书主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeCertificateById(Long id)
    {
        return baseUserResumeCertificateMapper.deleteBaseUserResumeCertificateById(id);
    }

    @Override
    public void deleteResumeCertificateByResumeId(Long id) {
        baseUserResumeCertificateMapper.deleteResumeCertificateByResumeId(id);
    }

    @Override
    public void deleteResumeCertificateByCertCode(String certCode) {
        baseUserResumeCertificateMapper.deleteResumeCertificateByCertCode(certCode);
    }
}

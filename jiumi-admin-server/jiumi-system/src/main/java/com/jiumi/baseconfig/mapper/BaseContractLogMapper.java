package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseContractLog;

/**
 * 合同日志Mapper接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseContractLogMapper 
{
    /**
     * 查询合同日志
     * 
     * @param id 合同日志主键
     * @return 合同日志
     */
    public BaseContractLog selectBaseContractLogById(Long id);

    /**
     * 查询合同日志列表
     * 
     * @param baseContractLog 合同日志
     * @return 合同日志集合
     */
    public List<BaseContractLog> selectBaseContractLogList(BaseContractLog baseContractLog);

    /**
     * 新增合同日志
     * 
     * @param baseContractLog 合同日志
     * @return 结果
     */
    public int insertBaseContractLog(BaseContractLog baseContractLog);

    /**
     * 修改合同日志
     * 
     * @param baseContractLog 合同日志
     * @return 结果
     */
    public int updateBaseContractLog(BaseContractLog baseContractLog);

    /**
     * 删除合同日志
     * 
     * @param id 合同日志主键
     * @return 结果
     */
    public int deleteBaseContractLogById(Long id);

    /**
     * 批量删除合同日志
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseContractLogByIds(Long[] ids);
}

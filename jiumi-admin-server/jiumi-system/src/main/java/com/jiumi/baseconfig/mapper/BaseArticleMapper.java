package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.BaseArticle;

/**
 * 文章Mapper接口
 * 
 * @author jiumi
 * @date 2022-12-05
 */
public interface BaseArticleMapper 
{
    /**
     * 查询文章
     * 
     * @param id 文章主键
     * @return 文章
     */
    public BaseArticle selectBaseArticleById(Long id);

    /**
     * 查询文章列表
     * 
     * @param baseArticle 文章
     * @return 文章集合
     */
    public List<BaseArticle> selectBaseArticleList(BaseArticle baseArticle);

    /**
     * 新增文章
     * 
     * @param baseArticle 文章
     * @return 结果
     */
    public int insertBaseArticle(BaseArticle baseArticle);

    /**
     * 修改文章
     * 
     * @param baseArticle 文章
     * @return 结果
     */
    public int updateBaseArticle(BaseArticle baseArticle);

    /**
     * 删除文章
     * 
     * @param id 文章主键
     * @return 结果
     */
    public int deleteBaseArticleById(Long id);

    /**
     * 批量删除文章
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseArticleByIds(Long[] ids);
}

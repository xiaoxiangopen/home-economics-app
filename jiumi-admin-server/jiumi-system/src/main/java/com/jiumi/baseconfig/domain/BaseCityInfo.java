package com.jiumi.baseconfig.domain;

import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 省市区对象 base_city_info
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseCityInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** code */
    @Excel(name = "code")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String value;

    /** 父级code */
    @Excel(name = "父级code")
    private String parentCode;

    /** 拼音 */
    @Excel(name = "拼音")
    private String pinyin;

    /** 首拼 */
    @Excel(name = "首拼")
    private String first;

    /** $column.columnComment */
    private String lat;

    /** $column.columnComment */
    private String lng;
    private List<BaseCityInfo> children = new ArrayList<BaseCityInfo>();

    public List<BaseCityInfo> getChildren() {
        return children;
    }

    public void setChildren(List<BaseCityInfo> children) {
        this.children = children;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
    public void setParentCode(String parentCode)
    {
        this.parentCode = parentCode;
    }

    public String getParentCode()
    {
        return parentCode;
    }
    public void setPinyin(String pinyin)
    {
        this.pinyin = pinyin;
    }

    public String getPinyin()
    {
        return pinyin;
    }
    public void setFirst(String first)
    {
        this.first = first;
    }

    public String getFirst()
    {
        return first;
    }
    public void setLat(String lat)
    {
        this.lat = lat;
    }

    public String getLat()
    {
        return lat;
    }
    public void setLng(String lng)
    {
        this.lng = lng;
    }

    public String getLng()
    {
        return lng;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("value", getValue())
            .append("parentCode", getParentCode())
            .append("pinyin", getPinyin())
            .append("first", getFirst())
            .append("lat", getLat())
            .append("lng", getLng())
            .toString();
    }
}

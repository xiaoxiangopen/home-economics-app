package com.jiumi.baseconfig.mapper;

import java.util.List;
import com.jiumi.baseconfig.domain.UserEvaluate;

/**
 * 阿姨评价Mapper接口
 * 
 * @author jiumi
 * @date 2023-01-13
 */
public interface UserEvaluateMapper 
{
    /**
     * 查询阿姨评价
     * 
     * @param id 阿姨评价主键
     * @return 阿姨评价
     */
    public UserEvaluate selectUserEvaluateById(Long id);

    /**
     * 查询阿姨评价列表
     * 
     * @param userEvaluate 阿姨评价
     * @return 阿姨评价集合
     */
    public List<UserEvaluate> selectUserEvaluateList(UserEvaluate userEvaluate);

    /**
     * 新增阿姨评价
     * 
     * @param userEvaluate 阿姨评价
     * @return 结果
     */
    public int insertUserEvaluate(UserEvaluate userEvaluate);

    /**
     * 修改阿姨评价
     * 
     * @param userEvaluate 阿姨评价
     * @return 结果
     */
    public int updateUserEvaluate(UserEvaluate userEvaluate);

    /**
     * 删除阿姨评价
     * 
     * @param id 阿姨评价主键
     * @return 结果
     */
    public int deleteUserEvaluateById(Long id);

    /**
     * 批量删除阿姨评价
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserEvaluateByIds(Long[] ids);
}

package com.jiumi.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiumi.common.annotation.Log;
import com.jiumi.common.core.controller.BaseController;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.enums.BusinessType;
import com.jiumi.baseconfig.domain.BaseCityInfo;
import com.jiumi.baseconfig.service.IBaseCityInfoService;
import com.jiumi.common.utils.poi.ExcelUtil;
import com.jiumi.common.core.page.TableDataInfo;

/**
 * 省市区Controller
 *
 * @author jiumi
 * @date 2022-12-05
 */
@RestController
@RequestMapping("/baseconfig/cityinfo")
public class BaseCityInfoController extends BaseController
{
    @Autowired
    private IBaseCityInfoService baseCityInfoService;

    /**
     * 查询省市区列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCityInfo baseCityInfo)
    {
        startPage();
        List<BaseCityInfo> list = baseCityInfoService.selectBaseCityInfoList(baseCityInfo);
        return getDataTable(list);
    }

    /**
     * 导出省市区列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:export')")
    @Log(title = "省市区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCityInfo baseCityInfo)
    {
        List<BaseCityInfo> list = baseCityInfoService.selectBaseCityInfoList(baseCityInfo);
        ExcelUtil<BaseCityInfo> util = new ExcelUtil<BaseCityInfo>(BaseCityInfo.class);
        util.exportExcel(response, list, "省市区数据");
    }

    /**
     * 获取省市区详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseCityInfoService.selectBaseCityInfoById(id));
    }

    /**
     * 新增省市区
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:add')")
    @Log(title = "省市区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCityInfo baseCityInfo)
    {
        return toAjax(baseCityInfoService.insertBaseCityInfo(baseCityInfo));
    }

    /**
     * 修改省市区
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:edit')")
    @Log(title = "省市区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCityInfo baseCityInfo)
    {
        return toAjax(baseCityInfoService.updateBaseCityInfo(baseCityInfo));
    }

    /**
     * 删除省市区
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:cityinfo:remove')")
    @Log(title = "省市区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCityInfoService.deleteBaseCityInfoByIds(ids));
    }
}

package com.jiumi.baseconfig.service;

import java.util.List;
import com.jiumi.baseconfig.domain.UserResumeHealthcert;

/**
 * 健康证书Service接口
 * 
 * @author jiumi
 * @date 2023-06-15
 */
public interface IUserResumeHealthcertService 
{
    /**
     * 查询健康证书
     * 
     * @param id 健康证书主键
     * @return 健康证书
     */
    public UserResumeHealthcert selectUserResumeHealthcertById(Long id);

    /**
     * 查询健康证书列表
     * 
     * @param userResumeHealthcert 健康证书
     * @return 健康证书集合
     */
    public List<UserResumeHealthcert> selectUserResumeHealthcertList(UserResumeHealthcert userResumeHealthcert);

    /**
     * 新增健康证书
     * 
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    public int insertUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert);

    /**
     * 修改健康证书
     * 
     * @param userResumeHealthcert 健康证书
     * @return 结果
     */
    public int updateUserResumeHealthcert(UserResumeHealthcert userResumeHealthcert);

    /**
     * 批量删除健康证书
     * 
     * @param ids 需要删除的健康证书主键集合
     * @return 结果
     */
    public int deleteUserResumeHealthcertByIds(Long[] ids);

    /**
     * 删除健康证书信息
     * 
     * @param id 健康证书主键
     * @return 结果
     */
    public int deleteUserResumeHealthcertById(Long id);
}

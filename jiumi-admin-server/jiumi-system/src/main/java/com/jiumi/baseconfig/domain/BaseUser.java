package com.jiumi.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 用户信息对象 base_user
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class BaseUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long userId;

    /** 登录账号名 */
    private String userName;

    /** 认证名称 */
    @Excel(name = "认证名称")
    private String authName;

    /** 关联号码 */
    @Excel(name = "手机号")
    private String phonenumber;

    /** 微信昵称 */
    @Excel(name = "昵称")
    private String nickName;

    /** 认证类型01平台02家政公司03雇主04阿姨 */
    @Excel(name = "认证类型",dictType = "base_user_type")
    private String userType;

    /** 推荐码 */
    @Excel(name = "邀请码")
    private String referrerCode;

    /** 密码 */
    private String password;

    /** open_id */
    private String openId;

    /** union_id */
    private String unionId;

    @Excel(name = "邀请人姓名")
    private String inviteUserName;

    @Excel(name = "邀请人手机号")
    private String inviteUserPhone;

    @Excel(name = "邀请人公司")
    private String inviteUserCompany;

    /** 邮箱 */
    private String email;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 头像 */
    private String avatar;

    /** 认证状态 01待审核02认证通过03认证不通过 */
    @Excel(name = "认证状态",dictType = "base_auth_status_type")
    private String authStatus;

    /** 认证时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "认证时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date authTime;

    private String authType;

    /** 家政公司名称 */
    @Excel(name = "所属家政公司")
    private String companyName;




    /** 邀请码 */
    private String inviteCode;

    /** 用户状态01上线02下线 */
    private String status;

    /** 积分数量 */
    private Long scoreAmount;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;

    @Excel(name = "发布信息数量")
    private int publishNum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 身份证号 */
    private String certCode;

    /** 有效期 */
    private String certEndDate;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    /** 生肖 */
    private String zodiac;

    /** 星座 */
    private String constellation;

    /** 民族 */
    private String nation;

    /** 籍贯 */
    private String nativePlace;

    /** 户籍地址 */

    private String address;

    /** 家政公司ID */
    private Long companyId;


    /** 授权家政公司员工ID */
    private Long authUserId;

    private String delFlag;
    private String certImage1;
    private String certImage2;

    private BaseUser authUser;

    private String publishType;






    private int age;

    private Long publishUserId;
    private String publishUserName;
    private String jobTypeName;
    private int lowSalary;
    private int highSalary;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date clickTime;

    private String clickStatus;

    private BaseCompany authCompany;

    private String workExperience;
    private String abilityLabel;
    private String healthUrl;

    private int queryType;

    private String signType;

    private String isAdmin;

    private String authUserName;
    private String accountStatus;

    private Long resumeId;
    private Long accountId;

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getPublishUserId() {
        return publishUserId;
    }

    public void setPublishUserId(Long publishUserId) {
        this.publishUserId = publishUserId;
    }

    public Long getResumeId() {
        return resumeId;
    }

    public void setResumeId(Long resumeId) {
        this.resumeId = resumeId;
    }

    public String getAuthUserName() {
        return authUserName;
    }

    public void setAuthUserName(String authUserName) {
        this.authUserName = authUserName;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public int getQueryType() {
        return queryType;
    }

    public void setQueryType(int queryType) {
        this.queryType = queryType;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public String getAbilityLabel() {
        return abilityLabel;
    }

    public void setAbilityLabel(String abilityLabel) {
        this.abilityLabel = abilityLabel;
    }

    public String getHealthUrl() {
        return healthUrl;
    }

    public void setHealthUrl(String healthUrl) {
        this.healthUrl = healthUrl;
    }

    public BaseCompany getAuthCompany() {
        return authCompany;
    }

    public void setAuthCompany(BaseCompany authCompany) {
        this.authCompany = authCompany;
    }

    public Date getClickTime() {
        return clickTime;
    }

    public void setClickTime(Date clickTime) {
        this.clickTime = clickTime;
    }

    public String getClickStatus() {
        return clickStatus;
    }

    public void setClickStatus(String clickStatus) {
        this.clickStatus = clickStatus;
    }

    private BaseUserIntention intention;


    public BaseUserIntention getIntention() {
        return intention;
    }

    public void setIntention(BaseUserIntention intention) {
        this.intention = intention;
    }

    public String getPublishUserName() {
        return publishUserName;
    }

    public void setPublishUserName(String publishUserName) {
        this.publishUserName = publishUserName;
    }

    public String getJobTypeName() {
        return jobTypeName;
    }

    public void setJobTypeName(String jobTypeName) {
        this.jobTypeName = jobTypeName;
    }

    public int getLowSalary() {
        return lowSalary;
    }

    public void setLowSalary(int lowSalary) {
        this.lowSalary = lowSalary;
    }

    public int getHighSalary() {
        return highSalary;
    }

    public void setHighSalary(int highSalary) {
        this.highSalary = highSalary;
    }

    public String getInviteUserPhone() {
        return inviteUserPhone;
    }

    public void setInviteUserPhone(String inviteUserPhone) {
        this.inviteUserPhone = inviteUserPhone;
    }

    public int getPublishNum() {
        return publishNum;
    }

    public void setPublishNum(int publishNum) {
        this.publishNum = publishNum;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getInviteUserName() {
        return inviteUserName;
    }

    public void setInviteUserName(String inviteUserName) {
        this.inviteUserName = inviteUserName;
    }

    public String getInviteUserCompany() {
        return inviteUserCompany;
    }

    public void setInviteUserCompany(String inviteUserCompany) {
        this.inviteUserCompany = inviteUserCompany;
    }

    public String getPublishType() {
        return publishType;
    }

    public void setPublishType(String publishType) {
        this.publishType = publishType;
    }

    public BaseUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(BaseUser authUser) {
        this.authUser = authUser;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getCertImage1() {
        return certImage1;
    }

    public void setCertImage1(String certImage1) {
        this.certImage1 = certImage1;
    }

    public String getCertImage2() {
        return certImage2;
    }

    public void setCertImage2(String certImage2) {
        this.certImage2 = certImage2;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getNickName()
    {
        return nickName;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getOpenId()
    {
        return openId;
    }
    public void setUnionId(String unionId)
    {
        this.unionId = unionId;
    }

    public String getUnionId()
    {
        return unionId;
    }
    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumber()
    {
        return phonenumber;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }
    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getSex()
    {
        return sex;
    }
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getAvatar()
    {
        return avatar;
    }
    public void setAuthStatus(String authStatus)
    {
        this.authStatus = authStatus;
    }

    public String getAuthStatus()
    {
        return authStatus;
    }
    public void setAuthTime(Date authTime)
    {
        this.authTime = authTime;
    }

    public Date getAuthTime()
    {
        return authTime;
    }
    public void setUserType(String userType)
    {
        this.userType = userType;
    }

    public String getUserType()
    {
        return userType;
    }
    public void setReferrerCode(String referrerCode)
    {
        this.referrerCode = referrerCode;
    }

    public String getReferrerCode()
    {
        return referrerCode;
    }
    public void setInviteCode(String inviteCode)
    {
        this.inviteCode = inviteCode;
    }

    public String getInviteCode()
    {
        return inviteCode;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setScoreAmount(Long scoreAmount)
    {
        this.scoreAmount = scoreAmount;
    }

    public Long getScoreAmount()
    {
        return scoreAmount;
    }
    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }
    public void setAuthName(String authName)
    {
        this.authName = authName;
    }

    public String getAuthName()
    {
        return authName;
    }
    public void setCertCode(String certCode)
    {
        this.certCode = certCode;
    }

    public String getCertCode()
    {
        return certCode;
    }
    public void setCertEndDate(String certEndDate)
    {
        this.certEndDate = certEndDate;
    }

    public String getCertEndDate()
    {
        return certEndDate;
    }
    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }
    public void setZodiac(String zodiac)
    {
        this.zodiac = zodiac;
    }

    public String getZodiac()
    {
        return zodiac;
    }
    public void setConstellation(String constellation)
    {
        this.constellation = constellation;
    }

    public String getConstellation()
    {
        return constellation;
    }
    public void setNation(String nation)
    {
        this.nation = nation;
    }

    public String getNation()
    {
        return nation;
    }
    public void setNativePlace(String nativePlace)
    {
        this.nativePlace = nativePlace;
    }

    public String getNativePlace()
    {
        return nativePlace;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setAuthUserId(Long authUserId)
    {
        this.authUserId = authUserId;
    }

    public Long getAuthUserId()
    {
        return authUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("userId", getUserId())
                .append("userName", getUserName())
                .append("nickName", getNickName())
                .append("password", getPassword())
                .append("openId", getOpenId())
                .append("unionId", getUnionId())
                .append("phonenumber", getPhonenumber())
                .append("email", getEmail())
                .append("sex", getSex())
                .append("avatar", getAvatar())
                .append("authStatus", getAuthStatus())
                .append("authTime", getAuthTime())
                .append("userType", getUserType())
                .append("referrerCode", getReferrerCode())
                .append("inviteCode", getInviteCode())
                .append("status", getStatus())
                .append("scoreAmount", getScoreAmount())
                .append("loginDate", getLoginDate())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("authName", getAuthName())
                .append("certCode", getCertCode())
                .append("certEndDate", getCertEndDate())
                .append("birthDate", getBirthDate())
                .append("zodiac", getZodiac())
                .append("constellation", getConstellation())
                .append("nation", getNation())
                .append("nativePlace", getNativePlace())
                .append("address", getAddress())
                .append("companyId", getCompanyId())
                .append("companyName", getCompanyName())
                .append("authUserId", getAuthUserId())
                .toString();
    }
}

package com.jiumi.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jiumi.baseconfig.mapper.BaseScoreConfigMapper;
import com.jiumi.baseconfig.domain.BaseScoreConfig;
import com.jiumi.baseconfig.service.IBaseScoreConfigService;

/**
 * 积分奖励设置Service业务层处理
 *
 * @author jiumi
 * @date 2022-12-05
 */
@Service
public class BaseScoreConfigServiceImpl implements IBaseScoreConfigService
{
    @Autowired
    private BaseScoreConfigMapper baseScoreConfigMapper;

    /**
     * 查询积分奖励设置
     *
     * @param id 积分奖励设置主键
     * @return 积分奖励设置
     */
    @Override
    public BaseScoreConfig selectBaseScoreConfigById(Long id)
    {
        return baseScoreConfigMapper.selectBaseScoreConfigById(id);
    }

    /**
     * 查询积分奖励设置列表
     *
     * @param baseScoreConfig 积分奖励设置
     * @return 积分奖励设置
     */
    @Override
    public List<BaseScoreConfig> selectBaseScoreConfigList(BaseScoreConfig baseScoreConfig)
    {
        return baseScoreConfigMapper.selectBaseScoreConfigList(baseScoreConfig);
    }

    /**
     * 新增积分奖励设置
     *
     * @param baseScoreConfig 积分奖励设置
     * @return 结果
     */
    @Override
    public int insertBaseScoreConfig(BaseScoreConfig baseScoreConfig)
    {
        return baseScoreConfigMapper.insertBaseScoreConfig(baseScoreConfig);
    }

    /**
     * 修改积分奖励设置
     *
     * @param baseScoreConfig 积分奖励设置
     * @return 结果
     */
    @Override
    public int updateBaseScoreConfig(BaseScoreConfig baseScoreConfig)
    {
        return baseScoreConfigMapper.updateBaseScoreConfig(baseScoreConfig);
    }

    /**
     * 批量删除积分奖励设置
     *
     * @param ids 需要删除的积分奖励设置主键
     * @return 结果
     */
    @Override
    public int deleteBaseScoreConfigByIds(Long[] ids)
    {
        return baseScoreConfigMapper.deleteBaseScoreConfigByIds(ids);
    }

    /**
     * 删除积分奖励设置信息
     *
     * @param id 积分奖励设置主键
     * @return 结果
     */
    @Override
    public int deleteBaseScoreConfigById(Long id)
    {
        return baseScoreConfigMapper.deleteBaseScoreConfigById(id);
    }
}

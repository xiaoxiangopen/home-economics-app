package com.jiumi.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author lun.zhang
 * @create 2022/10/18 10:56
 */
@Data
@Component
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@PropertySource("classpath:/config/tencentmap.properties")
@ConfigurationProperties(prefix = "tencent")
public class TencentMapConfig implements Serializable {
    private String tencentMapKey;
    private String tencentMapSK;
}

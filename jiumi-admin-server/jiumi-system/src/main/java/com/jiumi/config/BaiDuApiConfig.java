package com.jiumi.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author zl
 * 百度API
 */
@Getter
@Setter
@Builder
@Component
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@PropertySource("classpath:/config/baidu.properties")
@ConfigurationProperties(prefix = "baidu")
public class BaiDuApiConfig implements Serializable {
    private static final long serialVersionUID = -9044503427692786353L;

    private String appId;
    private String apiKey;
    private String secretKey;

    public BaiDuApiConfig() {

    }
}

package com.jiumi.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author lun.zhang
 * @create 2022/10/18 10:56
 */
@Data
@Component
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@PropertySource("classpath:/config/wx.properties")
@ConfigurationProperties(prefix = "wx")
public class WeiXinConfig implements Serializable {
    private String appId;
    private String appSecret;
}

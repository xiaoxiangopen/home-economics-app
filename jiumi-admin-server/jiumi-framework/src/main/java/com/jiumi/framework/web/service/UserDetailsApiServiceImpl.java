package com.jiumi.framework.web.service;

import com.jiumi.baseconfig.domain.BaseCompany;
import com.jiumi.baseconfig.domain.BaseCompanyAccount;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.service.IBaseCompanyAccountService;
import com.jiumi.baseconfig.service.IBaseCompanyService;
import com.jiumi.baseconfig.service.IBaseUserService;
import com.jiumi.common.core.domain.entity.SysUser;
import com.jiumi.common.exception.ServiceException;
import com.jiumi.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户验证处理
 *
 * @author jiumi
 */
@Service("userDetailsApiService")
public class UserDetailsApiServiceImpl implements UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsApiServiceImpl.class);

    @Autowired
    private IBaseUserService userService;

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private IBaseCompanyAccountService baseCompanyAccountService;
    @Autowired
    private IBaseCompanyService baseCompanyService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        //BaseUser user = userService.selectUserInfoByOpenId(username);
        BaseUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user))
        {
            log.info("登录用户：{} 不存在.", username);
            throw new ServiceException("登录用户：" + username + " 不存在");
        }
        if("01".equals(user.getUserType()) || "02".equals(user.getUserType()) || "05".equals(user.getUserType())){
            BaseCompany company= baseCompanyService.selectBaseCompanyById(user.getCompanyId());
            if(company!=null && !"Y".equals(company.getAvailableFlag())){
                throw new ServiceException("公司状态异常，请联系管理员");
            }
            BaseCompanyAccount param=new BaseCompanyAccount();
            param.setCompanyId(user.getCompanyId());
            param.setUserId(user.getUserId());
            List<BaseCompanyAccount> accountList=baseCompanyAccountService.selectBaseCompanyAccountList(param);
            if(accountList.size()>0){
                BaseCompanyAccount userAccount=accountList.get(0);
                if(userAccount.getUserId().intValue()==user.getUserId().intValue() && !"Y".equals(userAccount.getStatus())){
                    throw new ServiceException("您的账号已被禁用，请联系管理员");
                }
            }
        }


        SysUser suser=new SysUser();
        suser.setUserId(user.getUserId());
        suser.setUserName(user.getUserName());
        suser.setPassword(user.getPassword());
        passwordService.validate(suser);
        return createLoginUser(user);
    }

    public UserDetails createLoginUser(BaseUser user)
    {
        return new UserInfo(user);
    }
}

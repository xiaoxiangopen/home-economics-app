package com.jiumi.framework.config;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xudong.liu
 * @data 2021/3/15
 */
@Component
public class NoAuthPath {
    private static final List<String> NO_AUTH_PATH_LIST = new ArrayList<>();

    static {
        //放行支付接口
        NO_AUTH_PATH_LIST.add("/api/login");
        NO_AUTH_PATH_LIST.add("/api/loginApp1");
        NO_AUTH_PATH_LIST.add("/api/loginApp2");
        NO_AUTH_PATH_LIST.add("/api/business/getCompanyInfoById");
        NO_AUTH_PATH_LIST.add("/api/noAuth/**");
    }

    public static String[] getNoAuthPath() {
        String[] noPath = new String[NO_AUTH_PATH_LIST.size()];
        return NO_AUTH_PATH_LIST.toArray(noPath);
    }
}

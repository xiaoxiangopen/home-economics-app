package com.jiumi.framework.security.handle;

import com.alibaba.fastjson2.JSON;
import com.jiumi.common.constant.Constants;
import com.jiumi.common.constant.HttpStatus;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.utils.ServletUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.framework.manager.AsyncManager;
import com.jiumi.framework.manager.factory.AsyncFactory;
import com.jiumi.framework.web.service.TokenApiService;
import com.jiumi.framework.web.service.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lun.zhang
 * @create 2022/10/18 11:35
 */
@Configuration("logoutSuccessHandlerApiImpl")
public class LogoutSuccessHandlerApiImpl implements LogoutSuccessHandler {
    @Autowired
    private TokenApiService tokenApiService;
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserInfo loginUser = tokenApiService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser))
        {
            String userName = loginUser.getUsername();
            // 删除用户缓存记录
            tokenApiService.delLoginUser(loginUser.getToken());
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, Constants.LOGOUT, "退出成功"));
        }
        ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(HttpStatus.SUCCESS, "退出成功")));
    }
}

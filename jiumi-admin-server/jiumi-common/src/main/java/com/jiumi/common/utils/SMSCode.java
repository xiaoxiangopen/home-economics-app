package com.jiumi.common.utils;

import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.core.redis.RedisCache;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;


/**
 * @author 九米卧龙在此
 * @date 2020/10/9 14:45
 */
public class SMSCode {


    public static AjaxResult sendSms(RedisCache redisCache, String phone) throws Exception {
        AjaxResult ajaxResult=AjaxResult.success();
        try{
            HttpClient httpClient = new HttpClient();
            PostMethod postMethod = new PostMethod("http://api.1cloudsp.com/api/v2/single_send");
            postMethod.getParams().setContentCharset("UTF-8");
            postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());

            String accesskey = "替换成你的短信配置"; //用户开发key
            String accessSecret = "替换成你的短信配置"; //用户开发秘钥
            Integer code = (int) ((Math.random() * 9 + 1) * 100000);//生成6位随机数
            NameValuePair[] data = {
                    new NameValuePair("accesskey", accesskey),
                    new NameValuePair("secret", accessSecret),
                    new NameValuePair("sign", "111111"),
                    new NameValuePair("templateId", "111111"),
                    new NameValuePair("mobile", phone),
                    new NameValuePair("content", URLEncoder.encode(code+"", "utf-8"))//（示例模板：{1}您好，您的订单于{2}已通过{3}发货，运单号{4}）
            };
            postMethod.setRequestBody(data);
            postMethod.setRequestHeader("Connection", "close");
            int statusCode = httpClient.executeMethod(postMethod);
            System.out.println("statusCode: " + statusCode + ", body: "
                    + postMethod.getResponseBodyAsString());
            String key = "phone_verify_code" + phone;
            redisCache.setCacheObject(key, code,300, TimeUnit.SECONDS);
            //ajaxResult.put("phoneCode",code);
            return ajaxResult;
        }
        catch (IOException e) {
            e.printStackTrace(System.out);
            ajaxResult=AjaxResult.error(e.getMessage());
        }

        return ajaxResult;
    }



}

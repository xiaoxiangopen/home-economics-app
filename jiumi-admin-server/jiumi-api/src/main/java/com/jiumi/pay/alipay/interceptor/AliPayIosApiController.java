package com.jiumi.pay.alipay.interceptor;


import com.jiumi.pay.alipay.config.AliPayIosApiConfig;

/**
 * @author Javen
 */
public abstract class AliPayIosApiController {
	public abstract AliPayIosApiConfig getApiConfig();
}

package com.jiumi.pay.alipay.interceptor;


import com.jiumi.pay.alipay.config.AliPayAndroidApiConfig;

/**
 * @author Javen
 */
public abstract class AliPayApiController{
	public abstract AliPayAndroidApiConfig getApiConfig();
}

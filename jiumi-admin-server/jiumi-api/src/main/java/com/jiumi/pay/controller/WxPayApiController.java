package com.jiumi.pay.controller;

import com.ijpay.core.kit.PayKit;
import com.jiumi.api.entity.WxPayConfig;
import com.alibaba.fastjson2.JSON;
import com.ijpay.core.enums.SignType;
import com.ijpay.core.enums.TradeType;
import com.ijpay.core.kit.HttpKit;
import com.ijpay.core.kit.IpKit;
import com.ijpay.core.kit.WxPayKit;
import com.ijpay.wxpay.WxPayApi;
import com.ijpay.wxpay.WxPayApiConfig;
import com.ijpay.wxpay.WxPayApiConfigKit;
import com.ijpay.wxpay.model.OrderQueryModel;
import com.ijpay.wxpay.model.RefundModel;
import com.ijpay.wxpay.model.RefundQueryModel;
import com.ijpay.wxpay.model.UnifiedOrderModel;
import com.jiumi.baseconfig.domain.BaseOrder;
import com.jiumi.baseconfig.domain.BaseScoreConfig;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.domain.ScoreHistory;
import com.jiumi.baseconfig.service.IBaseOrderService;
import com.jiumi.baseconfig.service.IBaseUserService;
import com.jiumi.baseconfig.service.IScoreHistoryService;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.common.utils.uuid.IdUtils;
import com.jiumi.common.utils.uuid.Seq;
import com.jiumi.system.service.ISysConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lun.zhang
 * @create 2022/11/21 14:29
 */
@RequestMapping("/api")
@RestController
@Slf4j
public class WxPayApiController {

    @Autowired
    WxPayConfig wxPayConfig;

    private String notifyUrl;
    private String refundNotifyUrl;

    @Autowired
    private IBaseOrderService baseOrderService;

    @Autowired
    private IBaseUserService userInfoService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IScoreHistoryService scoreHistoryService;


    @PostConstruct()
    public void init(){
        try {
            //如果是证书模式，放开if,
            if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
                //TODO 确认证书所在路径
                wxPayConfig.setCertPath("D:\\1637042361_20230113_cert\\apiclient_cert.p12");
            }
            com.ijpay.wxpay.WxPayApiConfig reqConfig = new com.ijpay.wxpay.WxPayApiConfig();
            BeanUtils.copyProperties(wxPayConfig,reqConfig);
            WxPayApiConfigKit.setThreadLocalWxPayApiConfig(reqConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyUrl = wxPayConfig.getDomain() + wxPayConfig.getNotifyUrl();
        refundNotifyUrl = wxPayConfig.getDomain() + wxPayConfig.getRefundNotify();
        log.info("[微信支付]:当前微信支付回调地址" + notifyUrl);
        log.info("[微信支付]:当前微信退款回调地址" + refundNotifyUrl);
    }
    /**
     * 微信小程序支付
     */
    @RequestMapping(value = "/noAuth/wxpay/appPay", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public AjaxResult wxAppPay(HttpServletRequest request) {
        //需要通过授权来获取openId
        String outTradeNo = Seq.getId();

        String orderCode=request.getParameter("orderCode");
        String scoreAmount=request.getParameter("scoreAmount");
        int scoreNum=0;
        if(StringUtils.isNotEmpty(scoreAmount)){
            scoreNum=Integer.parseInt(scoreAmount);
        }

        BaseOrder baseOrder= baseOrderService.selectBaseOrderByOrderCode(orderCode);
        if(baseOrder==null){
            return AjaxResult.error("订单不存在");
        }
        if(!"01".equals(baseOrder.getPayStatus())){
            return AjaxResult.error("待支付状态的订单才能进行支付");
        }
        BaseUser orderUser= userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
        if(orderUser.getScoreAmount().intValue()<scoreNum){
            return AjaxResult.error("您的积分不足!");
        }
       int orderAmount= baseOrder.getOrderAmount().multiply(new BigDecimal(100)).intValue();
        if(orderAmount<=0){
            return AjaxResult.error("订单金额错误");
        }
        String ip = IpKit.getRealIp(request);
        if (StringUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        String percent = configService.selectConfigByKey("sys.customer.usescore.percent");
        double scorePercent=StringUtils.isNotEmpty(percent)?Double.valueOf(percent):1;
        double scoreMoney=scoreNum*scorePercent;
        if(baseOrder.getOrderAmount().doubleValue()<scoreMoney){
            return AjaxResult.error("请输入合适的积分");
        }
        baseOrder.setTradeCode(outTradeNo);
        baseOrder.setUpdateTime(DateUtils.getNowDate());
        //积分相抵
        if(baseOrder.getOrderAmount().doubleValue()==scoreMoney){
            baseOrder.setPayAmount(new BigDecimal(0));
            baseOrder.setScoreAmount(new BigDecimal(scoreNum));
            baseOrder.setPayTime(DateUtils.getNowDate());
            baseOrder.setPayStatus("02");
            baseOrderService.updateBaseOrder(baseOrder);

            BaseUser currentUser=userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
            BaseUser scoreUser=new BaseUser();
            scoreUser.setUserId(baseOrder.getUserId());
            scoreUser.setScoreAmount(currentUser.getScoreAmount()-baseOrder.getScoreAmount().intValue());
            userInfoService.updateBaseUserScore(scoreUser);

            ScoreHistory refHis=new ScoreHistory();
            refHis.setUserId(currentUser.getUserId());
            refHis.setUserName(currentUser.getUserName());
            refHis.setRemark("订单抵扣积分");
            refHis.setScore(new BigDecimal(scoreNum*-1));
            refHis.setCreateTime(DateUtils.getNowDate());
            scoreHistoryService.insertScoreHistory(refHis);

            AjaxResult payResult=  AjaxResult.success("支付成功");
            payResult.put("scoreFlag",'Y');
            return payResult;
        }
        BigDecimal payAmount=baseOrder.getOrderAmount().subtract(new BigDecimal(scoreMoney)).setScale(2, RoundingMode.HALF_UP);
        System.out.println("===========================");
        System.out.println(payAmount);
        baseOrder.setPayAmount(payAmount);
        baseOrder.setScoreAmount(new BigDecimal(scoreNum));
        //实际支付金额
        int orderPayAmount=payAmount.multiply(new BigDecimal(100)).intValue();
        System.out.println(orderPayAmount);
        WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getApiConfig(wxPayConfig.getAppId());
        String nonceStr=WxPayKit.generateStr();
        Map<String, String> params = UnifiedOrderModel
                .builder()
                .appid(wxPayApiConfig.getAppId())
                .mch_id(wxPayApiConfig.getMchId())
                .nonce_str(nonceStr)
                .body("让支付触手可及-家政服务APP支付")
                .attach("Node.js 版:https://jiumi.com")
                .out_trade_no(WxPayKit.generateStr())
                .total_fee(String.valueOf(orderPayAmount))
                .spbill_create_ip(ip)
                .notify_url(notifyUrl)
                .trade_type(TradeType.APP.getTradeType())
                .out_trade_no(outTradeNo)
                .build()
                .createSign(wxPayApiConfig.getPartnerKey(), SignType.MD5);

        Map<String, String> packageParams=null;
        try{
            String xmlResult = WxPayApi.pushOrder(false, params);

            log.info(xmlResult);
            Map<String, String> result = WxPayKit.xmlToMap(xmlResult);

            String returnCode = result.get("return_code");
            String returnMsg = result.get("return_msg");
            if (!WxPayKit.codeIsOk(returnCode)) {
                return  AjaxResult.error(returnMsg);
            }
            String resultCode = result.get("result_code");
            if (!WxPayKit.codeIsOk(resultCode)) {
                return  AjaxResult.error(returnMsg);
            }
            // 以下字段在 return_code 和 result_code 都为 SUCCESS 的时候有返回
            String prepayId = result.get("prepay_id");
            //appPrepayIdCreateSign(String appId, String partnerId, String prepayId, String partnerKey, SignType signType)
            packageParams = WxPayKit.appPrepayIdCreateSign(wxPayApiConfig.getAppId(),wxPayApiConfig.getMchId(),prepayId,wxPayApiConfig.getPartnerKey(), SignType.MD5);
            String jsonStr = JSON.toJSONString(packageParams);
            log.info("app支付的参数:" + jsonStr);
            baseOrderService.updateBaseOrder(baseOrder);
        }
        catch (Exception e){
            e.printStackTrace();
            return  AjaxResult.error(e.getMessage());
        }

        AjaxResult payResult=  AjaxResult.success(packageParams);
        payResult.put("scoreFlag",'N');
        return payResult;
    }


    @RequestMapping(value = "/wxpay/queryOrder", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String queryOrder(@RequestParam(value = "transactionId", required = false) String transactionId, @RequestParam(value = "outTradeNo", required = false) String outTradeNo) {
        try {
            WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getWxPayApiConfig();

            Map<String, String> params = OrderQueryModel.builder()
                    .appid(wxPayApiConfig.getAppId())
                    .mch_id(wxPayApiConfig.getMchId())
                    .transaction_id(transactionId)
                    .out_trade_no(outTradeNo)
                    .nonce_str(WxPayKit.generateStr())
                    .build()
                    .createSign(wxPayApiConfig.getPartnerKey(), SignType.MD5);
            log.info("请求参数：{}", WxPayKit.toXml(params));
            String query = WxPayApi.orderQuery(params);
            log.info("查询结果: {}", query);
            return query;
        } catch (Exception e) {
            e.printStackTrace();
            return "系统错误";
        }
    }

    /**
     * 微信退款
     */
    @RequestMapping(value = "/wxpay/refund", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String refund(@RequestParam(value = "transactionId", required = false) String transactionId,
                         @RequestParam(value = "outTradeNo", required = false) String outTradeNo) {
        try {
            log.info("transactionId: {} outTradeNo:{}", transactionId, outTradeNo);

            if (StringUtils.isEmpty(outTradeNo) && StringUtils.isEmpty(transactionId)) {
                return "transactionId、out_trade_no二选一";
            }
            WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getWxPayApiConfig();

            Map<String, String> params = RefundModel.builder()
                    .appid(wxPayApiConfig.getAppId())
                    .mch_id(wxPayApiConfig.getMchId())
                    .nonce_str(WxPayKit.generateStr())
                    .transaction_id(transactionId)
                    .out_trade_no(outTradeNo)
                    .out_refund_no(WxPayKit.generateStr())
                    .total_fee("1")
                    .refund_fee("1")
                    .notify_url(refundNotifyUrl)
                    .build()
                    .createSign(wxPayApiConfig.getPartnerKey(), SignType.MD5);
            String refundStr = WxPayApi.orderRefund(false, params, wxPayApiConfig.getCertPath(), wxPayApiConfig.getMchId());
            log.info("refundStr: {}", refundStr);
            return refundStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 微信退款查询
     */
    @RequestMapping(value = "/wxpay/refundQuery", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String refundQuery(@RequestParam("transactionId") String transactionId,
                              @RequestParam("out_trade_no") String outTradeNo,
                              @RequestParam("out_refund_no") String outRefundNo,
                              @RequestParam("refund_id") String refundId) {

        WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getWxPayApiConfig();

        Map<String, String> params = RefundQueryModel.builder()
                .appid(wxPayApiConfig.getAppId())
                .mch_id(wxPayApiConfig.getMchId())
                .nonce_str(WxPayKit.generateStr())
                .transaction_id(transactionId)
                .out_trade_no(outTradeNo)
                .out_refund_no(outRefundNo)
                .refund_id(refundId)
                .build()
                .createSign(wxPayApiConfig.getPartnerKey(), SignType.MD5);

        return WxPayApi.orderRefundQuery(false, params);
    }

    /**
     * 退款通知
     */
    @RequestMapping(value = "/noAuth/wxpay/refundNotify", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String refundNotify(HttpServletRequest request) {
        String xmlMsg = HttpKit.readData(request);
        log.info("退款通知=" + xmlMsg);
        Map<String, String> params = WxPayKit.xmlToMap(xmlMsg);

        String returnCode = params.get("return_code");
        // 注意重复通知的情况，同一订单号可能收到多次通知，请注意一定先判断订单状态
        if (WxPayKit.codeIsOk(returnCode)) {
            String reqInfo = params.get("req_info");
            String decryptData = WxPayKit.decryptData(reqInfo, WxPayApiConfigKit.getWxPayApiConfig().getPartnerKey());
            log.info("退款通知解密后的数据=" + decryptData);
            // 更新订单信息
            // 发送通知等
            Map<String, String> xml = new HashMap<String, String>(2);
            xml.put("return_code", "SUCCESS");
            xml.put("return_msg", "OK");
            return WxPayKit.toXml(xml);
        }
        return null;
    }
    @RequestMapping(value = "/noAuth/wxpay/payNotify", method = {RequestMethod.POST, RequestMethod.GET})
    public String payNotify(HttpServletRequest request) {
        String xmlMsg = HttpKit.readData(request);
        log.info("支付通知=" + xmlMsg);
        Map<String, String> params = WxPayKit.xmlToMap(xmlMsg);

        String returnCode = params.get("return_code");
        log.info("returnCode="+returnCode);
        // 注意重复通知的情况，同一订单号可能收到多次通知，请注意一定先判断订单状态
        // 注意此处签名方式需与统一下单的签名类型一致
        if (WxPayKit.verifyNotify(params, WxPayApiConfigKit.getWxPayApiConfig().getPartnerKey(), SignType.MD5)) {
            log.info("111111111111111111111");
            if (WxPayKit.codeIsOk(returnCode)) {
                // 更新订单信息
                log.info("returnCode="+returnCode);
                String outTradeNo = params.get("out_trade_no");
                BaseOrder baseOrder= baseOrderService.selectBaseOrderByTradeCode(outTradeNo);
                if("01".equals(baseOrder.getPayStatus())){
                    baseOrder.setPayStatus("02");
                    baseOrder.setPayTime(DateUtils.getNowDate());
                    baseOrder.setUpdateTime(DateUtils.getNowDate());
                    int result=baseOrderService.updateBaseOrder(baseOrder);
                    if(result>0){
                        int scoreAmount=baseOrder.getScoreAmount().intValue();
                        if(baseOrder.getScoreAmount().intValue()>0 && baseOrder.getOrderAmount().doubleValue()!=baseOrder.getPayAmount().doubleValue()){
                            BaseUser currentUser=userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
                            BaseUser scoreUser=new BaseUser();
                            scoreUser.setUserId(baseOrder.getUserId());
                            scoreUser.setScoreAmount(currentUser.getScoreAmount()-baseOrder.getScoreAmount().intValue());
                            userInfoService.updateBaseUserScore(scoreUser);

                            ScoreHistory refHis=new ScoreHistory();
                            refHis.setUserId(currentUser.getUserId());
                            refHis.setUserName(currentUser.getUserName());
                            refHis.setRemark("订单抵扣积分");
                            refHis.setScore(new BigDecimal(scoreAmount*-1));
                            refHis.setCreateTime(DateUtils.getNowDate());
                            scoreHistoryService.insertScoreHistory(refHis);
                        }
                    }
                }
                // 发送通知等
                Map<String, String> xml = new HashMap<String, String>(2);
                xml.put("return_code", "SUCCESS");
                xml.put("return_msg", "OK");
                return WxPayKit.toXml(xml);
            }
        }
        return null;
    }
}

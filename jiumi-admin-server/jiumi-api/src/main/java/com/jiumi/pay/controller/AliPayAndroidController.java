package com.jiumi.pay.controller;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeCreateRequest;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayFundAuthOrderFreezeResponse;
import com.alipay.api.response.AlipayFundCouponOrderAgreementPayResponse;
import com.alipay.api.response.AlipayTradeCreateResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.ijpay.core.enums.SignType;
import com.ijpay.core.enums.TradeType;
import com.ijpay.core.kit.IpKit;
import com.ijpay.core.kit.WxPayKit;
import com.ijpay.wxpay.WxPayApi;
import com.ijpay.wxpay.WxPayApiConfig;
import com.ijpay.wxpay.WxPayApiConfigKit;
import com.ijpay.wxpay.model.UnifiedOrderModel;
import com.jiumi.baseconfig.domain.BaseOrder;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.baseconfig.domain.ScoreHistory;
import com.jiumi.baseconfig.service.IBaseOrderService;
import com.jiumi.baseconfig.service.IBaseUserService;
import com.jiumi.baseconfig.service.IScoreHistoryService;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.uuid.Seq;
import com.jiumi.pay.alipay.AliPayApi;
import com.jiumi.pay.alipay.AliPayApiConfigKit;
import com.jiumi.pay.alipay.config.AliPayAndroidApiConfig;
import com.jiumi.pay.alipay.interceptor.AliPayApiController;
import com.jiumi.pay.domain.util.AjaxPayResult;
import com.jiumi.pay.domain.util.PayUtil;
import com.jiumi.pay.domain.util.RsaUtil;
import com.jiumi.pay.domain.util.StringUtils;
import com.jiumi.system.service.ISysConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Javen
 */
@RequestMapping("/api/aliPay")
@RestController
public class AliPayAndroidController extends AliPayApiController {
    private static Logger log = LoggerFactory.getLogger(AliPayAndroidController.class);
    @Autowired
    AliPayAndroidApiConfig aliPayApiConfig;

    private AlipayClient alipayClient;


    @Autowired
    private IBaseOrderService baseOrderService;

    @Autowired
    private IBaseUserService userInfoService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IScoreHistoryService scoreHistoryService;



    /**
     * 普通公钥模式
     */
    //private final static String NOTIFY_URL = "/aliPay/notify_url";
    /**
     * 证书模式
     */
     private final static String NOTIFY_URL = "/api/aliPay/notify_url";
    private final static String RETURN_URL = "/api/aliPay/return_url";
    /**
     * 证书模式
     */
    //private final static String RETURN_URL = "/aliPay/cert_return_url";



    @Override
    public AliPayAndroidApiConfig getApiConfig() {
        return aliPayApiConfig;
    }

    @RequestMapping("")
    @ResponseBody
    public String index() {
        return "欢迎使用 jiumi 支付";
    }

    @PostConstruct
    public void Contruct() throws AlipayApiException {
        try{
            AliPayApiConfigKit.setThreadLocalAliPayApiConfig(aliPayApiConfig.build());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping("/test")
    @ResponseBody
    public AliPayAndroidApiConfig test() {
        AliPayAndroidApiConfig aliPayApiConfig = AliPayApiConfigKit.getAliPayApiConfig();
        String charset = aliPayApiConfig.getCharset();
        log.info("charset>" + charset);
        return aliPayApiConfig;
    }


    /**
     * app支付
     */
    @RequestMapping(value = "/appPay", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public AjaxResult appPay(HttpServletRequest request) {
        //需要通过授权来获取openId
        String outTradeNo = Seq.getId();

        String orderCode=request.getParameter("orderCode");
        String scoreAmount=request.getParameter("scoreAmount");
        int scoreNum=0;
        if(com.jiumi.common.utils.StringUtils.isNotEmpty(scoreAmount)){
            scoreNum=Integer.parseInt(scoreAmount);
        }

        BaseOrder baseOrder= baseOrderService.selectBaseOrderByOrderCode(orderCode);
        if(baseOrder==null){
            return AjaxResult.error("订单不存在");
        }
        if(!"01".equals(baseOrder.getPayStatus())){
            return AjaxResult.error("待支付状态的订单才能进行支付");
        }
        BaseUser orderUser= userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
        if(orderUser.getScoreAmount().intValue()<scoreNum){
            return AjaxResult.error("您的积分不足!");
        }
        int orderAmount= baseOrder.getOrderAmount().multiply(new BigDecimal(100)).intValue();
        if(orderAmount<=0){
            return AjaxResult.error("订单金额错误");
        }
        String ip = IpKit.getRealIp(request);
        if (com.jiumi.common.utils.StringUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        String percent = configService.selectConfigByKey("sys.customer.usescore.percent");
        double scorePercent= com.jiumi.common.utils.StringUtils.isNotEmpty(percent)?Double.valueOf(percent):1;
        double scoreMoney=scoreNum*scorePercent;
        if(baseOrder.getOrderAmount().doubleValue()<scoreMoney){
            return AjaxResult.error("请输入合适的积分");
        }
        baseOrder.setPayType("02");
        baseOrder.setTradeCode(outTradeNo);
        baseOrder.setUpdateTime(DateUtils.getNowDate());
        //积分相抵
        if(baseOrder.getOrderAmount().doubleValue()==scoreMoney){
            baseOrder.setPayAmount(new BigDecimal(0));
            baseOrder.setScoreAmount(new BigDecimal(scoreNum));
            baseOrder.setPayTime(DateUtils.getNowDate());
            baseOrder.setPayStatus("02");
            baseOrderService.updateBaseOrder(baseOrder);

            BaseUser currentUser=userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
            BaseUser scoreUser=new BaseUser();
            scoreUser.setUserId(baseOrder.getUserId());
            scoreUser.setScoreAmount(currentUser.getScoreAmount()-baseOrder.getScoreAmount().intValue());
            userInfoService.updateBaseUserScore(scoreUser);

            ScoreHistory refHis=new ScoreHistory();
            refHis.setUserId(currentUser.getUserId());
            refHis.setUserName(currentUser.getUserName());
            refHis.setRemark("订单抵扣积分");
            refHis.setScore(new BigDecimal(scoreNum*-1));
            refHis.setCreateTime(DateUtils.getNowDate());
            scoreHistoryService.insertScoreHistory(refHis);

            AjaxResult payResult=  AjaxResult.success("支付成功");
            payResult.put("scoreFlag",'Y');
            return payResult;
        }
        BigDecimal payAmount=baseOrder.getOrderAmount().subtract(new BigDecimal(scoreMoney)).setScale(2, RoundingMode.HALF_UP);
        System.out.println("===========================");
        System.out.println(payAmount);
        baseOrder.setPayAmount(payAmount);
        baseOrder.setScoreAmount(new BigDecimal(scoreNum));


        String orderInfo="";
        try{

            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.setBody("家政服务支付");
            model.setSubject("家政服务支付");
            model.setOutTradeNo(outTradeNo);
            model.setTimeoutExpress("30m");
            model.setTotalAmount(String.valueOf(payAmount));
            model.setPassbackParams("callback params");
            model.setProductCode("QUICK_MSECURITY_PAY");
            orderInfo = AliPayApi.appPayToResponse(model, aliPayApiConfig.getDomain() + NOTIFY_URL).getBody();

            baseOrderService.updateBaseOrder(baseOrder);
        }
        catch (Exception e){
            e.printStackTrace();
            return  AjaxResult.error(e.getMessage());
        }

        AjaxResult payResult=  AjaxResult.success();
        payResult.put("scoreFlag",'N');
        payResult.put("payInfo",orderInfo);
        return payResult;


    }

    @RequestMapping(value = "/wapPayNoSdk")
    @ResponseBody
    public void wapPayNoSdk(HttpServletResponse response) {
        try {
            AliPayAndroidApiConfig aliPayApiConfig = AliPayApiConfigKit.getAliPayApiConfig();
            Map<String, String> paramsMap = new HashMap<>();
            paramsMap.put("app_id", aliPayApiConfig.getAppId());
            paramsMap.put("method", "alipay.trade.wap.pay");
            paramsMap.put("return_url", aliPayApiConfig.getDomain() + RETURN_URL);
            paramsMap.put("charset", aliPayApiConfig.getCharset());
            paramsMap.put("sign_type", aliPayApiConfig.getSignType());
            paramsMap.put("timestamp", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
            paramsMap.put("version", "1.0");
            paramsMap.put("notify_url", aliPayApiConfig.getDomain() + NOTIFY_URL);

            Map<String, String> bizMap = new HashMap<>();
            bizMap.put("body", "WQPay 聚合支付-H5");
            bizMap.put("subject", "WQPay 让支付触手可及");
            bizMap.put("out_trade_no", StringUtils.getOutTradeNo());
            bizMap.put("total_amount", "6.66");
            bizMap.put("product_code", "QUICK_WAP_WAY");

            paramsMap.put("biz_content", JSON.toJSONString(bizMap));

            String content = PayUtil.createLinkString(paramsMap);

            System.out.println(content);

            String encrypt = RsaUtil.encryptByPrivateKey(content, aliPayApiConfig.getPrivateKey());
            System.out.println(encrypt);
//            encrypt = AlipaySignature.rsaSign(content,aliPayApiConfig.getPrivateKey(), "UTF-8","RSA2");
//            System.out.println(encrypt);
            paramsMap.put("sign", encrypt);

            String url = aliPayApiConfig.getServiceUrl() + "?" + PayUtil.createLinkString(paramsMap, true);
            System.out.println(url);
            response.sendRedirect(url);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/wapPay")
    @ResponseBody
    public void wapPay(HttpServletResponse response) {
        String body = "我是测试数据-By Javen";
        String subject = "Javen Wap支付测试";
        String totalAmount = "1";
        String passbackParams = "1";
        String returnUrl = aliPayApiConfig.getDomain() + RETURN_URL;
        String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;

        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setBody(body);
        model.setSubject(subject);
        model.setTotalAmount(totalAmount);
        model.setPassbackParams(passbackParams);
        String outTradeNo = StringUtils.getOutTradeNo();
        System.out.println("wap outTradeNo>" + outTradeNo);
        model.setOutTradeNo(outTradeNo);
        model.setProductCode("QUICK_WAP_PAY");

        try {
            AliPayApi.wapPay(response, model, returnUrl, notifyUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * PC支付
     */
    @RequestMapping(value = "/pcPay")
    @ResponseBody
    public void pcPay(HttpServletResponse response) {
        try {
            String totalAmount = "0.08";
            String outTradeNo = StringUtils.getOutTradeNo();
            log.info("pc outTradeNo>" + outTradeNo);

            String returnUrl = aliPayApiConfig.getDomain() + RETURN_URL;
            String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;
            AlipayTradePagePayModel model = new AlipayTradePagePayModel();

            model.setOutTradeNo(outTradeNo);
            model.setProductCode("FAST_INSTANT_TRADE_PAY");
            model.setTotalAmount(totalAmount);
            model.setSubject("Javen PC支付测试");
            model.setBody("Javen WQPay PC支付测试");
            model.setPassbackParams("passback_params");
            /**
             * 花呗分期相关的设置,测试环境不支持花呗分期的测试
             * hb_fq_num代表花呗分期数，仅支持传入3、6、12，其他期数暂不支持，传入会报错；
             * hb_fq_seller_percent代表卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持，传入会报错。
             */
//            ExtendParams extendParams = new ExtendParams();
//            extendParams.setHbFqNum("3");
//            extendParams.setHbFqSellerPercent("0");
//            model.setExtendParams(extendParams);

            AliPayApi.tradePage(response, model, notifyUrl, returnUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/pcPayTest1")
    @ResponseBody
    public AjaxPayResult pcPayTest1(HttpServletRequest request, HttpServletResponse response) {
        try {
            //构造client
            CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
            //设置网关地址
            certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
            //设置应用Id
            certAlipayRequest.setAppId(aliPayApiConfig.getAppId());
            //设置应用私钥
            certAlipayRequest.setPrivateKey(aliPayApiConfig.getPrivateKey());
            //设置请求格式，固定值json
            certAlipayRequest.setFormat("json");
            //设置字符集
            certAlipayRequest.setCharset(aliPayApiConfig.getCharset());
            //设置签名类型
            certAlipayRequest.setSignType(aliPayApiConfig.getSignType());
            //设置应用公钥证书路径
            certAlipayRequest.setCertPath(aliPayApiConfig.getAppCertPath());
            //设置支付宝公钥证书路径
            certAlipayRequest.setAlipayPublicCertPath(aliPayApiConfig.getAliPayCertPath());
            //设置支付宝根证书路径
            certAlipayRequest.setRootCertPath(aliPayApiConfig.getAliPayRootCertPath());
            //构造client
            AlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);
            // 创建API对应的request类
            AlipayTradePrecreateRequest payrequest = new AlipayTradePrecreateRequest();
            String returnUrl = aliPayApiConfig.getDomain() + RETURN_URL;
            String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;
            payrequest.setNotifyUrl(notifyUrl);
            payrequest.setReturnUrl(returnUrl);
            payrequest.setBizContent ("{"   +
                    "\"out_trade_no\":\"20150320010101002\","   + //商户订单号
                    "\"total_amount\":\"0.01\","   +
                    "\"subject\":\"Iphone6 16G\","   +
                    "\"store_id\":\"NJ_001\","   +
                    "\"timeout_express\":\"90m\"}" ); //订单允许的最晚付款时间

            AlipayTradePrecreateResponse payresponse = alipayClient.certificateExecute( payrequest );
            String qrCode = alipayClient.pageExecute(payrequest).getQrCode();
            System.out.print(payresponse.getBody());
            return AjaxPayResult.success(payresponse);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 新版本支付SDK，网页扫码支付
     */
    @RequestMapping(value = "/pcPayNew")
    @ResponseBody
    public AlipayTradeCreateRequest pcPayNew(HttpServletResponse httpResponse) {
        AlipayTradeCreateRequest request = new AlipayTradeCreateRequest();
        AlipayTradeCreateModel model = new AlipayTradeCreateModel();
        model.setOutTradeNo(UUID.randomUUID().toString());
        model.setTotalAmount("88.88");
        model.setSubject("Iphone6 16G");
        model.setBuyerId("2088102177846880");
        request.setBizModel(model);
        return request;
    }




    @RequestMapping(value = "/tradePay")
    @ResponseBody
    public String tradePay(@RequestParam("authCode") String authCode, @RequestParam("scene") String scene) {
        String subject = null;
        String waveCode = "wave_code";
        String barCode = "bar_code";
        if (scene.equals(waveCode)) {
            subject = "Javen 支付宝声波支付测试";
        } else if (scene.equals(barCode)) {
            subject = "Javen 支付宝条形码支付测试";
        }
        String totalAmount = "100";
        String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;

        AlipayTradePayModel model = new AlipayTradePayModel();
        model.setAuthCode(authCode);
        model.setSubject(subject);
        model.setTotalAmount(totalAmount);
        model.setOutTradeNo(StringUtils.getOutTradeNo());
        model.setScene(scene);
        try {
            return AliPayApi.tradePayToResponse(model, notifyUrl).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 扫码支付
     */
    @RequestMapping(value = "/tradePreCreatePay")
    @ResponseBody
    public String tradePreCreatePay() {
        String subject = "Javen 支付宝扫码支付测试";
        String totalAmount = "11";
        String storeId = "123";
       String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;
        //String notifyUrl = aliPayApiConfig.getDomain() + "/aliPay/cert_notify_url";

        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
        model.setSubject(subject);
        model.setTotalAmount(totalAmount);
        model.setStoreId(storeId);
        model.setTimeoutExpress("5m");
        model.setOutTradeNo(StringUtils.getOutTradeNo());
        try {
            String resultStr = AliPayApi.tradePrecreatePayToResponse(model, notifyUrl).getBody();
            JSONObject jsonObject = JSONObject.parseObject(resultStr);
            return jsonObject.getJSONObject("alipay_trade_precreate_response").getString("qr_code");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 单笔转账到支付宝账户
     * https://docs.open.alipay.com/309/106235/
     */
    @RequestMapping(value = "/transfer")
    @ResponseBody
    public String transfer() {
        String totalAmount = "66";
        AlipayFundTransToaccountTransferModel model = new AlipayFundTransToaccountTransferModel();
        model.setOutBizNo(StringUtils.getOutTradeNo());
        model.setPayeeType("ALIPAY_LOGONID");
        model.setPayeeAccount("gxthqd7606@sandbox.com");
        model.setAmount(totalAmount);
        model.setPayerShowName("测试退款");
        model.setPayerRealName("沙箱环境");
        model.setRemark("javen测试单笔转账到支付宝");

        try {
            return AliPayApi.transferToResponse(model).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/transferQuery")
    @ResponseBody
    public String transferQuery(@RequestParam(required = false, name = "outBizNo") String outBizNo,
                                @RequestParam(required = false, name = "orderId") String orderId) {
        AlipayFundTransOrderQueryModel model = new AlipayFundTransOrderQueryModel();
        if (StringUtils.isNotEmpty(outBizNo)) {
            model.setOutBizNo(outBizNo);
        }
        if (StringUtils.isNotEmpty(orderId)) {
            model.setOrderId(orderId);
        }

        try {
            return AliPayApi.transferQueryToResponse(model).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/uniTransfer")
    @ResponseBody
    public String uniTransfer() {
        String totalAmount = "1";
        AlipayFundTransUniTransferModel model = new AlipayFundTransUniTransferModel();
        model.setOutBizNo(StringUtils.getOutTradeNo());
        model.setTransAmount(totalAmount);
        model.setProductCode("TRANS_ACCOUNT_NO_PWD");
        model.setBizScene("DIRECT_TRANSFER");
        model.setOrderTitle("统一转账-转账至支付宝账户");
        model.setRemark("WQPay 测试统一转账");

        Participant payeeInfo = new Participant();
        payeeInfo.setIdentity("gxthqd7606@sandbox.com");
        payeeInfo.setIdentityType("ALIPAY_LOGON_ID");
        payeeInfo.setName("沙箱环境");
        model.setPayeeInfo(payeeInfo);

        try {
            return AliPayApi.uniTransferToResponse(model,null).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/uniTransferQuery")
    @ResponseBody
    public String uniTransferQuery(@RequestParam(required = false, name = "outBizNo") String outBizNo,
                                   @RequestParam(required = false, name = "orderId") String orderId) {
        AlipayFundTransCommonQueryModel model = new AlipayFundTransCommonQueryModel();
        if (StringUtils.isNotEmpty(outBizNo)) {
            model.setOutBizNo(outBizNo);
        }
        if (StringUtils.isNotEmpty(orderId)) {
            model.setOrderId(orderId);
        }

        try {
            return AliPayApi.transCommonQueryToResponse(model,null).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/accountQuery")
    @ResponseBody
    public String accountQuery(@RequestParam(required = true, name = "aliPayUserId") String aliPayUserId) {
        AlipayFundAccountQueryModel model = new AlipayFundAccountQueryModel();
        model.setAlipayUserId(aliPayUserId);
        model.setAccountType("ACCTRANS_ACCOUNT");
        try {
            return AliPayApi.accountQueryToResponse(model,null).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 资金授权冻结接口
     */
    @RequestMapping(value = "/authOrderFreeze")
    @ResponseBody
    public AlipayFundAuthOrderFreezeResponse authOrderFreeze(@RequestParam("auth_code") String authCode) {
        try {
            AlipayFundAuthOrderFreezeModel model = new AlipayFundAuthOrderFreezeModel();
            model.setOutOrderNo(StringUtils.getOutTradeNo());
            model.setOutRequestNo(StringUtils.getOutTradeNo());
            model.setAuthCode(authCode);
            model.setAuthCodeType("bar_code");
            model.setOrderTitle("资金授权冻结-By WQPay");
            model.setAmount("36");
            model.setProductCode("PRE_AUTH");

            return AliPayApi.authOrderFreezeToResponse(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 红包协议支付接口
     * https://docs.open.alipay.com/301/106168/
     */
    @RequestMapping(value = "/agreementPay")
    @ResponseBody
    public AlipayFundCouponOrderAgreementPayResponse agreementPay() {
        try {
            AlipayFundCouponOrderAgreementPayModel model = new AlipayFundCouponOrderAgreementPayModel();
            model.setOutOrderNo(StringUtils.getOutTradeNo());
            model.setOutRequestNo(StringUtils.getOutTradeNo());
            model.setOrderTitle("红包协议支付接口-By WQPay");
            model.setAmount("36");
            model.setPayerUserId("2088102180432465");

            return AliPayApi.fundCouponOrderAgreementPayToResponse(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 下载对账单
     */
    @RequestMapping(value = "/dataDataServiceBill")
    @ResponseBody
    public String dataDataServiceBill(@RequestParam("billDate") String billDate) {
        try {
            AlipayDataDataserviceBillDownloadurlQueryModel model = new AlipayDataDataserviceBillDownloadurlQueryModel();
            model.setBillType("trade");
            model.setBillDate(billDate);
            return AliPayApi.billDownloadUrlQuery(model);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 退款
     */
    @RequestMapping(value = "/tradeRefund")
    @ResponseBody
    public String tradeRefund(@RequestParam(required = false, name = "outTradeNo") String outTradeNo, @RequestParam(required = false, name = "tradeNo") String tradeNo) {

        try {
            AlipayTradeRefundModel model = new AlipayTradeRefundModel();
            if (StringUtils.isNotEmpty(outTradeNo)) {
                model.setOutTradeNo(outTradeNo);
            }
            if (StringUtils.isNotEmpty(tradeNo)) {
                model.setTradeNo(tradeNo);
            }
            model.setRefundAmount("86.00");
            model.setRefundReason("正常退款");
            return AliPayApi.tradeRefundToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 交易查询
     */
    @RequestMapping(value = "/tradeQuery")
    @ResponseBody
    public String tradeQuery(@RequestParam(required = false, name = "outTradeNo") String outTradeNo, @RequestParam(required = false, name = "tradeNo") String tradeNo) {
        try {
            AlipayTradeQueryModel model = new AlipayTradeQueryModel();
            if (StringUtils.isNotEmpty(outTradeNo)) {
                model.setOutTradeNo(outTradeNo);
            }
            if (StringUtils.isNotEmpty(tradeNo)) {
                model.setTradeNo(tradeNo);
            }
            return AliPayApi.tradeQueryToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/tradeQueryByStr")
    @ResponseBody
    public String tradeQueryByStr(@RequestParam(required = false, name = "outTradeNo") String outTradeNo, @RequestParam(required = false, name = "tradeNo") String tradeNo) {
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        if (StringUtils.isNotEmpty(outTradeNo)) {
            model.setOutTradeNo(outTradeNo);
        }
        if (StringUtils.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }

        try {
            return AliPayApi.tradeQueryToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 创建订单
     * {"alipay_trade_create_response":{"code":"10000","msg":"Success","out_trade_no":"081014283315033","trade_no":"2017081021001004200200274066"},"sign":"ZagfFZntf0loojZzdrBNnHhenhyRrsXwHLBNt1Z/dBbx7cF1o7SZQrzNjRHHmVypHKuCmYifikZIqbNNrFJauSuhT4MQkBJE+YGPDtHqDf4Ajdsv3JEyAM3TR/Xm5gUOpzCY7w+RZzkHevsTd4cjKeGM54GBh0hQH/gSyhs4pEN3lRWopqcKkrkOGZPcmunkbrUAF7+AhKGUpK+AqDw4xmKFuVChDKaRdnhM6/yVsezJFXzlQeVgFjbfiWqULxBXq1gqicntyUxvRygKA+5zDTqE5Jj3XRDjVFIDBeOBAnM+u03fUP489wV5V5apyI449RWeybLg08Wo+jUmeOuXOA=="}
     */
    @RequestMapping(value = "/tradeCreate")
    @ResponseBody
    public String tradeCreate(@RequestParam("outTradeNo") String outTradeNo) {

        String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;

        AlipayTradeCreateModel model = new AlipayTradeCreateModel();
        model.setOutTradeNo(outTradeNo);
        model.setTotalAmount("88.88");
        model.setBody("Body");
        model.setSubject("Javen 测试统一收单交易创建接口");
        //买家支付宝账号，和buyer_id不能同时为空
        model.setBuyerLogonId("abpkvd0206@sandbox.com");
        try {
            AlipayTradeCreateResponse response = AliPayApi.tradeCreateToResponse(model, notifyUrl);
            return response.getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 撤销订单
     */
    @RequestMapping(value = "/tradeCancel")
    @ResponseBody
    public String tradeCancel(@RequestParam(required = false, name = "outTradeNo") String outTradeNo, @RequestParam(required = false, name = "tradeNo") String tradeNo) {
        try {
            AlipayTradeCancelModel model = new AlipayTradeCancelModel();
            if (StringUtils.isNotEmpty(outTradeNo)) {
                model.setOutTradeNo(outTradeNo);
            }
            if (StringUtils.isNotEmpty(tradeNo)) {
                model.setTradeNo(tradeNo);
            }

            return AliPayApi.tradeCancelToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 关闭订单
     */
    @RequestMapping(value = "/tradeClose")
    @ResponseBody
    public String tradeClose(@RequestParam("outTradeNo") String outTradeNo, @RequestParam("tradeNo") String tradeNo) {
        try {
            AlipayTradeCloseModel model = new AlipayTradeCloseModel();
            if (StringUtils.isNotEmpty(outTradeNo)) {
                model.setOutTradeNo(outTradeNo);
            }
            if (StringUtils.isNotEmpty(tradeNo)) {
                model.setTradeNo(tradeNo);
            }

            return AliPayApi.tradeCloseToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 结算
     */
    @RequestMapping(value = "/tradeOrderSettle")
    @ResponseBody
    public String tradeOrderSettle(@RequestParam("tradeNo") String tradeNo) {
        try {
            AlipayTradeOrderSettleModel model = new AlipayTradeOrderSettleModel();
            model.setOutRequestNo(StringUtils.getOutTradeNo());
            model.setTradeNo(tradeNo);

            return AliPayApi.tradeOrderSettleToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取应用授权URL并授权
     */
    @RequestMapping(value = "/toOauth")
    @ResponseBody
    public void toOauth(HttpServletResponse response) {
        try {
            String redirectUri = aliPayApiConfig.getDomain() + "/aliPay/redirect_uri";
            String oauth2Url = AliPayApi.getOauth2Url(aliPayApiConfig.getAppId(), redirectUri);
            response.sendRedirect(oauth2Url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 应用授权回调
     */
    @RequestMapping(value = "/redirect_uri")
    @ResponseBody
    public String redirectUri(@RequestParam("app_id") String appId, @RequestParam("app_auth_code") String appAuthCode) {
        try {
            System.out.println("app_id:" + appId);
            System.out.println("app_auth_code:" + appAuthCode);
            //使用app_auth_code换取app_auth_token
            AlipayOpenAuthTokenAppModel model = new AlipayOpenAuthTokenAppModel();
            model.setGrantType("authorization_code");
            model.setCode(appAuthCode);
            return AliPayApi.openAuthTokenAppToResponse(model).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查询授权信息
     */
    @RequestMapping(value = "/openAuthTokenAppQuery")
    @ResponseBody
    public String openAuthTokenAppQuery(@RequestParam("appAuthToken") String appAuthToken) {
        try {
            AlipayOpenAuthTokenAppQueryModel model = new AlipayOpenAuthTokenAppQueryModel();
            model.setAppAuthToken(appAuthToken);
            return AliPayApi.openAuthTokenAppQueryToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 批量付款到支付宝账户有密接口
     */
    @RequestMapping(value = "/batchTrans")
    @ResponseBody
    public void batchTrans(HttpServletResponse response) {
        try {
            String signType = "MD5";
            String notifyUrl = aliPayApiConfig.getDomain() + NOTIFY_URL;
            Map<String, String> params = new HashMap<>(15);
            params.put("partner", "PID");
            params.put("sign_type", signType);
            params.put("notify_url", notifyUrl);
            params.put("account_name", "xxx");
            params.put("detail_data", "流水号1^收款方账号1^收款账号姓名1^付款金额1^备注说明1|流水号2^收款方账号2^收款账号姓名2^付款金额2^备注说明2");
            params.put("batch_no", String.valueOf(System.currentTimeMillis()));
            params.put("batch_num", 1 + "");
            params.put("batch_fee", 10.00 + "");
            params.put("email", "xx@xxx.com");

            AliPayApi.batchTrans(params, aliPayApiConfig.getPrivateKey(), signType, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 地铁购票核销码发码
     */
    @RequestMapping(value = "/voucherGenerate")
    @ResponseBody
    public String voucherGenerate(@RequestParam("tradeNo") String tradeNo) {
        try {
            //需要支付成功的订单号
//			String tradeNo = getPara("tradeNo");

            AlipayCommerceCityfacilitatorVoucherGenerateModel model = new AlipayCommerceCityfacilitatorVoucherGenerateModel();
            model.setCityCode("440300");
            model.setTradeNo(tradeNo);
            model.setTotalFee("8");
            model.setTicketNum("2");
            model.setTicketType("oneway");
            model.setSiteBegin("001");
            model.setSiteEnd("002");
            model.setTicketPrice("4");
            return AliPayApi.voucherGenerateToResponse(model).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/return_url")
    @ResponseBody
    public String returnUrl(HttpServletRequest request,HttpServletResponse response) {
        log.info("*******************return_urlreturn_urlreturn_url******************************");
        try {
            // 获取支付宝GET过来反馈信息
            Map<String, String> map = AliPayApi.toMap(request);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                log.info(entry.getKey() + " = " + entry.getValue());
            }

            boolean verifyResult = AlipaySignature.rsaCheckV1(map, aliPayApiConfig.getAliPayPublicKey(), "UTF-8","RSA2");
            log.info("return_url "+aliPayApiConfig.getAliPayPublicKey());
            log.info(verifyResult+"");
            if (verifyResult) {
                // TODO 请在这里加上商户的业务逻辑程序代码
                System.out.println("return_url 验证成功");
                String outTradeCode=map.get("outTradeNo");
                response.sendRedirect("http://manage.huixuekc.com/order/BaseOrderPayInfo");
                return null;
            } else {
                System.out.println("return_url 验证失败");
                // TODO
                return "failure";
            }
        } catch (Exception e) {
            log.info("*******************return_urlreturn_urlreturn_url  error log log ******************************"+e.getMessage());
            e.printStackTrace();
            return "failure";
        }
    }

    @RequestMapping(value = "/cert_return_url")
    @ResponseBody
    public String certReturnUrl(HttpServletRequest request,HttpServletResponse response) {
        try {
            // 获取支付宝GET过来反馈信息
            Map<String, String> map = AliPayApi.toMap(request);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }

            boolean verifyResult = AlipaySignature.rsaCertCheckV1(map, aliPayApiConfig.getAliPayCertPath(), "UTF-8",
                    "RSA2");

            if (verifyResult) {
                // TODO 请在这里加上商户的业务逻辑程序代码
                System.out.println("return_url 验证成功");
                String outTradeCode=map.get("out_trade_no");
                response.sendRedirect("http://manage.huixuekc.com");
                return null;
            } else {
                System.out.println("certReturnUrl 验证失败");
                // TODO
                return "failure";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
    }


    @RequestMapping(value = "/notify_url")
    @ResponseBody
    public String notifyUrl(HttpServletRequest request) {
        log.info("*******************notify_urlnotify_urlnotify_url******************************");
        try {
            // 获取支付宝POST过来反馈信息
            Map<String, String> params = AliPayApi.toMap(request);
            log.info("*******************4444444444444444******************************");
            log.info(params+"");
            boolean verifyResult = AlipaySignature.rsaCheckV1(params, aliPayApiConfig.getAliPayPublicKey(), "UTF-8", "RSA2");
            log.info(verifyResult+"");
            if (verifyResult) {
                String orderCode=params.get("out_trade_no");
                BaseOrder baseOrder=baseOrderService.selectBaseOrderByTradeCode(orderCode);
                log.info("------------------------------------orderType--------");
                log.info(baseOrder.toString());
                if("01".equals(baseOrder.getPayStatus())){
                    baseOrder.setPayStatus("02");
                    baseOrder.setPayTime(DateUtils.getNowDate());
                    baseOrder.setUpdateTime(DateUtils.getNowDate());
                    int result=baseOrderService.updateBaseOrder(baseOrder);
                    if(result>0){
                        int scoreAmount=baseOrder.getScoreAmount().intValue();
                        if(baseOrder.getScoreAmount().intValue()>0 && baseOrder.getOrderAmount().doubleValue()!=baseOrder.getPayAmount().doubleValue()){
                            BaseUser currentUser=userInfoService.selectBaseUserByUserId(baseOrder.getUserId());
                            BaseUser scoreUser=new BaseUser();
                            scoreUser.setUserId(baseOrder.getUserId());
                            scoreUser.setScoreAmount(currentUser.getScoreAmount()-baseOrder.getScoreAmount().intValue());
                            userInfoService.updateBaseUserScore(scoreUser);

                            ScoreHistory refHis=new ScoreHistory();
                            refHis.setUserId(currentUser.getUserId());
                            refHis.setUserName(currentUser.getUserName());
                            refHis.setRemark("订单抵扣积分");
                            refHis.setScore(new BigDecimal(scoreAmount*-1));
                            refHis.setCreateTime(DateUtils.getNowDate());
                            scoreHistoryService.insertScoreHistory(refHis);
                        }
                    }
                }
                if("02".equals(baseOrder.getPayStatus())){
                    return "success";
                }
                // TODO 请在这里加上商户的业务逻辑程序代码 异步通知可能出现订单重复通知 需要做去重处理
                System.out.println("notify_url 验证成功succcess");
                return "success";
            } else {
                System.out.println("notify_url 验证失败");
                // TODO
                return "failure";
            }
        } catch (AlipayApiException e) {
            log.info("*******************notify_urlnotify_urlnotify_url  error log log ******************************"+e.getMessage());
            log.info(e.getErrMsg());
            e.printStackTrace();
            return "failure";
        }
    }

    /**
     * 支付回调接口
     * @param request
     * @return
     */
    @RequestMapping(value = "/cert_notify_url")
    @ResponseBody
    public String certNotifyUrl(HttpServletRequest request) {
        //PayCallBackApiController dataController = (PayCallBackApiController) AopContext.currentProxy();
        // dataController.aa(orderId);
        //获取支付宝POST过来反馈信息
        try {
            // 获取支付宝POST过来反馈信息
            Map<String, String> params = AliPayApi.toMap(request);

            for (Map.Entry<String, String> entry : params.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }

            boolean verifyResult = AlipaySignature.rsaCertCheckV1(params, aliPayApiConfig.getAliPayCertPath(), "UTF-8", "RSA2");

            if (verifyResult) {
                String orderCode=params.get("out_trade_no");
               /* OmsOrder order=omsOrderService.selectOmsOrderByOutTradeCode(orderCode);
                if("01".equals(order.getStatus())){
                    order.setPayTime(DateUtils.getNowDate());
                    order.setStatus("02");
                    omsOrderService.updateOmsOrder(order);
                    omsOrderService.deleteCartMaterial(order);
                    log.info("------------------------------------orderType-2222222-------");
                    log.info("---------------order.getOrderType()-----------"+order.getOrderType());
                    if("03".equals(order.getOrderType())){
                        userVipComboDetailService.updateUserVipComboDetailByOrderCode(order.getOrderCode());
                    }
                }
                if("02".equals(order.getStatus())){
                    return "success";
                }*/
            } else {
                System.out.println("certNotifyUrl 验证失败");
                // TODO
                return "failure";
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "failure";
        }
        return null;
    }


}

package com.jiumi.api.controller;
import com.jiumi.api.base.BaseApiController;
import com.jiumi.baseconfig.domain.*;
import com.jiumi.baseconfig.service.*;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.core.page.TableDataInfo;
import com.jiumi.common.core.redis.RedisCache;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.DictUtils;
import com.jiumi.common.utils.SecurityUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.common.utils.uuid.IdUtils;
import com.jiumi.common.utils.uuid.Seq;
import com.jiumi.system.service.ISysConfigService;
import io.swagger.annotations.ApiOperation;
import org.aspectj.weaver.loadtime.Aj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 发布简历接口
 *
 * @author jiumi
 * @date 2021-08-18
 */
@RestController
@RequestMapping("/api/business")
public class BusinessApiController extends BaseApiController
{
    private static final Logger logger = LoggerFactory.getLogger(BusinessApiController.class);

    @Autowired
    private IBaseUserService userService;

    @Autowired
    private IBaseOrderCombineService baseOrderCombineService;

    @Autowired
    private IBaseContractService baseContractService;

    @Autowired
    private IBaseCompanyAccountService baseCompanyAccountService;

    @Autowired
    private IBaseCompanyAddressService baseCompanyAddressService;

    @Autowired
    private IBaseCompanyService baseCompanyService;

    @Autowired
    private IBaseContractLogService baseContractLogService;

    @Autowired
    private IBaseAppointmentService baseAppointmentService;

    @Autowired
    private IBaseOrderService baseOrderService;

    @Autowired
    private IUserMessageService userMessageService;

    @Autowired
    private IContractContentService contractContentService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IBusinessConfigService businessConfigService;

    @Autowired
    private IBaseUserIntentionService baseUserIntentionService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBaseUserResumeService baseUserResumeService;


    @GetMapping("/getUserCombineOrder")
    public TableDataInfo getUserCombineOrder(BaseOrderCombine BaseOrderCombine) {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            BaseOrderCombine.setIsAdmin("Y");
        }
        else{
            BaseOrderCombine.setIsAdmin("N");
        }
        BaseOrderCombine.setAuntCompanyId(currentUser.getCompanyId());
        BaseOrderCombine.setUserId(currentUser.getUserId());
        startPage();
        startOrderBy();
        List list=baseOrderCombineService.selectUserOrderCombineList(BaseOrderCombine);
        return getDataTable(list);
    }

    @GetMapping("/getCombineOrderDetail")
    public AjaxResult getCombineOrderDetail(@RequestParam(value="id",required = true) Long id)
    {
        BaseOrderCombine order=baseOrderCombineService.selectBaseOrderCombineById(id);
        AjaxResult result= AjaxResult.success(order);
        BaseUser auntUser= userService.selectBaseUserByUserId(order.getAuntUserId());
        BaseUserIntention userIntention= baseUserIntentionService.selectBaseUserIntentionByUserId(auntUser.getUserId());
        BaseUserResume userResume=baseUserResumeService.selectBaseUserResumeByUserId(auntUser.getUserId());
        auntUser.setLowSalary((userIntention!=null && userIntention.getLowSalary()!=null)?userIntention.getLowSalary().intValue():0);
        auntUser.setHighSalary((userIntention!=null && userIntention.getHighSalary()!=null)?userIntention.getHighSalary().intValue():0);
        auntUser.setJobTypeName(userIntention.getTypeName());
        auntUser.setAbilityLabel(userResume.getAbilityLabel());
        result.put("auntUser",auntUser);
        result.put("employUser",userService.selectBaseUserByUserId(order.getEmployUserId()));
        result.put("publishUser",userService.selectBaseUserByUserId(order.getPublishUserId()));
        result.put("appointmentInfo",baseAppointmentService.selectBaseAppointmentById(order.getAppointmentId()));
        return result;
    }

    @GetMapping("/dealCombineOrder")
    public AjaxResult dealCombineOrder(@RequestParam(value="id",required = true) Long id,
                                       @RequestParam(value="approveStatus",required = true) String approveStatus)
    {
        BaseOrderCombine order=baseOrderCombineService.selectBaseOrderCombineById(id);
        if(order==null){
            return AjaxResult.error("合单记录不存在");
        }
        if(!"01".equals(order.getApplyStatus())){
            return AjaxResult.error("合单已处理");
        }
        if("02".equals(approveStatus) || "03".equals(approveStatus)){
            order.setApplyStatus(approveStatus);
            order.setConfirmTime(DateUtils.getNowDate());
            order.setUpdateBy(getUsername());
            order.setUpdateTime(DateUtils.getNowDate());
            baseOrderCombineService.updateBaseOrderCombine(order);
        }else{
            return AjaxResult.error("处理状态异常");
        }
        return AjaxResult.success("操作成功");
    }

    @PostMapping("/setUserCombineOrder")
    public AjaxResult setUserCombineOrder(@RequestBody BaseOrderCombine baseOrderCombine) {
        BaseAppointment appointment=baseAppointmentService.selectBaseAppointmentById(baseOrderCombine.getAppointmentId());
        if(appointment==null){
            return AjaxResult.error("预约记录不存在");
        }
        BaseOrderCombine checkOrder=new BaseOrderCombine();
        checkOrder.setAppointmentId(appointment.getId());
        checkOrder.setApplyStatus("02");
        List<BaseOrderCombine>  checkOrderList=baseOrderCombineService.selectBaseOrderCombineList(checkOrder);
        if(checkOrderList.size()>0){
            return AjaxResult.error("已经有阿姨合单成功");
        }
        BaseOrderCombine param=new BaseOrderCombine();
        param.setAppointmentId(appointment.getId());
        param.setAuntUserId(baseOrderCombine.getAuntUserId());
        List<BaseOrderCombine>  order=baseOrderCombineService.selectBaseOrderCombineList(param);
        if(order.size()>0){
            return AjaxResult.error("您已经提交，请勿重复操作");
        }
        BaseUser auntUser= userService.selectBaseUserByUserId(baseOrderCombine.getAuntUserId());
        BaseUser empUser= userService.selectBaseUserByUserId(appointment.getUserId());
        if(auntUser.getCompanyId().intValue()==empUser.getCompanyId().intValue()){
            return AjaxResult.error("雇主阿姨属同一家公司，不允许合单");
        }

        baseOrderCombine.setPublishUserId(getUserId());
        baseOrderCombine.setType(baseOrderCombine.getType());
        baseOrderCombine.setAuntType(appointment.getCategoryType());
        baseOrderCombine.setAuntCompanyId(auntUser.getCompanyId());
        baseOrderCombine.setEmployUserId(appointment.getUserId());
        baseOrderCombine.setEmployCompanyId(empUser.getCompanyId());
        baseOrderCombine.setApplyTime(DateUtils.getNowDate());
        baseOrderCombine.setApplyStatus("01");
        baseOrderCombine.setCreateBy(getUsername());
        baseOrderCombine.setCreateTime(DateUtils.getNowDate());
        int result= baseOrderCombineService.insertBaseOrderCombine(baseOrderCombine);
        if(result>0){
            /*UserMessage message=new UserMessage();
            message.setUserId(baseOrderCombine.getAuntUserId());
            message.setCategory("02");
            message.setTitle("合单消息");
            message.setLinkId(String.valueOf(baseOrderCombine.getId()));
            message.setMsgContent(empUser.getAuthName()+" "+ DictUtils.getDictLabel("base_appointment_user_type",appointment.getCategoryType())+" "+appointment.getSalaryAmount()+"元，"+appointment.getNeedServiceType());
            userMessageService.insertUserMessage(message);*/
        }
        AjaxResult rst=AjaxResult.success();
        rst.put("orderId",baseOrderCombine.getId());
        return rst;
    }

    @GetMapping("/getCompanyAuntList")
    public TableDataInfo getCompanyAuntList(BaseUser baseUser) {
        BaseUser currentUser= getLoginUser().getUserInfo();
        baseUser.setCompanyId(currentUser.getCompanyId());
        baseUser.setUserType("04");
        startPage();
        startOrderBy();
        List list=userService.selectCompanyAuntList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/getContractAuntList")
    public TableDataInfo getContractAuntList(BaseUser baseUser) {
        baseUser.setUserType("04");
        baseUser.setAuthStatus("02");
        startPage();
        startOrderBy();
        List list=userService.selectContractAuntList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/getContractEmployList")
    public TableDataInfo getContractEmployList(BaseUser baseUser) {
        baseUser.setUserType("03");
        baseUser.setAuthStatus("02");
        startPage();
        List list=userService.selectContractEmployList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/getCompanyEmployList")
    public TableDataInfo getCompanyEmployList(BaseAppointment appointment) {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        String isAdmin="N";
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            isAdmin="Y";
        }
        appointment.getParams().put("isAdmin",isAdmin);
        appointment.setUserId(getUserId());
        startPage();
        List<BaseAppointment> list=baseAppointmentService.selectCompanyAppointmentList(appointment);
        return getDataTable(list);
    }

    //公司负责人视角
    @GetMapping("/getCompanyEmployData")
    public TableDataInfo getCompanyEmployData(BaseUser baseUser) {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        String isAdmin="N";
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            isAdmin="Y";
        }

        baseUser.setAuthUserId(getUserId());
        baseUser.setIsAdmin(isAdmin);
        startPage();
        List list=userService.selectCompanyEpmloyList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/queryCompanyEmployData")
    public AjaxResult queryCompanyEmployData(BaseUser baseUser) {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        String isAdmin="N";
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            isAdmin="Y";
        }

        baseUser.setAuthUserId(getUserId());
        baseUser.setIsAdmin(isAdmin);
        startPage();
        List list=userService.selectQueryCompanyEpmloyList(baseUser);
        return AjaxResult.success(list);
    }

    //公司负责人视角
    @GetMapping("/getCompanyAuntData")
    public TableDataInfo getCompanyAuntData(BaseUser baseUser) {
        baseUser.setAuthUserId(getUserId());
        startPage();
        List list=userService.selectCompanyAuntList(baseUser);
        return getDataTable(list);
    }

    @GetMapping("/getEmployeeAuntData")
    public TableDataInfo getEmployeeAuntData(BaseUser baseUser) {
        baseUser.setCompanyId(getLoginUser().getUserInfo().getCompanyId());
        startPage();
        List list=userService.selectCompanyAuntList(baseUser);
        return getDataTable(list);
    }

    //公司负责人视角
    @GetMapping("/getCompanyContractData")
    public TableDataInfo getCompanyContractData(BaseContract baseContract) {
        startPage();
        List list=baseContractService.selectCompanyContractList(baseContract);
        return getDataTable(list);
    }

    //查询公司生效中的合同
    @GetMapping("/getCompanyEffectContract")
    public AjaxResult getCompanyEffectContract(BaseContract baseContract) {
        Long companyId= getLoginUser().getUserInfo().getCompanyId();
        baseContract.setCcompanyId(companyId);
        baseContract.setEffectFlag("Y");
        List list=baseContractService.selectCompanyContractList(baseContract);
        return AjaxResult.success(list);
    }

    @GetMapping("/getCompanyCombineOrder")
    public TableDataInfo getCompanyCombineOrder(BaseOrderCombine BaseOrderCombine) {
        BaseOrderCombine.setAuntCompanyId(getUserId());
        startPage();
        startOrderBy();
        List list=baseOrderCombineService.selectUserOrderCombineList(BaseOrderCombine);
        return getDataTable(list);
    }

    @PostMapping("/saveContract")
    public AjaxResult saveContract(@RequestBody BaseContract baseContract) {

        Long userId=getUserId();
        baseContract.setStatus("01");
        baseContract.setCreateUserId(getUserId());
        baseContract.setCreateBy(getUsername());
        AjaxResult ajaxResult=AjaxResult.success();
        String contractCode=DateUtils.dateTimeNowHH();
        String dateStr=contractCode.substring(0,8);
        List<BaseContract> numList=baseContractService.selectContractListByDate(dateStr);
        String tempCode = String.format("%03d", numList.size()+1);
        contractCode=contractCode+tempCode;
        baseContract.setContractCode(contractCode);
        BaseUser currentUser=userService.selectBaseUserByUserId(userId);
        if(!"02".equals(currentUser.getAuthStatus())){
            return AjaxResult.error("只有认证用户才能创建合同");
        }
        if("02".equals(baseContract.getCategoryType()) || "04".equals(baseContract.getCategoryType())){
            if("03".equals(currentUser.getUserType()) || "04".equals(currentUser.getUserType())){
                return AjaxResult.error("只有平台才能创建三方合同");
            }
            BaseCompany currentCompany=baseCompanyService.selectBaseCompanyById(currentUser.getCompanyId());
            if(!"02".equals(currentCompany.getAuthStatus())){
                return AjaxResult.error("只有认证的公司才能创建合同");
            }
            if(StringUtils.isEmpty(currentCompany.getSealUrl())){
                return AjaxResult.error("公司未上传公章");
            }
            BaseCompanyAddress compayAddress=new BaseCompanyAddress();
            compayAddress.setCompanyId(currentCompany.getId());
            List<BaseCompanyAddress> addressList= baseCompanyAddressService.selectBaseCompanyAddressList(compayAddress);
            String companyAddress=currentUser.getAddress();
            if(addressList.size()>0){
                companyAddress=addressList.get(0).getAddress();
            }
            baseContract.setCuserId(currentUser.getUserId());
            baseContract.setCuserPhone(currentUser.getPhonenumber());
            baseContract.setCuserAddress(baseContract.getCuserAddress());
            baseContract.setCcompanyName(currentUser.getCompanyName());
            baseContract.setCcompanyId(currentUser.getCompanyId());
        }
        if(StringUtils.isEmpty(baseContract.getServiceAddress())){
            BaseUser aUser= userService.selectBaseUserByUserId(baseContract.getAuserId());
            baseContract.setServiceAddress(baseContract.getAuserAddress());
        }
        int result= baseContractService.insertBaseContract(baseContract);
        ajaxResult.put("id",baseContract.getId());
        ajaxResult.put("contractCode",baseContract.getContractCode());
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"新建合同",getUsername());
        return ajaxResult;
    }




    @PostMapping("/saveContractAgain")
    public AjaxResult saveContractAgain(@RequestBody BaseContract baseContract) {
        BaseContract oldContract= baseContractService.selectBaseContractById(baseContract.getId());
        if(oldContract==null){
            return AjaxResult.error("合同不存在");
        }
        if(!"02".equals(oldContract.getCategoryType()) && !"04".equals(oldContract.getCategoryType()) ){
            return AjaxResult.error("只有三方合同才能进行更换服务员操作");
        }
        if(!"01".equals(oldContract.getStatus()) && !"04".equals(oldContract.getStatus()) && !"06".equals(oldContract.getStatus())){
            return AjaxResult.error("只有新建、被驳回、生效中的合同才能进行更换服务员操作");
        }
        baseContract.setStatus("01");
        baseContract.setCreateUserId(getUserId());
        baseContract.setCreateBy(getUsername());
        AjaxResult ajaxResult=AjaxResult.success();
        BaseContract param=new BaseContract();
        param.setContractCode(oldContract.getContractCode().substring(0,13));
        List<BaseContract> dataList=baseContractService.selectBaseContractList(param);
        String newContractCode=oldContract.getContractCode().substring(0,13)+"-"+String.format("%02d", dataList.size());
        baseContract.setContractCode(newContractCode);

        BaseOrder orderparam=new BaseOrder();
        orderparam.setOrderCode(oldContract.getContractCode());
        orderparam.setPayStatus("01");
        List<BaseOrder> concactOrder=baseOrderService.selectBaseOrderList(orderparam);
        if(concactOrder.size()>0){
            return AjaxResult.error("不能进行该操作，还存在未结清的订单");
        }
        baseContract.setUpdateBy(null);
        baseContract.setUpdateTime(null);
        baseContract.setConfirmTime(null);
        baseContract.setConfirmUserId(null);
        baseContract.setCreateTime(DateUtils.getNowDate());
        baseContract.setAsignatureDate(null);
        baseContract.setAsignatureUrl(null);
        baseContract.setBsignatureDate(null);
        baseContract.setBsignatureUrl(null);
        int result= baseContractService.insertBaseContract(baseContract);

        oldContract.setStatus("05");
        oldContract.setUpdateTime(DateUtils.getNowDate());
        oldContract.setUpdateBy(getUsername());
        baseContractService.updateBaseContract(oldContract);

        ajaxResult.put("id",baseContract.getId());
        ajaxResult.put("contractCode",baseContract.getContractCode());
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"更换家政服务员",getUsername());
        return ajaxResult;
    }


    //修改合同
    @PostMapping("/updateContract")
    public AjaxResult updateContract(@RequestBody BaseContract baseContract) {
        BaseContract currentContract= baseContractService.selectBaseContractById(baseContract.getId());
        if("06".equals(currentContract.getStatus())){
            //驳回再更改就是代签字状态
            baseContract.setStatus("01");
            baseContract.setCreateTime(DateUtils.getNowDate());
        }
        int result= baseContractService.updateBaseContract(baseContract);
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"修改合同",getUsername());
        return toAjax(result);
    }

    //甲方签字
    @PostMapping("/employSignContract")
    public AjaxResult employSignContract(@RequestBody BaseContract baseContract) {
        BaseContract contract=baseContractService.selectBaseContractById(baseContract.getId());
        if(!"01".equals(contract.getStatus())){
            return AjaxResult.error("合同状态异常");
        }
        if(contract.getAuserId().intValue()!=getUserId().intValue()){
            return AjaxResult.error("您不是甲方用户");
        }
        baseContract.setAsignatureDate(DateUtils.getNowDate());
        baseContract.setUpdateBy(getUsername());
        baseContract.setStatus("02");
        int result= baseContractService.updateBaseContract(baseContract);
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"雇主签字",getUsername());
        return toAjax(result);
    }

    //已方签字
    @PostMapping("/auntSignContract")
    public AjaxResult auntSignContract(@RequestBody BaseContract baseContract) {
        AjaxResult result= baseContractService.auntSignBaseContract(baseContract,getLoginUser().getUserInfo());
        return result;
    }

    @PostMapping("/backContract")
    public AjaxResult backContract(@RequestBody BaseContract baseContract) {
        BaseContract contract=baseContractService.selectBaseContractById(baseContract.getId());
        if(!"01".equals(contract.getStatus()) && !"02".equals(contract.getStatus()) && !"03".equals(contract.getStatus())){
            return AjaxResult.error("合同状态异常");
        }
        baseContract.setUpdateBy(getUsername());
        baseContract.setStatus("06");
        baseContract.setAsignatureUrl("");
        baseContract.setBsignatureUrl("");
        baseContract.setConfirmsealUrl("");
        int result= baseContractService.updateBaseContract(baseContract);
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"驳回签字",getUsername());
        return toAjax(result);
    }

    //丙方确认
    @PostMapping("/confirmSignContract")
    public AjaxResult confirmSignContract(@RequestBody BaseContract baseContract) {
        BaseUser currentUser=getLoginUser().getUserInfo();
        AjaxResult result= baseContractService.confirmSignContract(baseContract,currentUser);
        return result;
    }

    //终止合同
    @PostMapping("/endSignContract")
    public AjaxResult endSignContract(@RequestBody BaseContract baseContract) {
        BaseContract contract=baseContractService.selectBaseContractById(baseContract.getId());
        if("01".equals(contract.getCategoryType()) || "03".equals(contract.getCategoryType())){
            if(!"03".equals(contract.getStatus())){
                //return AjaxResult.error("进行中的合同才能进行终止");
            }
        }
        else if("02".equals(contract.getCategoryType()) || "04".equals(contract.getCategoryType())){
            if(!"04".equals(contract.getStatus())){
                //return AjaxResult.error("进行中的合同才能进行终止");
            }
        }
        if("05".equals(contract.getStatus())){
            return AjaxResult.error("该合同已经是终止状态，不能进行操作");
        }
        if("06".equals(contract.getStatus())){
            return AjaxResult.error("该合同已经是驳回状态，不能进行操作");
        }
        if("07".equals(contract.getStatus())){
            return AjaxResult.error("该合同已经是废弃状态，不能进行操作");
        }

        baseContract.setUpdateBy(getUsername());
        baseContract.setStatus("05");
        int result= baseContractService.updateBaseContract(baseContract);
        baseContractLogService.saveContractLogs(baseContract.getId(),getUserId(),"结束合同",getUsername());
        return toAjax(result);
    }

    @GetMapping("/getContractDetail")
    public AjaxResult getContractDetail(@RequestParam(value="id",required = true) Long id)
    {
        BaseContract baseContract=baseContractService.selectBaseContractById(id);
        return AjaxResult.success(baseContract);
    }

    /**
     * 雇主视角--我的合同
     * @return
     */
    @GetMapping("/getMyContractNum")
    public AjaxResult getMyContractNum()
    {

        BaseAppointment appointParam=new BaseAppointment();
        appointParam.setUserId(getUserId());
        appointParam.setApplyStatus("01");
        List<BaseAppointment> appointList= baseAppointmentService.selectBaseAppointmentList(appointParam);
        BaseContract param=new BaseContract();
        param.setAuserId(getUserId());
        List<BaseContract> contractList=baseContractService.selectUserContractList(param);

        List<BaseContract> signList=  contractList.stream().filter(contract->"01".equals(contract.getStatus())).collect(Collectors.toList());

        List<BaseContract> effectList=  contractList.stream().filter(contract->"05".equals(contract.getStatus())).collect(Collectors.toList());

        List<BaseContract> paymentList=  contractList.stream().filter(contract->"05".equals(contract.getStatus())).collect(Collectors.toList());
        BaseOrder orderParam=new BaseOrder();
        orderParam.setUserId(getUserId());
        orderParam.setPayStatus("01");
        List<BaseOrder> orderList=baseOrderService.selectBaseOrderList(orderParam);
        AjaxResult result=AjaxResult.success();
        result.put("appointmentNum",appointList.size());
        result.put("signNum",signList.size());
        result.put("effectNum",effectList.size());
        result.put("paymentNum",orderList.size());
        return result;
    }

    @GetMapping("/queryAppointmentData")
    public TableDataInfo queryAppointmentData(BaseAppointment appointment)
    {
        //appointment.setUserId(getUserId());
        appointment.setApplyStatus("01");
        startOrderBy();
        startPage();
        List<BaseAppointment> appointList= baseAppointmentService.selectBaseAppointmentList(appointment);
        return getDataTable(appointList);
    }

    /**
     * 阿姨视角--我的合同
     * @return
     */
    @GetMapping("/getAuntContractNum")
    public AjaxResult getAuntContractNum()
    {

        BaseContract param=new BaseContract();
        param.setAuserId(getUserId());
        List<BaseContract> contractList=baseContractService.selectUserContractList(param);

        List<BaseContract> signList=  contractList.stream().filter(contract->"02".equals(contract.getStatus())).collect(Collectors.toList());

        List<BaseContract> effectList=  contractList.stream().filter(contract->"04".equals(contract.getStatus())).collect(Collectors.toList());

        BaseOrder baseOrder=new BaseOrder();
        baseOrder.setUserId(getUserId());
        baseOrder.setUserType("阿姨");
        List<BaseOrder> orderList = baseOrderService.selectCompanyOrderList(baseOrder);

        List<BaseOrder> paymentList=  orderList.stream().filter(contract->"01".equals(contract.getPayStatus())).collect(Collectors.toList());
        AjaxResult result=AjaxResult.success();
        result.put("signNum",signList.size());
        result.put("effectNum",effectList.size());
        result.put("paymentNum",paymentList.size());
        return result;
    }

    @GetMapping("/getAgencyPageData")
    public AjaxResult getAgencyPageData()
    {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount accountParam=new BaseCompanyAccount();
        accountParam.setUserId(currentUser.getUserId());
        accountParam.setCompanyId(currentUser.getCompanyId());
        accountParam.setIsMain("Y");
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(accountParam);
        String isAdmin="N";
        if(account.size()>0){
            isAdmin="Y";
        }
        BaseUser userParam=new BaseUser();
        userParam.setAuthUserId(getUserId());
        userParam.setUserType("04");
        userParam.setIsAdmin(isAdmin);
        List<BaseUser> userList=userService.queryUserData(userParam);

        BaseAppointment custParam=new BaseAppointment();
        custParam.setUserId(currentUser.getUserId());
        custParam.setIsAdmin(isAdmin);
        List<BaseAppointment> custList=baseAppointmentService.selectAgencyAppointmentList(custParam);

        BaseOrderCombine orderParam=new BaseOrderCombine();
        orderParam.setAuntCompanyId(currentUser.getCompanyId());
        orderParam.setUserId(getUserId());
        orderParam.setIsAdmin(isAdmin);
        List<BaseOrderCombine> combineOrderList= baseOrderCombineService.selectUserOrderCombineList(orderParam);

        BaseContract param=new BaseContract();
        param.setAuserId(getUserId());
        param.setIsAdmin(isAdmin);
        List<BaseContract> contractList=baseContractService.selectUserContractList(param);

        AjaxResult result=AjaxResult.success();
        result.put("auntNum",userList.size());
        result.put("customerNum",custList.size());
        result.put("contractNum",contractList.size());
        result.put("combineOrderNum",combineOrderList.size());
        return result;
    }

    @GetMapping("/getAgencyAuntList")
    public TableDataInfo getAgencyAuntList(BaseUser baseUser)
    {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            baseUser.setIsAdmin("Y");
        }
        else{
            baseUser.setIsAdmin("N");
        }
        baseUser.setAuthUserId(getUserId());
        baseUser.setUserType("04");
        startOrderBy("a.create_time"," desc ");
        startPage();
        List<BaseUser> userList=userService.selectBaseUserList(baseUser);
        return getDataTable(userList);
    }

    @GetMapping("/getAgencyAuthCustomerList")
    public TableDataInfo getAgencyAuthCustomerList(BaseUser baseUser)
    {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            baseUser.setIsAdmin("Y");
        }
        else{
            baseUser.setIsAdmin("N");
        }
        baseUser.setUserType("03");
        baseUser.setAuthUserId(currentUser.getUserId());
        startOrderBy();
        startPage();
        List<BaseUser> custList=userService.queryUserData(baseUser);
        return getDataTable(custList);
    }

    @GetMapping("/getAgencyAppointmentList")
    public TableDataInfo getAgencyAppointmentList(BaseAppointment appointment)
    {
        BaseUser currentUser= getLoginUser().getUserInfo();
        BaseCompanyAccount param=new BaseCompanyAccount();
        param.setUserId(currentUser.getUserId());
        param.setCompanyId(currentUser.getCompanyId());
        param.setIsMain("Y");
        List<BaseCompanyAccount> account= baseCompanyAccountService.selectBaseCompanyAccountList(param);
        if(account.size()>0){
            appointment.setIsAdmin("Y");
        }
        else{
            appointment.setIsAdmin("N");
        }
        appointment.setUserId(getUserId());
        startOrderBy();
        startPage();
        List<BaseAppointment> appointList= baseAppointmentService.selectAgencyAppointmentList(appointment);
        return getDataTable(appointList);
    }

    @GetMapping("/getAgencyContractList")
    public TableDataInfo getAgencyContractList(BaseContract baseContract)
    {
        BaseUser currentUser= getLoginUser().getUserInfo();
        baseContract.setAuserId(getUserId());
        startOrderBy();
        startPage();
        List<BaseContract> contractList=baseContractService.selectUserContractList(baseContract);
        return getDataTable(contractList);
    }

    @GetMapping("/getAgencyCombineOrderList")
    public TableDataInfo getAgencyCombineOrderList(BaseOrderCombine orderCombine)
    {

        orderCombine.setUserId(getUserId());
        startOrderBy();
        startPage();
        List<BaseOrderCombine> combineOrderList= baseOrderCombineService.selectUserOrderCombineList(orderCombine);
        return getDataTable(combineOrderList);
    }




    @GetMapping("/getCompanyAccountData")
    public AjaxResult getCompanyAccountData()
    {
        Long companyId= getLoginUser().getUserInfo().getCompanyId();
        BaseUser param=new BaseUser();
        param.setCompanyId(companyId);
        List<BaseUser> userList= userService.selectCompanyAccountList(param);
        return AjaxResult.success(userList);
    }

    @PostMapping("/addCompanyAccount")
    public AjaxResult addCompanyAccount(@RequestBody BaseUser user)
    {
        BaseUser baseUser=userService.selectBaseUserByUserId(getUserId());
        Long companyId=baseUser .getCompanyId();
        BaseCompany company=baseCompanyService.selectBaseCompanyById(companyId);
        if(!"02".equals(baseUser.getAuthStatus())){
            return AjaxResult.error("只有认证的用户才能添加账号");
        }
        if(company==null){
            return AjaxResult.error("请先认证公司");
        }
        BaseUser queryUser= userService.selectUserByUserName(user.getPhonenumber());
        if(queryUser!=null){
            return AjaxResult.error("该手机号已经存在");
        }

        BaseUser newUser=new BaseUser();
        newUser.setUserName(user.getPhonenumber());
        newUser.setPhonenumber(user.getPhonenumber());
        newUser.setNickName(user.getNickName());
        newUser.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        newUser.setAuthStatus("02");
        newUser.setAuthType("01");
        newUser.setAuthTime(DateUtils.getNowDate());
        newUser.setUserType("05");
        String str=String.valueOf(ThreadLocalRandom.current().ints(100000,999999).limit(1).findFirst().getAsInt());
        newUser.setReferrerCode(str);
        newUser.setCompanyId(companyId);
        newUser.setCompanyName(getLoginUser().getUserInfo().getCompanyName());
        int result=userService.insertBaseUser(newUser);
        if(result>0){
            BaseCompanyAccount account=new BaseCompanyAccount();
            account.setCompanyId(companyId);
            account.setUserId(newUser.getUserId());
            account.setIsMain("N");
            account.setStatus("Y");
            account.setCreateBy(getUsername());
            baseCompanyAccountService.insertBaseCompanyAccount(account);
        }
        return toAjax(result);
    }

    @PostMapping("/editCompanyAccount")
    public AjaxResult editCompanyAccount(@RequestBody BaseUser user)
    {
        BaseUser currentUser=userService.selectBaseUserByUserId(user.getUserId());
        if(!user.getPhonenumber().equals(currentUser.getPhonenumber())){
            BaseUser editUser= userService.selectUserByUserPhone(user.getPhonenumber());
            if(editUser!=null){
                return AjaxResult.error("该手机号已经存在");
            }
        }
        BaseUser newUser=new BaseUser();
        newUser.setUserId(user.getUserId());
        newUser.setUserName(user.getPhonenumber());
        newUser.setPhonenumber(user.getPhonenumber());
        newUser.setNickName(user.getNickName());
        newUser.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        int result=userService.updateBaseUser(newUser);
        return toAjax(result);
    }

    @GetMapping("/getCompanyInfo")
    public AjaxResult getCompanyInfo()
    {
        Long companyId= getLoginUser().getUserInfo().getCompanyId();
        BaseCompany companyInfo= baseCompanyService.selectBaseCompanyById(companyId);
        BaseCompanyAddress param=new BaseCompanyAddress();
        param.setCompanyId(companyId);
        List<BaseCompanyAddress> addressList= baseCompanyAddressService.selectBaseCompanyAddressList(param);
        companyInfo.setAdddressList(addressList);
        return AjaxResult.success(companyInfo);
    }

    @GetMapping("/getCompanyInfoById")
    public AjaxResult getCompanyInfoById(@RequestParam(value="id",required = true) Long id)
    {
        BaseCompany companyInfo= baseCompanyService.selectBaseCompanyById(id);
        if(companyInfo==null){
            return AjaxResult.error("公司不存在");
        }
        BaseCompanyAddress param=new BaseCompanyAddress();
        param.setCompanyId(id);
        List<BaseCompanyAddress> addressList= baseCompanyAddressService.selectBaseCompanyAddressList(param);
        companyInfo.setAdddressList(addressList);
        return AjaxResult.success(companyInfo);
    }


    @PostMapping("/saveCompanyAddress")
    public AjaxResult saveCompanyAddress(@RequestBody BaseCompanyAddress address)
    {
        Long companyId= getLoginUser().getUserInfo().getCompanyId();
        int result=0;
        if(address.getId()!=null){
            result=baseCompanyAddressService.updateBaseCompanyAddress(address);
        }else{
            address.setCompanyId(companyId);
            result=baseCompanyAddressService.insertBaseCompanyAddress(address);
        }
        return toAjax(result);
    }

    @GetMapping("/removeCompanyAddress")
    public AjaxResult removeCompanyAddress(@RequestParam(value="id",required = true) Long id)
    {
        int result=baseCompanyAddressService.deleteBaseCompanyAddressById(id);
        return toAjax(result);
    }



    @GetMapping("/getContractLogsList")
    public AjaxResult getContractLogsList(@RequestParam(value="id",required = true) Long id)
    {
        BaseContractLog param=new BaseContractLog();
        param.setContractId(id);
        List<BaseContractLog> logsList=baseContractLogService.selectBaseContractLogList(param);
        return AjaxResult.success(logsList);
    }

    //查询公司账单
    @GetMapping("/getCompanyBillList")
    public TableDataInfo getCompanyBillList(BaseOrder baseOrder)
    {
        BaseUser currentUser=getLoginUser().getUserInfo();
        Long userId= currentUser.getUserId();
        String companyFlag="N";
        if("01".equals(currentUser.getUserType()) || "02".equals(currentUser.getUserType()) ||"05".equals(currentUser.getUserType())){
            companyFlag="Y";
        }
        baseOrder.getParams().put("companyFlag",companyFlag);
        baseOrder.setUserId(userId);
        startPage();
        List<BaseOrder> list = baseOrderService.selectCompanyOrderList(baseOrder);
        return getDataTable(list);
    }

    @GetMapping("/getCompanyBillDetail")
    public AjaxResult getCompanyBillDetail(@RequestParam(value="id",required = true) Long id)
    {
        BaseOrder order= baseOrderService.selectBaseOrderById(id);
        if(order==null){
            return AjaxResult.error("账单不存在");
        }
        return AjaxResult.success(order);
    }

    //取消订单
    @GetMapping("/removeBillOrder")
    public AjaxResult removeBillOrder(@RequestParam(value="id",required = true) Long id)
    {
        BaseOrder order= baseOrderService.selectBaseOrderById(id);
        if(order==null){
            return AjaxResult.error("账单不存在");
        }
        /*if("阿姨私签联系费用".equals(order.getTitle()) || "身份验证验证费用订单".equals(order.getTitle())){
            baseOrderService.deleteBaseOrderById(id);
        }else{
            return AjaxResult.error("该账单不允许操作");
        }*/
        if("02".equals(order.getPayStatus())){
            return AjaxResult.error("该账单不允许操作");
        }
        baseOrderService.deleteBaseOrderById(id);
        return AjaxResult.success(order);
    }

    @PostMapping("/addCompanyBillOrder")
    public AjaxResult addCompanyBillOrder(@RequestBody BaseOrder baseOrder)
    {
        BaseUser userInfo=userService.selectBaseUserByUserId(baseOrder.getUserId());
        if(userInfo==null){
            return AjaxResult.error("用户不存在");
        }
        BaseContract contract= baseContractService.selectBaseContractByCode(baseOrder.getContractCode());
        if(contract==null){
            return AjaxResult.error("合同不存在");
        }
        if(!"04".equals(contract.getStatus())){
            return AjaxResult.error("请选择生效中的合同");
        }
        if("01".equals(baseOrder.getUserType())){
            baseOrder.setUserType("雇主");
        }
        else if("02".equals(baseOrder.getUserType())){
            baseOrder.setUserType("阿姨");
        }
        baseOrder.setCompanyId(getLoginUser().getUserInfo().getCompanyId());
        baseOrder.setCompanyName(getLoginUser().getUserInfo().getCompanyName());
        baseOrder.setOrderCode(Seq.getId());
        baseOrder.setTitle("自定义账单");
        baseOrder.setCreateTime(DateUtils.getNowDate());
        baseOrder.setCreateBy(getUsername());
        baseOrder.setPayStatus("01");
        return toAjax(baseOrderService.insertBaseOrder(baseOrder));
    }



    @GetMapping("/addUserBillOrder")
    public AjaxResult addUserBillOrder(@RequestParam(value="userId",required = true) Long userId)
    {
        BaseUser resumeUser=userService.selectBaseUserByUserId(userId);
        if(resumeUser==null){
            return AjaxResult.error("阿姨信息不存在");
        }
        BaseUserIntention userIntention= baseUserIntentionService.selectBaseUserIntentionByUserId(userId);
        Long configId=10L;
        String title="阿姨私签联系费用";
        String remak="阿姨私签联系费用";
        if(StringUtils.isNotEmpty(userIntention.getTypeId())){
            if(userIntention.getTypeId().indexOf("1")!=-1){
                configId=5L;
                remak="阿姨私签联系费用";
            }
            else if(userIntention.getTypeId().indexOf("2")!=-1){
                configId=6L;
                remak="阿姨私签联系费用";
            }
            else if(userIntention.getTypeId().indexOf("3")!=-1){
                configId=7L;
                remak="阿姨私签联系费用";
            }
            else if(userIntention.getTypeId().indexOf("4")!=-1){
                configId=8L;
                remak="阿姨私签联系费用";
            }
            else if(userIntention.getTypeId().indexOf("5")!=-1){
                configId=9L;
                remak="阿姨签联系费用";
            }else{
                configId=10L;
                remak="阿姨私签联系费用";
            }
        }
        BusinessConfig businessConfig=businessConfigService.selectBusinessConfigById(configId);
        if(businessConfig==null){
            return AjaxResult.error("参数设置错误");
        }
        BaseUser currentUser=userService.selectBaseUserByUserId(getUserId());
        BaseOrder baseOrder=new BaseOrder();
        baseOrder.setUserId(currentUser.getUserId());
        baseOrder.setTitle(title);
        baseOrder.setTargetUserId(userId);
        baseOrder.setOrderAmount(new BigDecimal(businessConfig.getValue()));
        baseOrder.setUserType(DictUtils.getDictLabel("base_user_type",currentUser.getUserType()));
        baseOrder.setOrderCode(Seq.getId());
        baseOrder.setCreateTime(DateUtils.getNowDate());
        baseOrder.setCreateBy(getUsername());
        baseOrder.setPayStatus("01");
        baseOrder.setPayType("01");
        baseOrder.setRemark(remak+"(提示:妥善保存阿姨电话，退出后无法再次免费获取)");
        String companyId = configService.selectConfigByKey("sys.account.companyId");
        if (StringUtils.isEmpty(companyId)) {
            companyId = "1";
        }
        BaseCompany company = baseCompanyService.selectBaseCompanyById(Long.valueOf(companyId));
        baseOrder.setCompanyId(company.getId());
        baseOrder.setCompanyName(company.getCompanyName());
        baseOrder.setCreateBy(currentUser.getUserName());
        baseOrderService.insertBaseOrder(baseOrder);
        String userKey="orderCode-"+baseOrder.getOrderCode();
        redisCache.setCacheObject(userKey,"N",7, TimeUnit.DAYS);
        return AjaxResult.success(baseOrder);
    }

    @GetMapping("/addUserCertCheckOrder")
    public AjaxResult addUserCertCheckOrder()
    {
        BusinessConfig businessConfig=businessConfigService.selectBusinessConfigById(11L);
        if(businessConfig==null){
            return AjaxResult.error("参数设置错误");
        }
        BaseUser currentUser=userService.selectBaseUserByUserId(getUserId());
        BaseOrder baseOrder=new BaseOrder();
        baseOrder.setUserId(currentUser.getUserId());
        String title="身份验证验证费用订单";
        baseOrder.setTitle(title);
        baseOrder.setOrderAmount(new BigDecimal(businessConfig.getValue()));
        baseOrder.setUserType(DictUtils.getDictLabel("base_user_type",currentUser.getUserType()));
        baseOrder.setOrderCode(Seq.getId());
        baseOrder.setCreateTime(DateUtils.getNowDate());
        baseOrder.setCreateBy(getUsername());
        baseOrder.setPayStatus("01");
        baseOrder.setPayType("01");
        baseOrder.setRemark("验证费(支付后进入验证界面)");
        String companyId = configService.selectConfigByKey("sys.account.companyId");
        if (StringUtils.isEmpty(companyId)) {
            companyId = "1";
        }
        BaseCompany company = baseCompanyService.selectBaseCompanyById(Long.valueOf(companyId));
        baseOrder.setCompanyId(company.getId());
        baseOrder.setCompanyName(company.getCompanyName());
        baseOrder.setCreateBy(currentUser.getUserName());
        baseOrderService.insertBaseOrder(baseOrder);
        String userKey="orderCode-"+baseOrder.getOrderCode();
        redisCache.setCacheObject(userKey,"N",7, TimeUnit.DAYS);
        return AjaxResult.success(baseOrder);
    }

    @GetMapping("/prevContractContent")
    public AjaxResult prevContractContent(@RequestParam(value="id",required = true) Long id)
    {
        BaseContract contract=baseContractService.selectBaseContractById(id);
        if(contract==null){
            return AjaxResult.error("合同不存在不存在");
        }
        ContractContent content= contractContentService.selectContractContentById(Long.valueOf(contract.getCategoryType()));
        String contentStr=content.getContent();
        contentStr=contentStr.replaceAll("##auserName##",StringUtils.isNotEmpty(contract.getAuserName())?contract.getAuserName():"");
        contentStr=contentStr.replaceAll("##acertCode##",StringUtils.isNotEmpty(contract.getAcertCode())?contract.getAcertCode():"");
        contentStr=contentStr.replaceAll("##auserPhone##",StringUtils.isNotEmpty(contract.getAuserPhone())?contract.getAuserPhone():"");
        contentStr=contentStr.replaceAll("##auserAddress##",StringUtils.isNotEmpty(contract.getAuserAddress())?contract.getAuserAddress():"");
        contentStr=contentStr.replaceAll("##buserName##",StringUtils.isNotEmpty(contract.getBuserName())?contract.getBuserName():"");
        contentStr=contentStr.replaceAll("##bcertCode##",StringUtils.isNotEmpty(contract.getBcertCode())?contract.getBcertCode():"");
        contentStr=contentStr.replaceAll("##buserPhone##",StringUtils.isNotEmpty(contract.getBuserPhone())?contract.getBuserPhone():"");
        contentStr=contentStr.replaceAll("##buserAddress##",StringUtils.isNotEmpty(contract.getBuserAddress())?contract.getBuserAddress():"");
        contentStr=contentStr.replaceAll("##bcontactName##",StringUtils.isNotEmpty(contract.getBcontactName())?contract.getBcontactName():"");
        contentStr=contentStr.replaceAll("##bcontactPhone##",StringUtils.isNotEmpty(contract.getBcontactPhone())?contract.getBcontactPhone():"");
        contentStr=contentStr.replaceAll("##ccompanyName##",StringUtils.isNotEmpty(contract.getCcompanyName())?contract.getCcompanyName():"");
        contentStr=contentStr.replaceAll("##ccompanyPhone##",StringUtils.isNotEmpty(contract.getCcompanyPhone())?contract.getCcompanyPhone():contract.getCuserPhone());
        contentStr=contentStr.replaceAll("##cuserAddress##",StringUtils.isNotEmpty(contract.getCuserAddress())?contract.getCuserAddress():"");
        contentStr=contentStr.replaceAll("##serviceAddress##", StringUtils.isNotEmpty(contract.getServiceAddress())?contract.getServiceAddress():"");
        contentStr=contentStr.replaceAll("##serviceDays##",contract.getServiceDays()!=null?String.valueOf(contract.getServiceDays()):"");
        contentStr=contentStr.replaceAll("##expecteDate##",contract.getExpecteDate()!=null?DateUtils.parseDateToStr("yyyy年MM月dd日",contract.getExpecteDate()):"");
        contentStr=contentStr.replaceAll("##serviceAmount##",contract.getServiceAmount()!=null?String.valueOf(contract.getServiceAmount()):"");
        contentStr=contentStr.replaceAll("##depositAmount##",contract.getDepositAmount()!=null?String.valueOf(contract.getDepositAmount()):"");
        contentStr=contentStr.replaceAll("##insuranceContent##",StringUtils.isNotEmpty(contract.getInsuranceContent())?contract.getInsuranceContent():"");
        contentStr=contentStr.replaceAll("##otherContent##",StringUtils.isNotEmpty(contract.getOtherContent())?contract.getOtherContent():"");
        contentStr=contentStr.replaceAll("##ccontactPhone##",StringUtils.isNotEmpty(contract.getCcontactPhone())?contract.getCcontactPhone():"");
        contentStr=contentStr.replaceAll("##signDate##",contract.getSignDate()!=null?DateUtils.parseDateToStr("yyyy年MM月dd日",contract.getSignDate()):"");
        contentStr=contentStr.replaceAll("##asignatureUrl##","<img src='"+contract.getAsignatureUrl()+"' style='width:150px;'/>");
        contentStr=contentStr.replaceAll("##bsignatureUrl##","<img src='"+contract.getBsignatureUrl()+"' style='width:150px;'/>");
        contentStr=contentStr.replaceAll("##liveType##","01".equals(contract.getLiveType())?"住家":"不住家");
        contentStr=contentStr.replaceAll("##workTime##",StringUtils.isNotEmpty(contract.getWorkTime())?contract.getWorkTime():"");
        if("04".equals(contract.getStatus()) || "05".equals(contract.getStatus())) {
            contentStr = contentStr.replaceAll("##confirmsealUrl##", "<img src='" + contract.getConfirmsealUrl() + "' style='width:150px;'/>");
        }else{
            contentStr = contentStr.replaceAll("##confirmsealUrl##", "");
        }
        if(contract.getServiceType()!=null){
            String serviceContent="";
            if("一般家务".equals(contract.getServiceType())){
                serviceContent="一般家务（做饭 洗衣服 打扫房间卫生） ";
            }
            else if("育儿（主带）".equals(contract.getServiceType())){
                serviceContent="育儿（主带）（宝宝性别："+contract.getBabySex()+"；宝宝当前月龄："+contract.getBabyMonth()+"个月";
            }
            else if("育儿（辅带）".equals(contract.getServiceType())){
                serviceContent="育儿（辅带）（宝宝性别："+contract.getBabySex()+"；宝宝当前月龄："+contract.getBabyMonth()+"个月";
            }
            else if("老人照护".equals(contract.getServiceType())){
                serviceContent="老人照护（性别："+contract.getBabySex()+"；年龄："+contract.getBabyMonth()+"岁；身体状况:"+contract.getBabyHealty();
            }
            else if("病人照护".equals(contract.getServiceType())){
                serviceContent="病人照护（性别："+contract.getBabySex()+"；年龄："+contract.getBabyMonth()+"岁；身体状况:"+contract.getBabyHealty();
            }
            contentStr=contentStr.replaceAll("##serviceContent##",serviceContent);
        }
        if(contract.getSignType()!=null){
            String signTypeContent="";
            if("01".equals(contract.getSignType())){
                signTypeContent="服务费"+contract.getServiceAmount()+"元/年";
            }
            else if("02".equals(contract.getSignType())){
                signTypeContent="签单费"+contract.getSignAmount()+"元整（一次性收取），服务费"+contract.getServiceAmount()+"元/月。";
            }
            contentStr=contentStr.replaceAll("##signTypeContent##",signTypeContent);
        }
        contentStr=contentStr.replaceAll("##serviceStartDate##",contract.getServiceStartDate()!=null?DateUtils.parseDateToStr("yyyy年MM月dd日",contract.getServiceStartDate()):"");
        contentStr=contentStr.replaceAll("##salaryAmount##",contract.getSalaryAmount()!=null?String.valueOf(contract.getSalaryAmount()):"");
        contentStr=contentStr.replaceAll("##workDayWeek##",contract.getWorkDayWeek()!=null?String.valueOf(7-contract.getWorkDayWeek()):"");
        content.setContent(contentStr);
        return AjaxResult.success(content);
    }
}

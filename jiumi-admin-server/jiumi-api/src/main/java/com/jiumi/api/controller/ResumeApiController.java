package com.jiumi.api.controller;
import com.alibaba.fastjson2.JSONObject;
import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.AppSigner;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;
import com.jiumi.api.base.BaseApiController;
import com.jiumi.api.util.CheckCertUtil;
import com.jiumi.api.util.http.util.HttpUtils;
import com.jiumi.baseconfig.domain.*;
import com.jiumi.baseconfig.service.*;
import com.jiumi.common.core.domain.AjaxResult;
import com.jiumi.common.core.redis.RedisCache;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.system.service.ISysConfigService;
import com.jiumi.util.BirthUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import com.jiumi.api.util.http.Response;

/**
 * 发布简历接口
 *
 * @author jiumi
 * @date 2021-08-18
 */
@RestController
@RequestMapping("/api/resume")
public class ResumeApiController extends BaseApiController
{
    private static final Logger logger = LoggerFactory.getLogger(ResumeApiController.class);



    @Autowired
    private IBaseUserService userService;


    @Autowired
    private IBaseUserIntentionService baseUserIntentionService;

    @Autowired
    private IBaseUserResumeService baseUserResumeService;

    @Autowired
    private IBaseUserResumeExperienceService baseUserResumeExperienceService;

    @Autowired
    private IBaseUserResumeCertificateService baseUserResumeCertificateService;

    @Autowired
    private IBaseUserIntentionCityService baseUserIntentionCityService;


    @Autowired
    private IBaseUserFocusService baseUserFocusService;

    @Autowired
    private IUserEvaluateService userEvaluateService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBaseCityInfoService baseCityInfoService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IUserResumeHealthcertService userResumeHealthcertService;

    @Autowired
    private IBaseCompanyService baseCompanyService;


    @PostMapping("/saveUserResume")
    public AjaxResult saveUserResume(@RequestBody BaseUserResume userResume)
    {
        Long userID = getUserId();
        userResume.setUserId(userID);
        BaseUserResume resume= baseUserResumeService.selectBaseUserResumeByUserCertCode(userResume.getCertCode());
        if(resume!=null){
            if(resume.getUserId().intValue()!=userID.intValue()){
                //进行账户合并
                BaseUser currentUser=userService.selectBaseUserByUserId(userID);
                BaseUser oldUser= userService.selectBaseUserByUserId(resume.getUserId());
                userService.deleteBaseUserByUserId(resume.getUserId());
                currentUser.setCompanyId(oldUser.getCompanyId());
                currentUser.setCompanyName(oldUser.getCompanyName());
                currentUser.setAuthUserId(oldUser.getAuthUserId());
                currentUser.setUpdateTime(DateUtils.getNowDate());
                currentUser.setPublishType(oldUser.getPublishType());
                currentUser.setPublishUserName(oldUser.getPublishUserName());
                userService.updateBaseUser(currentUser);
                resume.setUserId(userID);

                baseUserResumeService.updateBaseUserResumeInfo(resume);
                userResume.setId(resume.getId());
                userResume.setUserId(userID);

                BaseUserIntention param=new BaseUserIntention();
                param.setUserId(currentUser.getUserId());
                List<BaseUserIntention> intentList= baseUserIntentionService.selectBaseUserIntentionList(param);
                BaseUserIntention baseUserIntention=userResume.getIntention();
                if(intentList.size()>0){
                    BaseUserIntention intention= intentList.get(0);
                    if(baseUserIntention!=null) {
                        baseUserIntention.setId(intention.getId());
                        baseUserIntentionService.updateBaseUserIntention(baseUserIntention);
                        baseUserIntentionCityService.deleteBaseUserIntentionCityByIntentionId(intention.getId());
                        List<BaseUserIntentionCity> cityList = baseUserIntention.getCityList();
                        cityList.stream().forEach(city -> {
                            city.setIntentionId(intention.getId());
                            baseUserIntentionCityService.insertBaseUserIntentionCity(city);
                        });
                    }
                }else{
                    if(baseUserIntention!=null) {
                        baseUserIntention.setUserId(currentUser.getUserId());
                        baseUserIntentionService.insertBaseUserIntention(baseUserIntention);
                        List<BaseUserIntentionCity> cityList = baseUserIntention.getCityList();
                        if(cityList!=null){
                            cityList.stream().forEach(city -> {
                                city.setIntentionId(baseUserIntention.getId());
                                baseUserIntentionCityService.insertBaseUserIntentionCity(city);
                            });
                        }
                    }
                }
            }
        }
        return toAjax(baseUserResumeService.saveBaseUserResume(userResume));
    }

    @PostMapping("/saveUserResumeAuth")
    public AjaxResult saveUserResumeAuth(@RequestBody BaseUserResume userResume)
    {
        BaseUserResume resumeInfo=null;
        if(userResume.getId()!=null){
            BaseUserResume resume=baseUserResumeService.selectBaseUserResumeById(userResume.getId());
            BaseUserResume checkResume= baseUserResumeService.selectBaseUserResumeByUserCertCode(userResume.getCertCode());
            if(checkResume!=null){
                if(checkResume.getUserId().intValue()!=resume.getUserId().intValue()){
                    return AjaxResult.error("身份证号已经存在，请联系管理员");
                }
                BaseUser userInfo=new BaseUser();
                userInfo.setUserId(userResume.getUserId());
                userInfo.setAvatar(userResume.getAvatar());
                userInfo.setWorkExperience(userResume.getWorkExperience());
                userInfo.setUpdateTime(DateUtils.getNowDate());
                userService.updateBaseUser(userInfo);
                baseUserResumeService.updateBaseUserResume(userResume);
            }
        }else{
            BaseUser baseUser= userService.selectUserByUserCertCode(userResume.getCertCode());
            if(baseUser!=null){
                return AjaxResult.error("阿姨信息已经存在");
            }
            BaseUser currentUser=userService.selectBaseUserByUserId(getUserId());
            if(!"02".equals(currentUser.getAuthStatus())){
                return AjaxResult.error("请先进行认证");
            }
            resumeInfo= baseUserResumeService.saveBaseUserResumeAuth(userResume,currentUser);
        }
        return AjaxResult.success(resumeInfo);
    }

    @PostMapping("/saveResumeInfo")
    public AjaxResult saveResumeInfo(@RequestBody BaseUserResume userResume)
    {
        BaseUserResume resume=baseUserResumeService.selectBaseUserResumeById(userResume.getId());
        if(resume==null){
            return AjaxResult.error("请先填写基本信息");
        }
        BaseUserResume checkResume= baseUserResumeService.selectBaseUserResumeByUserCertCode(userResume.getCertCode());
        if(checkResume!=null){
            if(checkResume.getUserId().intValue()!=resume.getUserId().intValue()){
                return AjaxResult.error("身份证号已经存在，请联系管理员");
            }
        }
        int result=baseUserResumeService.updateBaseUserResume(userResume);
        return toAjax(result);
    }

    /**
     * 查询当前用户简历
     * @return
     */
    @GetMapping(value = "getUserResumeDetail")
    public AjaxResult getUserResumeDetail()
    {
        Long userID = getUserId();
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeByUserId(userID);
        if(detail==null){
            AjaxResult result= AjaxResult.success();
            result.put("userInfo",null);
            result.put("resumeInfo",detail);

            return result;
        }
        BaseUser sysUser= userService.selectBaseUserByUserId(detail.getUserId());
        sysUser.setPassword(null);

        BaseUserResumeExperience expParam=new BaseUserResumeExperience();
        expParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeExperience>  experienceList=baseUserResumeExperienceService.selectBaseUserResumeExperienceList(expParam);
        detail.setExperienceList(experienceList);

        BaseUserResumeCertificate certParam=new BaseUserResumeCertificate();
        certParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeCertificate> certList= baseUserResumeCertificateService.selectBaseUserResumeCertificateList(certParam);
        detail.setCertList(certList);

        UserResumeHealthcert healthParam=new UserResumeHealthcert();
        healthParam.setResumeId(Long.valueOf(detail.getId()));
        List<UserResumeHealthcert> healthList= userResumeHealthcertService.selectUserResumeHealthcertList(healthParam);
        detail.setHealthList(healthList);

        BaseUserIntention intention=baseUserIntentionService.selectBaseUserIntentionByUserId(userID);
        detail.setIntention(intention);

        if(intention!=null){
            BaseUserIntentionCity param=new BaseUserIntentionCity();
            param.setIntentionId(intention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntentionCityService.selectBaseUserIntentionCityList(param);
            intention.setCityList(cityList);
        }else{
            intention=new BaseUserIntention();
        }

        AjaxResult result= AjaxResult.success();
        detail.setNickName(sysUser.getNickName());
        detail.setBirthDate(sysUser.getBirthDate());
        detail.setZodiac(sysUser.getZodiac());
        detail.setConstellation(sysUser.getConstellation());
        detail.setNation(sysUser.getNation());
        detail.setNativePlace(sysUser.getNativePlace());
        detail.setAvatar(sysUser.getAvatar());
        detail.setAuthName(sysUser.getAuthName());
        detail.setCompanyId(sysUser.getCompanyId());
        detail.setCompanyName(sysUser.getCompanyName());
        detail.setSex(sysUser.getSex());
        detail.setPhonenumber(sysUser.getPhonenumber());
        detail.setCurrentTime(DateUtils.getNowDate());
        result.put("userInfo",sysUser);
        result.put("resumeInfo",detail);
        if(sysUser.getAuthUserId()!=null){
            BaseUser authUser= userService.selectBaseUserByUserId(sysUser.getAuthUserId());
            result.put("authUser",authUser);
        }else{
            BaseUser authUser= userService.selectBaseUserByUserId(1L);
            result.put("authUser",authUser);
        }

        return result;
    }


    @ApiOperation("查询当前用户从业意向")
    @GetMapping("/getUserIntention")
    public AjaxResult getUserIntention(@RequestParam(value="userId",required = true) Long userId) {
        BaseUserIntention param=new BaseUserIntention();
        param.setUserId(userId);
        List<BaseUserIntention> intentList= baseUserIntentionService.selectBaseUserIntentionList(param);
        if(intentList.size()>0){
            BaseUserIntention intention= intentList.get(0);
            BaseUserIntentionCity city=new BaseUserIntentionCity();
            city.setIntentionId(intention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntentionCityService.selectBaseUserIntentionCityList(city);
            intention.setCityList(cityList);
            return AjaxResult.success(intention);
        }else{
            return AjaxResult.success(new BaseUserIntention());
        }
    }

    /**
     * 保存求职意向
     */
    @PostMapping("/saveIntentionDetail")
    public AjaxResult saveIntentionDetail(@RequestBody BaseUserIntention baseUserIntention)
    {
        BaseUser currentUser= userService.selectBaseUserByUserId(baseUserIntention.getUserId());
        if(currentUser==null){
            return AjaxResult.error("用户不存在");
        }
        BaseUserIntention param=new BaseUserIntention();
        param.setUserId(currentUser.getUserId());
        List<BaseUserIntention> intentList= baseUserIntentionService.selectBaseUserIntentionList(param);
        if(intentList.size()>0){
            BaseUserIntention intention= intentList.get(0);
            baseUserIntention.setId(intention.getId());
            baseUserIntentionService.updateBaseUserIntention(baseUserIntention);
            baseUserIntentionCityService.deleteBaseUserIntentionCityByIntentionId(intention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntention.getCityList();
            cityList.stream().forEach(city->{
                city.setIntentionId(intention.getId());
                baseUserIntentionCityService.insertBaseUserIntentionCity(city);
            });
        }else{
            baseUserIntentionService.insertBaseUserIntention(baseUserIntention);
            List<BaseUserIntentionCity> cityList=baseUserIntention.getCityList();
            cityList.stream().forEach(city->{
                city.setIntentionId(baseUserIntention.getId());
                baseUserIntentionCityService.insertBaseUserIntentionCity(city);
            });
        }
        return AjaxResult.success("保存成功");
    }

    /**
     * 保存求职意向城市
     */
    @PostMapping("/saveIntentionCity")
    public AjaxResult saveIntentionCity(@RequestBody BaseUserIntention baseUserIntention)
    {
        BaseUser currentUser= userService.selectBaseUserByUserId(baseUserIntention.getUserId());
        if(currentUser==null){
            return AjaxResult.error("用户不存在");
        }
        BaseUserIntention currentIntention= baseUserIntentionService.selectBaseUserIntentionByUserId(baseUserIntention.getUserId());
        if(currentIntention!=null){
            baseUserIntentionCityService.deleteBaseUserIntentionCityByIntentionId(currentIntention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntention.getCityList();
            cityList.stream().forEach(city->{
                city.setIntentionId(currentIntention.getId());
                baseUserIntentionCityService.insertBaseUserIntentionCity(city);
            });
        }else{
            baseUserIntentionService.insertBaseUserIntention(baseUserIntention);
            List<BaseUserIntentionCity> cityList=baseUserIntention.getCityList();
            cityList.stream().forEach(city->{
                city.setIntentionId(baseUserIntention.getId());
                baseUserIntentionCityService.insertBaseUserIntentionCity(city);
            });
        }
        return AjaxResult.success("保存成功");
    }


    /**
     * 删除求职意向
     */
    @GetMapping("/removeIntentionDetail")
    public AjaxResult removeIntentionDetail(@RequestParam(value="id",required = true) Long id)
    {
        int result=baseUserIntentionService.deleteBaseUserIntentionById(id);
        if(result>0){
            baseUserIntentionCityService.deleteBaseUserIntentionCityByIntentionId(id);
        }
        return toAjax(result);
    }

    @GetMapping("/getUserResumeList")
    public AjaxResult getUserResumeList(BaseUserResume baseUserResume)
    {
        Long userID = getUserId();
        baseUserResume.setUserId(userID);
        List<BaseUserResume> list = baseUserResumeService.selectBaseUserResumeList(baseUserResume);
        return AjaxResult.success(list);
    }

    /**
     * 获取求职简历详细信息  合单列表 查找阿姨
     */
    @GetMapping(value = "getUserResumeDetailById")
    public AjaxResult getUserResumeDetailById(@RequestParam(value="id",required = true) Long id)
    {
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeById(id);
        BaseUser sysUser= userService.selectBaseUserByUserId(detail.getUserId());

        BaseUserResumeExperience expParam=new BaseUserResumeExperience();
        expParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeExperience>  experienceList=baseUserResumeExperienceService.selectBaseUserResumeExperienceList(expParam);
        detail.setExperienceList(experienceList);

        BaseUserResumeCertificate certParam=new BaseUserResumeCertificate();
        certParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeCertificate> certList= baseUserResumeCertificateService.selectBaseUserResumeCertificateList(certParam);
        detail.setCertList(certList);

        UserResumeHealthcert healthParam=new UserResumeHealthcert();
        healthParam.setResumeId(Long.valueOf(detail.getId()));
        List<UserResumeHealthcert> healthList= userResumeHealthcertService.selectUserResumeHealthcertList(healthParam);
        detail.setHealthList(healthList);

        BaseUserIntention intention=baseUserIntentionService.selectBaseUserIntentionByUserId(detail.getUserId());
        detail.setIntention(intention);

        if(intention!=null){
            BaseUserIntentionCity param=new BaseUserIntentionCity();
            param.setIntentionId(intention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntentionCityService.selectBaseUserIntentionCityList(param);
            intention.setCityList(cityList);
        }else{
            intention=new BaseUserIntention();
        }

        AjaxResult result= AjaxResult.success();
        detail.setNickName(sysUser.getNickName());
        detail.setBirthDate(sysUser.getBirthDate());
        detail.setZodiac(sysUser.getZodiac());
        detail.setConstellation(sysUser.getConstellation());
        detail.setNation(sysUser.getNation());
        detail.setNativePlace(sysUser.getNativePlace());
        detail.setAvatar(sysUser.getAvatar());
        detail.setAuthName(sysUser.getAuthName());
        detail.setCompanyId(sysUser.getCompanyId());
        detail.setCompanyName(sysUser.getCompanyName());
        detail.setSex(sysUser.getSex());
        detail.setPhonenumber(sysUser.getPhonenumber());
        result.put("userInfo",sysUser);
        result.put("resumeInfo",detail);

        String kefuPhone = configService.selectConfigByKey("sys.customer.phonenumber");
        logger.info("111111111111111111111111111111111111111111111111111111111111111111111111");
        BaseUser authUser= userService.selectBaseUserByUserId(sysUser.getAuthUserId());
        logger.info("111111111111111111111111111111111111111111111111111111111111111111111111");
        if(authUser!=null){
            if(authUser.getUserId().intValue()==1){
                authUser= userService.selectBaseUserByUserId(1L);
                authUser.setPhonenumber(kefuPhone);
                authUser.setPassword(null);
            }
        }else{
            authUser= userService.selectBaseUserByUserId(1L);
            authUser.setPhonenumber(kefuPhone);
            authUser.setPassword(null);
        }
        result.put("authUser",authUser);
        BaseUserFocus focus=new BaseUserFocus();
        focus.setUserId(getUserId());
        focus.setFocusUserId(detail.getUserId());
        List<BaseUserFocus> focusList=baseUserFocusService.selectBaseUserFocusList(focus);
        if(focusList.size()>0){
            result.put("focusFlag",'Y');
        }else{
            result.put("focusFlag",'N');
        }

        return result;
    }

    //首页阿姨列表 查询阿姨简历详细信息
    @GetMapping(value = "getUserResumeDetailByUserId")
    public AjaxResult getUserResumeDetailByUserId(@RequestParam(value="userId",required = true) Long userId)
    {
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeByUserId(userId);
        if(detail==null){
            return AjaxResult.error("请先创建简历");
        }
        BaseUser sysUser= userService.selectBaseUserByUserId(detail.getUserId());

        BaseUserResumeExperience expParam=new BaseUserResumeExperience();
        expParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeExperience>  experienceList=baseUserResumeExperienceService.selectBaseUserResumeExperienceList(expParam);
        detail.setExperienceList(experienceList);

        BaseUserResumeCertificate certParam=new BaseUserResumeCertificate();
        certParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeCertificate> certList= baseUserResumeCertificateService.selectBaseUserResumeCertificateList(certParam);
        detail.setCertList(certList);

        UserResumeHealthcert healthParam=new UserResumeHealthcert();
        healthParam.setResumeId(Long.valueOf(detail.getId()));
        List<UserResumeHealthcert> healthList= userResumeHealthcertService.selectUserResumeHealthcertList(healthParam);
        detail.setHealthList(healthList);

        BaseUserIntention intention=baseUserIntentionService.selectBaseUserIntentionByUserId(userId);
        detail.setIntention(intention);
        if(intention!=null){
            BaseUserIntentionCity param=new BaseUserIntentionCity();
            param.setIntentionId(intention.getId());
            List<BaseUserIntentionCity> cityList=baseUserIntentionCityService.selectBaseUserIntentionCityList(param);
            intention.setCityList(cityList);
        }else{
            intention=new BaseUserIntention();
        }
        AjaxResult result= AjaxResult.success();
        detail.setNickName(sysUser.getNickName());
        detail.setBirthDate(sysUser.getBirthDate());
        detail.setZodiac(sysUser.getZodiac());
        detail.setConstellation(sysUser.getConstellation());
        detail.setNation(sysUser.getNation());
        detail.setNativePlace(sysUser.getNativePlace());
        detail.setAvatar(sysUser.getAvatar());
        detail.setAuthName(sysUser.getAuthName());
        detail.setCompanyId(sysUser.getCompanyId());
        detail.setCompanyName(sysUser.getCompanyName());
        detail.setSex(sysUser.getSex());
        detail.setPhonenumber(sysUser.getPhonenumber());
        sysUser.setPassword(null);
        result.put("userInfo",sysUser);
        result.put("resumeInfo",detail);
        String kefuPhone = configService.selectConfigByKey("sys.customer.phonenumber");
        //当前使用用户
        try{
            BaseUser currentBaseUser=getLoginUser().getUserInfo();
            logger.info("22222222222222222222222222222222222222222222222222222222222222222222222");
            BaseUser currentUser= userService.selectBaseUserByUserId(currentBaseUser.getUserId());
            logger.info("22222222222222222222222222222222222222222222222222222222222222222222222");
            if("03".equals(currentUser.getUserType())){
                if(currentUser.getAuthUserId()!=null){
                    BaseUser authUser= userService.selectBaseUserByUserId(currentUser.getAuthUserId());
                    if(authUser==null || authUser.getUserId().intValue()==1){
                        authUser.setPhonenumber(kefuPhone);
                    }
                    authUser.setPassword(null);
                    result.put("authUser",authUser);
                }else{
                    BaseUser authUser= userService.selectBaseUserByUserId(1L);
                    authUser.setPhonenumber(kefuPhone);
                    authUser.setPassword(null);
                    result.put("authUser",authUser);
                }
            }
            else if("04".equals(currentUser.getUserType())){
                BaseUser authUser= userService.selectBaseUserByUserId(currentUser.getAuthUserId());
                if(authUser!=null){
                    if(authUser.getUserId().intValue()==1){
                        authUser= userService.selectBaseUserByUserId(1L);
                        authUser.setPhonenumber(kefuPhone);
                        authUser.setPassword(null);
                    }
                }else{
                    authUser= userService.selectBaseUserByUserId(1L);
                    authUser.setPhonenumber(kefuPhone);
                    authUser.setPassword(null);
                }
                result.put("authUser",authUser);
            }else{
                BaseUser authUser= userService.selectBaseUserByUserId(1L);
                authUser.setPhonenumber(kefuPhone);
                authUser.setPassword(null);
                result.put("authUser",authUser);
            }
        }catch(Exception e){
            logger.info("未获取到用户信息");
            BaseUser authUser= userService.selectBaseUserByUserId(1L);
            authUser.setPhonenumber(kefuPhone);
            authUser.setPassword(null);
            result.put("authUser",authUser);
        }


        result.put("focusFlag", 'N');
        try {
            BaseUserFocus focus = new BaseUserFocus();
            focus.setUserId(getUserId());
            focus.setFocusUserId(userId);
            focus.setType("01");
            List<BaseUserFocus> focusList = baseUserFocusService.selectBaseUserFocusList(focus);
            if (focusList.size() > 0) {
                result.put("focusFlag", 'Y');
            } else {
                result.put("focusFlag", 'N');
            }
        }
        catch (Exception e){
            logger.info(e.getMessage());
        }

        UserEvaluate param = new UserEvaluate();
        param.setUserId(userId);
        List<UserEvaluate> evaluateList = userEvaluateService.selectUserEvaluateList(param);
        evaluateList.stream().forEach(evaluate->{
            String name=evaluate.getEvaluateUserName();
            if(StringUtils.isNotEmpty(name)){
                if(name.length()<=1){
                    evaluate.setEvaluateUserName(name);
                }else{
                    name=name.substring(0,1)+"*"+name.substring(name.length()-1);
                    evaluate.setEvaluateUserName(name);
                }
            }

        });
        List<UserEvaluate>  employEvaluate=evaluateList.stream().filter(user->"01".equals(user.getType())).collect(Collectors.toList());
        List<UserEvaluate>  agencyEvaluate=evaluateList.stream().filter(user->"02".equals(user.getType())).collect(Collectors.toList());

        result.put("employEvaluate", employEvaluate);
        result.put("agencyEvaluate", agencyEvaluate);

        return result;
    }



    /**
     * 获取工作经历详细信息
     */
    @GetMapping(value = "getUserExprienceList")
    public AjaxResult getUserExprienceList(@RequestParam(value="id",required = true) Long id)
    {
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeById(id);
        BaseUserResumeExperience param=new BaseUserResumeExperience();
        param.setResumeId(detail.getId());
        List<BaseUserResumeExperience> list=baseUserResumeExperienceService.selectBaseUserResumeExperienceList(param);
        return AjaxResult.success(list);
    }
    /**
     * 获取工作经历详细信息
     */
    @GetMapping(value = "getUserExprienceDetail")
    public AjaxResult getUserExprienceDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeExperienceService.selectBaseUserResumeExperienceById(id));
    }

    /**
     * 新增工作经历
     */
    @PostMapping(value = "addUserExprience")
    public AjaxResult addUserExprience(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.insertBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 修改工作经历
     */
    @PostMapping(value = "editUserExprience")
    public AjaxResult edit(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.updateBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 删除工作经历
     */
    @GetMapping("/removeUserExprience")
    public AjaxResult remove(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeExperienceService.deleteBaseUserResumeExperienceById(id));
    }



    @GetMapping("/getResumeCertList")
    public AjaxResult getResumeCertList(@RequestParam(value="id",required = true) Long id)
    {
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeById(id);
        if(detail==null){
            return AjaxResult.success(new ArrayList<>());
        }
        BaseUserResumeCertificate param=new BaseUserResumeCertificate();
        param.setResumeId(detail.getId());
        List<BaseUserResumeCertificate> list = baseUserResumeCertificateService.selectBaseUserResumeCertificateList(param);
        return AjaxResult.success(list);
    }


    /**
     * 获取职业证书详细信息
     */
    @GetMapping(value = "/getResumeCertDetail")
    public AjaxResult getResumeCertDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeCertificateService.selectBaseUserResumeCertificateById(id));
    }

    /**
     * 新增职业证书
     */
    @PostMapping(value = "/addResumeCertDetail")
    public AjaxResult addResumeCertDetail(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.insertBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 修改职业证书
     */
    @PostMapping(value = "/editResumeCertDetail")
    public AjaxResult editResumeCertDetail(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.updateBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 删除职业证书
     */
    @GetMapping("/removeResumeCertDetail")
    public AjaxResult removeResumeCertDetail(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeCertificateService.deleteBaseUserResumeCertificateById(id));
    }

    /**
     * 新增健康证
     */
    @PostMapping(value = "/addResumeHealthDetail")
    public AjaxResult addResumeHealthDetail(@RequestBody UserResumeHealthcert userResumeHealthcert)
    {
        return toAjax(userResumeHealthcertService.insertUserResumeHealthcert(userResumeHealthcert));
    }

    /**
     * 修改健康证
     */
    @PostMapping(value = "/editResumeHealthDetail")
    public AjaxResult editResumeHealthDetail(@RequestBody UserResumeHealthcert userResumeHealthcert)
    {
        return toAjax(userResumeHealthcertService.updateUserResumeHealthcert(userResumeHealthcert));
    }

    /**
     * 删除健康证
     */
    @GetMapping("/removeResumeHealthDetail")
    public AjaxResult removeResumeHealthDetail(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(userResumeHealthcertService.deleteUserResumeHealthcertById(id));
    }

    @GetMapping(value = "/setAuthUser")
    public AjaxResult setAuthUser(@RequestParam(value="userId",required = true) Long userId,
                                  @RequestParam(value="authUserId",required = true) Long authUserId)
    {
        BaseUser baseUser=userService.selectBaseUserByUserId(userId);
        if(baseUser==null){
            return AjaxResult.error("用户信息不存在");
        }
        baseUser.setAuthUserId(authUserId);
        int result=userService.updateAuthUser(baseUser);
        return AjaxResult.success("操作成功");
    }

    @GetMapping(value = "checkCertCode")
    public AjaxResult checkCertCode(@RequestParam(value="name",required = true) String name,
                                    @RequestParam(value="certCode",required = true) String certCode,
                                    @RequestParam(value="orderCode",required = false) String orderCode) {
        JSONObject checkResult= CheckCertUtil.testCertCode(name,certCode);
        String userKey="orderCode-"+orderCode;
        if(checkResult.getBoolean("isok")){
            AjaxResult dataResult= AjaxResult.success("操作成功");
            JSONObject idResult=checkResult.getJSONObject("IdCardInfor");
            dataResult.put("authName",name);
            dataResult.put("certCode",certCode);
            dataResult.put("address",idResult.getString("area"));


            dataResult.put("nativePlace",idResult.getString("area"));

            String birthStr=certCode.substring(6,14);
            Date birthDay=DateUtils.dateTime("yyyy-MM-dd",idResult.getString("birthday"));
            dataResult.put("birthDate",birthDay);
            String sex=certCode.substring(16,17);
            dataResult.put("sex",idResult.getString("sex"));
            String constellation = BirthUtils.getConstellation(birthDay);
            dataResult.put("constellation",constellation);
            String zodiac = BirthUtils.getZodiac(Integer.parseInt(birthStr.substring(0, 4)));
            dataResult.put("zodiac",zodiac);
            dataResult.put("nation","汉族");
            redisCache.setCacheObject(userKey,"Y",1, TimeUnit.DAYS);
            return dataResult;
        }else{
            redisCache.setCacheObject(userKey,"Y",1, TimeUnit.DAYS);
            return AjaxResult.error("验证失败，"+"身份证姓名、号码不正确");
        }
    }




    @GetMapping(value = "checkUserUseFlag")
    public AjaxResult checkUserUseFlag(@RequestParam(value="orderCode",required = true) String orderCode) {
        String userKey="orderCode-"+orderCode;
        String result=redisCache.getCacheObject(userKey);
        AjaxResult ajaxResult= AjaxResult.success();
        if(StringUtils.isEmpty(result)){
            result="Y";
        }
        ajaxResult.put("data",result);
        return ajaxResult;
    }



    @GetMapping("/removeResumeInfo")
    public AjaxResult removeResumeInfo(@RequestParam(value = "id", required = true) Long id) {
        BaseUserResume resumeInfo= baseUserResumeService.selectBaseUserResumeById(id);
        BaseUser userInfo= userService.selectBaseUserByUserId(resumeInfo.getUserId());
        BaseUser currentUser= getLoginUser().getUserInfo();
        if("01".equals(currentUser.getUserType())||"02".equals(currentUser.getUserType())||"05".equals(currentUser.getUserType())){
            BaseCompany company = baseCompanyService.selectBaseCompanyById(Long.valueOf(userInfo.getCompanyId()));
            if(company==null){
                String companyId = configService.selectConfigByKey("sys.account.companyId");
                if (StringUtils.isEmpty(companyId)) {
                    companyId = "1";
                }
                company = baseCompanyService.selectBaseCompanyById(Long.valueOf(companyId));
            }
            userInfo.setAuthUserId(company.getUserId());
            userInfo.setCompanyName(company.getCompanyName());
            userInfo.setUpdateTime(DateUtils.getNowDate());
            userInfo.setUpdateBy(getUsername());
            userService.updateBaseUser(userInfo);
            return AjaxResult.success("操作成功成功");
        }
        if("04".equals(currentUser.getUserType())){
            baseUserResumeService.removeBaseUserResumeById(resumeInfo.getId());
            baseUserIntentionService.deleteBaseUserIntentionByUserId(resumeInfo.getUserId());
            baseUserResumeService.deleteBaseUserResumeById(id);
            baseUserResumeExperienceService.deleteBaseUserResumeExperienceByResumeId(id);
            baseUserResumeCertificateService.deleteResumeCertificateByResumeId(id);
        }

        return AjaxResult.success("删除成功");
    }

}

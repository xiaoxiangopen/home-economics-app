package com.jiumi.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiumi.common.annotation.Excel;
import com.jiumi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户信息对象 base_user
 *
 * @author jiumi
 * @date 2022-12-05
 */
public class IDCardDo
{
    private static final long serialVersionUID = 1L;



    /** 性别 */
    @Excel(name = "性别")
    private String sex;


    /** 认证名称 */
    @Excel(name = "认证名称")
    private String authName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String certCode;

    /** 有效期 */
    private String certEndDate;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthDate;

    /** 生肖 */
    @Excel(name = "生肖")
    private String zodiac;

    /** 星座 */
    @Excel(name = "星座")
    private String constellation;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;

    /** 户籍地址 */
    @Excel(name = "户籍地址")
    private String address;


    private String certImage1;
    private String certImage2;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertEndDate() {
        return certEndDate;
    }

    public void setCertEndDate(String certEndDate) {
        this.certEndDate = certEndDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getZodiac() {
        return zodiac;
    }

    public void setZodiac(String zodiac) {
        this.zodiac = zodiac;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCertImage1() {
        return certImage1;
    }

    public void setCertImage1(String certImage1) {
        this.certImage1 = certImage1;
    }

    public String getCertImage2() {
        return certImage2;
    }

    public void setCertImage2(String certImage2) {
        this.certImage2 = certImage2;
    }
}

package com.jiumi.api.service.impl;

import com.jiumi.api.service.ILoginApiService;
import com.jiumi.baseconfig.domain.BaseUser;
import com.jiumi.common.constant.Constants;
import com.jiumi.common.core.domain.entity.SysUser;
import com.jiumi.common.core.domain.model.LoginUser;
import com.jiumi.common.core.redis.RedisCache;
import com.jiumi.common.enums.UserStatus;
import com.jiumi.common.exception.ServiceException;
import com.jiumi.common.exception.base.BaseException;
import com.jiumi.common.exception.user.UserPasswordNotMatchException;
import com.jiumi.common.utils.DateUtils;
import com.jiumi.common.utils.MessageUtils;
import com.jiumi.common.utils.SecurityUtils;
import com.jiumi.common.utils.StringUtils;
import com.jiumi.framework.manager.AsyncManager;
import com.jiumi.framework.manager.factory.AsyncFactory;
import com.jiumi.framework.security.context.AuthenticationContextHolder;
import com.jiumi.framework.web.service.TokenApiService;
import com.jiumi.framework.web.service.UserInfo;
import com.jiumi.baseconfig.service.IBaseUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lun.zhang
 * @create 2022/10/18 11:08
 */
@Service
public class LoginApiServiceImpl implements ILoginApiService {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private IBaseUserService userInfoService;

    @Resource(name = "authenticationManagerApi")
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenApiService tokenService;

    @Override
    public Map<String, Object> login(Map<String, Object> userInfo) {
        String username = String.valueOf(userInfo.get("openid"));
        String sessionKey = String.valueOf(userInfo.get("session_key"));
        String avatar = (null == userInfo.get("avatar")) ? "" : String.valueOf(userInfo.get("avatar"));
        String sex = (null == userInfo.get("sex")) ? "" : String.valueOf(userInfo.get("sex"));
        String nickName = (null == userInfo.get("nickName")) ? "" : String.valueOf(userInfo.get("nickName"));
        Authentication authentication = null;
        BaseUser user = userInfoService.selectUserByUserName(username);
        if (StringUtils.isNull(user)) {
            //增加用户信息
            user = new BaseUser();
            user.setUserName(username);
            user.setOpenId(username);
            user.setAvatar(avatar);
            user.setSex(sex);
            user.setPassword(SecurityUtils.encryptPassword("admin123"));
            //设置用户免费查看次数信息
            userInfoService.insertBaseUser(user);
        } else {
            if (StringUtils.isNotEmpty(avatar)) {
                user.setAvatar(avatar);
            }
            userInfoService.updateBaseUser(user);
        }
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, "admin123"));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            } else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new RuntimeException(e.getMessage());
            }
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        UserInfo loginUser = (UserInfo) authentication.getPrincipal();
        if (StringUtils.isNotNull(loginUser)) {
            //修改登录时间
            BaseUser u = new BaseUser();
            u.setLoginDate(DateUtils.getNowDate());
            loginUser.getUserInfo().setLoginDate(u.getLoginDate());
            u.setUserId(loginUser.getUserInfo().getUserId());
            userInfoService.updateBaseUser(user);
        }
        Map<String, Object> result = new HashMap<>();
        result.put(Constants.TOKEN, tokenService.createToken(loginUser));
        result.put("userInfo", loginUser.getUserInfo());
        result.put("sessionKey", sessionKey);
        return result;
    }
    @Override
    public String loginByCode(String username, String code,String uuid)
    {
        //演示环境注释掉了这段代码，正式环境需要放开
      /*  String key = "phone_verify_code" + username;
        String authCode=redisCache.getCacheObject(key)+"";
        if(!code.equals(authCode)){
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, "验证码不匹配"));
            throw new ServiceException("验证码不匹配");
        }*/


        BaseUser user=userInfoService.selectUserByUserName(username);
        if (StringUtils.isNull(user))
        {
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        }
        else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            log.info("登录用户：{} 已被删除.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已被删除");
        }
        else if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            log.info("登录用户：{} 已被停用.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已停用");
        }
        UserInfo loginUser = new UserInfo(user);
        loginUser.setUserId(user.getUserId());
        //recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }

    @Override
    public String loginApp(String username, String password, String uuid)
    {
        // 用户验证
        Authentication authentication = null;
        try
        {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            AuthenticationContextHolder.setContext(authenticationToken);
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager.authenticate(authenticationToken);
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        UserInfo loginUser = (UserInfo) authentication.getPrincipal();
        //recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }
}

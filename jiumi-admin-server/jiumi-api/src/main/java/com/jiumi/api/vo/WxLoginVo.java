package com.jiumi.api.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author lun.zhang
 * @create 2022/10/18 11:00
 */
@Data
public class WxLoginVo {
    @NotEmpty(message = "code is empty")
    private String code;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 性别
     */
    private String gender;
    /**
     * 头像
     */
    private String avatarUrl;

    private String iv;
    private String encryptedData;
}

package com.jiumi.api.entity;

import com.jiumi.baseconfig.domain.CalcConfig;
import com.jiumi.common.annotation.Excel;

import java.util.List;

public class CalculatorDo {

    private static final long serialVersionUID = 1L;

    private String measureType;

    private String measureName;

    private List<CalcConfig> configList;

    public String getMeasureType() {
        return measureType;
    }

    public void setMeasureType(String measureType) {
        this.measureType = measureType;
    }

    public String getMeasureName() {
        return measureName;
    }

    public void setMeasureName(String measureName) {
        this.measureName = measureName;
    }

    public List<CalcConfig> getConfigList() {
        return configList;
    }

    public void setConfigList(List<CalcConfig> configList) {
        this.configList = configList;
    }
}

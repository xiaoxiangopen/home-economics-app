package com.jiumi.api.util;

import com.alibaba.fastjson2.JSONObject;
import com.jiumi.api.util.http.util.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

public class CheckCertUtil {

    public static JSONObject testCertCode(String name,String certCode) {
        String host = "https://zid.market.alicloudapi.com";
        String path = "/idcheck/Post";
        String method = "POST";
        String appcode = "0b529c021016478d8f5616fb66bd9369";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
        bodys.put("cardNo", certCode);
        bodys.put("realName", name);

        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            int httpCode= response.getStatusLine().getStatusCode();
            if (httpCode == 200) {

                JSONObject requestResult=JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
                JSONObject resultData=requestResult.getJSONObject("result");
                System.out.println(resultData);
                return resultData;
            } else {

                // Map<String, List<String>> map = response.getAllHeaders().map();
                //String error = map.get("X-Ca-Error-Message").get(0);
                String error = response.getFirstHeader("X-Ca-Error-Message").getValue();
                String errorInfo="";
                if (httpCode == 400 && error.equals("Invalid AppCode `not exists`")) {
                    errorInfo="AppCode错误 ";
                } else if (httpCode == 400 && error.equals("Invalid Url")) {
                    errorInfo="请求的 Method、Path 或者环境错误";
                } else if (httpCode == 400 && error.equals("Invalid Param Location")) {
                    errorInfo="参数错误";
                } else if (httpCode == 403 && error.equals("Unauthorized")) {
                    errorInfo="服务未被授权（或URL和Path不正确）";
                } else if (httpCode == 403 && error.equals("Quota Exhausted")) {
                    errorInfo="套餐包次数用完 ";
                } else {
                    errorInfo="参数名错误 或 其他错误";
                }
                JSONObject result=new JSONObject();
                result.put("isok",false);
                result.put("error",errorInfo);
                return result;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }
}

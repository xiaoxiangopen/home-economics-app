package com.jiumi.api.service;

import java.util.Map;

/**
 * @author lun.zhang
 * @create 2022/10/18 11:07
 */
public interface ILoginApiService {
    /**
     * 登录
     *
     * @param map 地图
     */
    public   Map<String ,Object> login(Map<String,Object> map);

    String loginApp(String username, String password, String uuid);

    String loginByCode(String username, String code, String uuid);
}

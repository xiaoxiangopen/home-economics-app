/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : home-economics-app

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2024-03-25 16:37:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `base_appointment`
-- ----------------------------
DROP TABLE IF EXISTS `base_appointment`;
CREATE TABLE `base_appointment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(30) DEFAULT NULL COMMENT '编号',
  `category_type` varchar(10) DEFAULT NULL COMMENT '类型1月嫂2育儿嫂3钟点工 关联表 base_service_type',
  `user_id` bigint(20) DEFAULT NULL COMMENT '发布信息用户ID',
  `user_name` varchar(30) DEFAULT NULL COMMENT '联系人姓名',
  `user_phone` varchar(20) DEFAULT NULL COMMENT '联系人手机号',
  `user_address` varchar(300) DEFAULT NULL COMMENT '联系人地址',
  `need_service_type` varchar(500) DEFAULT '' COMMENT '服务内容',
  `service_period` varchar(30) DEFAULT NULL COMMENT '服务周期',
  `salary_amount` double(8,2) DEFAULT NULL COMMENT '工资预算',
  `combine_amount` double(8,2) DEFAULT NULL COMMENT '合单奖励',
  `baby_expect_date` datetime DEFAULT NULL COMMENT '预产期',
  `baby_num` varchar(20) DEFAULT '0' COMMENT '宝宝数量',
  `mother_age` varchar(20) DEFAULT '0' COMMENT '妈妈年龄',
  `first_flag` varchar(2) DEFAULT 'Y' COMMENT '是否新手妈妈',
  `relation` varchar(40) DEFAULT '' COMMENT '您与孕妈的关系',
  `work_content` varchar(300) DEFAULT '' COMMENT '工作内容',
  `work_type` varchar(10) DEFAULT NULL COMMENT '用工类型01住家02不住家03都可以',
  `month_work_days` varchar(20) DEFAULT '' COMMENT '每月上班天数',
  `import_service` varchar(255) DEFAULT '' COMMENT '保姆重点服务内容',
  `home_area` varchar(20) DEFAULT '' COMMENT '家庭面积',
  `home_person_num` varchar(20) DEFAULT '0' COMMENT '家庭人数',
  `require_native` varchar(30) DEFAULT '' COMMENT '籍贯要求',
  `require_age` varchar(20) DEFAULT '' COMMENT '年龄要求',
  `require_skill` varchar(30) DEFAULT '' COMMENT '技能要求',
  `use_worker_type` varchar(30) DEFAULT '' COMMENT '用工类型01上午用人02下午用人',
  `work_long` varchar(30) DEFAULT '' COMMENT '每日用工时长',
  `apply_time` datetime DEFAULT NULL COMMENT '预约时间',
  `apply_status` varchar(2) DEFAULT '01' COMMENT '预约状态01生效中02已下线',
  `top_flag` varchar(2) DEFAULT '' COMMENT '置顶状态',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='预约';

-- ----------------------------
-- Records of base_appointment
-- ----------------------------

-- ----------------------------
-- Table structure for `base_app_copyright`
-- ----------------------------
DROP TABLE IF EXISTS `base_app_copyright`;
CREATE TABLE `base_app_copyright` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `copyright_no` varchar(50) DEFAULT NULL COMMENT '版本号',
  `app_type` varchar(20) DEFAULT NULL COMMENT 'app类型',
  `app_desc` varchar(500) DEFAULT NULL COMMENT 'app描述',
  `must_flag` varchar(10) DEFAULT NULL COMMENT '是否强制更新',
  `download_url` varchar(200) DEFAULT NULL COMMENT '下载地址',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='app版本控制';

-- ----------------------------
-- Records of base_app_copyright
-- ----------------------------

-- ----------------------------
-- Table structure for `base_article`
-- ----------------------------
DROP TABLE IF EXISTS `base_article`;
CREATE TABLE `base_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `type` char(2) DEFAULT NULL COMMENT '文章类型',
  `cover_image` varchar(800) DEFAULT NULL COMMENT '封面图',
  `content` longtext COMMENT '内容',
  `show_flag` varchar(10) DEFAULT 'Y' COMMENT '是否显示',
  `read_num` int(11) DEFAULT NULL COMMENT '阅读量',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `create_by` varchar(100) DEFAULT NULL COMMENT '发布者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(100) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章表';

-- ----------------------------
-- Records of base_article
-- ----------------------------

-- ----------------------------
-- Table structure for `base_banner`
-- ----------------------------
DROP TABLE IF EXISTS `base_banner`;
CREATE TABLE `base_banner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category_type` varchar(2) DEFAULT NULL COMMENT '分类01引导页02banner',
  `image_url` varchar(800) DEFAULT NULL COMMENT '图片',
  `link_type` varchar(10) DEFAULT NULL COMMENT '指向类型01雇主02阿姨03资讯公告',
  `item_id` varchar(100) DEFAULT NULL COMMENT '文章id',
  `item_name` varchar(100) DEFAULT NULL COMMENT '指向名称',
  `sort` tinyint(4) DEFAULT NULL COMMENT '序号',
  `status` char(1) DEFAULT NULL COMMENT '状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='轮播图';

-- ----------------------------
-- Records of base_banner
-- ----------------------------

-- ----------------------------
-- Table structure for `base_business_config`
-- ----------------------------
DROP TABLE IF EXISTS `base_business_config`;
CREATE TABLE `base_business_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(100) DEFAULT NULL COMMENT '事项',
  `value` varchar(100) DEFAULT NULL COMMENT '值',
  `unit` varchar(10) DEFAULT NULL COMMENT '单位',
  `type` varchar(10) DEFAULT NULL COMMENT '类型 1 业务设置 2业务时效',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业务设置';

-- ----------------------------
-- Records of base_business_config
-- ----------------------------

-- ----------------------------
-- Table structure for `base_calc_config`
-- ----------------------------
DROP TABLE IF EXISTS `base_calc_config`;
CREATE TABLE `base_calc_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_type` varchar(10) DEFAULT NULL COMMENT '角色类型01月嫂02育儿嫂03家务保姆04护老保姆05钟点工',
  `measure_type` varchar(10) DEFAULT NULL COMMENT '指标ID',
  `measure_name` varchar(30) DEFAULT NULL COMMENT '指标类型',
  `item_name` varchar(30) DEFAULT NULL COMMENT '选项名称',
  `item_value` double(8,2) DEFAULT NULL COMMENT '选项因素',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='工资计算器配置';

-- ----------------------------
-- Records of base_calc_config
-- ----------------------------

-- ----------------------------
-- Table structure for `base_city_info`
-- ----------------------------
DROP TABLE IF EXISTS `base_city_info`;
CREATE TABLE `base_city_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` varchar(100) DEFAULT NULL COMMENT 'code',
  `value` varchar(100) DEFAULT NULL COMMENT '名称',
  `parent_code` varchar(100) DEFAULT NULL COMMENT '父级code',
  `pinyin` varchar(100) DEFAULT NULL COMMENT '拼音',
  `first` char(1) DEFAULT NULL COMMENT '首拼',
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3712 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='省市区表';

-- ----------------------------
-- Records of base_city_info
-- ----------------------------

-- ----------------------------
-- Table structure for `base_company`
-- ----------------------------
DROP TABLE IF EXISTS `base_company`;
CREATE TABLE `base_company` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `short_name` varchar(30) NOT NULL COMMENT '公司简称',
  `company_name` varchar(50) NOT NULL COMMENT '公司全称',
  `business_license_code` varchar(100) NOT NULL COMMENT '营业执照编号',
  `business_license_url` varchar(300) NOT NULL COMMENT '营业执照图片',
  `seal_url` varchar(300) DEFAULT NULL COMMENT '公章图片',
  `contact_name` varchar(300) DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(255) DEFAULT NULL COMMENT '联系人电话',
  `auth_status` varchar(10) DEFAULT NULL COMMENT '认证状态01待审核02已认证03认证不通过',
  `auth_time` datetime DEFAULT NULL COMMENT '认证时间',
  `auth_remark` varchar(300) DEFAULT NULL COMMENT '驳回原因',
  `available_flag` varchar(10) DEFAULT 'Y' COMMENT '正常状态  Y正常N禁用',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='家政公司';

-- ----------------------------
-- Records of base_company
-- ----------------------------

-- ----------------------------
-- Table structure for `base_company_account`
-- ----------------------------
DROP TABLE IF EXISTS `base_company_account`;
CREATE TABLE `base_company_account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_id` bigint(20) DEFAULT NULL COMMENT '公司ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `is_main` varchar(2) DEFAULT 'N' COMMENT '是否主要Y是N否',
  `status` varchar(10) DEFAULT 'Y' COMMENT '状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_index` (`user_id`,`company_id`) USING BTREE COMMENT '唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='公司招聘人员';

-- ----------------------------
-- Records of base_company_account
-- ----------------------------

-- ----------------------------
-- Table structure for `base_company_address`
-- ----------------------------
DROP TABLE IF EXISTS `base_company_address`;
CREATE TABLE `base_company_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `bind_phone` varchar(20) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `longitude` decimal(10,4) DEFAULT NULL,
  `latitude` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_company_address
-- ----------------------------

-- ----------------------------
-- Table structure for `base_contract`
-- ----------------------------
DROP TABLE IF EXISTS `base_contract`;
CREATE TABLE `base_contract` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category_type` varchar(2) DEFAULT NULL COMMENT '合同类型01家政协议02家政协议三方03月嫂协议04月嫂协议三方',
  `contract_code` varchar(30) DEFAULT NULL COMMENT '合同编号',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建人ID',
  `auser_id` bigint(20) DEFAULT NULL COMMENT '甲方用户ID',
  `auser_name` varchar(30) DEFAULT NULL COMMENT '甲姓名',
  `acert_code` varchar(30) DEFAULT NULL COMMENT '甲方身份证号',
  `auser_phone` varchar(20) DEFAULT NULL COMMENT '甲方手机号',
  `auser_address` varchar(200) DEFAULT NULL COMMENT '甲方住址',
  `buser_id` bigint(20) DEFAULT NULL COMMENT '已方用户ID',
  `buser_name` varchar(30) DEFAULT NULL COMMENT '乙姓名',
  `bcert_code` varchar(30) DEFAULT NULL COMMENT '乙方身份证号',
  `buser_phone` varchar(20) DEFAULT NULL COMMENT '乙方手机号',
  `buser_address` varchar(200) DEFAULT NULL COMMENT '乙方住址',
  `bcontact_name` varchar(30) DEFAULT NULL COMMENT '乙方紧急联系人姓名',
  `bcontact_phone` varchar(30) DEFAULT NULL COMMENT '乙方紧急联系人电话',
  `ccompany_id` bigint(20) DEFAULT NULL COMMENT '丙方家政公司ID',
  `ccompany_name` varchar(200) DEFAULT NULL COMMENT '丙方公司',
  `ccompany_phone` varchar(30) DEFAULT NULL COMMENT '丙方公司电话',
  `cuser_id` bigint(20) DEFAULT NULL COMMENT '丙方用户ID',
  `cuser_phone` varchar(20) DEFAULT NULL COMMENT '丙方手机号',
  `cuser_address` varchar(200) DEFAULT NULL COMMENT '丙方住址',
  `status` varchar(2) DEFAULT NULL COMMENT '签约状态01待雇主签字02待阿姨签字03待确认04生效中05已结束06驳回07废弃',
  `live_type` varchar(2) DEFAULT NULL COMMENT '是否住家01住家02不住家',
  `work_time` varchar(30) DEFAULT NULL COMMENT '工作时间',
  `service_type` varchar(100) DEFAULT NULL COMMENT '服务类型：01 一般家务 02育儿主带03育儿辅带04 老人 05病人',
  `baby_sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `baby_month` varchar(30) DEFAULT NULL COMMENT '月份',
  `baby_healty` varchar(200) DEFAULT NULL COMMENT '身体情况：自理，半自理，不能自理',
  `service_address` varchar(500) DEFAULT NULL COMMENT '服务地址',
  `expecte_date` datetime DEFAULT NULL COMMENT '预产期',
  `service_start_date` datetime DEFAULT NULL COMMENT '服务开始日期',
  `salary_amount` double(8,2) DEFAULT NULL COMMENT '乙方工资',
  `work_day_week` int(11) DEFAULT NULL COMMENT '每周工作天数',
  `other_content` varchar(2000) DEFAULT NULL COMMENT '补充条款',
  `insurance_content` varchar(2000) DEFAULT NULL COMMENT '保险',
  `sign_type` varchar(2) DEFAULT NULL COMMENT '签订类型01一次性付清 02按月支付',
  `sign_amount` double(8,2) DEFAULT NULL COMMENT '签单费',
  `service_amount` double(8,2) DEFAULT NULL COMMENT '服务费 01 年 02 月',
  `service_pay_date` int(11) DEFAULT NULL COMMENT '每月付款日',
  `deposit_amount` double(8,2) DEFAULT NULL COMMENT '保证金',
  `service_days` int(11) DEFAULT NULL COMMENT '服务天数(月嫂用)',
  `sign_date` datetime DEFAULT NULL COMMENT '签订日期',
  `asignature_url` varchar(500) DEFAULT NULL COMMENT '甲方签字',
  `asignature_date` date DEFAULT NULL COMMENT '甲方签字日期',
  `bsignature_url` varchar(500) DEFAULT NULL COMMENT '乙方签字',
  `bsignature_date` date DEFAULT NULL COMMENT '已方签字日期',
  `ccontact_phone` varchar(255) DEFAULT NULL COMMENT '联系中介公司电话',
  `confirm_user_id` bigint(20) DEFAULT NULL COMMENT '确认人ID',
  `confirm_time` datetime DEFAULT NULL COMMENT '确认时间',
  `confirmseal_url` varchar(300) DEFAULT NULL COMMENT '公司公章',
  `effect_date` datetime DEFAULT NULL COMMENT '合同生效时间',
  `pay_type` varchar(10) DEFAULT NULL COMMENT '支付方式 online线上 offline线下',
  `pay_status` varchar(2) DEFAULT NULL COMMENT '支付状态01待支付02已支付',
  `create_by` varchar(100) DEFAULT NULL COMMENT '发布者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(100) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='合同管理';

-- ----------------------------
-- Records of base_contract
-- ----------------------------

-- ----------------------------
-- Table structure for `base_contract_content`
-- ----------------------------
DROP TABLE IF EXISTS `base_contract_content`;
CREATE TABLE `base_contract_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(300) DEFAULT NULL COMMENT '标题',
  `content` longtext COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='合同内容';

-- ----------------------------
-- Records of base_contract_content
-- ----------------------------

-- ----------------------------
-- Table structure for `base_contract_log`
-- ----------------------------
DROP TABLE IF EXISTS `base_contract_log`;
CREATE TABLE `base_contract_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `contract_id` bigint(20) DEFAULT NULL COMMENT '合同ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '操作人ID',
  `content` varchar(300) DEFAULT NULL COMMENT '操作内容',
  `create_by` varchar(100) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='合同日志';

-- ----------------------------
-- Records of base_contract_log
-- ----------------------------

-- ----------------------------
-- Table structure for `base_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `base_feedback`;
CREATE TABLE `base_feedback` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '形象ID',
  `phone` varchar(30) DEFAULT NULL COMMENT '手机号',
  `type` varchar(30) DEFAULT NULL COMMENT '反馈类型',
  `content` varchar(255) DEFAULT NULL COMMENT '反馈内容',
  `image_url` varchar(255) DEFAULT NULL COMMENT '发卡图片',
  `status` varchar(255) DEFAULT NULL COMMENT '处理状态',
  `create_time` datetime DEFAULT NULL COMMENT '提交时间',
  `deal_time` datetime DEFAULT NULL COMMENT '处理时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户反馈';

-- ----------------------------
-- Records of base_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `base_goods`
-- ----------------------------
DROP TABLE IF EXISTS `base_goods`;
CREATE TABLE `base_goods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `goods_name` varchar(30) DEFAULT NULL COMMENT '奖品名称',
  `sku_name` text COMMENT '规格描述',
  `price` int(11) DEFAULT NULL COMMENT '积分',
  `cover_image` varchar(300) DEFAULT NULL COMMENT '封面图片',
  `image` varchar(800) DEFAULT NULL COMMENT '图片',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序',
  `stock_num` int(11) DEFAULT '0' COMMENT '库存',
  `status` varchar(2) DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '添加人',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='积分奖品';

-- ----------------------------
-- Records of base_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `base_lables`
-- ----------------------------
DROP TABLE IF EXISTS `base_lables`;
CREATE TABLE `base_lables` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `label` varchar(30) DEFAULT NULL COMMENT '标签',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序编号',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='标签';

-- ----------------------------
-- Records of base_lables
-- ----------------------------

-- ----------------------------
-- Table structure for `base_order`
-- ----------------------------
DROP TABLE IF EXISTS `base_order`;
CREATE TABLE `base_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `user_type` varchar(10) DEFAULT NULL COMMENT '付款方类型',
  `title` varchar(30) DEFAULT NULL COMMENT '订单标题',
  `order_code` varchar(30) DEFAULT NULL COMMENT '订单编号',
  `contract_code` varchar(30) DEFAULT '' COMMENT '合同编号',
  `order_amount` double(10,2) DEFAULT NULL COMMENT '支付金额',
  `pay_amount` double(10,2) DEFAULT '0.00' COMMENT '实际支付金额',
  `score_amount` double(8,2) DEFAULT NULL COMMENT '支付积分',
  `pay_type` varchar(2) DEFAULT '01' COMMENT '支付方式01微信02支付宝',
  `pay_status` varchar(2) DEFAULT NULL COMMENT '支付状态01待支付02已支付03已退款04已关闭',
  `trade_code` varchar(50) DEFAULT NULL COMMENT '支付订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '支付状态',
  `company_id` bigint(20) DEFAULT NULL COMMENT '公司ID',
  `company_name` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `create_by` varchar(100) DEFAULT NULL COMMENT '发布者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(100) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL COMMENT '修改人',
  `target_user_id` bigint(20) DEFAULT NULL COMMENT '目标用户user_id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=733 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='合同订单';

-- ----------------------------
-- Records of base_order
-- ----------------------------

-- ----------------------------
-- Table structure for `base_order_combine`
-- ----------------------------
DROP TABLE IF EXISTS `base_order_combine`;
CREATE TABLE `base_order_combine` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `publish_user_id` bigint(20) DEFAULT NULL COMMENT '发布人ID',
  `appointment_id` varchar(20) DEFAULT NULL COMMENT '预约信息ID',
  `type` varchar(2) DEFAULT NULL COMMENT '合单类型01阿姨找雇主02雇主找阿姨',
  `aunt_company_id` bigint(20) DEFAULT NULL COMMENT '阿姨方家政公司ID',
  `aunt_user_id` bigint(20) DEFAULT NULL COMMENT '阿姨ID',
  `aunt_type` varchar(2) DEFAULT NULL COMMENT '雇工类型1月嫂,2育儿嫂,3护老保姆,4医院护工,5钟点工',
  `employ_company_id` bigint(20) DEFAULT NULL COMMENT '雇主家政公司ID',
  `employ_user_id` bigint(20) DEFAULT NULL COMMENT '雇主ID',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `apply_status` varchar(2) DEFAULT NULL COMMENT '申请状态',
  `confirm_time` datetime DEFAULT NULL COMMENT '确认时间',
  `create_by` varchar(100) DEFAULT NULL COMMENT '发布者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(100) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='合单记录';

-- ----------------------------
-- Records of base_order_combine
-- ----------------------------

-- ----------------------------
-- Table structure for `base_order_exchange`
-- ----------------------------
DROP TABLE IF EXISTS `base_order_exchange`;
CREATE TABLE `base_order_exchange` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_code` varchar(30) DEFAULT NULL COMMENT '订单编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(30) DEFAULT NULL COMMENT '用户名',
  `phone` varchar(30) DEFAULT NULL COMMENT '用户手机号',
  `order_amount` int(11) DEFAULT NULL COMMENT '订单金额',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '兑换商品ID',
  `goods_num` int(11) DEFAULT NULL COMMENT '商品数量',
  `exchange_time` datetime DEFAULT NULL COMMENT '兑换时间',
  `address` varchar(30) DEFAULT NULL COMMENT '邮寄状态',
  `expressage_type` varchar(40) DEFAULT NULL COMMENT '快递类型',
  `expressage_code` varchar(30) DEFAULT NULL COMMENT '快递单号',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 01待发货 02已完成',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '添加人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户兑换奖品记录';

-- ----------------------------
-- Records of base_order_exchange
-- ----------------------------

-- ----------------------------
-- Table structure for `base_register_history`
-- ----------------------------
DROP TABLE IF EXISTS `base_register_history`;
CREATE TABLE `base_register_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_phone` varchar(30) DEFAULT NULL COMMENT '手机号',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `logout_time` datetime DEFAULT NULL COMMENT '注销时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户记录表';

-- ----------------------------
-- Records of base_register_history
-- ----------------------------

-- ----------------------------
-- Table structure for `base_score_config`
-- ----------------------------
DROP TABLE IF EXISTS `base_score_config`;
CREATE TABLE `base_score_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `item_name` varchar(30) DEFAULT NULL COMMENT '事项名称',
  `reward_amount` int(11) DEFAULT NULL COMMENT '奖励积分数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='积分奖励设置';

-- ----------------------------
-- Records of base_score_config
-- ----------------------------

-- ----------------------------
-- Table structure for `base_score_history`
-- ----------------------------
DROP TABLE IF EXISTS `base_score_history`;
CREATE TABLE `base_score_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户',
  `remark` varchar(100) DEFAULT NULL COMMENT '积分项说明',
  `score` decimal(10,2) DEFAULT NULL COMMENT '积分',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=527 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='积分记录信息';

-- ----------------------------
-- Records of base_score_history
-- ----------------------------

-- ----------------------------
-- Table structure for `base_service_type`
-- ----------------------------
DROP TABLE IF EXISTS `base_service_type`;
CREATE TABLE `base_service_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) DEFAULT NULL COMMENT '服务名称',
  `icon_url` varchar(300) DEFAULT NULL COMMENT '图标',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序编号',
  `useable_flag` varchar(2) DEFAULT 'Y' COMMENT '是否可用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='服务项目';

-- ----------------------------
-- Records of base_service_type
-- ----------------------------

-- ----------------------------
-- Table structure for `base_text_content`
-- ----------------------------
DROP TABLE IF EXISTS `base_text_content`;
CREATE TABLE `base_text_content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注',
  `content` longtext COMMENT '文本内容',
  `status` varchar(10) DEFAULT NULL COMMENT '是否可用',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文本管理';

-- ----------------------------
-- Records of base_text_content
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user`
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_name` varchar(255) DEFAULT NULL COMMENT '登录账号名',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '微信昵称',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `open_id` varchar(255) DEFAULT NULL COMMENT 'open_id',
  `union_id` varchar(255) DEFAULT NULL COMMENT 'union_id',
  `phonenumber` varchar(20) DEFAULT NULL COMMENT '关联号码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `avatar` varchar(800) CHARACTER SET utf32 COLLATE utf32_general_ci DEFAULT NULL COMMENT '头像',
  `auth_status` varchar(2) DEFAULT NULL COMMENT '认证状态 01待审核02认证通过03认证不通过',
  `auth_type` varchar(2) DEFAULT NULL COMMENT '认证类型：01家政公司认证02雇主认证03阿姨认证',
  `auth_time` datetime DEFAULT NULL COMMENT '认证时间',
  `user_type` varchar(10) DEFAULT '01' COMMENT '认证类型01平台02家政公司03雇主04阿姨 05 家政员工',
  `referrer_code` varchar(30) DEFAULT NULL COMMENT '推荐码',
  `invite_code` varchar(30) DEFAULT NULL COMMENT '邀请码',
  `status` varchar(2) DEFAULT NULL COMMENT '用户状态01上线02下线',
  `click_status` varchar(2) DEFAULT NULL COMMENT '打卡状态01未打卡02已打卡',
  `click_time` datetime DEFAULT NULL COMMENT '最近一次打卡时间',
  `score_amount` int(11) DEFAULT '0' COMMENT '积分数量',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `auth_name` varchar(50) DEFAULT NULL COMMENT '认证名称',
  `cert_code` varchar(30) DEFAULT NULL COMMENT '身份证号',
  `cert_end_date` varchar(30) DEFAULT NULL COMMENT '有效期',
  `birth_date` datetime DEFAULT NULL COMMENT '出生日期',
  `zodiac` varchar(10) DEFAULT NULL COMMENT '生肖',
  `constellation` varchar(20) DEFAULT NULL COMMENT '星座',
  `nation` varchar(30) DEFAULT NULL COMMENT '民族',
  `native_place` varchar(30) DEFAULT NULL COMMENT '籍贯',
  `address` varchar(300) DEFAULT NULL COMMENT '户籍地址',
  `company_id` bigint(20) DEFAULT NULL COMMENT '家政公司ID',
  `company_name` varchar(30) DEFAULT NULL COMMENT '家政公司名称',
  `auth_user_id` bigint(20) DEFAULT NULL COMMENT '授权家政公司员工ID',
  `cert_image1` varchar(300) DEFAULT NULL COMMENT '身份证正面',
  `cert_image2` varchar(300) DEFAULT NULL COMMENT '身份证反面',
  `publish_user_id` bigint(20) DEFAULT NULL COMMENT '发布人ID',
  `publish_type` varchar(10) DEFAULT '01' COMMENT '发布类型01自己发布02代发布',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户信息';

-- ----------------------------
-- Records of base_user
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_clock`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_clock`;
CREATE TABLE `base_user_clock` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `clock_date` varchar(30) DEFAULT NULL COMMENT '打卡日期',
  `clock_time` datetime DEFAULT NULL COMMENT '打卡时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=386 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户打卡记录';

-- ----------------------------
-- Records of base_user_clock
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_evaluate`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_evaluate`;
CREATE TABLE `base_user_evaluate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `type` varchar(2) DEFAULT NULL COMMENT '评价类型01雇主02中介',
  `evaluate_user_id` bigint(20) DEFAULT NULL COMMENT '评价人ID',
  `evaluate_user_name` varchar(200) DEFAULT NULL COMMENT '评价人姓名',
  `evaluate_user_avatar` varchar(500) DEFAULT NULL COMMENT '评价人头像',
  `contract_id` bigint(20) DEFAULT NULL COMMENT '合同ID',
  `score_num` int(11) DEFAULT NULL COMMENT '星级',
  `content` varchar(100) DEFAULT NULL COMMENT '评价内容',
  `create_by` varchar(100) DEFAULT NULL COMMENT '评价人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '浏览时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='阿姨评价表';

-- ----------------------------
-- Records of base_user_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_focus`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_focus`;
CREATE TABLE `base_user_focus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `type` varchar(2) DEFAULT NULL COMMENT '关注类型 01保姆信息02雇工信息',
  `focus_user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '浏览时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户关注';

-- ----------------------------
-- Records of base_user_focus
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_intention`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_intention`;
CREATE TABLE `base_user_intention` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `type_id` varchar(255) DEFAULT NULL COMMENT '岗位编码',
  `type_name` varchar(50) DEFAULT NULL COMMENT '岗位名称',
  `low_salary` int(11) DEFAULT '0' COMMENT '最低工资',
  `high_salary` int(11) DEFAULT '0' COMMENT '最高工资',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `live_type` varchar(10) DEFAULT NULL COMMENT '是否住家01住家02不住家03都可以',
  `sign_type` varchar(2) DEFAULT '01' COMMENT '签约类型：01公签02私签03都可以',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_id_index` (`user_id`) USING BTREE COMMENT '用户ID唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='求职意向';

-- ----------------------------
-- Records of base_user_intention
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_intention_city`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_intention_city`;
CREATE TABLE `base_user_intention_city` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `intention_id` bigint(20) DEFAULT NULL COMMENT '求职意向id',
  `province` varchar(30) DEFAULT NULL COMMENT '省份',
  `city` varchar(30) DEFAULT NULL COMMENT '城市',
  `area` varchar(30) DEFAULT NULL COMMENT '区域',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=862 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='求职意向城市';

-- ----------------------------
-- Records of base_user_intention_city
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_invite_detail`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_invite_detail`;
CREATE TABLE `base_user_invite_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(64) DEFAULT NULL,
  `invite_user_id` bigint(20) DEFAULT NULL COMMENT '邀请人用户ID',
  `invite_name` varchar(30) DEFAULT NULL COMMENT '邀请人姓名',
  `invite_time` datetime DEFAULT NULL COMMENT '邀请时间',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户邀请';

-- ----------------------------
-- Records of base_user_invite_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_message`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_message`;
CREATE TABLE `base_user_message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `category` varchar(2) DEFAULT '01' COMMENT '消息分类01签约通知02合单消息03推荐雇主04推荐阿姨',
  `title` varchar(30) DEFAULT NULL COMMENT '标题',
  `link_id` varchar(30) DEFAULT NULL COMMENT '连接ID',
  `msg_content` varchar(2000) DEFAULT NULL COMMENT '消息内容',
  `read_status` varchar(2) DEFAULT 'N' COMMENT '是否阅读Y是N否',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `read_time` datetime DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2832 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户消息提醒';

-- ----------------------------
-- Records of base_user_message
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_resume`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_resume`;
CREATE TABLE `base_user_resume` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `cert_code` varchar(30) DEFAULT NULL COMMENT '身份证号',
  `contact_phone` varchar(255) DEFAULT NULL COMMENT '联系人电话',
  `education` varchar(30) DEFAULT NULL COMMENT '学历',
  `work_experience` varchar(50) DEFAULT NULL COMMENT '工作经验',
  `religion` varchar(50) DEFAULT NULL COMMENT '宗教',
  `height` double(8,2) DEFAULT NULL COMMENT '身高',
  `weigh` double(8,2) DEFAULT NULL COMMENT '体重',
  `introduction` varchar(2000) DEFAULT '' COMMENT '个人优势',
  `image_url` varchar(4000) DEFAULT NULL COMMENT '附件地址',
  `video_url` varchar(500) DEFAULT NULL COMMENT '视频地址',
  `default_flag` varchar(2) DEFAULT 'N' COMMENT '默认简历',
  `health_time` datetime DEFAULT NULL COMMENT '健康证有效时间',
  `health_url` text COMMENT '健康证图片',
  `ability_label` varchar(400) DEFAULT NULL COMMENT '能力标签',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `cert_code_index` (`cert_code`) USING BTREE COMMENT '身份证号唯一'
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='求职简历表';

-- ----------------------------
-- Records of base_user_resume
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_resume_certificate`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_resume_certificate`;
CREATE TABLE `base_user_resume_certificate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `resume_id` bigint(20) DEFAULT NULL COMMENT '简历ID',
  `certificate_name` varchar(50) DEFAULT NULL COMMENT '证书名称',
  `incept_date` varchar(20) DEFAULT NULL COMMENT '获得时间',
  `certificate_image` varchar(500) DEFAULT NULL COMMENT '证件照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='职业证书';

-- ----------------------------
-- Records of base_user_resume_certificate
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_resume_experience`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_resume_experience`;
CREATE TABLE `base_user_resume_experience` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `resume_id` bigint(20) DEFAULT NULL COMMENT '简历ID',
  `post_name` varchar(50) DEFAULT NULL COMMENT '岗位名称',
  `start_date` date DEFAULT NULL COMMENT '开始时间',
  `end_date` date DEFAULT NULL COMMENT '结束时间',
  `work_content` text COMMENT '工作内容',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='工作经历';

-- ----------------------------
-- Records of base_user_resume_experience
-- ----------------------------

-- ----------------------------
-- Table structure for `base_user_resume_healthcert`
-- ----------------------------
DROP TABLE IF EXISTS `base_user_resume_healthcert`;
CREATE TABLE `base_user_resume_healthcert` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `resume_id` bigint(20) DEFAULT NULL COMMENT '简历ID',
  `certificate_name` varchar(50) DEFAULT NULL COMMENT '健康证名称名称',
  `incept_date` varchar(20) DEFAULT NULL COMMENT '有效期',
  `certificate_image` varchar(500) DEFAULT NULL COMMENT '证件照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='健康证书';

-- ----------------------------
-- Records of base_user_resume_healthcert
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Blob类型的触发器表';

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`,`calendar_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日历信息表';

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Cron类型的触发器表';

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(20) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(20) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) NOT NULL COMMENT '状态',
  `job_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`,`entry_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='已触发的触发器表';

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='任务详细信息表';

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`,`lock_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='存储的悲观锁信息表';

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`,`trigger_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='暂停的触发器表';

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(20) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(20) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`,`instance_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='调度器状态表';

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(20) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(20) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(20) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='简单触发器的信息表';

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='同步机制的行锁表';

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(20) DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(20) DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(20) NOT NULL COMMENT '开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(6) DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='触发器详细信息表';

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(11) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=149883 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(1000) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5370 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(11) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(11) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2136 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `type` varchar(10) DEFAULT NULL COMMENT '发送对象',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(11) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(11) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(11) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1320 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(11) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(11) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
